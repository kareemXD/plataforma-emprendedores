<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableShippingConfig extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shipping_config', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('user_id');
            $table->string('box_name')->nullable();
            $table->double('box_length', 10, 2)->defatul(1);
            $table->double('box_height', 10, 2)->defatul(1);
            $table->double('box_width', 10, 2)->defatul(1);
            $table->double('box_maximum_weight', 10, 2)->defatul(1);
            $table->double('box_dimension')->default(1);
            $table->char('status')->defatul(1);
            $table->string('shipping_type')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shipping_config');
    }
}
