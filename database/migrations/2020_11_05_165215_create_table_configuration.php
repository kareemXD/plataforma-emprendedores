<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableConfiguration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('configurations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('conekta_private_key', 100)->nullable();
            $table->string('conekta_public_key', 100)->nullable();
            $table->string('legal_notice_of_privacy')->nullable();
            $table->string('legal_terms_and_conditions')->nullable();
            $table->text('pixel_facebook_tag')->nullable();
            $table->text('google_analytics_tag')->nullable();
            $table->text('facebook_chat_tag')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('configurations');
    }
}
