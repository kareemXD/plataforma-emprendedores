<?php

use App\Role;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        DB::table('permissions')->truncate();
        Schema::enableForeignKeyConstraints();

        DB::table('permissions')->insert([
            //USERS
            [
                'name' => 'list_users',
                'display_name' => 'Lista de usuarios',
                'description' => 'Consulta de todos los usuarios en el sistema',
                'created_at' => date('Y-m-d H:i:s'),
            ],
            [
                'name' => 'create_users',
                'display_name' => 'Crear usuario',
                'description' => 'Creación de nuevos usuarios',
                'created_at' => date('Y-m-d H:i:s'),
            ],
            [
                'name' => 'edit_users',
                'display_name' => 'Editar usuario',
                'description' => 'Edición de los usuarios registrados en el sistema',
                'created_at' => date('Y-m-d H:i:s'),
            ],
            [
                'name' => 'delete_users',
                'display_name' => 'Eliminar usuario',
                'description' => 'Eliminación de los usuarios registrados en el sistema',
                'created_at' => date('Y-m-d H:i:s'),
            ],

            //ROLES
            [
                'name' => 'list_roles',
                'display_name' => 'Lista de roles',
                'description' => 'Consulta de todos los roles en el sistema',
                'created_at' => date('Y-m-d H:i:s'),
            ],
            [
                'name' => 'create_roles',
                'display_name' => 'Crear rol',
                'description' => 'Creación de nuevos roles',
                'created_at' => date('Y-m-d H:i:s'),
            ],
            [
                'name' => 'edit_roles',
                'display_name' => 'Editar rol',
                'description' => 'Edición de los roles registrados en el sistema',
                'created_at' => date('Y-m-d H:i:s'),
            ],
            [
                'name' => 'delete_roles',
                'display_name' => 'Eliminar rol',
                'description' => 'Eliminación de los roles registrados en el sistema',
                'created_at' => date('Y-m-d H:i:s'),
            ],
            //article
            [
                'name' => 'list_article',
                'display_name' => 'Lista de articulos',
                'description' => 'Consulta de todos los articulos en el sistema',
                'created_at' => date('Y-m-d H:i:s'),
            ],
            [
                'name' => 'create_article',
                'display_name' => 'Crear articulo',
                'description' => 'Creación de nuevos articulos',
                'created_at' => date('Y-m-d H:i:s'),
            ],
            [
                'name' => 'show_article',
                'display_name' => 'Mostrar articulo',
                'description' => 'Mostrar articulo registrados en el sistema',
                'created_at' => date('Y-m-d H:i:s'),
            ],
            [
                'name' => 'edit_article',
                'display_name' => 'Editar articulo',
                'description' => 'Edición de los articulos registrados en el sistema',
                'created_at' => date('Y-m-d H:i:s'),
            ],
            [
                'name' => 'delete_article',
                'display_name' => 'Eliminar articulo',
                'description' => 'Eliminación de los articulos registrados en el sistema',
                'created_at' => date('Y-m-d H:i:s'),
            ],

            //BANNERS
            [
                'name' => 'list_banners',
                'display_name' => 'Lista de banners',
                'description' => 'Consulta de todos los banners en el sistema',
                'created_at' => date('Y-m-d H:i:s'),
            ],
            [
                'name' => 'create_banners',
                'display_name' => 'Crear banner',
                'description' => 'Creación de nuevos banners',
                'created_at' => date('Y-m-d H:i:s'),
            ],
            [
                'name' => 'edit_banners',
                'display_name' => 'Editar banners',
                'description' => 'Edición de los banners registrados en el sistema',
                'created_at' => date('Y-m-d H:i:s'),
            ],
            [
                'name' => 'delete_banners',
                'display_name' => 'Eliminar banners',
                'description' => 'Eliminación de los banners registrados en el sistema',
                'created_at' => date('Y-m-d H:i:s'),
            ],

            //NEWSLETTER USERS
            [
                'name' => 'list_newsletter_users',
                'display_name' => 'Lista de usuarios de newsletter',
                'description' => 'Consulta de todos los usuarios registrados en el newsletter del sistema',
                'created_at' => date('Y-m-d H:i:s'),
            ],
            [
                'name' => 'delete_newsletter_users',
                'display_name' => 'Eliminar usuarios de newsletter',
                'description' => 'Eliminación de los usuarios registrados en el newsletter del sistema',
                'created_at' => date('Y-m-d H:i:s'),
            ],
            //coupons
            [
                'name' => 'list_coupons',
                'display_name' => 'Lista de cupones',
                'description' => 'Lista de cupones.',
                'created_at' => date('Y-m-d H:i:s'),
            ],
            [
                'name' => 'create_coupons',
                'display_name' => 'Crear cupón',
                'description' => 'Crear cupones',
                'created_at' => date('Y-m-d H:i:s'),
            ],
            [
                'name' => 'edit_coupons',
                'display_name' => 'Crear cupón',
                'description' => 'Editar cupones',
                'created_at' => date('Y-m-d H:i:s'),
            ],
            [
                'name' => 'delete_coupons',
                'display_name' => 'Eliminar cupones',
                'description' => 'Eliminar cupones',
                'created_at' => date('Y-m-d H:i:s'),
            ],
            [
                'name' => 'show_coupons',
                'display_name' => 'Consultar cupones',
                'description' => 'Consultar Cupones',
                'created_at' => date('Y-m-d H:i:s'),
            ],
            [
                'name' => 'configuration',
                'display_name' => 'Configuración',
                'description' => 'Configuración del sistema',
                'created_at' => date('Y-m-d H:i:s'),
            ],
            //BANNERS
            [
                'name' => 'list_template',
                'display_name' => 'Lista de plantillas',
                'description' => 'Consulta de todos los plantillas en el sistema',
                'created_at' => date('Y-m-d H:i:s'),
            ],
            [
                'name' => 'create_template',
                'display_name' => 'Crear plantilla',
                'description' => 'Creación de nuevos plantillas',
                'created_at' => date('Y-m-d H:i:s'),
            ],
            [
                'name' => 'edit_template',
                'display_name' => 'Editar plantilla',
                'description' => 'Edición de los plantillas registrados en el sistema',
                'created_at' => date('Y-m-d H:i:s'),
            ],
            [
                'name' => 'delete_template',
                'display_name' => 'Eliminar plantillas',
                'description' => 'Eliminación de los plantillas registrados en el sistema',
                'created_at' => date('Y-m-d H:i:s'),
            ],
            //abandoned cart
            [
                'name' => 'list_abandoned_cart',
                'display_name' => 'Lista de usuarios de newsletter',
                'description' => 'Consulta de todos los usuarios registrados en el newsletter del sistema',
                'created_at' => date('Y-m-d H:i:s'),
            ],
            [
                'name' => 'update_abandoned_cart',
                'display_name' => 'Eliminar usuarios de newsletter',
                'description' => 'Eliminación de los usuarios registrados en el newsletter del sistema',
                'created_at' => date('Y-m-d H:i:s'),
            ],
            //shiping config
            [
                'name' => 'shipping_config_box',
                'display_name' => 'Configurar envío',
                'description' => 'Configurar envío.',
                'created_at' => date('Y-m-d H:i:s'),
            ],

            // RULES CONFIGURATOR
            [
                'name' => 'list_rules_configurator',
                'display_name' => 'Lista de reglas',
                'description' => 'Consulta de todos los reglas en el sistema',
                'created_at' => date('Y-m-d H:i:s'),
            ],
            [
                'name' => 'create_rules_configurator',
                'display_name' => 'Crear regla',
                'description' => 'Creación de nuevas reglas',
                'created_at' => date('Y-m-d H:i:s'),
            ],
            [
                'name' => 'edit_rules_configurator',
                'display_name' => 'Editar regla',
                'description' => 'Edición de las reglas registrados en el sistema',
                'created_at' => date('Y-m-d H:i:s'),
            ],
            [
                'name' => 'delete_rules_configurator',
                'display_name' => 'Eliminar reglas',
                'description' => 'Eliminación de las reglas registrados en el sistema',
                'created_at' => date('Y-m-d H:i:s'),
            ],

            // GLOBAL COSTS CONFIGURATOR
            [
                'name' => 'global_utility',
                'display_name' => 'Utilidad Global',
                'description' => 'Permite editat la Utilidad Global',
                'created_at' => date('Y-m-d H:i:s'),
            ],
            [
                'name' => 'list_global_costs_configurator',
                'display_name' => 'Lista de costos globales',
                'description' => 'Consulta de todos los costos globales en el sistema',
                'created_at' => date('Y-m-d H:i:s'),
            ],
            [
                'name' => 'create_global_costs_configurator',
                'display_name' => 'Crear costo global',
                'description' => 'Creación de nuevos costos globales',
                'created_at' => date('Y-m-d H:i:s'),
            ],
            [
                'name' => 'edit_global_costs_configurator',
                'display_name' => 'Editar costo global',
                'description' => 'Edición de los costos globales registrados en el sistema',
                'created_at' => date('Y-m-d H:i:s'),
            ],
            [
                'name' => 'delete_global_costs_configurator',
                'display_name' => 'Eliminar costos globales',
                'description' => 'Eliminación de los costos globales registrados en el sistema',
                'created_at' => date('Y-m-d H:i:s'),
            ],

        ]);

        $ids = DB::table('permissions')->get()->pluck('id');

        $adminRole = Role::find(1);
        $adminRole->perms()->sync($ids);

    }
}
