<?php

use App\Role;
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        DB::table('roles')->truncate();
        Schema::enableForeignKeyConstraints();

        $adminRole = Role::create([
            'name' => 'admin',
            'display_name' => 'Administrador',
            'description' => 'Usuario con todos los permisos',
        ]);

        Role::create([
            'name' => 'user',
            'display_name' => 'Usuario',
            'description' => 'Usuario con permisos limitados',
        ]);

        $admin = User::find(1);
        $admin->attachRole($adminRole);
    }
}
