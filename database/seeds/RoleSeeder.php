<?php

use App\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions[] = Permission::firstOrCreate([
          'name' => 'Usuarios.*',
          'display_name' => 'Usuarios'
        ]);
        $permissions[] = Permission::firstOrCreate([
          'name' => 'Usuarios.create',
          'display_name' => 'Usuarios'
        ]);
        $permissions[] = Permission::firstOrCreate([
          'name' => 'Usuarios.edit',
          'display_name' => 'Usuarios'
        ]);
        $permissions[] = Permission::firstOrCreate([
          'name' => 'Usuarios.view',
          'display_name' => 'Usuarios'
        ]);
        $permissions[] = Permission::firstOrCreate([
          'name' => 'Usuarios.delete',
          'display_name' => 'Usuarios'
        ]);

        $permissions[] = Permission::firstOrCreate([
          'name' => 'Roles.*',
          'display_name' => 'Roles'
        ]);
        $permissions[] = Permission::firstOrCreate([
          'name' => 'Roles.create',
          'display_name' => 'Roles'
        ]);
        $permissions[] = Permission::firstOrCreate([
          'name' => 'Roles.edit',
          'display_name' => 'Roles'
        ]);
        $permissions[] = Permission::firstOrCreate([
          'name' => 'Roles.view',
          'display_name' => 'Roles'
        ]);
        $permissions[] = Permission::firstOrCreate([
          'name' => 'Roles.delete',
          'display_name' => 'Roles'
        ]);

        $permissions[] = Permission::firstOrCreate([
          'name' => 'ShippingConfiguration.*',
          'display_name' => 'Configuración de envío'
        ]);
        $permissions[] = Permission::firstOrCreate([
          'name' => 'ShippingConfiguration.create',
          'display_name' => 'Configuración de envío'
        ]);
        $permissions[] = Permission::firstOrCreate([
          'name' => 'ShippingConfiguration.edit',
          'display_name' => 'Configuración de envío'
        ]);
        $permissions[] = Permission::firstOrCreate([
          'name' => 'ShippingConfiguration.view',
          'display_name' => 'Configuración de envío'
        ]);
        $permissions[] = Permission::firstOrCreate([
          'name' => 'ShippingConfiguration.delete',
          'display_name' => 'Configuración de envío'
        ]);

        $permissions[] = Permission::firstOrCreate([
          'name' => 'Configurations.*',
          'display_name' => 'Configuraciones'
        ]);
        $permissions[] = Permission::firstOrCreate([
          'name' => 'Configurations.create',
          'display_name' => 'Configuraciones'
        ]);
        $permissions[] = Permission::firstOrCreate([
          'name' => 'Configurations.edit',
          'display_name' => 'Configuraciones'
        ]);
        $permissions[] = Permission::firstOrCreate([
          'name' => 'Configurations.view',
          'display_name' => 'Configuraciones'
        ]);
        $permissions[] = Permission::firstOrCreate([
          'name' => 'Configurations.delete',
          'display_name' => 'Configuraciones'
        ]);

        $permissions[] = Permission::firstOrCreate([
          'name' => 'GlobalCostConfiguration.*',
          'display_name' => 'Configuración de costos globales'
        ]);
        $permissions[] = Permission::firstOrCreate([
          'name' => 'GlobalCostConfiguration.create',
          'display_name' => 'Configuración de costos globales'
        ]);
        $permissions[] = Permission::firstOrCreate([
          'name' => 'GlobalCostConfiguration.edit',
          'display_name' => 'Configuración de costos globales'
        ]);
        $permissions[] = Permission::firstOrCreate([
          'name' => 'GlobalCostConfiguration.view',
          'display_name' => 'Configuración de costos globales'
        ]);
        $permissions[] = Permission::firstOrCreate([
          'name' => 'GlobalCostConfiguration.delete',
          'display_name' => 'Configuración de costos globales'
        ]);

        $permissions[] = Permission::firstOrCreate([
          'name' => 'GlobalUtilityConfiguration.*',
          'display_name' => 'Configuración global de utilidad'
        ]);
        $permissions[] = Permission::firstOrCreate([
          'name' => 'GlobalUtilityConfiguration.create',
          'display_name' => 'Configuración global de utilidad'
        ]);
        $permissions[] = Permission::firstOrCreate([
          'name' => 'GlobalUtilityConfiguration.edit',
          'display_name' => 'Configuración global de utilidad'
        ]);
        $permissions[] = Permission::firstOrCreate([
          'name' => 'GlobalUtilityConfiguration.view',
          'display_name' => 'Configuración global de utilidad'
        ]);
        $permissions[] = Permission::firstOrCreate([
          'name' => 'GlobalUtilityConfiguration.delete',
          'display_name' => 'Configuración global de utilidad'
        ]);

        $permissions[] = Permission::firstOrCreate([
          'name' => 'SpecialCostConfiguration.*',
          'display_name' => 'Configuración de costos especiales'
        ]);
        $permissions[] = Permission::firstOrCreate([
          'name' => 'SpecialCostConfiguration.create',
          'display_name' => 'Configuración de costos especiales'
        ]);
        $permissions[] = Permission::firstOrCreate([
          'name' => 'SpecialCostConfiguration.edit',
          'display_name' => 'Configuración de costos especiales'
        ]);
        $permissions[] = Permission::firstOrCreate([
          'name' => 'SpecialCostConfiguration.view',
          'display_name' => 'Configuración de costos especiales'
        ]);
        $permissions[] = Permission::firstOrCreate([
          'name' => 'SpecialCostConfiguration.delete',
          'display_name' => 'Configuración de costos especiales'
        ]);

        $permissions[] = Permission::firstOrCreate([
          'name' => 'ViewEntrepreneurialCost.*',
          'display_name' => 'Costo del emprendedor'
        ]);
        $permissions[] = Permission::firstOrCreate([
          'name' => 'ViewEntrepreneurialCost.create',
          'display_name' => 'Costo del emprendedor'
        ]);
        $permissions[] = Permission::firstOrCreate([
          'name' => 'ViewEntrepreneurialCost.edit',
          'display_name' => 'Costo del emprendedor'
        ]);
        $permissions[] = Permission::firstOrCreate([
          'name' => 'ViewEntrepreneurialCost.view',
          'display_name' => 'Costo del emprendedor'
        ]);
        $permissions[] = Permission::firstOrCreate([
          'name' => 'ViewEntrepreneurialCost.delete',
          'display_name' => 'Costo del emprendedor'
        ]);

        $permissions[] = Permission::firstOrCreate([
          'name' => 'Box.*',
          'display_name' => 'Cajas'
        ]);
        $permissions[] = Permission::firstOrCreate([
          'name' => 'Box.create',
          'display_name' => 'Cajas'
        ]);
        $permissions[] = Permission::firstOrCreate([
          'name' => 'Box.edit',
          'display_name' => 'Cajas'
        ]);
        $permissions[] = Permission::firstOrCreate([
          'name' => 'Box.view',
          'display_name' => 'Cajas'
        ]);
        $permissions[] = Permission::firstOrCreate([
          'name' => 'Box.delete',
          'display_name' => 'Cajas'
        ]);

        $permissions[] = Permission::firstOrCreate([
          'name' => 'CreditRequest.*',
          'display_name' => 'Solicitudes de crédito'
        ]);
        $permissions[] = Permission::firstOrCreate([
          'name' => 'CreditRequest.create',
          'display_name' => 'Solicitudes de crédito'
        ]);
        $permissions[] = Permission::firstOrCreate([
          'name' => 'CreditRequest.edit',
          'display_name' => 'Solicitudes de crédito'
        ]);
        $permissions[] = Permission::firstOrCreate([
          'name' => 'CreditRequest.view',
          'display_name' => 'Solicitudes de crédito'
        ]);
        $permissions[] = Permission::firstOrCreate([
          'name' => 'CreditRequest.delete',
          'display_name' => 'Solicitudes de crédito'
        ]);

        $permissions[] = Permission::firstOrCreate([
          'name' => 'EnterprisingsList.*',
          'display_name' => 'Emprendedores'
        ]);
        $permissions[] = Permission::firstOrCreate([
          'name' => 'EnterprisingsList.create',
          'display_name' => 'Emprendedores'
        ]);
        $permissions[] = Permission::firstOrCreate([
          'name' => 'EnterprisingsList.edit',
          'display_name' => 'Emprendedores'
        ]);
        $permissions[] = Permission::firstOrCreate([
          'name' => 'EnterprisingsList.view',
          'display_name' => 'Emprendedores'
        ]);
        $permissions[] = Permission::firstOrCreate([
          'name' => 'EnterprisingsList.delete',
          'display_name' => 'Emprendedores'
        ]);

        $permissions[] = Permission::firstOrCreate([
          'name' => 'Products.*',
          'display_name' => 'Productos'
        ]);
        $permissions[] = Permission::firstOrCreate([
          'name' => 'Products.create',
          'display_name' => 'Productos'
        ]);
        $permissions[] = Permission::firstOrCreate([
          'name' => 'Products.edit',
          'display_name' => 'Productos'
        ]);
        $permissions[] = Permission::firstOrCreate([
          'name' => 'Products.view',
          'display_name' => 'Productos'
        ]);
        $permissions[] = Permission::firstOrCreate([
          'name' => 'Products.delete',
          'display_name' => 'Productos'
        ]);

        $permissions[] = Permission::firstOrCreate([
          'name' => 'ShipmentCostConfiguration.*',
          'display_name' => 'Configuración de costos de envío'
        ]);
        $permissions[] = Permission::firstOrCreate([
          'name' => 'ShipmentCostConfiguration.create',
          'display_name' => 'Configuración de costos de envío'
        ]);
        $permissions[] = Permission::firstOrCreate([
          'name' => 'ShipmentCostConfiguration.edit',
          'display_name' => 'Configuración de costos de envío'
        ]);
        $permissions[] = Permission::firstOrCreate([
          'name' => 'ShipmentCostConfiguration.view',
          'display_name' => 'Configuración de costos de envío'
        ]);
        $permissions[] = Permission::firstOrCreate([
          'name' => 'ShipmentCostConfiguration.delete',
          'display_name' => 'Configuración de costos de envío'
        ]);

        $permissions[] = Permission::firstOrCreate([
          'name' => 'Location.*',
          'display_name' => 'Ubicaciones'
        ]);
        $permissions[] = Permission::firstOrCreate([
          'name' => 'Location.create',
          'display_name' => 'Ubicaciones'
        ]);
        $permissions[] = Permission::firstOrCreate([
          'name' => 'Location.edit',
          'display_name' => 'Ubicaciones'
        ]);
        $permissions[] = Permission::firstOrCreate([
          'name' => 'Location.view',
          'display_name' => 'Ubicaciones'
        ]);
        $permissions[] = Permission::firstOrCreate([
          'name' => 'Location.delete',
          'display_name' => 'Ubicaciones'
        ]);

        $permissions[] = Permission::firstOrCreate([
          'name' => 'Container.*',
          'display_name' => 'Contenedores'
        ]);
        $permissions[] = Permission::firstOrCreate([
          'name' => 'Container.create',
          'display_name' => 'Contenedores'
        ]);
        $permissions[] = Permission::firstOrCreate([
          'name' => 'Container.edit',
          'display_name' => 'Contenedores'
        ]);
        $permissions[] = Permission::firstOrCreate([
          'name' => 'Container.view',
          'display_name' => 'Contenedores'
        ]);
        $permissions[] = Permission::firstOrCreate([
          'name' => 'Container.delete',
          'display_name' => 'Contenedores'
        ]);

        $permissions[] = Permission::firstOrCreate([
            'name' => 'Picking.*',
            'display_name' => 'Picking'
        ]);
        $permissions[] = Permission::firstOrCreate([
            'name' => 'Picking.create',
             'display_name' => 'Picking'
        ]);
        $permissions[] = Permission::firstOrCreate([
            'name' => 'Picking.edit',
             'display_name' => 'Picking'
        ]);
        $permissions[] = Permission::firstOrCreate([
            'name' => 'Picking.view',
             'display_name' => 'Picking'
        ]);
        $permissions[] = Permission::firstOrCreate([
            'name' => 'Picking.delete',
             'display_name' => 'Picking'
        ]);

        $admin = Role::firstOrCreate(['name' => 'Admin']);
        $admin->syncPermissions($permissions);

        $userAdmin = User::find(1);
        $userAdmin->syncRoles(['Admin']);
    }
}
