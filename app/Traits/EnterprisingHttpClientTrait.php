<?php
/* 
  Api requests to DEGMA005-2 (emprendedor)
*/
namespace App\Traits;

use Illuminate\Support\Facades\Log;
use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Exception\ServerException;

trait EnterprisingHttpClientTrait {
    /**
     * gets a new GuzzleClient
     *
     * @author Giovanny González <giovanny.gonzalez@nuvem.mx>
     * created 22/06/2021
     * @params
     * @return void
     *
     */
    protected function enterprisingRequest($method, $uri, $authenticatedRequest, $payload = [], $headers = [])
    {
      $enterprisingToken = request()->session()->get('enterprising_admin_panel_access_token');
      
      $body = [];
      if($authenticatedRequest && !$enterprisingToken)
        throw new \Exception("enterprising_admin_panel_access_token doesn\'t exist in session", 1);
      else {
        $headers['Authorization']  = 'Bearer ' . $enterprisingToken;
        $body['headers'] = $headers;
      }
      
      if(count($payload) > 0) $body['json'] = $payload;

      $url = config('enterprising_admin_panel.api_url') . $uri;
      $client = new GuzzleClient();
      $stream = $client->request($method, $url, $body);
      $responseBody = json_decode($stream->getBody()->getContents()) ?? [];
      return $responseBody;
    }
}