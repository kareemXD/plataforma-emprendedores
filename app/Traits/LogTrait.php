<?php

namespace App\Traits;

use Carbon\Carbon;
use App\Models\Log\Log;

trait LogTrait {
    
    /**
    * Allow create or update relation log.
    *
    * @author José Vega <jose.vega@nuvem.mx>
    * @created 2020-04-26
    * @param
    * @return void
    */
    public static function bootLogTrait()
    {
        static::created(function ($model) {
            $log = new Log;
            $log->created_by = auth()->id();
            $log->created_at_date = now();
            $model->log()->save($log);
        });

        static::updating(function($model)
        {
            if(!$model->log()->exists()){
                $log = new Log;
                $log->updated_by = auth()->id();
                $log->updated_at_date = now();
                $model->log()->save($log);
            }else{
                $model->log()->update([
                    'updated_by' => auth()->id(),
                    'updated_at_date' => now()]);
            }
        });
    }

    /**
    * Relation morph one with the model Log.
    *
    * @author José Vega <jose.vega@nuvem.mx>
    * @created 2020-04-26
    * @param
    * @return \Illuminate\Database\Eloquent\Relations\morphOne
    */
    public function log()
    {
        return $this->morphOne(Log::class, 'loggable');
    }

    /**
    * Get the information of the user who created the record.
    *
    * @author José Vega <jose.vega@nuvem.mx>
    * @created 2020-04-28
    * @param
    * @return void
    */
    public function createdBy()
    {
        return $this->log->userCreated();
    }

    /**
    * Get the information of the user who updated the record.
    *
    * @author José Vega <jose.vega@nuvem.mx>
    * @created 2020-04-28
    * @param
    * @return void
    */
    public function updatedBy()
    {
        return $this->log->userUpdated();
    }

    /**
    * Get the information of the user who deleted the record.
    *
    * @author José Vega <jose.vega@nuvem.mx>
    * @created 2020-04-28
    * @param
    * @return void
    */
    public function deletedBy()
    {
        return $this->log->userDeleted();
    }

    /**
    * Toggles status / Estatus and update deleted_by and deleted_at_date attributes
    * @author José Vega <jose.vega@nuvem.mx>
    * @created 2020-04-27
    * @param
    * @return void
    */
    public function toggleStatus()
    {
        isset($this->Estatus) 
        ?
            $this->Estatus = $this->Estatus ? 0 : 1
        :
            $this->status = $this->status ? 0 : 1;
        
        $this->save();
        
        if(!$this->log()->exists()){
            $log = new Log;
            $log->deleted_by = auth()->id();
            $log->deleted_at_date = now();
            $this->log()->save($log);
        }else{
            $this->log()->update([
                'deleted_by' => auth()->id(),
                'deleted_at_date' => now()]);
        }   
    }
}