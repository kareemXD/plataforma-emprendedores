<?php

namespace App;

use App\Cliente;
use Illuminate\Database\Eloquent\Model;
use App\Http\Requests\CreditRequestRequest;

class CreditRequest extends Model
{
    protected $connection = 'medicadepot';
    protected $table = 'zAE_SolicitudesCreditoEmprendedores';
    public $timestamps = false;
    protected $primaryKey = 'Id';
    protected $guarded = ['Id'];
    public $appends = ['request_human_date'];

  /**
   * request human date appended attribute
   *
   * @author Giovanny González <giovanny.gonzalez@nuvem.mx>
   * created 21/05/2021
   * @params
   * @return string
   *
   */
  public function getRequestHumanDateAttribute() 
  {
    return date('d/m/Y', strtotime($this->Fecha_solicitud));
  }

  /**
   * Get the enterprising associated with the CreditRequest
   *
   * @author Giovanny González <giovanny.gonzalez@nuvem.mx>
   * created 21/05/2021
   * @params
   * @return \Illuminate\Database\Eloquent\Relations\HasOne
   *
   */
  public function enterprising()
  {
      return $this->hasOne(Cliente::class, 'Id_Cliente', 'Id_Cliente');
  }

  /**
   * Data table scope
   *
   * @author Giovanny González <giovanny.gonzalez@nuvem.mx>
   * created 20/05/2021
   * @params \Illuminate\Database\Eloquent\Builder $query
   * @return \Illuminate\Database\Eloquent\Builder 
   *
   */
  public function scopeDataTable($query)
  {
    return $query->select('zAE_SolicitudesCreditoEmprendedores.Id', 'Clientes.Razon_soc', 'Clientes.Id_Cliente', 
    'zAE_SolicitudesCreditoEmprendedores.Fecha_solicitud', 'zAE_SolicitudesCreditoEmprendedores.Monto_a_incrementar', 
    'zAE_SolicitudesCreditoEmprendedores.Monto_ponderado_maximo', 'zAE_SolicitudesCreditoEmprendedores.Estatus')
    ->join('Clientes', 'zAE_SolicitudesCreditoEmprendedores.Id_Cliente', 'Clientes.Id_Cliente');
  }

  /**
   * default values on creating new records
   *
   * @author Giovanny González <giovanny.gonzalez@nuvem.mx>
   * created 21/05/2021
   * @params
   * @return void
   *
   */
    public static function boot()
    {
      parent::boot();
      self::creating(function($model) {
        $model->Estatus = '1';
      });
    }

  /**
   * Saves creditRequest Model
   *
   * @author Giovanny González <giovanny.gonzalez@nuvem.mx>
   * created 21/05/2021
   * @params App\Http\Requests\CreditRequestRequest $request
   * @return void
   *
   */
  public function saveModel(CreditRequestRequest $request)
  {
    $data = $request->only([
      'Id_Cliente',
      'Fecha_solicitud',
      'Monto_a_incrementar',
      'Monto_ponderado_maximo'
    ]);
    $this->fill($data)->save();
  }
}
