<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CalculatedCostBase extends Model
{
    const CONNECTION  = 'medicadepot';
    const TABLE       = 'BaseCostosCalculados';
    const PRIMARYKEY  = 'Id_BaseCostoCalculado';

    const DESCRIPCION      = 'Descripcion';
    const CONSTANTEFORMULA = 'ConstanteFormula';
    const ESTATUS          = 'Estatus';

    protected $connection = self::CONNECTION;
    protected $table      = self::TABLE;
    protected $primaryKey = self::PRIMARYKEY;
    public $timestamps    = false;

    /**
     * Possible status
     * 
     * @author Benito Huerta <benito.huerta@nuvem.mx>
     * @created 2020/02/17
     * @var int
     */
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    /**
     * The attributes that are mass assignable.
     * 
     * @author Benito Huerta <benito.huerta@nuvem.mx>
     * @created 2020/02/17
     * @var array
     */
    protected $fillable = [
    	self::PRIMARYKEY,
    	self::DESCRIPCION,
        self::CONSTANTEFORMULA,
    	self::ESTATUS,
    ];

}
