<?php

namespace App;

use App\ArticuloSeccion;
use Illuminate\Http\Request;
use App\Models\MedicaDepot\Group;
use App\Models\MedicaDepot\Profit;
use Illuminate\Support\Facades\Log;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use App\Models\MedicaDepot\ArticlePhoto;
use Yajra\DataTables\Facades\DataTables as DT;

class Product extends Model
{
    const CONNECTION  = 'medicadepot';
    const TABLE       = 'Articulos';
    const PRIMARYKEY  = 'Id_Prod1';
    const DESCRIPCION = 'Descrip1';
    const PRIORIDAD   = 'Prioridad';
    const ESTATUS     = 'Estatus';
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    protected $connection = self::CONNECTION;
    protected $table      = self::TABLE;
    protected $primaryKey = self::PRIMARYKEY;
    protected $keyType= 'string';
    public $timestamps    = false;

    protected $casts = [
        'Id_Prod1' => 'string'
    ];
    protected $fillable = [
        'Id_Prod1',
        'Id_Prod3',
        'CBarrasN1',
        'Id_CodSat',
        'Descrip1',
        'Descrip2',
        'Id_Prov1',
        'Id_Prov2',
        'Inventaria',
        'Lprecios',
        'Man_LoteCad',
        'Activo',
        'eCommerce',
        'Observa',
        'Registro',
        'IVA',
        'Utmin_Lici',
        'Flete',
        'Tiempo_Sur',
        'Id_Categoria',
        'Id_Familia',
        'Id_Marca',
        'Id_EspMed',
        'Id_estatus',
        'Origen',
        'Niveles',
        'id_Empaque1',
        'id_Empaque2',
        'id_Empaque3',
        'Unidad_N2',
        'Unidad_N3',
        'Nivel_Comp',
        'Nivel_Vta',
        'Nivel_Vta2',
        'Ult_Costo',
        'Costo_Prom',
        'CM_MasBajo',
        'UCambio_CM',
        'UCambio_CL',
        'UCambio_Pre',
        'Ult_Compra',
        'Ult_Venta',
        'Peso',
        'Ancho',
        'Alto',
        'Largo',
        'Especificacion',
        'Indicacion'
    ];

    protected $hidden = ['pivot'];

    /**
     * Relation hasMany to ArticuloSeccion
     *
     * @author Giovanny González <giovanny.gonzalez@nuvem.mx>
     * created 15/06/2021
     * @params
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     *
     */
    public function locations()
    {
        return $this->hasMany(ArticuloSeccion::class, 'Articulo_Id_Prod1', 'Id_Prod1' );
    }

    /**
     * Relation hasMany to ArticlePhoto
     * @author José Vega <jose.vega@nuvem.mx>
     * @created 2021/04/16
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function images()
    {
        return $this->hasMany(ArticlePhoto::class, strval( 'Id_Prod1' ) , strval( 'Id_Prod1' ));
    }

    /**
     * Relation hasOne to Profit
     * @author José Vega <jose.vega@nuvem.mx>
     * @created 2021/04/19
     * @return \Illuminate\Database\Eloquent\Relations\hasOne
     */
    public function profit()
    {
        return $this->hasOne(Profit::class, 'Id_Prod1');
    }

    /**
     * Relation belongsToMany to Group
     * @author José Vega <jose.vega@nuvem.mx>
     * @created 2021/05/04
     * @return \Illuminate\Database\Eloquent\Relations\belongsToMany
     */
    public function groups()
    {
        return $this->belongsToMany(Group::class, 'ArticulosGrupos', 'Articulos_Id_Prod1', 'Grupo_Id');
    }

    /**
     * Filters Articulos with activo status.
     * @author José Vega <jose.vega@nuvem.mx>
     * @created 2021/04/19
     * @param param $query
     * @return void
     */
    public function scopeActive($query){
        return $query->where('Activo','=',1);
    }

    /**
     * Filters Articulos with active ecommerce.
     * @author José Vega <jose.vega@nuvem.mx>
     * @created 2021/05/14
     * @param param $query
     * @return void
     */
    public function scopeEcommerce($query){
        return $query->where('eCommerce','=',1);
    }

    /**
    * Filter barcode
    * @auth Oscar Castellanos
    * @date 2021-06-11
    * @param QueryBuilder $query
    * @return QueryBuilder
    */
    public function scopeNoBarcode($query)
    {
        return $query->whereRaw('LEN(Id_Prod3) < 8');
    }

    /**
     * Get the Yajra DataTable of Model
     *
     * @author José Vega <jose.vega@nuvem.mx>
     * @created 2021/03/24
     * @params
     * @return Yajra\DataTables\Facades\DataTables
     */
    public static function datatable()
    {
    	return DT::of(self::query())
    		->editColumn('Estatus', function ($product) {
                return view('products.partials.status', ['product' => $product]);
            })
            ->addColumn('Actions', function ($product) {
                return view('products.partials.buttons', ['product' => $product]);
            })
            ->rawColumns(['Estatus', 'Actions'])
            ->make(true);
    }

    /**
     * Save images in server
     *
     * @author José Vega <jose.vega@nuvem.mx>
     * @created 2020-04-08
     * @param String idProd
     * @param Array $images
     * @param String $disk
     * @return void
     */
    public function saveImages($idProd, $images, $disk)
    {
        $product = $this->find($idProd);
        foreach ($images as $image) {
            $fileName = Storage::disk($disk)->putFile(null, $image);
            $articlePhoto = new ArticlePhoto;
            $articlePhoto->Foto = '../storage/products/'.$fileName;
            $product->images()->save($articlePhoto);
        }
    }

    /**
     * Update images in server
     *
     * @author José Vega <jose.vega@nuvem.mx>
     * @created 2020-04-19
     * @param String idProd
     * @param Array $images
     * @param String $disk
     * @return void
     */
    public function updateImages($idProd, $images, $disk)
    {
        $product = $this->find($idProd);
        $currentImages = $product->images;
        if(count($currentImages) > 0){
            $product->images()->delete();
            foreach ($images as $key=>$image) {
                Storage::disk($disk)->delete($currentImages[$key]->Foto);
                $fileName = Storage::disk($disk)->putFile(null, $image);
                $articlePhoto = new ArticlePhoto;
                $articlePhoto->Foto = '../storage/products/'.$fileName;
                $product->images()->save($articlePhoto);
            }
        }else{
            foreach ($images as $image) {
                $fileName = Storage::disk($disk)->putFile(null, $image);
                $articlePhoto = new ArticlePhoto;
                $articlePhoto->Foto = '../storage/products/'.$fileName;
                $product->images()->save($articlePhoto);
            }
        }
    }

    /**
     *
     * Filling up default values on creating
     * @author José Vega <jose.vega@nuvem.mx>
     * @created 2020-04-19
     * @param
     * @return void
     */
    public static function boot()
    {
        parent::boot();
        self::creating(function(Product $product) {
            $product->Activo = 1;
        });
    }

    /**
     * Change the model status
     *
     * @author José Vega <jose.vega@nuvem.mx>
     * @created 2020-04-16
     * @param
     * @return void
     */
    public function toggleStatus()
    {
        $this->Activo = $this->Activo ? self::STATUS_INACTIVE : self::STATUS_ACTIVE;
        $this->save();
    }
  /**
   * Saves Product's locations
   *
   * @author Giovanny González <giovanny.gonzalez@nuvem.mx>
   * created 15/06/2021
   * @params
   * @return void
   *
   */
  public function saveLocations($idProd1, $locations)
  {
      $this->locations()->delete();
      foreach($locations as $location) {
        $theresAMatch = ArticuloSeccion::findModel($idProd1, $location->seccion->Id)->first();
        if(!$theresAMatch)
          ArticuloSeccion::create([
            'Articulo_Id_Prod1' => $idProd1,
            'zAE_Seccion_Id' => $location->seccion->Id
          ]);
      }
  }
      /**
     * search the products in the database.
     * @author Oscar Castellanos <Oscar.castellanos@nuvem.mx>
     * @created 25/06/2021
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function searchProducts(Request $request)
    {
        $productsQuery = $this->active()->select();
        $requestJson = json_decode($request->filters);
        if ($requestJson->family != 0) {
            $productsQuery->where('Id_Familia','=',$requestJson->family);
        }
        if ($requestJson->brand != 0) {
            $productsQuery->where('Id_Marca','=',$requestJson->brand);
        }
        if ($requestJson->especiality != 0) {
            $productsQuery->where('Id_EspMed','=',$requestJson->especiality);
        }
        if ($requestJson->category != 0) {
            $productsQuery->where('Id_Categoria','=',$requestJson->category);
        }
        if ($requestJson->weight != "") {
            $productsQuery->where('peso',$requestJson->weight,$requestJson->weight_t);
        }
        if ($requestJson->volumen != "") {
            $productsQuery->whereRaw("[Alto]*[Ancho]*[Largo] {$requestJson->volumen} {$requestJson->volumen_t}");
        }

        $products = $productsQuery->get();
        return $products;
    }
}
