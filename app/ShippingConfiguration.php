<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Database\Eloquent\Model;
use App\Http\Requests\ShippingConfigurationRequest;

class ShippingConfiguration extends Model
{
    protected $connection = 'medicadepot';
    protected $table = 'CostoEnvio';
    protected $primaryKey = 'Id_CostoEnvio';
    protected $guarded = ['Id_CostoEnvio'];
    public $timestamps = false;
    /**
     *
     * creates or updates a shipping configuration
     * @author Giovanny González <giovanny.gonzalez@nuvem.mx>
     * created 01/04/2021
     * @params App\Http\Requests\ShippingConfigurationRequest $request
     * @return void
     */
    public function saveModel(ShippingConfigurationRequest $request)
    {
        $tiempoEstimado = explode('-', $request->TiempoEstimadoEnvio);
        $request->merge([
            'TiempoEstimadoMinimo' => intval($tiempoEstimado[0]),
            'TiempoEstimadoMaximo' => ($tiempoEstimado[1] ?? null)]
        );
        $this->fill($request->except('Id_CostoEnvio', 'TiempoEstimadoEnvio'));
        $this->save();
    }
    /**
     *
     * Scope data table entries
     * @author Giovanny González <giovanny.gonzalez@nuvem.mx>
     * created 01/04/2021
     * @params Illuminate\Database\Eloquent\Builder $query
     * @return Illuminate\Database\Eloquent\Builder
     */
    public static function scopeDataTableEntries($query)
    {
        return $query->select('Id_CostoEnvio', 'Descripcion', 'Id_CostoEnvioTipo',
        'AplicaEnvioGratuito', 'MontoMinimoEnvioGratuito',
        DB::raw("CONCAT(TiempoEstimadoMinimo, '-', TiempoEstimadoMaximo) as TiempoEstimadoEnvio"),
        'Tarifa', DB::raw("'1' as status"));
    }
}
