<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
    protected $connection = 'medicadepot';
    protected $table = 'Categorias';
    public $timestamps = false;
    /**
     *
     * select options
     * @author Giovanny González <giovanny.gonzalez@nuvem.mx>
     * created 13/05/2021
     * @params
     * @return void
     */
    public static function filterSelectOptions() {
        $allOption = new Categoria();
        $allOption->Id_Categoria = -1;
        $allOption->Categoria = 'TODAS';
        return self::select('Id_Categoria', 'Categoria')->orderBy('Categoria')->get()
        ->prepend($allOption);
    }
}
