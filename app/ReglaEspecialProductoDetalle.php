<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReglaEspecialProductoDetalle extends Model
{
    protected $connection = 'medicadepot';
    protected $table = 'ReglasEspecialesProductosDetalles';
    protected $primaryKey = 'Id_ReglaEspecialProductoDetalle';
    protected $guarded = ['Id_ReglaEspecialProductoDetalle'];
    public $timestamps = false;

    /**
     *
     * Filling up default values on creating
     * @author Giovanny González <giovanny.gonzalez@nuvem.mx>
     * created 16/03/2021
     * @params
     * @return void
     */
    public static function boot()
    {
        parent::boot();
        self::creating(function(ReglaEspecialProductoDetalle $product) {
            $product->Estatus = 1;
        });
    }
}
