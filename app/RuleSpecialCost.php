<?php

namespace App;

use App\RuleSpecialCost;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use App\Http\Requests\RuleSpecialCostRequest;
use Yajra\DataTables\Facades\DataTables as DT;

class RuleSpecialCost extends Model
{
    const CONNECTION  = 'medicadepot';
    const TABLE       = 'ReglasCostosEspeciales';
    const PRIMARYKEY  = 'Id_ReglaCostoEspecial';
    const DESCRIPCION = 'Descripcion';
    const PRIORIDAD   = 'Prioridad';
    const ESTATUS     = 'Estatus';

    protected $connection = self::CONNECTION;
    protected $table      = self::TABLE;
    protected $primaryKey = self::PRIMARYKEY;
    public $timestamps    = false;

    /**
     * Possible status
     * 
     * @author Benito Huerta <benito.huerta@nuvem.mx>
     * @created 2020/02/16
     * @var int
     */
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    /**
     * The attributes that are mass assignable.
     * 
     * @author Benito Huerta <benito.huerta@nuvem.mx>
     * @created 2020/02/16
     * @var array
     */
    protected $fillable = [
    	self::PRIMARYKEY,
    	self::DESCRIPCION,
    	self::PRIORIDAD,
    	self::ESTATUS
    ];

    /**
     * Active families query.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-04-05
     * @param  \App\Http\Requests\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function scopeActive($query){
        return $query->where('Estatus','=',1);
    }

    /**
     * Add data before adding or updating a record
     *
     * @author Benito Huerta <benito.huerta@nuvem.mx>
     * @created 2020/02/16
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        // When creating a record, add this information by default.
        static::creating(function ($query) {
            $query->Estatus = 1;
        });

    }

    /**
     * Get the Yajra DataTable of Model
     *
     * @author Benito Huerta <benito.huerta@nuvem.mx>
     * @created 2020/02/16
     * @return Yajra\DataTables\Facades\DataTables
     */
    public static function datatable()
    {
    	return DT::of(self::query())
    		->editColumn('Estatus', function ($rule) {
                return view('rules_configurator.partials.status', ['rule' => $rule]);
            })
            ->addColumn('Actions', function ($rule) {
                return view('rules_configurator.partials.buttons', ['rule' => $rule]);
            })
            ->rawColumns(['Estatus', 'Actions'])
            ->make(true);
    }

    /**
     * Change the model status
     *
     * @author Benito Huerta <benito.huerta@nuvem.mx>
     * @created 2020/02/16
     * @return void
     */
    public function toggleStatus()
    {
        $this->Estatus = $this->Estatus ? self::STATUS_INACTIVE : self::STATUS_ACTIVE;
        $this->save();
    }

    /**
     * Verify if a priority exist in db.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-03-31
     * @param  \App\Http\Requests\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function existPriority(Request $request){
        $requestJson = json_decode($request->priority);
        if (RuleSpecialCost::where('Prioridad', '=', $requestJson->priority)->exists()) {
            return response()->json(['priority' => true]);
        }else{
            return response()->json(['priority' => false]);
        }
    }
}
