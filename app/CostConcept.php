<?php

namespace App;

use Illuminate\Support\Facades\Log;
use Illuminate\Database\Eloquent\Model;
use App\Http\Requests\CostConceptRequest;
use App\Models\MedicaDepot\SpecialCostConcept;
use Yajra\DataTables\Facades\DataTables as DT;

class CostConcept extends Model
{
    const CONNECTION  = 'medicadepot';
    const TABLE       = 'ConceptosCostos';
    const PRIMARYKEY  = 'Id_ConceptoCosto';
    const DESCRIPCION = 'Descripcion';
    const TIPO        = 'Tipo';
    const PORCENTAJE  = 'Porcentaje';
    const MONTO       = 'Monto';
    const ESTATUS     = 'Estatus';
    const PRIORIDAD   = 'Prioridad';
    const ID_BASE_COSTO_CALCULADO = 'Id_BaseCostoCalculado';

    protected $connection = self::CONNECTION;
    protected $table      = self::TABLE;
    protected $primaryKey = self::PRIMARYKEY;
    public $timestamps    = false;

    /**
     * Possible status
     *
     * @author Benito Huerta <benito.huerta@nuvem.mx>
     * @created 2020/02/17
     * @var int
     */
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    /**
     * The attributes that are mass assignable.
     *
     * @author Benito Huerta <benito.huerta@nuvem.mx>
     * @created 2020/02/17
     * @var array
     */
    protected $fillable = [
    	self::PRIMARYKEY,
    	self::DESCRIPCION,
        self::TIPO,
        self::PORCENTAJE,
    	self::PRIORIDAD,
        self::MONTO,
    	self::ESTATUS,
        self::PRIORIDAD,
        self::ID_BASE_COSTO_CALCULADO
    ];

    protected $appends = [
        'type_name'
    ];

    /**
     *
     * Scope Cost Concepts only id and description
     * @author Giovanny González <giovanny.gonzalez@nuvem.mx>
     * created 11/03/2021
     * @params
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function scopeSelectOptions()
    {
        return self::select(['id_ConceptoCosto', 'Descripcion']);
    }

    /**
     * Active concept costs query.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-04-07
     * @param  \App\Http\Requests\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function scopeActive($query){
        return $query->where('Estatus','=',1);
    }

    /**
     * Add data before adding or updating a record
     *
     * @author Benito Huerta <benito.huerta@nuvem.mx>
     * @created 2020/02/17
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        // When creating a record, add this information by default.
        static::creating(function ($query) {
            $query->Id_ConceptoCosto = self::getNextId();
            $query->Estatus = 1;
        });

    }

    /**
     * Get the Yajra DataTable of Model
     *
     * @author Benito Huerta <benito.huerta@nuvem.mx>
     * @created 2020/02/17
     * @return Yajra\DataTables\Facades\DataTables
     */
    public static function datatable()
    {
    	return DT::of(self::query())
            ->editColumn('Estatus', function ($cost) {
                return view('global_costs_configurator.partials.status', ['cost' => $cost]);
            })
            ->addColumn('Actions', function ($cost) {
                return view('global_costs_configurator.partials.buttons', ['cost' => $cost]);
            })
            ->rawColumns(['Estatus', 'Actions'])
            ->make(true);
    }

    /**
     * Change the model status
     *
     * @author Benito Huerta <benito.huerta@nuvem.mx>
     * @created 2020/02/17
     * @return void
     */
    public function toggleStatus()
    {
        $this->Estatus = $this->Estatus ? self::STATUS_INACTIVE : self::STATUS_ACTIVE;
        $this->save();
    }

    /**
     * Get the next Id of record
     *
     * @author Benito Huerta <benito.huerta@nuvem.mx>
     * @created 2020/02/17
     * @return int
     */
    public static function getNextId()
    {
        $id = self::max(self::PRIMARYKEY);
        return (!empty($id)) ? $id + 1 : 1;
    }

    /**
     * Relation HasOne to CalculatedCostBase
     *
     * @author Benito Huerta <benito.huerta@nuvem.mx>
     * @created 2020/02/17
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function calculatedCostBase()
    {
        return $this->hasOne(CalculatedCostBase::class, self::ID_BASE_COSTO_CALCULADO, self::ID_BASE_COSTO_CALCULADO);
    }

    /**
     * Relation HasOne to CalculatedCostBase
     *
     * @author Benito Huerta <benito.huerta@nuvem.mx>
     * @created 2020/02/17
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function specialCostConcept()
    {
        return $this->hasOne(SpecialCostConcept::class);
    }

    /**
     * Get the array attributes to update the model
     *
     * @author Benito Huerta <benito.huerta@nuvem.mx>
     * @created 2020/02/22
     * @param  CostConceptRequest $request
     * @return array
     */
    public static function getAttributesToUpdate(CostConceptRequest $request)
    {
        $data = $request->only(['Descripcion', 'Tipo']);

        switch ($request->Tipo) {
            case 'P':
                $data['Porcentaje'] = $request->Porcentaje;
                $data['Monto'] = NULL;
                $data['Id_BaseCostoCalculado'] = null;
                break;

            case 'M':
                $data['Porcentaje'] = NULL;
                $data['Monto'] = $request->Monto;
                $data['Id_BaseCostoCalculado'] = null;
                break;

            case 'C':
                $data['Porcentaje'] = NULL;
                $data['Monto'] = $request->Monto;
                $data['Id_BaseCostoCalculado'] = $request->Id_BaseCostoCalculado;
                break;

            default:
                break;
        }

        return $data;
    }
    /**
     * Get the name of the type
     *
     * @author Iván Morales <ivan.morales@nuvem.mx>
     * @created 2021/04/27
     * @param 
     * @return String
     */
    public function getTypeNameAttribute()
    {
        $namesTypes = [
            "M" => "Monto",
            "P" => "Porcentaje",
            "C" => "Calculado"
        ];

        return array_key_exists($this->Tipo, $namesTypes) ? $namesTypes[$this->Tipo] : 'Desconocido';
    }
}
