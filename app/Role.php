<?php

namespace App;

use App\Traits\LogTrait;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Transformers\RoleTransformer;
use Spatie\Permission\Models\Role as SpatieRole;

class Role extends SpatieRole
{
    use LogTrait;
    
    public $transformer = RoleTransformer::class;

    // Possible status
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    protected $fillable = [
        'name',
        'guard_name',
        'description',
        'status'
    ];

    /**
     * Filters roles with actived status.
     * @auth Octavio Cornejo
     * @date 2021-02-17
     * @param param
     * @return void
     */
    public function scopeActive($query)
    {
        $query->where('status', self::STATUS_ACTIVE);
    }

    /**
     * Transformes the query with given request parameters.
     * @auth Octavio Cornejo
     * @date 2021-02-24
     * @param QueryBuilder $query
     * @return QueryBuilder
     */
    public function scopeTransform($query)
    {
        if (request()->has('simple'))
            $query->select(['id', 'name']);
    }

    /**
     * Assigns the selected permissions to selected role.
     * @auth Octavio Cornejo
     * @date 2021-02-24
     * @param array $permissions
     * @return void
     */
    public function assignPermissions(array $permissions)
    {
        foreach($permissions as $permission) {
            $perm = $permission['perm'];
            $permission['*'] = $permission['all'];

            unset($permission['all']);
            unset($permission['perm']);
            unset($permission['perm_display_name']);

            foreach ($permission as $item => $value) {
                if ($value)
                    $this->givePermissionTo("{$perm}.{$item}");
                else
                    $this->revokePermissionTo("{$perm}.{$item}");
            }
        }
    }

    /**
     * Gets the status of every permission compared to selected role.
     * @auth Octavio Cornejo
     * @date 2021-02-24
     * @param
     * @return Collection $permissions
     */
    public function permissionsStatus()
    {
        $permissions = Permission::select(
            DB::raw("SUBSTRING_INDEX(permissions.NAME, '.', 1) AS perm"),
            DB::raw("SUBSTRING_INDEX(permissions.display_name, '.', 1) AS perm_display_name"),
            DB::raw("exists(SELECT 1 FROM role_has_permissions rhp left join permissions p2 ON p2.id = rhp.permission_id where p2.name LIKE CONCAT(perm, '.', '\\*')  AND rhp.role_id = {$this->id}) AS 'all'"),
            DB::raw("exists(SELECT 1 FROM role_has_permissions rhp left join permissions p2 ON p2.id = rhp.permission_id where p2.name LIKE CONCAT(perm, '.', 'create')  AND rhp.role_id = {$this->id}) AS 'create'"),
            DB::raw("exists(SELECT 1 FROM role_has_permissions rhp left join permissions p2 ON p2.id = rhp.permission_id where p2.name LIKE CONCAT(perm, '.', 'edit')  AND rhp.role_id = {$this->id}) AS 'edit'"),
            DB::raw("exists(SELECT 1 FROM role_has_permissions rhp left join permissions p2 ON p2.id = rhp.permission_id where p2.name LIKE CONCAT(perm, '.', 'view')  AND rhp.role_id = {$this->id}) AS 'view'"),
            DB::raw("exists(SELECT 1 FROM role_has_permissions rhp left join permissions p2 ON p2.id = rhp.permission_id where p2.name LIKE CONCAT(perm, '.', 'delete')  AND rhp.role_id = {$this->id}) AS 'delete'")
        )
        ->leftJoin('role_has_permissions', 'role_has_permissions.permission_id', 'permissions.id')
        ->leftJoin('roles', 'roles.id', 'role_has_permissions.role_id')
        ->groupBy('perm_display_name', 'perm')
        ->get();
        return $permissions;
    }

}
