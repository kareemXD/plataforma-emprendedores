<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;

class EntrepreneurCostsExport implements FromCollection, WithHeadings
{
    private $myHeadings;
    private $collectionData;

    public function __construct($myHeadings, $collectionData){
        $this->myHeadings = $myHeadings;
        $this->collectionData = $collectionData;
    }
    /**
     * Queries the user to be exported
     * 
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-04-14
     * @param  
     * @return \Illuminate\Support\Collection
     */

    public function collection()
    {
        return $collection = collect($this->collectionData);
    }

    /**
     * Returns an array with headings file
     * 
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-04-14
     * @param  
     * @return Array
     */
    public function headings(): array
    {
        return $this->myHeadings;
    }
}

