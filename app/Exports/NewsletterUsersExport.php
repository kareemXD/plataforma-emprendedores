<?php

namespace App\Exports;

use Carbon\Carbon;
use App\NewsletterUser;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;

class NewsletterUsersExport implements FromQuery, WithHeadings, WithMapping
{
    use Exportable;

    /**
     * Queries the user to be exported
     * 
     * @author ramiro.garcilazo@losfra.dev
     * @created 2020-10-15
     * @return \Illuminate\Support\Collection
     */
    public function query()
    {
        return NewsletterUser::query()->where('status', 1);
    }

    /**
     * Maps the fields that the Excel file will contain
     * 
     * @author ramiro.garcilazo@losfra.dev
     * @created 2020-10-15
     * @var NewsletterUser $newsletter_user
     * @return array
     */
    public function map($newsletter_user): array
    {
        return [
            $newsletter_user->email,
            $newsletter_user->created_at->toDayDateTimeString(),
        ];
    }

    /**
     * Sets the headers for the Excel file's columns
     * 
     * @author ramiro.garcilazo@losfra.dev
     * @created 2020-10-15
     * @return array
     */
    public function headings(): array
    {
        return [
            'Correo electrónico',
            'Fecha de registro',
        ];
    }
}
