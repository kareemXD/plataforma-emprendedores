<?php

namespace App\Exports;

use Illuminate\Support\Facades\Log;
use App\SolicitudTransaccionEmprendedor;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;

class CreditRequestsExport implements FromCollection, WithMapping, WithHeadings
{
    private $collection;

    public function __construct($collection) {
      $this->collection = $collection;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return $this->collection;
    }

    public function map($row): array
    {
        return [
           !$row->Monto ? '' : number_format($row->Monto, 2, '.', ','),
           !$row->Tipo_solicitud ? '' : SolicitudTransaccionEmprendedor::TIPOS[$row->Tipo_solicitud],
           !$row->Fecha_transaccion ? '' : date('d/m/Y', strtotime($row->Fecha_transaccion))
        ];
    }

    public function headings(): array
    {
        return [
            'Monto',
            'Tipo de Transacción',
            'Fecha de Transacción',
        ];
    }
}
