<?php

namespace App\Listeners;

use Illuminate\Support\Facades\Log;
use GuzzleHttp\Client as GuzzleClient;
use Illuminate\Queue\InteractsWithQueue;
use GuzzleHttp\Exception\ServerException;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserLoggedIn
{
    /**
     * Handle the success login event.
     *
     * @author Giovanny González <giovanny.gonzalez@nuvem.mx>
     * created 11/06/2021
     * @params object  $event
     * @return void
     *
     */
    public function handle($event)
    {
        $this->getEnterprisingAdminPanelXsrfCookie();
    }
    /**
     * Gets xsrf-cookie from enterprinsing admin panel
     *
     * @author Giovanny González <giovanny.gonzalez@nuvem.mx>
     * created 11/06/2021
     * @params
     * @return void
     *
     */
    private function getEnterprisingAdminPanelXsrfCookie()
    {
        try {
            $client = new GuzzleClient();
            $apiLoginUrl = config('enterprising_admin_panel.api_url') . '/login';
            $credentials = [
              'email' => config('enterprising_admin_panel.email'),
              'password' => config('enterprising_admin_panel.password')
            ];
            $stream = $client->request('POST', $apiLoginUrl, ['json' => $credentials]);
            $response = json_decode($stream->getBody()->getContents());
            request()->session()->put('enterprising_admin_panel_access_token', $response->access_token);
        } catch (ServerException $exception) {
            $responseBody = $exception->getResponse()->getBody(true);
        }
    }
}
