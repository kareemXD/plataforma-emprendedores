<?php

namespace App\Listeners;

use Illuminate\Support\Facades\Log;
use GuzzleHttp\Client as GuzzleClient;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserLogout
{
    /**
     * Handle the logout event.
     *
     * @author Giovanny González <giovanny.gonzalez@nuvem.mx>
     * created 11/06/2021
     * @params object  $event
     * @return void
     *
     */
    public function handle($event)
    {
        //Logout from Enterprising admin panel (invalidate token)
        $client = new GuzzleClient();
        $apiLoginUrl = config('enterprising_admin_panel.api_url') . '/logout';
        $enterprisingToken = request()->session()->get('enterprising_admin_panel_access_token');
        $stream = $client->request('POST', $apiLoginUrl, ['headers' => ['Authorization' => 'Bearer ' . $enterprisingToken]]);

        //Delete token from session
        request()->session()->forget('enterprising_admin_panel_access_token');
    }
}
