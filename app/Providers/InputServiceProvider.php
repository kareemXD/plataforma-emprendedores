<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class InputServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        \Form::component('labelMdl', 'components.inputs.label_mdl', [
            'id', 'label', 'container'
        ]);
    }
}
