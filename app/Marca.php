<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Marca extends Model
{
    protected $connection = 'medicadepot';
    protected $table = 'Marcas';
    public $timestamps = false;

    /**
     *
     * select options
     * @author Giovanny González <giovanny.gonzalez@nuvem.mx>
     * created 13/05/2021
     * @params
     * @return void
     */
    public static function filterSelectOptions() {
        $allOption = new Marca();
        $allOption->Id_Marca = -1;
        $allOption->Marca = 'TODAS';
        return self::select('Id_Marca', 'Marca')->orderBy('Marca')->get()
        ->prepend($allOption);
    }
}
