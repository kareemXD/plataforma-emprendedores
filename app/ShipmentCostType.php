<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShipmentCostType extends Model
{
    protected $connection = 'medicadepot';
    protected $table = 'CostoEnvioTipo';
    protected $primaryKey = 'Id_CostoEnvioTipo';
    protected $guarded = ['Id_CostoEnvioTipo'];
    public $timestamps = false;

    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    /**
     * get actives shipment costs types
     * @author Kareem Lorenzana <@kareem.lorenzana@nuvem.mx>
     * @created 27/04/2021
     * @param  $query
     * @return $query
     */
    public function scopeActive($query){
        return $query->where('Estatus','=',self::STATUS_ACTIVE);
    }
}
