<?php

    namespace App\Repositories;

    use App\Location;
    use App\Rack;

    class LocationRepository{

        private $location, $rack;

         /**
         * Instance class for controller.
         * @auth Kareem Lorenzana <kareem.lorenzana@nuvem.mx>
         * @created 2021-05-27
         * @params Location $location, Rack $rack
         * @return void
         */
        public function __construct(
            Location $location,
            Rack $rack
        ){
            $this->location = $location;
            $this->rack = $rack;
        }

        /**
         * Instance class for controller.
         * @auth Kareem Lorenzana <kareem.lorenzana@nuvem.mx>
         * @created 2021-05-27
         * @param
         * @return query
         */
        public function getLocations()
        {
            $locations = $this->location->active()->get();
            return $locations;
        }

        /**
         * create or update location in database
         * @auth Kareem Lorenzana <kareem.lorenzana@nuvem.mx>
         * @created 2021-05-27
         * @params  $data, Location $location
         * @return \App\Location
         */
        public function storeUpdateLocation($data, Location $location)
        {
            try {
                if($data->isMethod('post')){
                    $location = $this->location->create($data->all());
                    return $location;
                }elseif($data->isMethod('put')){
                    $location->update($data->all());
                    return $location;
                }
            } catch (\Exception $e) {
                return $e->getMessage();
            }

        }

        /**
         * return all racks for select
         * @auth Kareem Lorenzana <kareem.lorenzana@nuvem.mx>
         * @created 2021-05-27
         * @params
         * @return \App\Rack
         */
        public function getRacks()
        {
            return $this->rack->all();
        }
}
