<?php

    namespace App\Repositories;

    use App\Container;

    class ContainerRepository{

        private $container;

         /**
         * Instance class for controller.
         * @auth Kareem Lorenzana <kareem.lorenzana@nuvem.mx>
         * @created 2021-05-28
         * @params Container $container
         * @return void
         */
        public function __construct(
            Container $container
        ){
            $this->container = $container;
        }

        /**
         * Instance class for controller.
         * @auth Kareem Lorenzana <kareem.lorenzana@nuvem.mx>
         * @created 2021-05-28
         * @param
         * @return query
         */
        public function getContainers()
        {
            $containers = $this->container->active()->get();
            return $containers;
        }

        /**
         * create or update container in database
         * @auth Kareem Lorenzana <kareem.lorenzana@nuvem.mx>
         * @created 2021-05-28
         * @params  $data, Container $container
         * @return \App\Container
         */
        public function storeUpdateContainer($data, Container $container)
        {
            try {
                if($data->isMethod('post')){
                    $container = $this->container->create($data->all());
                    return $container;
                }elseif($data->isMethod('put')){
                    $container->update($data->all());
                    return $container;
                }
            } catch (\Exception $e) {
                return $e->getMessage();
            }

        }
}
