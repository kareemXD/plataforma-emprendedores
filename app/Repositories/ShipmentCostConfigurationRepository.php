<?php 

    namespace App\Repositories;

    use App\ShipmentCost;
    use App\ShipmentCostType;
    use Illuminate\Http\Request;

    class ShipmentCostConfigurationRepository{

        private $shipmentCost, $shipmentCostType;

        /**
         * Instance class for controller.
         * @auth Kareem Lorenzana <kareem.lorenzana@nuvem.mx>
         * @created 2021-04-27
         * @param ShipmentCost $shipmentCost
         * @param ShipmentCostType $shipmentCostType
         * @return void
         */
        public function __construct(
            ShipmentCost $shipmentCost,
            ShipmentCostType $shipmentCostType
        ){
            $this->shipmentCost = $shipmentCost;
            $this->shipmentCostType = $shipmentCostType;
        }

        
        /**
         * Display all active shipmentCosts
         * @author Kareem Lorenzana <kareem.lorenzana@nuvem.mx>
         * @params
         * @created 2021-04-27
         * @return \Illuminate\Http\Response
         */
        public function getActiveShipmentCost()
        {
            return $this->shipmentCost->active()->get();
        }

         /**
         * Display all active shipmentCostsTypes for select
         * @author Kareem Lorenzana <kareem.lorenzana@nuvem.mx>
         * @params
         * @created 2021-04-28
         * @return \Illuminate\Http\Response
         */
        public function getActiveListShipmentCostTypes()
        {
            return $this->shipmentCostType->active()->select("Id_CostoEnvioTipo", "Tipo")->get();
        }

        /**
         * create new shipmentcost in database
         * @auth Kareem Lorenzana <kareem.lorenzana@nuvem.mx>
         * @created 2021-04-28
         * @params  $data, ShipmentCost $shipmentCost
         * @return \App\ShipmentCost
         */
        public function storeUpdateShipmentCost($data, ShipmentCost $shipmentCost)
        {
            try {
                if($data->isMethod('post')){
                    $shipmentCost = $this->shipmentCost->create($data->all());
                    $shipmentCost->status = 1;
                    return $shipmentCost;
                }elseif($data->isMethod('put')){
                    $shipmentCost->update($data->all());
                    return $shipmentCost;
                }
            } catch (\Exception $e) {
                return $e->getMessage();
            }
            
        }
    }