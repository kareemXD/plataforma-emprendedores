<?php 

    namespace App\Repositories;
    use Illuminate\Http\Request;
    use App\Models\MedicaDepot\Box;


    class BoxRepository{

        private $box;

        public function __construct(
            Box $box
        ){
            $this->box = $box;
        }

        /**
         * Display a all active boxes
         * @author Kareem Lorenzana <kareem.lorenzana@nuvem.mx>
         * @params
         * @date 2021-04-19
         * @return \Illuminate\Http\Response
         */
        public function getActiveBoxes()
        {
            return $this->box->active()->get();
        }

        /**
         * Display a list of boxes by status
         * 
         * @author Benito Huerta <benito.huerta@nuvem.mx>
         * @date 2021-06-10
         * @return \Illuminate\Http\Response
         */
        public function withEstatusFilter()
        {
            if(!request()->has('estatusFilter'))
                return $this->box->active()->get();

            $estatus = request()->get('estatusFilter');
            
            if($estatus == 2)
                return $this->box->get();

            return $this->box->where('Estatus', $estatus)->get();
        }

        /**
         * Display list of boxes
         * @author Kareem Lorenzana <kareem.lorenzana@nuvem.mx>
         * @params
         * @date 2021-04-19
         * @return \Illuminate\Http\Response
         */
        public function list()
        {
            if(request()->ajax())
                return $this->box->active()->datatable();
        }

        /**
         * create new box in database
         * @auth Kareem Lorenzana <kareem.lorenzana@nuvem.mx>
         * @date 2021-04-18
         * @param  \App\Http\Requests\Request  $request
         * @return \App\Models\Box
         */
        public function storeUpdateBox($data, Box $box)
        {
            try {
                if($data->isMethod('post')){
                    $box = $this->box->create($data->all());
                    $box->Estatus = $box->STATUS_ACTIVE;
                    return $box;
                }elseif($data->isMethod('put')){
                    $box->update($data->all());
                    return $box;
                }
            } catch (\Exception $e) {
                return $e->getMessage();
            }
            
        }
        
        
    }