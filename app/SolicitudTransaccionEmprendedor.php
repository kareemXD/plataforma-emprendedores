<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SolicitudTransaccionEmprendedor extends Model
{
    protected $connection = 'medicadepot';
    protected $table = 'zAE_SolicitudesTransaccionEmprendedores';
    public $timestamps = false;
    protected $primaryKey = 'Id';
    protected $guarded = ['Id'];
    public $appends = ['request_human_date'];

    const TIPOS = [
      '1' => 'Incremento de crédito', 
      '2' => 'Disminución de crédito', 
      '3' => 'Incremento de ponderación', 
      '4' => 'Disminución de ponderación'
    ];

    /**
     * request human date appended attribute
     *
     * @author Giovanny González <giovanny.gonzalez@nuvem.mx>
     * created 21/05/2021
     * @params
     * @return string
     *
     */
    public function getRequestHumanDateAttribute() 
    {
      return date('d/m/Y', strtotime($this->Fecha_solicitud));
    }
  /**
   * Get the enterprising associated with the CreditRequest
   *
   * @author Giovanny González <giovanny.gonzalez@nuvem.mx>
   * created 21/05/2021
   * @params
   * @return \Illuminate\Database\Eloquent\Relations\HasOne
   *
   */
  public function enterprising()
  {
      return $this->hasOne(Cliente::class, 'Id_Cliente', 'Id_Cliente');
  }

  /**
   * Data table scope
   *
   * @author Giovanny González <giovanny.gonzalez@nuvem.mx>
   * created 20/05/2021
   * @params \Illuminate\Database\Eloquent\Builder $query
   * @return \Illuminate\Database\Eloquent\Builder 
   *
   */
  public function scopeDataTable($query)
  {
    return $query->select('zAE_SolicitudesTransaccionEmprendedores.Id', 'Clientes.Razon_soc', 'Clientes.Id_Cliente', 
    'zAE_Emprendedores.Credito_actual', 'zAE_Emprendedores.Credito_utilizado', 'zAE_Emprendedores.Credito_disponible',
    'zAE_Emprendedores.Monto_ponderado_maximo')
    ->join('Clientes', 'zAE_SolicitudesTransaccionEmprendedores.Id_Cliente', 'Clientes.Id_Cliente')
    ->leftJoin('zAE_Emprendedores', 'zAE_Emprendedores.Id_Cliente', 'Clientes.Id_Cliente');
  }
}
