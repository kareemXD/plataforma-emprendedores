<?php

namespace App;

use App\Traits\LogTrait;
use Laravel\Passport\HasApiTokens;
use App\Notifications\PasswordReset;
use App\Transformers\UserTransformer;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Yajra\DataTables\Facades\DataTables as DT;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable, HasRoles, LogTrait;

    public $transformer = UserTransformer::class;

    // Possible status
    const STATUS_ACTIVE = 1;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'last_name',
        'second_last_name',
        'email',
        'password',
        'cellphone',
        'status',
        'created_by',
        'edited_by'
    ];

    protected $appends = [
        'full_name'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    
    /**
     * Put a password default in a new user.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-04-27
     * @param
     * @return void
     */
    public static function boot(){
        parent::boot();
        static::creating(function($query) {
            $query->password = bcrypt('secret');
        });
    }

    /**
     * Get concatenated name of user.
     * @auth Octavio Cornejo
     * @date 2021-02-26
     * @param
     * @return String
     */
    public function getFullNameAttribute()
    {
        return "{$this->name} {$this->last_name} {$this->second_last_name}";
    }

    /**
     * @author Luis pena <luis.pena@nuvemtecnologia.mx> 04/11/2020
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function shippingConfig()
    {
        return $this->hasMany('App\ShippingConfig', 'user_id');
    }

    /**
     * Filters users with actived status.
     * @auth Octavio Cornejo
     * @date 2021-02-17
     * @param param
     * @return void
     */
    public function scopeActive($query)
    {
        $query->where('status', self::STATUS_ACTIVE);
    }

    /**
     * @author Octavio Cornejo
     * @created 2020-04-22
     * @param array $attributes
     * @param array $options
     * @return bool
     */
    public function update(array $attributes = [], array $options = [])
    {
        if (is_null(request('password')) and is_null(request('password_confirmation')))
            unset($attributes['password'], $attributes['password_confirmation']);
        return parent::update($attributes, $options);
    }

    /**
     * @author Octavio Cornejo
     * @created 2020-04-22
     * @return mixed
     * @throws \Exception
     */
    public static function datatable()
    {
        return DT::of(static::query())
            ->editColumn('status', function ($user) {
                return view('users.partials.status', ['user' => $user]);
            })
            ->addColumn('actions', function ($user) {
                return view('users.partials.buttons', ['user' => $user]);
            })
            ->rawColumns(['actions', 'status'])
            ->make(true);
    }

    /**
     * Send the password reset notification.
     * @auth Octavio Cornejo
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new PasswordReset($token));
    }

     /**
     * Get the proper menú according to user type value.
     * @auth Octavio Cornejo
     * @date 2021-03-02
     * @param
     * @return Array
     */
    public function getMenu()
    {
        return config('menu.admin');
    }

        /**
     * Can filter the data by given role name.
     * @auth Octavio Cornejo
     * @date 2021-02-25
     * @param QueryBuilder $query
     * @return QueryBuilder
     */
    public function scopeFilters($query)
    {
        if (request()->has('by_role'))
            $query->whereHas('roles', function($q) {
                $q->where('name', request('by_role'));
            });
    }

    /**
     * Validate if the roles given are strings or arrays and take the value.
     * @auth Octavio Cornejo
     * @date 2021-03-08
     * @param param
     * @return void
     */
    public function assignRoles(array $roles)
    {
        $toAssign = [];
        foreach($roles as $role) {
            if (is_string($role)) array_push($toAssign, $role);
            else array_push($toAssign, $role['name']);
        }

        $this->syncRoles($toAssign);

        return $this;
    }

}
