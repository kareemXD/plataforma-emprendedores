<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Observers\ShippingConfigObserver;

class ShippingConfig extends Model
{
    protected $table = 'shipping_config';

    protected $fillable = [
        'user_id',
        'box_name',
        'box_length',
        'box_height',
        'box_width',
        'box_maximum_weight',
        'box_dimension',
        'status',
        'shipping_type'
    ];

    protected $casts = [
        'shipping_type' => 'object',
    ];

    /**
    * Bootstrap any application services.
    * @author luis peña <luis.pena@nuvem.mx> 04/11/2020
    * @return void
    */
    public static function boot()
    {
        parent::boot();
        parent::observe(ShippingConfigObserver::class);
    }

    /**
     * Toggle the status attribute
     *
     * @author luis peña <luis.pena@nuvem.mx> 04/11/2020
     * @param
     */
    public function toggleStatus()
    {

        $this->status = $this->status == 1 ? 0 : 1;
        $this->save();
    }
}
