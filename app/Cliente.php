<?php

namespace App;

use App\ViewArticulosEC;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Database\Eloquent\Model;
use App\SolicitudTransaccionEmprendedor;

class Cliente extends Model
{
    protected $connection = 'medicadepot';
    protected $table = 'Clientes';
    protected $primaryKey = 'Id_Cliente';
    public $timestamps = false;


    /**
     * scope select options
     *
     * @author Giovanny González <giovanny.gonzalez@nuvem.mx>
     * created 20/05/2021
     * @params \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder 
     *
     */
    public function scopeSelectOptions($query)
    {
      $query->select('Id_Cliente', 'Razon_Soc');
    }

    /**
     * Get the enterprising associated with the CreditRequest
     *
     * @author Giovanny González <giovanny.gonzalez@nuvem.mx>
     * created 21/05/2021
     * @params
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     *
     */
    public function creditRequests()
    {
        return $this->hasMany(SolicitudTransaccionEmprendedor::class, 'Id_Cliente', 'Id_Cliente');
    }

    /**
     * scope credit requests history
     *
     * @author Giovanny González <giovanny.gonzalez@nuvem.mx>
     * created 27/05/2021
     * @params 
     * @return Illuminate\Database\Eloquent\Collection
     *
     */
    public function creditRequestsHistory()
    { 
      return $this->creditRequests()->select('zAE_SolicitudesTransaccionEmprendedores.Monto',
      'zAE_TiposSolicitudes.Id as Tipo_solicitud', 'zAE_HistorialTransaccionesEmprendedores.Fecha_transaccion')
      ->leftJoin('zAE_TiposSolicitudes', 'zAE_TiposSolicitudes.Id', 'zAE_SolicitudesTransaccionEmprendedores.zAE_TipoSolicitud_Id')
      ->leftJoin('zAE_HistorialTransaccionesEmprendedores', 'zAE_HistorialTransaccionesEmprendedores.zAE_SolicitudTransaccionEmprendedor_Id',
      'zAE_SolicitudesTransaccionEmprendedores.Id')->get();
    }

    /**
     *
     * Enterprising's products
     * @author Giovanny González <giovanny.gonzalez@nuvem.mx>
     * created 13/05/2021
     * @params
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function assignedProducts()
    {
        $idCliente = $this->Id_Cliente;
        $query = ViewArticulosEC::select('vArticulosEC.Id_Prod1', 'Marcas.Marca', 'Familias.Familia', 
        'Especialidades_Medicas.Especialidad', 'Categorias.Categoria',
        DB::raw('CASE WHEN zAE_ArticulosClientesExcepciones.Articulos_Id_Prod1 IS NULL THEN 1 ELSE 0 END as Estatus'))
        ->leftJoin('Marcas', 'Marcas.Id_Marca', 'vArticulosEC.Id_Marca')
        ->leftJoin('Familias', 'Familias.Id_Familia', 'vArticulosEC.Id_Familia')
        ->leftJoin('Especialidades_Medicas', 'Especialidades_Medicas.Id_EspMed', 'vArticulosEC.Id_EspMed')
        ->leftJoin('Categorias', 'Categorias.Id_Categoria', 'vArticulosEC.Id_Categoria')
        ->leftJoin('zAE_ArticulosClientesExcepciones', 'zAE_ArticulosClientesExcepciones.Articulos_Id_Prod1', 'vArticulosEC.Id_Prod1');

        if(request()->brand && request()->brand > -1)
            $query->where('Marcas.Id_Marca', request()->brand);
        if(request()->family && request()->family > -1)
            $query->where('Familias.Id_Familia', request()->family);
        if(request()->especiality && request()->especiality > -1)
            $query->where('Especialidades_Medicas.Id_EspMed', request()->especiality);
        if(request()->category && request()->category > -1)
            $query->where('Categorias.Id_Categoria', request()->category);

        return $query->get();
    }
}
