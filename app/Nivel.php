<?php

namespace App;

use App\Rack;
use App\Seccion;
use App\Traits\LogTrait;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Nivel extends Model
{
    use LogTrait;
    protected $connection = 'medicadepot';
    protected $table = 'zAE_Niveles';
    protected $primaryKey = 'Id';
    protected $fillable = [
        'Nombre',
        'zAE_Rak_Id',
        'Estatus'
    ];
    public $timestamps = false;

    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    /**
     * Get all of the secciones for the Nivel
     *
     * @author Giovanny González <giovanny.gonzalez@nuvem.mx>
     * created 15/06/2021
     * @params
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     *
     */
    public function secciones()
    {
        return $this->hasMany(Seccion::class, 'zAE_Nivel_Id', 'Id');
    }

    /**
     * Get all of the secciones for the Nivel only status active
     *
     * @author Iván Morales | ivan.morales@nuvem.mx
     * created 25/06/2021
     * @params
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     *
     */
    public function sectionsActive()
    {
        return $this->hasMany(Seccion::class, 'zAE_Nivel_Id', 'Id')->active();
    }
    /**
     * Get the rack associated with the Nivel
     *
     * @author Giovanny González <giovanny.gonzalez@nuvem.mx>
     * created 15/06/2021
     * @params
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     *
     */
    public function rack()
    {
        return $this->hasOne(Rack::class, 'Id', 'zAE_Rak_Id');
    }

    /**
     * Get the record with status active
     * @author Iván Morales <ivan.morales@nuvem.mx>
     * @created 2021/06/18
     * @param QueryBuilder $query
     * @return QueryBuilder
     */
    public function scopeActive($query)
    {
        return $query->where('Estatus', self::STATUS_ACTIVE);
    }

    /**
     * Create a record for each section recibed
     * @author Iván Morales <ivan.morales@nuvem.mx>
     * @created 2021/06/24
     * @param Request $request
     * @return collect
     */
    public function storeSections($request)
    {
        try {
            DB::connection('medicadepot')->beginTransaction();
            $sections = $request->sections;
            $sectionsSaved = collect();
            foreach ($sections as $section) {
                $newSection = Seccion::create(['Nombre' => $section['Nombre'], 'zAE_Nivel_Id' => $this->Id]);
                $sectionsSaved->push($newSection);
            }
            DB::connection('medicadepot')->commit();
            return $sectionsSaved;
        } catch (\Throwable $th) {
            DB::connection('medicadepot')->rollback();
            throw $th;
        }
    }

    /**
     * Toggle the status of a level and sections related
     * @author Iván Morales <ivan.morales@nuvem.mx>
     * @created 2021/06/25
     * @param
     * @return StdClass
     */
    public function deleteLevel(){
        try {
            DB::connection('medicadepot')->beginTransaction();
            $modelsDeleted = new \StdClass();
            $this->toggleStatus();
            $modelsDeleted->level = $this;
            $sections = $this->sectionsActive;
            $sectionsDeleted = collect();
            foreach ($sections as $section) {
                $section->toggleStatus();
                $sectionsDeleted->push($section);
            }
            $modelsDeleted->sections = $sectionsDeleted;
            DB::connection('medicadepot')->commit();
            return $modelsDeleted;
        } catch (\Throwable $th) {
            DB::connection('medicadepot')->rollback();
            throw $th;
        }
    }
}
