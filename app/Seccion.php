<?php

namespace App;

use App\Nivel;
use App\Traits\LogTrait;
use Illuminate\Database\Eloquent\Model;

class Seccion extends Model
{
    use LogTrait;

    protected $connection = 'medicadepot';
    protected $table = 'zAE_Secciones';
    protected $primaryKey = 'Id';
    protected $fillable = [
        'Nombre',
        'zAE_Nivel_Id',
        'Estatus',
    ];
    public $timestamps = false;

    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    /**
     * Get the nivel associated with the Seccion
     *
     * @author Giovanny González <giovanny.gonzalez@nuvem.mx>
     * created 15/06/2021
     * @params
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     *
     */
    public function nivel()
    {
        return $this->hasOne(Nivel::class, 'Id', 'zAE_Nivel_Id');
    }

    /**
     * Get the records with status active
     * @author Iván Morales <ivan.morales@nuvem.mx>
     * @created 2021/06/23
     * @param QueryBuilder $query
     * @return QueryBuilder
     */
    public function scopeActive($query)
    {
        return $query->where('Estatus', self::STATUS_ACTIVE);
    }

    /**
     * Get the records ordered by level id descendent
     * @author Iván Morales <ivan.morales@nuvem.mx>
     * @created 2021/06/25
     * @param QueryBuilder $query
     * @return QueryBuilder
     */
    public function scopeOrderedLevelDesc($query)
    {
        return $query->orderBy('zAE_Nivel_Id', 'desc');
    }
}
