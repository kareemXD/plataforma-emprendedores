<?php

namespace App;

use App\Nivel;
use App\Traits\LogTrait;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Rack extends Model
{
    use LogTrait;

    protected $connection = 'medicadepot';
    protected $table = 'zAE_Raks';
    protected $primaryKey = 'Id';
    protected $fillable = [
        'Nombre',
    ];
    
    public $timestamps = false;

    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    /**
     * Get all of the niveles for the Rack
     *
     * @author Giovanny González <giovanny.gonzalez@nuvem.mx>
     * created 14/06/2021
     * @params
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     *
     */
    public function niveles()
    {
        return $this->hasMany(Nivel::class, 'zAE_Rak_Id', 'Id');
    }

    /**
     * Relation hasMany with Nivel Model
     *
     * @author Iván Morales <ivan.morales@nuvem.mx>
     * created 18/06/2021
     * @params
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     *
     */
    public function levelsActive()
    {
        return $this->hasMany(Nivel::class, 'zAE_Rak_Id', 'Id')->active();
    }

    /**
     * Get the record with status active
     * @author Iván Morales <ivan.morales@nuvem.mx>
     * @created 2021/06/17
     * @param QueryBuilder $query
     * @return QueryBuilder
     */
    public function scopeActive($query)
    {
        return $query->where('Estatus', self::STATUS_ACTIVE);
    }

    /**
     * Store the rack and store each level
     * @author Iván Morales <ivan.morales@nuvem.mx>
     * @created 2021/06/18
     * @param Request $request
     * @return collect
     */
    public static function storeRack($request){
        try {
            DB::connection('medicadepot')->beginTransaction();
            $newRack = self::create(['Nombre' => $request->name]);
            $newRackId = $newRack->Id;
            $levels = $request->levels;
            $levelsAdded = collect();
            foreach ($levels as $level) {
                $newLevel = Nivel::create(['Nombre' => $level['Nombre'], 'zAE_Rak_Id' => $newRackId]);
                $levelsAdded->push($newLevel);
            }
            $newRack->levels = $levelsAdded;
            DB::connection('medicadepot')->commit();
            return $newRack;
        } catch (\Throwable $th) {
            DB::connection('medicadepot')->rollback();
            throw $th;
        }
    }

    /**
     * Toggle the status of a rack, levels and sections related
     * @author Iván Morales <ivan.morales@nuvem.mx>
     * @created 2021/06/25
     * @param
     * @return StdClass
     */
    public function deleteRack(){
        try {
            DB::connection('medicadepot')->beginTransaction();
            $modelsDeleted = new \StdClass();
            $this->toggleStatus();
            $modelsDeleted->rack = $this;
            $levels = $this->levelsActive;
            $levelsDeleted = collect();
            $sectionsDeleted = collect();
            foreach ($levels as $level) {
                $level->toggleStatus();
                $sections = $level->sectionsActive;
                foreach ($sections as $section) {
                    $section->toggleStatus();
                    $sectionsDeleted->push($section);
                }
                $levelsDeleted->push($level);
            }
            $modelsDeleted->levels = $levelsDeleted;
            $modelsDeleted->sections = $sectionsDeleted;
            DB::connection('medicadepot')->commit();
            return $modelsDeleted;
        } catch (\Throwable $th) {
            DB::connection('medicadepot')->rollback();
            throw $th;
        }
    }
}
