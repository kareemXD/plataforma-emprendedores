<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Familia extends Model
{
    protected $connection = 'medicadepot';
    protected $table = 'Familias';
    public $timestamps = false;

    /**
     *
     * select options
     * @author Giovanny González <giovanny.gonzalez@nuvem.mx>
     * created 13/05/2021
     * @params
     * @return void
     */
    public static function filterSelectOptions() {
        $allOption = new Familia();
        $allOption->Id_Familia = -1;
        $allOption->Familia = 'TODAS';
        return self::select('Id_Familia', 'Familia')->orderBy('Familia')->get()
        ->prepend($allOption);
    }
}
