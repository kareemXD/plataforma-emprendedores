<?php

namespace App;

use App\Seccion;
use Illuminate\Support\Facades\Log;
use Illuminate\Database\Eloquent\Model;

class ArticuloSeccion extends Model
{
    protected $connection = 'medicadepot';
    protected $table = 'zAE_ArticulosSecciones';
    public $timestamps = false;
    protected $guarded = [];
    /**
     * Get the seccion associated with the ArticuloSeccion
     *
     * @author Giovanny González <giovanny.gonzalez@nuvem.mx>
     * created 15/06/2021
     * @params
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     *
     */
    public function seccion()
    {
        return $this->hasOne(Seccion::class, 'Id', 'zAE_Seccion_Id');
    }
    /**
     * findModel scope
     *
     * @author Giovanny González <giovanny.gonzalez@nuvem.mx>
     * created 15/06/2021
     * @params Illuminate\Database\Eloquent\Builder $query
     * @return Illuminate\Database\Eloquent\Builder
     *
     */
    public function scopeFindModel($query, $idProd1, $seccionId)
    {
      return $query->where('Articulo_Id_Prod1', $idProd1)
        ->where('zAE_Seccion_Id', $seccionId);
    }
}
