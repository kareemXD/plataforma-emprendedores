<?php

namespace App;

use App\Product;
use App\RuleSpecialCost;
use Illuminate\Http\Request;
use App\ReglaEspecialProducto;
use Illuminate\Support\Facades\DB;
use App\Models\MedicaDepot\Article;
use Illuminate\Support\Facades\Log;
use App\ReglaEspecialProductoDetalle;
use Illuminate\Database\Eloquent\Model;

class CostoEspecial extends Model
{
    protected $connection = 'medicadepot';
    protected $table = 'CostosEspeciales';
    protected $primaryKey = 'Id_CostoEspecial';
    protected $guarded = ['Id_CostoEspecial'];
    public $timestamps = false;

    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    /**
     *
     * CostoEspecial hasMany ReglaEspecialProducto
     * @author Giovanny González <giovanny.gonzalez@nuvem.mx>
     * created 12/03/2021
     * @params
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function reglas_especiales_productos()
    {
        return $this->hasMany(ReglaEspecialProducto::class, 'Id_CostoEspecial', 'Id_CostoEspecial');
    }

    /**
     * Active families query.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-04-05
     * @param  \App\Http\Requests\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function scopeActive($query){
        return $query->where('Estatus','=',1);
    }

    /**
     *
     * Filling up default values on creating
     * @author Giovanny González <giovanny.gonzalez@nuvem.mx>
     * created 12/03/2021
     * @params
     * @return void
     */
    public static function boot()
    {
        parent::boot();
        self::creating(function(CostoEspecial $costo) {
            $costo->Estatus = 1;
        });
    }

    /**
     *
     * create method override
     * @author Giovanny González <giovanny.gonzalez@nuvem.mx>
     * created 12/03/2021
     * @params Illuminate\Http\Request $request
     * @return void
     */
    public function store(Request $request)
    {
        DB::transaction(function () use ($request) {
            $this->fill($request->only(['Descripcion', 'Id_ReglaCostoEspecial']));
            $this->save();

            //Reglas
            foreach($request->reglas_especiales_productos as $regla) {
                //Regla
                $regla = $this->reglas_especiales_productos()->create([
                    'Id_CostoEspecial' => $this->Id_CostoEspecial,
                    'Descripcion' => $regla['description'],
                    'Tipo' => $regla['type'],
                    'FechaInicio' => $regla['startDate'],
                    'FechaFin' => $regla['endDate'],
                    'Id_BaseCostoCalculado' => $regla['idBaseCostoCalculado'],
                    'Id_ConceptoCosto' => $regla['cost'],
                    'Cantidad' => $regla['amount'],
                ]);
                //Producto - regla
                foreach ($request->reglas_especiales_productos_detalles as $producto) {
                    Log::notice($producto['Id_Prod1']);
                    $regla->productos()->create([
                        'Id_ReglaEspecialProducto' => $regla->Id_ReglaEspecialProducto,
                        'Id_Prod1' => $producto['Id_Prod1']
                    ]);
                }
            }
        });
    }

    /**
     * Update a special cost data
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-04-13
     * @params Illuminate\Http\Request $request
     * @return void
     */
    public function updateData(Request $request)
    {
        $data = $request->only(['Descripcion', 'Id_ReglaCostoEspecial']);
        $this->update($data);
        //Reglas
        foreach($request->reglas_especiales_productos as $key => $rule){
            $data = [
                'Id_CostoEspecial' => $this->Id_CostoEspecial,
                'Descripcion' => $rule['description'],
                'Tipo' => $rule['type'],
                'FechaInicio' => $rule['startDate'],
                'FechaFin' => $rule['endDate'],
                'Id_BaseCostoCalculado' => $rule['idBaseCostoCalculado'],
                'Id_ConceptoCosto' => $rule['cost'],
                'Cantidad' => $rule['amount']
            ];
            if($rule['id'] != null){
                $this->reglas_especiales_productos[$key]->update($data);
                $this->reglas_especiales_productos[$key]->update($data);
                foreach ($this->reglas_especiales_productos[$key]->productos as $product) {
                    $product->delete();
                }
                //Productos
                foreach ($request->reglas_especiales_productos_detalles as $producto) {
                    $this->reglas_especiales_productos[$key]->productos()->create([
                        'Id_ReglaEspecialProducto' => $rule['id'],
                        'Id_Prod1' => $producto['Id_Prod1']
                    ]);
                }
            }else{
                $regla = $this->reglas_especiales_productos()->create($data);
                //Productos
                foreach ($request->reglas_especiales_productos_detalles as $producto) {
                    $regla->productos()->create([
                        'Id_ReglaEspecialProducto' => $rule['id'],
                        'Id_Prod1' => $producto['Id_Prod1']
                    ]);
                }
            }
        }
    }

    /**
     *
     * Gets costo especial main data table showed on index view
     * @author Giovanny González <giovanny.gonzalez@nuvem.mx>
     * created 16/03/2021
     * @params
     * @return Object
     */
    public static function getDataTable()
    {
        $query = self::select('CostosEspeciales.Id_CostoEspecial', 'CostosEspeciales.Descripcion',
        DB::raw("CASE CostosEspeciales.Estatus WHEN 0 THEN 'Inactivo' ELSE 'Activo' END as Estatus"),
        'productos.productos')
        ->join(
            DB::raw("
                (SELECT CostosEspeciales.Id_CostoEspecial,
                STRING_AGG(Articulos.Descrip1, '<br>') as productos
                FROM ReglasEspecialesProductosDetalles
                JOIN Articulos on Articulos.Id_Prod1 = ReglasEspecialesProductosDetalles.Id_Prod1
                JOIN ReglasEspecialesProductos on ReglasEspecialesProductos.Id_ReglaEspecialProducto  = ReglasEspecialesProductosDetalles.Id_ReglaEspecialProducto
                JOIN CostosEspeciales on CostosEspeciales.Id_CostoEspecial = ReglasEspecialesProductos.Id_CostoEspecial
                GROUP BY CostosEspeciales.Id_CostoEspecial) as productos"), 'productos.Id_CostoEspecial', 'CostosEspeciales.Id_CostoEspecial'
        );
        return datatables()->of($query)
        ->addColumn('actions', function ($special_price) {
            return view('special_prices.partials.buttons', ['special_price' => $special_price]);
        })
        ->rawColumns(['actions', 'productos'])
        ->make(true);
    }

    /**
     * search the products in the database.
     * @author daniel.cruz@losfra.dev
     * @created 2020-12-30
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function searchProducts(Request $request)
    {
        $productsQuery = Product::active()->select('Id_Prod1', 'Descrip1')->ecommerce();
        $requestJson = json_decode($request->filters);
        if ($requestJson->family != 0) {
            $productsQuery->where('Id_Familia','=',$requestJson->family)->ecommerce();
        }
        if ($requestJson->brand != 0) {
            $productsQuery->where('Id_Marca','=',$requestJson->brand)->ecommerce();
        }
        if ($requestJson->especiality != 0) {
            $productsQuery->where('Id_EspMed','=',$requestJson->especiality)->ecommerce();
        }
        if ($requestJson->category != 0) {
            $productsQuery->where('Id_Categoria','=',$requestJson->category)->ecommerce();
        }
        if ($requestJson->weight != "") {
            $productsQuery->where('peso',$requestJson->weight,$requestJson->weight_t)->ecommerce();
        }
        if ($requestJson->volumen != "") {
            $productsQuery->whereRaw("([Alto] * [Ancho] * [Largo]) {$requestJson->volumen} {$requestJson->volumen_t}");
        }

        $products = $productsQuery->get();
        return $products;
    }

    /**
     * Change the model status
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-04-12
     * @param
     * @return void
     */
    public function toggleStatus()
    {
        $this->Estatus = $this->Estatus ? self::STATUS_INACTIVE : self::STATUS_ACTIVE;
        $this->save();
    }
}
