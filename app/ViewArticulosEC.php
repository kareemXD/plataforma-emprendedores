<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ViewArticulosEC extends Model
{
    protected $connection = 'medicadepot';
    public $table = "vArticulosEC";
}
