<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShipmentCost extends Model
{
    protected $connection = 'medicadepot';
    protected $table = 'CostoEnvio';
    protected $primaryKey = 'Id_CostoEnvio';
    protected $guarded = ['Id_CostoEnvio'];
    
    public $timestamps = false;

    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    protected $appends = ["estimated_time", "rate_format"];

    /**
     * return estimated_time attribute for shipment cost estimated time
     * @author Kareem Lorenzana <kareem.lorenzana@nuvem.mx>
     * created 27/04/2021
     * @params
     * @return string
     */
    public function getEstimatedTimeAttribute()
    {
        return $this->TiempoEstimadoMinimo."-".$this->TiempoEstimadoMaximo." días hábiles";
    }

    /**
     * return estimated_time attribute for shipment cost estimated time
     * @author Kareem Lorenzana <kareem.lorenzana@nuvem.mx>
     * created 27/04/2021
     * @params
     * @return string
     */
    public function getRateFormatAttribute()
    {
        return "$".number_format($this->Tarifa, 2);
    }

    /**
     * ShipmentCost belongsTo ShipmentCostType
     * @author Kareem Lorenzana <kareem.lorenzana@nuvem.mx>
     * created 27/04/2021
     * @params
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function shipmentCostType()
    {
        return $this->belongsTo(ShipmentCostType::class, 'Id_CostoEnvioTipo', 'Id_CostoEnvioTipo');
    }

    
    /**
     * get active shipment costs
     * @author Kareem Lorenzana <@kareem.lorenzana@nuvem.mx>
     * @created 27/04/2021
     * @param  $query
     * @return $query
     */
    public function scopeActive($query){
        return $query->where('Estatus','=',self::STATUS_ACTIVE);
    }

     /**
     * Toggles status
     * @author Kareem Lorenzana <@kareem.lorenzana@nuvem.mx>
     * @created 2020-04-27
     * @param  
     * @return void
     **/
    public function toggleStatus()
    {
        $this->Estatus = $this->Estatus ? self::STATUS_INACTIVE : self::STATUS_ACTIVE;
        $this->save();
    }
   
}
