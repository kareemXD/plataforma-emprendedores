<?php

namespace App\Observers;

use Auth;
use App\ShippingConfig;

class ShippingConfigObserver
{
    /**
     * Handle the shipping config "created" event.
     * @Author luis peña <luis.pena@nuvem.mx> 04/11/2020
     * @param  \App\ShippingConfig  $shippingConfig
     * @return void
     */
    public function creating(ShippingConfig $shippingConfig) {
        $shippingConfig->user_id = Auth::user()->id;
        $shippingConfig->box_dimension = (($shippingConfig->box_length * $shippingConfig->box_width ) * $shippingConfig->box_height);
    }

    /**
     * Handle the shipping config "updated" event.
     * @Author luis peña <luis.pena@nuvem.mx> 04/11/2020
     * @param  \App\ShippingConfig  $shippingConfig
     * @return void
     */
    public function saving(ShippingConfig $shippingConfig)
    {
        $shippingConfig->user_id = Auth::user()->id;
        $shippingConfig->box_dimension = (($shippingConfig->box_length * $shippingConfig->box_width ) * $shippingConfig->box_height);
    }
}
