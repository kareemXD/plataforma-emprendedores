<?php

namespace App\Transformers;

use App\Role;
use League\Fractal\TransformerAbstract;

class RoleTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Role $role)
    {
        return [
            'id' => (int) $role->id,
            'name' => (string) $role->name,
            'display_name' => (string) $role->display_name,
            'description' => (string) $role->description,
            'status' => (int) $role->status,
            'created_at' => (string) $role->created_at,
            'updated_at' => (string) $role->updated_at,
            'links' => [
//                [
//                    'rel' => 'self',
//                    'href' => route('users.show', $role->id)
//                ],
            ]
        ];
    }

    /**
     * @author Octavio Cornejo
     * @param $attribute
     * @return bool
     */
    public static function hasAttribute($attribute)
    {
        $attributes = [
            'id',
            'name',
            'display_name',
            'description',
            'status',
            'created_at',
            'updated_at',
        ];

        return in_array($attribute, $attributes);
    }

}
