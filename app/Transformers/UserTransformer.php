<?php

namespace App\Transformers;

use App\User;
use League\Fractal\TransformerAbstract;

class UserTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(User $user)
    {
        return [
            'id' => (int) $user->id,
            'name' => (string) $user->name,
            'last_name' => (string) $user->last_name,
            'second_last_name' => (string) $user->second_last_name,
            'full_name' => (string) $user->full_name,
            'email' => (string) $user->email,
            'status' => (int) $user->status,
            'is_admin' => $user->is_admin,
            'created_at' => (string) $user->created_at,
            'updated_at' => (string) $user->updated_at,
            'links' => [
//                [
//                    'rel' => 'self',
//                    'href' => route('users.show', $user->id)
//                ],
            ]
        ];
    }

    /**
     * @author Octavio Cornejo
     * @param $attribute
     * @return bool
     */
    public static function hasAttribute($attribute)
    {
        $attributes = [
            'id',
            'name',
            'last_name',
            'second_last_name',
            'email',
            'status',
            'created_at',
            'updated_at',
        ];

        return in_array($attribute, $attributes);
    }

}
