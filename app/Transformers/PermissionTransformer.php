<?php

namespace App\Transformers;

use App\Permission;
use League\Fractal\TransformerAbstract;

class PermissionTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Permission $permission)
    {
        return [
            'id' => (int) $permission->id,
            'name' => (string) $permission->name,
            'display_name' => (string) $permission->display_name,
            'description' => (string) $permission->description,
            'status' => (int) $permission->status,
            'created_at' => (string) $permission->created_at,
            'updated_at' => (string) $permission->updated_at,
            'links' => [
//                [
//                    'rel' => 'self',
//                    'href' => route('users.show', $permission->id)
//                ],
            ]
        ];
    }

    /**
     * @author Octavio Cornejo
     * @param $attribute
     * @return bool
     */
    public static function hasAttribute($attribute)
    {
        $attributes = [
            'id',
            'name',
            'display_name',
            'description',
            'status',
            'created_at',
            'updated_at',
        ];

        return in_array($attribute, $attributes);
    }

}
