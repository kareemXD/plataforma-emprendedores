<?php

namespace App\Transformers;

use App\Article;
use League\Fractal\TransformerAbstract;

class ArticleTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Article $article)
    {
        return [
            'article' => (object) $article,
            'url_img_public' => (string)  asset('storage/article_files/'.$article->icon)
        ];
    }


}
