<?php

namespace App;

use Zizaco\Entrust\EntrustPermission;
use App\Transformers\PermissionTransformer;

class Permission extends EntrustPermission
{
    public $transformer = PermissionTransformer::class;

    protected $fillable = [
        'name',
        'display_name',
        'description',
    ];
}
