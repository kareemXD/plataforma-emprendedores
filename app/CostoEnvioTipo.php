<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CostoEnvioTipo extends Model
{
    protected $connection = 'medicadepot';
    protected $table = 'CostoEnvioTipo';
    protected $primaryKey = 'Id_CostoEnvioTipo';
    protected $guarded = ['Id_CostoEnvioTipo'];
}
