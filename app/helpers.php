<?php
if (!function_exists('validate_token')) {
    /**
     * @author Fernando Montes de Oca
     * @created 2020-04-25
     * @param String $index
     * @return bool
     */
    function validate_token($token)
    {
        try {
            $client = DB::connection('medicadepot')->table('Clientes')->select('token')->where('token', $token)->first();
            return !is_null($client);
        } catch (Exception $e) {
            return false;
        }


    }
}
