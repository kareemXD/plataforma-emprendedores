<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Emprendedor extends Model
{
    protected $connection = 'medicadepot';
    protected $table = 'zAE_Emprendedores';
    public $timestamps = false;
}
