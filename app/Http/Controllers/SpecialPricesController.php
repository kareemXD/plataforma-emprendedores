<?php

namespace App\Http\Controllers;

use Validator;
use App\Banner;
use App\CostConcept;
use App\CostoEspecial;
use App\CalculatedCostBase;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Http\Requests\CostoEspecialRequest;
use Yajra\DataTables\Facades\DataTables as DT;

class SpecialPricesController extends Controller
{
    /**
     *
     * Returns special prices index view or special prices data table based on request type
     * @author Giovanny González <giovanny.gonzalez@nuvem.mx>
     * created 16/03/2021
     * @params json | view
     * @return
     */
    public function index()
    {
        if (request()->ajax() or request()->expectsJson())
            return CostoEspecial::getDataTable();

        return view('special_prices.index');
    }

    /**
     * Show the form for creating a new resource.
     * @author daniel.cruz@losfra.dev
     * @created 2020-12-30
     * @updated 09/03/2021 Giovanny González <giovanny.gonzalez@nuvem.mx>
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $prices = DB::connection('medicadepot')->table('ReglasCostosEspeciales')
        ->where('Estatus', 1)->get(['Id_ReglaCostoEspecial', 'Descripcion']);
        $families = DB::connection('medicadepot')->table('Familias')->select('Id_Familia', 'Familia')->where('Activo','=',1)->get();
        $brands = DB::connection('medicadepot')->table('Marcas')->select('Id_Marca', 'Marca')->where('Activo','=',1)->get();
        $especialities = DB::connection('medicadepot')->table('Especialidades_Medicas')->select('Id_EspMed', 'Especialidad')->where('Activo','=',1)->get();
        $categories = DB::connection('medicadepot')->table('Categorias')->select('Id_Categoria', 'Categoria')->where('Activo','=',1)->get();
        $conceptos_costos = CostConcept::selectOptions()->get();
        $bases_calculo = CalculatedCostBase::get(['Descripcion', 'Id_BaseCostoCalculado']);
        $bases_calculo_pluck = CalculatedCostBase::pluck('Descripcion', 'Id_BaseCostoCalculado');

        return view('special_prices.create')->with([
            'prices' => $prices,
            'families' => $families,
            'brands' => $brands,
            'especialities' => $especialities,
            'categories' => $categories,
            'conceptos_costos' => $conceptos_costos,
            'bases_calculo' => $bases_calculo,
            'bases_calculo_pluck' => $bases_calculo_pluck
        ]);

    }

    /**
     * search the products in the database.
     * @author daniel.cruz@losfra.dev
     * @created 2020-12-30
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function searchProducts(Request $request)
    {
        if (request()->ajax() or request()->expectsJson()) {

            $products_query = DB::connection('medicadepot')->table('Articulos')->select('Id_Prod1', 'Descrip1')->where('Activo','=',1);
            if ($request->family != 0) {
                $products_query->where('Id_Familia','=',$request->family);
            }
            if ($request->brand != 0) {
                $products_query->where('Id_Marca','=',$request->brand);
            }
            if ($request->especiality != 0) {
                $products_query->where('Id_EspMed','=',$request->especiality);
            }
            if ($request->category != 0) {
                $products_query->where('Id_Categoria','=',$request->category);
            }
            $products = $products_query->get();

            return DT::of($products)
            ->addColumn('check', function ($product) {
                return view('special_prices.partials.checkbox', ['product' => $product]);
            })
            ->rawColumns(['actions'])
            ->make(true);
        }

    }

    /**
     *
     * Stores a new special prices config
     * @author Giovanny González <giovanny.gonzalez@nuvem.mx>
     * created 12/03/2021
     * @params App\Http\Requests\CostoEspecialRequest
     * @return \Illuminate\Http\Response
     */
    public function store(CostoEspecialRequest $request)
    {
        try {
            $costo_especial = new CostoEspecial();
            $costo_especial->store($request);
            $request->session()->flash('message', 'Costo especial creado correctamente.');
            return response([], 200);
        } catch (\Exception $e) {
            Log::error($e);
            return response(['message' => $e->getMessage()], 500);
        }
    }

    /**
     *
     * Shows a special price
     * @author Giovanny González <giovanny.gonzalez@nuvem.mx>
     * created 16/03/2021
     * @params
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        try {
            $costos_especiales = CostoEspecial::find(request()->Id_CostoEspecial)
            ->reglas_especiales_productos()->select('Descripcion',
            DB::raw("CASE Tipo WHEN 'P' THEN 'Porcentaje' WHEN 'M' THEN 'Monto' WHEN 'C' THEN 'Calculado' ELSE '404' END as Tipo"));
            return DT::of($costos_especiales)->make(true);
            return response(compact('costos_especiales'), 200);
        } catch (\Exception $e) {
            return response([], 500);
        }
    }

    /**
     *
     * Changes a CostoEspecial Estatus to zero
     * @author Giovanny González <giovanny.gonzalez@nuvem.mx>
     * created 16/03/2021
     * @params
     * @return void
     */
    public function destroy()
    {
        CostoEspecial::find(request()->Id_CostoEspecial)->update([
            'Estatus' => 0
        ]);
    }

    /**
     *
     * Deletes a special cost
     * @author Giovanny González <giovanny.gonzalez@nuvem.mx>
     * created 16/03/2021
     * @params
     * @return void
     */
    public function delete()
    {
        DB::transaction(function() {
            $costoEspecial = CostoEspecial::find(request()->Id_CostoEspecial);
            foreach($costoEspecial->reglas_especiales_productos as $regla) {
                $regla->productos()->delete();
                $regla->delete();
            }
            $costoEspecial->delete();
        });
    }
}
