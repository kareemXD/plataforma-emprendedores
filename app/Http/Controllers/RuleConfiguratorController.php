<?php

namespace App\Http\Controllers;

use App\RuleSpecialCost;
use Illuminate\Http\Request;
use App\Http\Requests\RuleSpecialCostRequest;

class RuleConfiguratorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @author Benito Huerta <benito.huerta@nuvem.mx>
     * @created 2020/02/16
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(request()->ajax())
            return RuleSpecialCost::datatable();

        return view('rules_configurator.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @author Benito Huerta <benito.huerta@nuvem.mx>
     * @created 2020/02/16
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('rules_configurator.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @author Benito Huerta <benito.huerta@nuvem.mx>
     * @created 2020/02/16
     * @param  \App\Http\Requests\RuleSpecialCostRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RuleSpecialCostRequest $request)
    {
        RuleSpecialCost::create($request->all());

        return redirect()->route('rules-configurator.index')->with(['message' => 'Regla guardada correctamente']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  RuleSpecialCost $rule
     * @return \Illuminate\Http\Response
     * @author Benito Huerta <benito.huerta@nuvem.mx>
     * @created 2020/02/16
     */
    public function edit(RuleSpecialCost $rule)
    {
        return view('rules_configurator.edit', compact('rule'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\RuleSpecialCostRequest  $request
     * @param  RuleSpecialCost $rule
     * @return \Illuminate\Http\Response
     * @author Benito Huerta <benito.huerta@nuvem.mx>
     * @created 2020/02/16
     */
    public function update(RuleSpecialCostRequest $request, RuleSpecialCost $rule)
    {
        $rule->update($request->all());
        $rule->refresh();

        return redirect()->route('rules-configurator.index')->with(['message' => 'Regla actualizada correctamente']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  RuleSpecialCost $rule
     * @return \Illuminate\Http\Response
     * @author Benito Huerta <benito.huerta@nuvem.mx>
     * @created 2020/02/16
     */
    public function destroy(RuleSpecialCost $rule)
    {
        $rule->toggleStatus();
        return response()->json(['rule' => $rule->refresh()]);
    }
}
