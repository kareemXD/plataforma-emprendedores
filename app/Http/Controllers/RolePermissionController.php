<?php

namespace App\Http\Controllers;

use App\Role;
use App\Permission;

class RolePermissionController extends Controller
{
    /**
     * Gets the role related permissions.
     * @author Octavio Cornejo
     * @created 2020-04-22
     * @param Role $role
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Role $role)
    {
        return response()->json($role->perms);
    }

    /**
     * Assigns permissions to given role.
     * @author Octavio Cornejo
     * @created 2020-04-22
     * @param Role $role
     * @param Permission $permission
     * @return \Illuminate\Http\JsonResponse
     */
    public function assign(Role $role, Permission $permission)
    {
        $ids = array_map('intval', request('perms'));
        $role->perms()->attach($ids);
        return response()->json('assign');
    }

    /**
     * Detaches a permission from a given role.
     * @author Octavio Cornejo
     * @created 2020-04-22
     * @param Role $role
     * @param Permission $permission
     * @return \Illuminate\Http\JsonResponse
     */
    public function unassign(Role $role, Permission $permission)
    {
        $ids = array_map('intval', request('perms'));
        $role->perms()->detach($ids);
        return response()->json('unasigned');
    }
}
