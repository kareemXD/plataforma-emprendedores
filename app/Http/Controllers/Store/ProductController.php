<?php

namespace App\Http\Controllers\Store;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = DB::connection('medicadepot')
            ->table('vArticulosCosEco')->where('ID_PROD1', $id)->first();
        return view('store.product', ['product' => $product]);

    }

}
