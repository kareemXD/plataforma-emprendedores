<?php

namespace App\Http\Controllers\Store;

use App\Banner;
use App\Article;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @updated 2020/12/23 Noe Reyes <noe.reyes@nuvemtecnologia.mx>
     * @param
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $articles = Article::active()->get();
        $banners = Banner::active()->get();
        $products = DB::connection('medicadepot')
            ->table('vArticulosCosEco')->take(8)->get();

        return view('store.home')->with(
            [
                'banners' => $banners,
                'products' => $products,
                'articles' => $articles
            ]
        );
    }
}
