<?php

namespace App\Http\Controllers;

use App\CostConcept;
use App\CalculatedCostBase;
use Illuminate\Http\Request;
use App\Http\Requests\CostConceptRequest;

class GlobalCostConfiguratorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @author Benito Huerta <benito.huerta@nuvem.mx>
     * @created 2020/02/17
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(request()->ajax())
            return CostConcept::datatable();

        $bases = CalculatedCostBase::pluck('Descripcion', 'Id_BaseCostoCalculado');
        return view('global_costs_configurator.index', compact('bases'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @author Benito Huerta <benito.huerta@nuvem.mx>
     * @created 2020/02/17
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $bases = CalculatedCostBase::pluck('Descripcion', 'Id_BaseCostoCalculado');
        return view('global_costs_configurator.create', compact('bases'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @author Benito Huerta <benito.huerta@nuvem.mx>
     * @created 2020/02/17
     * @param  \App\Http\Requests\CostConceptRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(CostConceptRequest $request)
    {
        $datos = $request->all();
        $datos['Prioridad'] = 0;

        CostConcept::create($datos);

        return redirect()->route('global-costs-configurator.index')->with(['message' => 'Costo guardado correctamente']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @author Benito Huerta <benito.huerta@nuvem.mx>
     * @created 2020/02/17
     * @param  CostConcept $cost
     * @return \Illuminate\Http\Response
     */
    public function edit(CostConcept $cost)
    {   
        $bases = CalculatedCostBase::pluck('Descripcion', 'Id_BaseCostoCalculado');
        return view('global_costs_configurator.edit', compact('cost', 'bases'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @author Benito Huerta <benito.huerta@nuvem.mx>
     * @created 2020/02/17
     * @param  \App\Http\Requests\CostConceptCostConceptRequest $request
     * @param  CostConcept $cost
     * @return \Illuminate\Http\Response
     */
    public function update(CostConceptRequest $request, CostConcept $cost)
    {
        $data = CostConcept::getAttributesToUpdate($request);
        $cost->update($data);
        $cost->refresh();

        return redirect()->route('global-costs-configurator.index')->with(['message' => 'Costo actualizado correctamente']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @author Benito Huerta <benito.huerta@nuvem.mx>
     * @created 2020/02/17
     * @param  CostConcept $cost
     * @return \Illuminate\Http\Response
     */
    public function destroy(CostConcept $cost)
    {
        $cost->toggleStatus();
        return response()->json(['cost' => $cost->refresh()]);
    }
}
