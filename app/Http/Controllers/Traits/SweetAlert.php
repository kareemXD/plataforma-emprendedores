<?php

namespace App\Http\Controllers\Traits;

trait SweetAlert {

    /**
     * Return an array with a Sweet Alert success type data.
     *
     * @author Luis Peña <luis.pena@nuvem.mx> | 09/10/2020
     * @params String $title, String $text, Float $timer
     * @return array
     */
    public static function success($title, $text, $timer = 3000)
    {
        return [
            'title' => $title,
            'text' => $text,
            'type' => 'success',
            'timer' => $timer
        ];
    }

    /**
     * Return an array with a Sweet Alert warning type data.
     *
     * @author Luis Peña <luis.pena@nuvem.mx> | 09/10/2020
     * @params String $title, String $text, Float $timer
     * @return array
     */
    public static function warning($title, $text, $timer = 3000)
    {
        return [
            'title' => $title,
            'text' => $text,
            'type' => 'warning',
            'timer' => $timer
        ];
    }

    /**
     * Return an array with a Sweet Alert error type data.
     *
     * @author Luis Peña <luis.pena@nuvem.mx> | 09/10/2020
     * @params String $title, String $text, Float $timer
     * @return array
     */
    public static function error($title, $text, $timer = 3000)
    {
        return [
            'title' => $title,
            'text' => $text,
            'type' => 'error',
            'timer' => $timer
        ];
    }

    /**
     * Return an array with a Sweet Alert info type data.
     *
     * @author Luis Peña <luis.pena@nuvem.mx> | 09/10/2020
     * @params String $title, String $text, Float $timer
     * @return array
     */
    public static function info($title, $text, $timer = 3000)
    {
        return [
            'title' => $title,
            'text' => $text,
            'type' => 'info',
            'timer' => $timer
        ];
    }

    /**
     * Return an array with a Sweet Alert data.
     *
     * @author Luis Peña <luis.pena@nuvem.mx> | 09/10/2020
     * @params String $title, String $text, Float $timer
     * @return array
     */
    public static function message($title, $text, $type = 'success', $timer = 3000)
    {
        return [
            'title' => $title,
            'text' => $text,
            'type' => $type,
            'timer' => $timer
        ];
    }

    /**
     * Return an array with a Sweet Alert html data.
     *
     * @author Luis Peña <luis.pena@nuvem.mx> | 09/10/2020
     * @params String $title, String $html
     * @return array
     */
    public static function html($title, $html)
    {
        return [
            'title' => $title,
            'html' => $html,
        ];
    }
}
