<?php

namespace App\Http\Controllers;

use App\Configuration;
use Illuminate\Http\Request;

class ConfigurationController extends Controller
{
    public $configuration;

    /**
     * ConfigurationController constructor.
     */
    public function __construct()
    {
        $this->configuration = Configuration::firstOrCreate(['id' => 1]);
    }

    /**
     * @author Luis Pena  <luis.pena@nuvem.mx> | 05/11/2020
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('configurations.index', [
            'configuration' => $this->configuration
        ]);
    }

    /**
     * @author Luis Pena  <luis.pena@nuvem.mx> | 05/11/2020
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request)
    {
        $requestData = $request->all();

        if ($request->hasFile('legal_notice_of_privacy')) {
            $requestData['legal_notice_of_privacy'] = Configuration::uploadFile($request->file('legal_notice_of_privacy'), 'legals', 'notice_of_privacy');
        }

        if ($request->hasFile('legal_terms_and_conditions')) {
            $requestData['legal_terms_and_conditions'] = Configuration::uploadFile($request->file('legal_terms_and_conditions'), 'legals', 'terms_and_conditions');
        }

        $this->configuration->update($requestData);
        return redirect('/configurations');
    }
}
