<?php

namespace App\Http\Controllers\Api\Article;

use App\Article;
use Illuminate\Http\Request;
use App\Traits\ApiResponserTrait;
use App\Http\Controllers\Controller;
use App\Http\Requests\ArticleRequest;

class ArticleController extends Controller
{
    use ApiResponserTrait;

    /**
    * show all blog
    * @author Luis Peña <luis.pena@nuvem.mx> | 08/10/2020
    * @return Illuminate\Http\JsonResponse
    */
    public function index()
    {
        return $this->showAll(Article::with('user')->orderBy('id', 'desc')->get());
    }

    /**
    * save blog
     * @author Luis Peña <luis.pena@nuvem.mx> | 08/10/2020
     * @param ArticleRequest $request
     * @return Illuminate\Http\JsonResponse
     */
    public function store(ArticleRequest $request)
    {
        try {
            $article = new Article;
            $article->fill($request->all());
            $article->user_id = auth()->user()->id;
            $article->icon = $article->saveIcon($request->file('icon'), 'article_file_'.$article->id, 'article_file');
            $article->save();

            return $this->showOne($article);
        } catch (\Exception $e) {
            return $this->errorResponse($e->getMessage(),404);
        }
    }

    /**
     * show especific blog
     * @author Luis Peña <luis.pena@nuvem.mx> | 09/10/2020
     * @param Article $article
     * @return Illuminate\Http\JsonResponse
     */
    public function show(Article $article)
    {
        try {
            return $this->showOne($article);
        } catch (\Exception $e) {
            return $this->errorResponse($e->getMessage(),404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @author Luis Peña <luis.pena@nuvem.mx> | 09/10/2020
     * @param  ArticleRequest $request
     * @param Article $article
     * @return Illuminate\Http\JsonResponse
     */
    public function update(ArticleRequest $request, Article $article)
    {
        try{
            $article->fill($request->all());
            if($request->exists('icon')){
               $article->icon =  $article->saveIcon($request->file('icon'), 'article_file_'.$article->id, 'article_file');
            }
            $article->save();

            return $this->showOne($article);
        } catch (\Exception $e) {
            return $this->errorResponse($e->getMessage(),404);
        }
    }

    /**
     * Remove the specified resource from storage.
      * @author Luis Peña <luis.pena@nuvem.mx> | 09/10/2020
      * @param Article $article
      * @return Illuminate\Http\JsonResponse
      */
    public function destroy(Article $article)
    {
        $status = $article->toggleStatus();
        return $this->showOne($article);
        return response()->json($msg);
    }
}
