<?php

namespace App\Http\Controllers\Api\ShippingConfiguration\Box;

use Illuminate\Http\Request;
use App\Models\MedicaDepot\Box;
use App\Http\Requests\BoxRequest;
use App\Traits\ApiResponserTrait;
use App\Repositories\BoxRepository;
use App\Http\Controllers\Controller;

class BoxController extends Controller
{
    use ApiResponserTrait;

    private $boxRepository;

    public function __construct(
        BoxRepository $boxRepository
    ){
        $this->boxRepository = $boxRepository;
    }
    
    /**
     * Display a listing of the boxes.
     * @author Kareem Lorenzana <kareem.lorenzana@nuvem.mx>
     * @params
     * @date 2021-04-19
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $boxes = $this->boxRepository->withEstatusFilter();
        return response()->json(compact('boxes'));
    }

    /**
     * Store a new created box
     * @author Kareem Lorenzana <kareem.lorenzana@nuvem.mx>
     * @date 2021-04-16
     * @params
     * @return \Illuminate\Http\Response
     */
    public function store(BoxRequest $request)
    {
        $box = $this->boxRepository->storeUpdateBox($request, new Box());
        if($box instanceof Box){
            return $this->successResponse($box, 201);
        }elseif($box){
            return $this->errorResponse($box,500);
        }
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(BoxRequest $request, Box $box)
    {
        $box = $this->boxRepository->storeUpdateBox($request, $box);
        if($box instanceof Box){
            return $this->successResponse($box, 201);
        } else {
            return $this->errorResponse($box,404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Box $box)
    {
        $box->toggleStatus();
        return response()->json(['box' => $box->refresh()]);
    }
}
