<?php

namespace App\Http\Controllers\Api\ShippingConfiguration;

use App\CostoEnvioTipo;
use Illuminate\Http\Request;
use App\ShippingConfiguration;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use App\Http\Requests\ShippingConfigurationRequest;

class ShippingConfigurationController extends Controller
{
    /**
     * Display a listing of the resource.
     * @author Giovanny González <giovanny.gonzalez@nuvem.mx>
     * @params
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $shippingConfigurations = ShippingConfiguration::dataTableEntries()->get();
        $tiposCostosEnvio = CostoEnvioTipo::get(['Id_CostoEnvioTipo as value', 'Tipo as name']);
        return response(compact('shippingConfigurations', 'tiposCostosEnvio'));
    }

    /**
     * Store a newly created resource in storage.
     * @author Giovanny González <giovanny.gonzalez@nuvem.mx>
     * @param  App\Http\Requests\ShippingConfigurationRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ShippingConfigurationRequest $request)
    {
        try {
            $model = new ShippingConfiguration();
            $model->saveModel($request);
            $status = 200;
            return response(compact('model', 'status'), 200);
        } catch (\Exception $e) {
            return response([], 500);
        }
    }

    /**
     *
     * Updates a shipping configuration
     * @author Giovanny González <giovanny.gonzalez@nuvem.mx>
     * created 01/04/2021
     * @param  App\Http\Requests\ShippingConfigurationRequest  $request,  App\ShippingConfiguration $shippingConfiguration
     * @return \Illuminate\Http\Response
     */
    public function update(ShippingConfigurationRequest $request, ShippingConfiguration $shippingConfiguration)
    {
        $model = $shippingConfiguration;
        try {
            $model->saveModel($request);
            $status = 200;
            return response(compact('model', 'status'), 200);
        } catch (\Exception $e) {
            return response([], 500);
        }
    }

    /**
     *
     * Deletes a shipping configuration
     * @author Giovanny González <giovanny.gonzalez@nuvem.mx>
     * created 01/04/2021
     * @params App\ShippingConfiguration $shippingConfiguration
     * @return \Illuminate\Http\Response
     */
    public function destroy(ShippingConfiguration $shippingConfiguration)
    {
        try {
            $shippingConfiguration->delete();
            return response([], 200);
        } catch (\Exception $e) {
            Log::error($e);
            return response([], 500);
        }
    }
}
