<?php

namespace App\Http\Controllers\Api;

use App\Cliente;
use App\Emprendedor;
use App\CreditRequest;
use Illuminate\Http\Request;
use App\Traits\ApiResponserTrait;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\CreditRequestsExport;
use App\SolicitudTransaccionEmprendedor;
use App\Http\Requests\CreditRequestRequest;


class CreditRequestController extends Controller
{
  use ApiResponserTrait;

  public function __construct()
  {
    $this->middleware('permission:CreditRequest.*')->only('index');
  }
  /**
   * Credit requests list
   *
   * @author Giovanny González <giovanny.gonzalez@nuvem.mx>
   * created 20/05/2021
   * @params
   * @return Illuminate\Http\JsonResponse
   *
   */
    public function index()
    {
      try {       
        $list = SolicitudTransaccionEmprendedor::dataTable()->get();
        return $this->successResponse(compact('list'), 200);
      } catch (\Exception $e) {
        Log::error($e);
        return $this->errorResponse($e->getMessage(), 500);
      }
    }
  /**
   * Enterprising's credit requests list
   *
   * @author Giovanny González <giovanny.gonzalez@nuvem.mx>
   * created 20/05/2021
   * @params App\Cliente $enterprising
   * @return Illuminate\Http\JsonResponse
   *
   */
    public function enterprisingRequests(Cliente $enterprising)
    {
      try {       
        $list = $enterprising->creditRequestsHistory();
        return $this->successResponse(compact('list'), 200);
      } catch (\Exception $e) {
        Log::error($e);
        return $this->errorResponse($e->getMessage(), 500);
      }
    }
  /**
   * Enterprising's credit requests export
   *
   * @author Giovanny González <giovanny.gonzalez@nuvem.mx>
   * created 20/05/2021
   * @params App\Cliente $enterprising
   * @return Illuminate\Http\JsonResponse
   *
   */
    public function enterprisingRequestsExport(Cliente $enterprising)
    {
      try {       
        $list = $enterprising->creditRequestsHistory();
        return Excel::download(new CreditRequestsExport($enterprising->creditRequestsHistory()), 'Historial de Crédito.xlsx');
      } catch (\Exception $e) {
        Log::error($e);
      }
    }
}
