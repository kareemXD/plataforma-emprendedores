<?php

namespace App\Http\Controllers\Api\Sanctum;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SanctumController extends Controller
{
    /**
     * Get user authenticated info.
     * @auth Octavio Cornejo
     * @date 2021-03-02
     * @param param
     * @return void
     */
    public function show(Request $request)
    {
        $user = $request->user();
        $user->assignedPermissions = $user->getAllPermissions()->pluck('name');
        $user->menu = $user->getMenu();
        return $user;
    }
}
