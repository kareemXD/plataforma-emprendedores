<?php

namespace App\Http\Controllers\api;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\MedicaDepot\UserNego;
use App\Http\Requests\LoginNegoRequest;

class AuthNegoController extends Controller
{    
    /**
     * Validates given information to login, and if is authenticated assigns an access token
     * and return it with the user information.
     * @author Jose Vega
     * @created 2020-05-19
     * @param LoginNegoRequest $request
     * @return Response
     */
    public function userExists(LoginNegoRequest $request)
    {
        $user = new UserNego();
        $userData = $user->existInDb($request);
        return response()->json(compact('userData'));
    }

    /**
     * If is user nego then update the local user data
     * @author Jose Vega
     * @created 2020-05-27
     * @param Request $request
     * @return Response
     */
    public function isUserNego(Request $request)
    {
        $user = new UserNego();
        $user->updateUserNegoData($request);
        return response(['message' => 'Updated successfully']);
    }
}
