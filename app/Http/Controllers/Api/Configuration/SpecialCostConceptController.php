<?php

namespace App\Http\Controllers\Api\Configuration;

use App\CostConcept;
use Illuminate\Http\Request;
use App\Models\MedicaDepot\Group;
use App\Traits\ApiResponserTrait;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use App\Models\MedicaDepot\SpecialCostConcept;
use App\Http\Requests\Configuration\SpecialCostConceptRequest;

class SpecialCostConceptController extends Controller
{
    use ApiResponserTrait;

    /**
     * Display a listing of the resource.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-05-03
     * @param
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $specialCostConcepts = SpecialCostConcept::active()->with('costConcept')->get();
        return $this->successResponse($specialCostConcepts, 200);
    }

    /**
     * Store a newly created resource in storage.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-05-03
     * @param  \Illuminate\Http\SpecialCostConceptRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(SpecialCostConceptRequest $request)
    {
        try {
            $specialCostConcept = SpecialCostConcept::create($request->all());
            $costGlobalConcept = CostConcept::find($request->ConceptosCostos_Id);
            $specialCostConcept->costConcept()->associate($costGlobalConcept)->save();
            $group = Group::find($request->Grupo_Id);
            $group->specialCostConcepts()->save($specialCostConcept);
            return $this->successResponse($specialCostConcept, 200);
        } catch (\Exception $e) {
            Log::error($e);
            return  $this->errorResponse([$e], 500);
        }
    }

    /**
     * Update the specified resource in storage.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-05-03
     * @param  \Illuminate\Http\SpecialCostConceptRequest  $request
     * @param  SpecialCostConcept  $specialCostConcept
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(SpecialCostConceptRequest $request, SpecialCostConcept $specialCostConcept)
    {
        try {
            $specialCostConcept->update($request->all());
            return response(['message' => 'Update successfully']);
        } catch (\Exception $e) {
            Log::error($e);
            return  $this->errorResponse([$e], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-05-03
     * @param  SpecialCostConcept  $specialCostConcept
     * @return \Illuminate\Http\Response
     */
    public function destroy(SpecialCostConcept $specialCostConcept)
    {
        $specialCostConcept->toggleStatus();
        return response(['message' => 'Deleted successfully']);
    }
}
