<?php

namespace App\Http\Controllers\Api\Configuration;

use Illuminate\Http\Request;
use App\Models\MedicaDepot\Group;
use App\Traits\ApiResponserTrait;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use App\Http\Requests\Configuration\GroupRequest;

class GroupController extends Controller
{
    use ApiResponserTrait;
    
    /**
     * UserController constructor.
     */
    public function __construct()
    {
        //$this->middleware('permission:SpecialCostConfiguration.*|SpecialCostConfiguration.view|SpecialCostConfiguration.create|SpecialCostConfiguration.edit|SpecialCostConfiguration.delete')->only('index');
        $this->middleware('permission:SpecialCostConfiguration.create')->only('store');
        $this->middleware('permission:SpecialCostConfiguration.edit')->only('update');
        $this->middleware('permission:SpecialCostConfiguration.delete')->only('destroy');
    }

    /**
     * Display a listing of the resource.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-05-05
     * @param
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $groups = Group::active()->get();
        return $this->successResponse($groups, 200);
    }

    /**
     * Store a newly created resource in storage.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-05-03
     * @param  \Illuminate\Http\GroupRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(GroupRequest $request)
    {
        try {
            $newGroup = Group::create($request->all());
            return $this->successResponse($newGroup, 200);
        } catch (\Exception $e) {
            Log::error($e);
            return response([$e], 500);
        }
    }

    /**
     * Update the specified resource in storage.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-05-05
     * @param  \Illuminate\Http\GroupRequest  $request
     * @param  Group  $group
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(GroupRequest $request, Group $group)
    {
        try {
            $group->update($request->all());
            return $this->successResponse('Updated successfully', 200);
        } catch (\Exception $e) {
            Log::error($e);
            return  $this->errorResponse([$e], 500);
        }
    }

    /**
     * Gets cost data from given id.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-04-12
     * @param Group  $group
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Group $group)
    {   
        return $this->successResponse($group, 200);
    }

    /**
     * Remove the specified resource from storage.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-05-05
     * @param  Group $group
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Group $group)
    {
        $group->toggleStatus();
        return $this->successResponse('Deleted successfully', 200);
    }

    /**
     * Calls a method to relate a group of products (items) to a group.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-05-04
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addProductsToGroup(Request $request)
    {
        try {
            $group = Group::find($request->groupId);
            $group->addProducts($request);
            return $this->successResponse('Created successfully', 200);
        } catch (\Exception $e) {
            Log::error($e);
            return  $this->errorResponse([$e], 500);
        }
    }

    /**
     * Get array of special costs concepts relate to a group.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-05-04
     * @param  Group $group
     * @return \Illuminate\Http\JsonResponse
     */
    public function getProductsFromGroup(Group $group)
    {   
        return $this->successResponse($group->specialCostConcepts->where('Estatus', '=', '1'), 200);
    }

    /**
     * Return products to edit.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-04-13
     * @param  Group $group
     * @return \Illuminate\Http\JsonResponse
     */
    public function getProductsToEdit(Group $group){
        $productsToEdit = $group->products()->get(['Id_Prod1', 'Descrip1']);
        return $this->successResponse($productsToEdit, 200);
    }

    /**
     * Detach products of a group.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-05-05
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Http\Group  $group
     * @return \Illuminate\Http\JsonResponse
     */
    public function detachProducts(Request $request, Group $group){        
        try {
            $group->detachProducts($request);
            return $this->successResponse('Deleted successfully', 200);
        } catch (\Exception $e) {
            Log::error($e);
            return  $this->errorResponse([$e], 500);
        }
    }
}
