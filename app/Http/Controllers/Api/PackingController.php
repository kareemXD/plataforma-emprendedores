<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Traits\ApiResponserTrait;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use App\Traits\EnterprisingHttpClientTrait;
use GuzzleHttp\Exception\BadResponseException;

class PackingController extends Controller
{
    use ApiResponserTrait, EnterprisingHttpClientTrait;
    
    /**
     * Packing orders
     *
     * @author José Vega <jose.vega@nuvem.mx>
     * created 24/06/2021
     * @params
     * @return Illuminate\Http\JsonResponse
     *
     */
    public function index()
    {
        try {
          $data = $this->enterprisingRequest('GET', '/packing', true);
          return $this->successResponse($data, 200);
        } catch (BadResponseException $e) {
          $responseBody = json_decode($e->getResponse()->getBody());
          $errorMessage = $responseBody ? $responseBody->error : '';
          return $this->errorResponse($errorMessage, $e->getResponse()->getStatusCode());
        } catch (\Exception $e) {
          Log::error($e);
          return $this->errorResponse($e->getMessage(), 500);
        }
    }
}
