<?php

namespace App\Http\Controllers\Api\User;

use App\User;
use App\Mail\UsuarioNuevo;
use App\Http\Requests\UserRequest;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Api\ApiController;

class UserController extends ApiController
{
    /**
     * UserController constructor.
     */
    public function __construct()
    {
        //$this->middleware('permission:Usuarios.view|Usuarios.create|Usuarios.edit|Usuarios.delete|Usuarios.*')->only('index');
        //$this->middleware('permission:Usuarios.create')->only('store');
        $this->middleware('permission:Usuarios.edit')->only('update');
        $this->middleware('permission:Usuarios.delete')->only('destroy');
    }

    /**
     * Shows all users registered.
     * @author Octavio Cornejo
     * @created 2020-04-30
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $users = User::with('roles')->filters()->active()->get();

        return $this->showAll($users, 200);
    }

    /**
     * Stores a new user with given data.
     * @author Octavio Cornejo
     * @created 2020-04-30
     * @param UserRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(UserRequest $request)
    {
        $user = User::create($request->all());
        if($request->id_user_nego){
            $user->password = $request->password; 
            $user->id_user_nego = $request->id_user_nego;
            $user->save();
        }
        $user->syncRoles($request->roles);

        Mail::to($user->email)->send(new UsuarioNuevo($user));

        return $this->showOne($user);
    }

    /**
     * Gets user data from given id.
     * @author Octavio Cornejo
     * @created 2020-05-01
     * @param User $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(User $user)
    {
        return $this->showOne($user);
    }

    /**
     * If the data has been modified, updates the user's data.
     * @author Octavio Cornejo
     * @created 2020-04-30
     * @param UserRequest $request
     * @param User $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UserRequest $request, User $user)
    {
        $user->update($request->all());
        $user->assignRoles($request->roles);

        return $this->showOne($user);
    }

    /**
     * Toggles user status.
     * If status is 'active' then changes to 'inactive' and vice versa.
     * @author Octavio Cornejo
     * @created 2020-04-30
     * @param User $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(User $user)
    {
        $user->toggleStatus();
        return $this->showOne($user);
    }
}
