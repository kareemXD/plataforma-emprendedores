<?php

namespace App\Http\Controllers\Api;

use App\Traits\ApiResponserTrait;
use App\Http\Controllers\Controller;

class ApiController extends Controller
{
    use ApiResponserTrait;

    public function __construct(){}
}
