<?php

namespace App\Http\Controllers\Api;

use App\Pedido;
use Illuminate\Http\Request;
use App\Traits\ApiResponserTrait;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use App\Http\Requests\PickingRequest;
use App\Traits\EnterprisingHttpClientTrait;
use GuzzleHttp\Exception\BadResponseException;


class PickingController extends Controller
{
    use ApiResponserTrait, EnterprisingHttpClientTrait;

  /**
   * Picking orders
   *
   * @author Giovanny González <giovanny.gonzalez@nuvem.mx>
   * created 08/06/2021
   * @params
   * @return Illuminate\Http\JsonResponse
   *
   */
    public function index()
    {
      try {
        $data = $this->enterprisingRequest('GET', '/picking', true);
        return $this->successResponse($data, 200);
      } catch (BadResponseException $e) {
        $responseBody = json_decode($e->getResponse()->getBody());
        $errorMessage = $responseBody ? $responseBody->error : '';
        return $this->errorResponse($errorMessage, $e->getResponse()->getStatusCode());
      } catch (\Exception $e) {
        Log::error($e);
        return $this->errorResponse($e->getMessage(), 500);
      }
    }
  /**
   * Picking order's details
   *
   * @author Giovanny González <giovanny.gonzalez@nuvem.mx>
   * created 08/06/2021
   * @params
   * @return Illuminate\Http\JsonResponse
   *
   */
    public function show($orderId)
    {
      try {
        $data = $this->enterprisingRequest('GET', "/picking/$orderId", true);
        return $this->successResponse($data, 200);
      } catch (BadResponseException $e) {
        $responseBody = json_decode($e->getResponse()->getBody());
        $errorMessage = $responseBody ? $responseBody->error : '';
        return $this->errorResponse($errorMessage, $e->getResponse()->getStatusCode());
      } catch (\Exception $e) {
        Log::error($e);
        return $this->errorResponse($e->getMessage(), 500);
      }
    }
  /**
   * Creates a new picking_product
   *
   * @author Giovanny González <giovanny.gonzalez@nuvem.mx>
   * created 22/06/2021
   * @params App\Http\Requests\PickingRequest $request
   * @return Illuminate\Http\JsonResponse
   *
   */
    public function store(PickingRequest $request)
    {
        try {
          $data = $this->enterprisingRequest('POST', "/picking", true, request()->all());
          return $this->successResponse($data, 200);
        } catch (BadResponseException $e) {
          $responseBody = json_decode($e->getResponse()->getBody());
          $errorMessage = $responseBody ? $responseBody->error : '';
          return $this->errorResponse($errorMessage, $e->getResponse()->getStatusCode());
        } catch (\Exception $e) {
          Log::error($e);
          return $this->errorResponse($e->getMessage(), 500);
        }
    }
  /**
   * register incomplete assortment reason on shopping cart product
   *
   * @author Giovanny González <giovanny.gonzalez@nuvem.mx>
   * created 22/06/2021
   * @params
   * @return Illuminate\Http\JsonResponse
   *
   */
    public function incompleteAssortment()
    {
        try {
          $data = $this->enterprisingRequest('PATCH', "/picking/incomplete-assortment-reason", true, request()->all());
          return $this->successResponse($data, 200);
        } catch (BadResponseException $e) {
          $responseBody = json_decode($e->getResponse()->getBody());
          $errorMessage = $responseBody ? $responseBody->error : '';
          return $this->errorResponse($errorMessage, $e->getResponse()->getStatusCode());
        } catch (\Exception $e) {
          Log::error($e);
          return $this->errorResponse($e->getMessage(), 500);
        }
    }
    /**
     * updates order status to packing
     *
     * @author Giovanny González <giovanny.gonzalez@nuvem.mx>
     * created 23/06/2021
     * @params number $onrderId
     * @return Illuminate\Http\JsonResponse
     *
     */
    public function turnOrderToPacking($orderId)
    {
        try {
          $data = $this->enterprisingRequest('PATCH', "/picking/turn-order-to-packing/$orderId", true);
          return $this->successResponse($data, 200);
        } catch (BadResponseException $e) {
          $responseBody = json_decode($e->getResponse()->getBody());
          $errorMessage = $responseBody ? $responseBody->error : '';
          return $this->errorResponse($errorMessage, $e->getResponse()->getStatusCode());
        } catch (\Exception $e) {
          Log::error($e);
          return $this->errorResponse($e->getMessage(), 500);
        }
    }
    /**
     * Products replaced and product's assortmenr marked as completed
     *
     * @author Giovanny González <giovanny.gonzalez@nuvem.mx>
     * created 23/06/2021
     * @params number $onrderId
     * @return Illuminate\Http\JsonResponse
     *
     */
    public function productsReplaced()
    {
        try {
          $data = $this->enterprisingRequest('PATCH', "/picking/products-replaced", true, request()->all());
          return $this->successResponse($data, 200);
        } catch (BadResponseException $e) {
          $responseBody = json_decode($e->getResponse()->getBody());
          $errorMessage = $responseBody ? $responseBody->error : '';
          return $this->errorResponse($errorMessage, $e->getResponse()->getStatusCode());
        } catch (\Exception $e) {
          Log::error($e);
          return $this->errorResponse($e->getMessage(), 500);
        }
    }
}
