<?php

namespace App\Http\Controllers\Api\MedicaDepot;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Api\ApiController;

class SufferingController extends ApiController
{
    /**
     * @author Fernando Montes de Oca
     * @created 2020-04-25
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $categories = DB::connection('medicadepot')
            ->table('Padecimientos')
            ->select([
                'id_padecim',
                'padecimiento',
                'sintomas',
            ])->get();
        return $this->showAll($categories, 200);
    }
}
