<?php

namespace App\Http\Controllers\Api\MedicaDepot;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Api\ApiController;


class SubstanceController extends ApiController
{
    /**
     * @author Fernando Montes de Oca
     * @created 2020-04-25
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $categories = DB::connection('medicadepot')
            ->table('Substancias')
            ->select([
                'id_subst',
                'substancia',
            ])->get();
        return $this->showAll($categories, 200);
    }
}
