<?php

namespace App\Http\Controllers\Api\MedicaDepot;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Api\ApiController;

class CategoryController extends ApiController
{
    /**
     * @author Fernando Montes de Oca
     * @created 2020-04-25
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $categories = DB::connection('medicadepot')
            ->table('Categorias')
            ->select([
                'id_categoria',
                'categoria',
                'ap_presenta',
                'ap_padecim',
                'ap_substanc',
                'ap_medicamento'
            ])->where('activo', true)->get();
        return $this->showAll($categories, 200);
    }
}
