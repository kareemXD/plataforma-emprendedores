<?php

namespace App\Http\Controllers\Api\Role;

use App\Role;
use App\Http\Requests\RoleRequest;
use App\Http\Controllers\Api\ApiController;

class RoleController extends ApiController
{
    /**
     * RoleController constructor.
     */
    public function __construct()
    {
        $this->middleware('permission:Roles.create')->only('store');
        $this->middleware('permission:Roles.edit')->only('update');
        $this->middleware('permission:Roles.delete')->only('destroy');
    }

    /**
     * Shows all roles registered.
     * @author Octavio Cornejo
     * @created 2020-04-30
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $roles = Role::active()->transform()->get();

        return $this->showAll($roles, 200);
    }

    /**
     * Stores a new role with given data.
     * @author Octavio Cornejo
     * @created 2020-04-30
     * @param RoleRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(RoleRequest $request)
    {
        $role = Role::create($request->all());

        return $this->showOne($role);
    }

    /**
     * Gets role data from given id.
     * @author Octavio Cornejo
     * @created 2020-05-01
     * @param Role $role
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Role $role)
    {
        return $this->showOne($role);
    }

    /**
     * If the data has been modified, updates the role's data.
     * @author Octavio Cornejo
     * @created 2020-04-30
     * @param RoleRequest $request
     * @param Role $role
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(RoleRequest $request, Role $role)
    {
        $role->fill($request->all()); //Before to update, it fills the object with the request data.
        if ($role->isClean()) { //Here mades a validation if the object has a difference from the original(about data).
            //If isn't different, doens't make sense to make an update. So, we inform that to role.
            return $this->errorResponse('Se debe especificar al menos un valor diferente para actualizar', 422);
        }
        //Otherwise, if is there a differnece, it is update.
        $role->save();
        return $this->showOne($role);
    }

    /**
     * Toggles role status.
     * If status is 'active' then changes to 'inactive' and vice versa.
     * @author Octavio Cornejo
     * @created 2020-04-30
     * @param Role $role
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Role $role)
    {
        $role->toggleStatus(); //changes the role status.
        return $this->showOne($role);
    }
}
