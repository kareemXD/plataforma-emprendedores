<?php

namespace App\Http\Controllers\Api\Role;

use App\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Api\ApiController;

class RolePermissionController extends ApiController
{
    /**
     * Gets permissions related to selected role.
     * @auth Octavio Cornejo
     * @date 2021-02-23
     * @param Role $role
     * @return JsonResponse
     */
    public function index(Role $role)
    {
        $permissions = $role->permissionsStatus();
        return response()->json(compact('permissions'));
    }

    /**
     * Assing/Unassign the permissions to selected role.
     * @auth Octavio Cornejo
     * @date 2021-02-24
     * @param Role $role
     * @param Request $request
     * @return JsonResponse
     */
    public function sync(Role $role, Request $request)
    {
        $role->assignPermissions($request->all());
        return response()->json(['message' => 'Permissions assigned']);
    }

}
