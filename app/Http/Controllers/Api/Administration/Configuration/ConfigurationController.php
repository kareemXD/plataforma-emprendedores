<?php

namespace App\Http\Controllers\Api\Administration\Configuration;

use Illuminate\Http\Request;
use App\Traits\ApiResponserTrait;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use App\Models\MedicaDepot\Configuration;
use App\Models\MedicaDepot\SkydropXConfiguration;
use App\Models\MedicaDepot\SkydropXPackageDeliveries;

class ConfigurationController extends Controller
{
    use ApiResponserTrait;

    /**
     * ConfigurationController constructor apply permissions.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-04-29
     * @param 
     * @return
     */
    public function __construct()
    {
        //$this->middleware('permission:Configurations.view')->only('index');
        $this->middleware('permission:Configurations.edit')->only('update');
    }

    /**
     * Display a listing of the resource.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-04-28
     * @updated 2021-06-11 Benito Huerta <benito.huerta@nuvem.mx> | The Package Deliveries are appended to configuration object
     * @param 
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $zAEConfiguracionSkydropxId = 1;

        $firstConfiguration = Configuration::first();
        $firstConfiguration != null ? $idFirst = $firstConfiguration->Id : $idFirst = 0; 
        $configuration = Configuration::firstOrCreate(
            ['Id' => $idFirst],
			['AmbienteActivo' => 'Pruebas',
			'zAE_ConfiguracionSkydropx_Id' => $zAEConfiguracionSkydropxId]
		);

        $packageDeliveries = SkydropXPackageDeliveries::all();
        $skydropXConfiguration = SkydropXConfiguration::withApprovedPackageDeliveries($zAEConfiguracionSkydropxId)
                                                        ->first();

        $configuration->packageDeliveries = $packageDeliveries;
        $configuration->approvedPackageDeliveries = $skydropXConfiguration->approvedPackageDeliveries;

        return $this->successResponse($configuration, 200);
    }

    /**
     * Update the specified resource in storage.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-04-28
     * @updated 2021-06-11 Benito Huerta <benito.huerta@nuvem.mx> | The Package Deliveries are sync to configuration object
     * @param  \Illuminate\Http\Request  $request
     * @param  Configuration  $configuration
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, Configuration $configuration)
    {
        try {

            $data = $configuration->mapOutData($request);
            $configuration->update($data);

            $skydropXConfiguration = SkydropXConfiguration::first();
            $skydropXConfiguration->approvedPackageDeliveries()->sync($request->approvedPackageDeliveries);

            return $this->successResponse($configuration, 200);

        } catch (\Exception $e) {
            Log::error($e);
            return  $this->errorResponse([$e->getMessage()], 500);
        }
    }
}
