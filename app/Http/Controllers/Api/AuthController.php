<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\LoginRequest;
use App\Http\Controllers\Controller;
use App\Traits\EnterprisingAdminPanelSessionTrait;

class AuthController extends Controller
{

    use EnterprisingAdminPanelSessionTrait;
    /**
     * Validates given information to login, and if is authenticated assigns an access token
     * and return it with the user information.
     * @author Octavio Cornejo
     * @created 2020-04-30
     * @param LoginRequest $request
     * @return Response
     */
    public function login(LoginRequest $request)
    {
        if (!auth()->attempt($request->all())) //Here validates the given information.
            return response(['message' => 'Invalid credentials.']);

        $accessToken = auth()->user()->createToken('authToken')->accessToken; //assigns the token to user.

        return response([
            'user' => \auth()->user(),
            'access_token' => $accessToken
        ]);
    }

    /**
     * Revokes user's token to logout.
     * @author Octavio Cornejo
     * @created 2020-05-01
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        request()->user()->token()->revoke();
        return response()->json(['message' => 'you have logged out.']);
    }

    /**
     * Gets the authenticated user data.
     * @author Octavio Cornejo
     * @created 2020-05-01
     * @return \Illuminate\Http\JsonResponse
     */
    public function myProfile()
    {
        return response()->json([
            'user' => \auth()->user()
        ]);
    }
}
