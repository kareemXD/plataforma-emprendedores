<?php

namespace App\Http\Controllers\Api\ProductConfiguration\ViewCostEntrepreneurial;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\Controller;
use App\Exports\EntrepreneurCostsExport;

class ViewCostEntrepreneurialController extends Controller
{

    /**
     * Method construct, verify the permissions from user for access to the functions.
     * @auth Iván Morales <ivan.morales@nuvem.mx>
     * @date 2021-04-28
     */
    public function __construct()
    {
        //$this->middleware('permission:ViewEntrepreneurialCost.view')->only('index');
    }

    /**
     * Display a listing of the resource.
     *
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-03-30
     * @params
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $costs = DB::connection('medicadepot')->select('SET NOCOUNT ON ; EXEC SP_Costos_Emp');
        return response()->json(compact('costs'));
    }

    /**
     * Display a listing of the resource.
     *
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-03-30
     * @params
     * @return Maatwebsite\Excel\Facades\Excel
     */
    public function exportCosts()
    {
        $data = DB::connection('medicadepot')->select('SET NOCOUNT ON ; EXEC SP_Costos_Emp');
        $headings = [];
        foreach ($data[0] as $key => $value) {
            array_push($headings, $key);
        }
        return Excel::download(new EntrepreneurCostsExport($headings, $data), 'costs.xlsx');
    }

    

}
