<?php

namespace App\Http\Controllers\Api\ProductConfiguration\Container;

use App\Container;
use Illuminate\Http\Request;
use App\Traits\ApiResponserTrait;
use App\Http\Controllers\Controller;
use App\Http\Requests\ContainerRequest;
use App\Repositories\ContainerRepository;

class ContainerController extends Controller
{
    use ApiResponserTrait;

    private $containerRepository;

    /**
     * Instance class for controller.
     * @auth Kareem Lorenzana <kareem.lorenzana@nuvem.mx>
     * @created 2021-05-28
     * @param ContainerRepository $containerRepository
     * @return void
     */
    public function __construct(
        ContainerRepository $containerRepository
    ){
        $this->containerRepository = $containerRepository;
    }

    /**
     * Display a listing of the resource.
     * @auth Kareem Lorenzana <kareem.lorenzana@nuvem.mx>
     * @date 2021-05-28
     * @param
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $containers = $this->containerRepository->getContainers();
        return response()->json(compact('containers'));
    }

    /**
     * save new container in database
     * @auth Kareem Lorenzana <kareem.lorenzana@nuvem.mx>
     * @date 2021-05-28
     * @param ContainerRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(ContainerRequest $request)
    {
        $container = $this->containerRepository->storeUpdateContainer($request, new Container());
        if($container instanceof Container){
            return $this->successResponse("Caja de picking registrada correctamente", 201);
        }elseif($container){
            return $this->errorResponse($container,500);
        }
    }


    /**
     * update a container in database
     * @auth Kareem Lorenzana <kareem.lorenzana@nuvem.mx>
     * @date 2021-05-28
     * @params ContainerRequest $request, Container $container
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(ContainerRequest $request, Container $container)
    {
        $container = $this->containerRepository->storeUpdateContainer($request, $container);
        if($container instanceof Container){
            return $this->successResponse("Caja de picking actualizada correctamente", 201);
        }elseif($container){
            return $this->errorResponse($container,500);
        }
    }

    /**
     * toggle status in database for delete method
     * @auth Kareem Lorenzana
     * @date 2021-05-28
     * @param Container $container
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Container $container)
    {
        $container->toggleStatus();
        return response()->json(['container' => $container->refresh()]);
    }
}
