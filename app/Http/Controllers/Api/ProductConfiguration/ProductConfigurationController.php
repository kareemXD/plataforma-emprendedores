<?php

namespace App\Http\Controllers\Api\ProductConfiguration;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class ProductConfigurationController extends Controller
{
    /**
     * Return a list of suppliers.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-03-26
     * @param
     * @return \Illuminate\Http\Response
     */
    public function listOfSuppliers()
    {
        $suppliers = DB::connection('medicadepot')->table('Proveedores')->select('Id_Prov')->get();
        return response()->json(compact('suppliers'));
    }
    
    /**
     * Return a list of categories.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-03-26
     * @param
     * @return \Illuminate\Http\Response
     */
    public function listOfCategories()
    {
        $categories = DB::connection('medicadepot')->table('Categorias')->select('Id_Categoria', 'Categoria')->get();
        return response()->json(compact('categories'));
    }

    /**
     * Return a list of families.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-03-29
     * @param
     * @return \Illuminate\Http\Response
     */
    public function listOfFamilies()
    {
        $families = DB::connection('medicadepot')->table('Familias')->select('Id_Familia', 'Familia')->get();
        return response()->json(compact('families'));
    }

    /**
     * Return a list of marks.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-03-29
     * @param
     * @return \Illuminate\Http\Response
     */
    public function listOfMarks()
    {
        $marks = DB::connection('medicadepot')->table('Marcas')->select('Id_Marca', 'Marca')->get();
        return response()->json(compact('marks'));
    }

    /**
     * Return a list of specialties.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-03-29
     * @param
     * @return \Illuminate\Http\Response
     */
    public function listOfSpecialties()
    {
        $specialties = DB::connection('medicadepot')->table('Especialidades_Medicas')->select('Id_EspMed', 'Especialidad')->get();
        return response()->json(compact('specialties'));
    }

    /**
     * Return a list of status.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-03-29
     * @param
     * @return \Illuminate\Http\Response
     */
    public function listOfStatus()
    {
        $status = DB::connection('medicadepot')->table('Estatus_Prod')->select('Id_Estatus', 'Descripcion')->get();
        return response()->json(compact('status'));
    }

    /**
     * Return a list of countries.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-03-29
     * @param
     * @return \Illuminate\Http\Response
     */
    public function listOfCountries()
    {
        $countries = DB::connection('medicadepot')->table('Paises')->select('Id_Pais', 'Pais')->get();
        return response()->json(compact('countries'));
    }

    /**
     * Return a list of packaging.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-03-29
     * @param
     * @return \Illuminate\Http\Response
     */
    public function listOfPackaging()
    {
        $packaging = DB::connection('medicadepot')->table('Empaques')->select('Id_Empaque', 'Empaque')->get();
        return response()->json(compact('packaging'));
    }
}
