<?php

namespace App\Http\Controllers\Api\ProductConfiguration\ShipmentCostConfiguration;

use App\ShipmentCost;
use Illuminate\Http\Request;
use App\Traits\ApiResponserTrait;
use App\Http\Controllers\Controller;
use App\Http\Requests\ShipmentCostRequest;
use App\Repositories\ShipmentCostConfigurationRepository;


class ShipmentCostConfigurationController extends Controller
{

    use ApiResponserTrait;

    private $shipmentCostConfigurationRepository;

     /**
     * Instance class for controller.
     * @auth Kareem Lorenzana <kareem.lorenzana@nuvem.mx>
     * @created 2021-04-28
     * @params ShipmentCostConfigurationRepository $shipmentCostConfigurationRepository
     * @return void
     */
    public function __construct(
        ShipmentCostConfigurationRepository $shipmentCostConfigurationRepository
    ){
        $this->shipmentCostConfigurationRepository = $shipmentCostConfigurationRepository;
    }
    
    /**
     * Display a listing of the resource.
     * @auth Kareem Lorenzana <kareem.lorenzana@nuvem.mx>
     * @created 2021-04-28
     * @params 
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $shipmentCosts = $this->shipmentCostConfigurationRepository->getActiveShipmentCost();
        return response()->json(compact('shipmentCosts'));
    }

    /**
     * store new shipment cost configuration
     * @auth Kareem Lorenzana <kareem.lorenzana@nuvem.mx>
     * @created 2021-04-28
     * @param  ShipmentCostRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(ShipmentCostRequest $request)
    {
        $shipmentCost = $this->shipmentCostConfigurationRepository->storeUpdateShipmentCost($request, new ShipmentCost());
        if($shipmentCost instanceof ShipmentCost){
            return $this->successResponse($shipmentCost, 201);
        }elseif($shipmentCost){
            return $this->errorResponse($shipmentCost,500);
        }
    }

     /**
     * update shipment cost configuration
     * @auth Kareem Lorenzana <kareem.lorenzana@nuvem.mx>
     * @created 2021-04-28
     * @param  ShipmentCostRequest $request
     * @param  ShipmentCost $shipmentCostConfiguration
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(ShipmentCostRequest $request, ShipmentCost $shipmentCostConfiguration)
    {
        $shipmentCost = $this->shipmentCostConfigurationRepository->storeUpdateShipmentCost($request, $shipmentCostConfiguration);
        if($shipmentCost instanceof ShipmentCost){
            return $this->successResponse($shipmentCost, 201);
        }elseif($shipmentCost){
            return $this->errorResponse($shipmentCost,500);
        }
    }

    /**
     * Remove the especified shipcost
     * @auth Kareem Lorenzana <kareem.lorenzana@nuvem.mx>
     * @created 2021-04-27
     * @param  ShipmentCost $shipmentCostConfiguration
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(ShipmentCost $shipmentCostConfiguration)
    {
        $shipmentCostConfiguration->toggleStatus();
        return response()->json(['shipmentCost' => $shipmentCostConfiguration->refresh()]);
    }

    /**
     * return list of shipmentcostypes for v-select
     * @auth Kareem Lorenzana <kareem.lorenzana@nuvem.mx>
     * @created 2021-04-28
     * @param  
     * @return \Illuminate\Http\JsonResponse
     */
    public function listShipmentCostTypes()
    {
        $shipmentCostTypes = $this->shipmentCostConfigurationRepository->getActiveListShipmentCostTypes();
        return response()->json(compact('shipmentCostTypes'));
    }
}
