<?php

namespace App\Http\Controllers\Api\ProductConfiguration\RuleConfigurator;

use App\RuleSpecialCost;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use App\Http\Requests\RuleSpecialCostRequest;

class RuleConfiguratorController extends Controller
{
    /**
     * Display a listing of the resource.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-03-31
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rules = RuleSpecialCost::active()->get();
        return response()->json(compact('rules'));
    }

    /**
     * Store a newly created resource in storage.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-03-31
     * @param  \App\Http\Requests\RuleSpecialCostRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RuleSpecialCostRequest $request)
    {
        try {
            RuleSpecialCost::create($request->all());
            return response(['message' => 'Created successfully']);
        } catch (\Exception $e) {
            Log::error($e);
            return response([$e], 500);
        }
    }

    /**
     * Update the specified resource in storage.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-04-07
     * @param  \App\Http\Requests\RuleSpecialCostRequest  $request
     * @param  RuleSpecialCost $rule
     * @return \Illuminate\Http\Response
     */
    public function update(RuleSpecialCostRequest $request, RuleSpecialCost $rule)
    {   
        try {
            $rule->update($request->all());
            $rule->refresh();
            return response(['message' => 'Edited successfully']);
        } catch (\Exception $e) {
            Log::error($e);
            return response([$e], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-04-07
     * @param  RuleSpecialCost $rule
     * @return \Illuminate\Http\Response
     */
    public function destroy(RuleSpecialCost $rule)
    {
        $rule->toggleStatus();
        return response()->json(['rule' => $rule->refresh()]);
    }

    /**
     * Verify if a priority exist in db.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-03-31
     * @param  \App\Http\Requests\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function existPriority(Request $request){
        $specialCost = new RuleSpecialCost();
        return  $specialCost->existPriority($request);
    }
}
