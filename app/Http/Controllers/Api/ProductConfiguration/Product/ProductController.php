<?php

namespace App\Http\Controllers\Api\ProductConfiguration\Product;

use App\Rack;
use App\Nivel;
use App\Product;
use Illuminate\Http\Request;
use App\Traits\ApiResponserTrait;
use App\Models\MedicaDepot\Profit;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use App\Http\Requests\ProductConfiguration\ProductRequest;

class ProductController extends Controller
{
  use ApiResponserTrait;
    /**
     * Display a listing of the resource.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-03-26
     * @updated 2020/06/22 Noe Reyes <noe.reyes@nuvem.mx>
     * @param
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::active()->with('images', 'profit', 'locations.seccion.nivel.rack')
            ->paginate(50);
        return response()->json(compact('products'));

    }

    /**
     * Retrieves data to fill rack select options on location form
     *
     * @author Giovanny González <giovanny.gonzalez@nuvem.mx>
     * created 14/06/2021
     * @params
     * @return Illuminate\Http\JsonResponse
     *
     */
    public function getRackSelectsOptions()
    {
      try {
        $racks = Rack::all();
        return $this->successResponse($racks, 200);
      } catch (\Exception $e) {
        Log::error($e);
        return $this->errorResponse($e->getMessage(), 500);
      }
    }
    /**
     * Retrieves data to fill nivel select options on location form
     *
     * @author Giovanny González <giovanny.gonzalez@nuvem.mx>
     * created 14/06/2021
     * @params App\Rack $rack
     * @return Illuminate\Http\JsonResponse
     *
     */
    public function getNivelSelectsOptions(Rack $rack)
    {
      try {
        $niveles = $rack->niveles;
        return $this->successResponse($niveles, 200);
      } catch (\Exception $e) {
        Log::error($e);
        return $this->errorResponse($e->getMessage(), 500);
      }
    }
    /**
     * Retrieves data to fill seccion select options on location form
     *
     * @author Giovanny González <giovanny.gonzalez@nuvem.mx>
     * created 14/06/2021
     * @params App\Rack $rack
     * @return Illuminate\Http\JsonResponse
     *
     */
    public function getSeccionSelectsOptions(Nivel $nivel)
    {
      try {
        $secciones = $nivel->secciones;
        return $this->successResponse($secciones, 200);
      } catch (\Exception $e) {
        Log::error($e);
        return $this->errorResponse($e->getMessage(), 500);
      }
    }

    /**
     * Store a newly created resource in storage.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-03-26
     * @param  \Illuminate\Http\ProductRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductRequest $request)
    {
        try {
            $object = json_decode($request->newProduct);

            $existentIdProd1 = Product::find($object->Id_Prod1);
            if($existentIdProd1)
                return response(['message' => 'La clave nemotécnica ya existe, escriba una diferente.'], 400);
            $existentIdProd3 = Product::where('Id_Prod3', $object->Id_Prod3)->first();
            if($existentIdProd3)
                return response(['message' => 'La clave licitación ya existe, escriba una diferente.'], 400);

            $data = json_decode(json_encode($object), true);
            $product = Product::create($data);

            if($request->exists('images'))
                $product->saveImages($data['Id_Prod1'] ,$request->images, 'products');
            if(!empty($object->locations));
                $product->saveLocations($object->Id_Prod1, $object->locations);

            return response(['message' => 'Created successfully']);
        } catch (\Exception $e) {
            Log::error($e);
            return response([$e], 500);
        }
    }

    /**
     * Update the specified resource in storage.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-04-16
     * @param  \Illuminate\Http\Request  $request
     * @param  Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        try {
            $object = json_decode($request->newProduct);
            $data = json_decode(json_encode($object), true);
            $product->update($data);
            if($request->exists('images')){
                $product->updateImages($data['Id_Prod1'] ,$request->images, 'products');
            }
            if(!empty($object->locations));
                $product->saveLocations($object->Id_Prod1, $object->locations);

            return response(['message' => 'Updated successfully']);
        } catch (\Exception $e) {
            Log::error($e);
            return response([$e], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-04-16
     * @param  RuleSpecialCost $rule
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $product->toggleStatus();
        return response()->json(['product' => $product->refresh()]);
    }

    /**
     * Store a newly created resource in storage.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-04-19
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Http\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function storeProfit(Request $request, Product $product)
    {
        try {
            $profit = new Profit;
            $profit->Utilidad = $request->newProductProfit;
            $profit->Id_Prod1 = $product->Id_Prod1;
            $product->profit()->save($profit);
            return response(['message' => $request->all()]);
        } catch (\Exception $e) {
            Log::error($e);
            return response([$e], 500);
        }
    }

    /**
     * Update a profit.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-04-19
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Http\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function updateProfit(Request $request, Product $product)
    {
        try {
            $product->profit()->update(['Utilidad'=> $request->newProductProfit]);
            return response(['message' => $request->all()]);
        } catch (\Exception $e) {
            Log::error($e);
            return response([$e], 500);
        }
    }

    /**
     * Search for a specific product.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-04-19
     * @updated Oscar Castellanos <oscar.castellanos@nuvem.mx> 11/06/2021
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function searchProduct(Request $request){
        $products =  Product::active()->where('Descrip1', 'LIKE', "%$request->search%")->paginate(50);
        $products=$products->merge(Product::active()->where('Id_Prod3', 'LIKE', "%$request->search%")->paginate(50));
        $products=$products->merge(Product::active()->where('Id_Prod1', 'LIKE', "%$request->search%")->paginate(50));
        return response()->json(compact('products'));
    }

    /**
     * Filter products.
     * @auth Oscar Castellanos <oscar.castellanos@nuvem.mx>
     * @date 2021-06-11
     * @updated 2020/06/22 Noe Reyes <noe.reyes@nuvem.mx>
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function filterProduct(Request $request){
        if ($request->filter == "E-commerce") {
            $products = Product::ecommerce();
        } elseif ($request->filter == "Sin codigo de barras") {
            $products = Product::noBarcode();
        } else {
            $products = Product::query();
        }

        $products = $products->active()->with('images', 'profit')->paginate(50);
        return response()->json(compact('products'));
    }
       /**
     * search the products in the database.
     * @auth
     * @date
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function searchProducts(Request $request)
    {
        $product = new Product();
        $products = $product->searchProducts($request);
        return response()->json(compact('products'));
    }
}
