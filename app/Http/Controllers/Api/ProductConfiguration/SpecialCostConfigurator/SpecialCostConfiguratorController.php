<?php

namespace App\Http\Controllers\Api\ProductConfiguration\SpecialCostConfigurator;

use App\Product;
use App\CostConcept;
use App\CostoEspecial;
use App\RuleSpecialCost;
use App\CalculatedCostBase;
use Illuminate\Http\Request;
use App\Models\MedicaDepot\Brand;
use App\Models\MedicaDepot\Family;
use Illuminate\Support\Facades\DB;
use App\Models\MedicaDepot\Article;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use App\Models\MedicaDepot\Category;
use App\Http\Requests\CostoEspecialRequest;
use App\Models\MedicaDepot\MedicalSpecialty;

class SpecialCostConfiguratorController extends Controller
{
    /**
     * Display a listing of the resource.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-03-31
     * @params
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $specialCosts = DB::connection('medicadepot')->table('CostosEspeciales')->select('CostosEspeciales.Id_CostoEspecial', 'CostosEspeciales.Descripcion',
        'CostosEspeciales.Estatus',
        'productos.productos')->join(
            DB::raw("
                (SELECT distinct CostosEspeciales.Id_CostoEspecial,
                STRING_AGG(Articulos.Descrip1, CHAR(10)) as productos
                FROM ReglasEspecialesProductosDetalles
                JOIN Articulos on Articulos.Id_Prod1 = ReglasEspecialesProductosDetalles.Id_Prod1
                JOIN ReglasEspecialesProductos on ReglasEspecialesProductos.Id_ReglaEspecialProducto  = ReglasEspecialesProductosDetalles.Id_ReglaEspecialProducto
                JOIN CostosEspeciales on CostosEspeciales.Id_CostoEspecial = ReglasEspecialesProductos.Id_CostoEspecial
                GROUP BY CostosEspeciales.Id_CostoEspecial) as productos"), 'productos.Id_CostoEspecial', 'CostosEspeciales.Id_CostoEspecial'
        )->where('Estatus', 1)->get();
        return response()->json(compact('specialCosts'));
    }

    /**
     * Stores a new special prices config
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-03-31
     * @params App\Http\Requests\CostoEspecialRequest
     * @return \Illuminate\Http\Response
     */
    public function store(CostoEspecialRequest $request)
    {
        try {
            $costoEspecial = new CostoEspecial();
            $costoEspecial->store($request);
            return response(['Created successfully.'], 200);
        } catch (\Exception $e) {
            Log::error($e);
            return response(['message' => $e->getMessage()], 500);
        }
    }

    /**
     * Update the specified resource in storage.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-03-31
     * @param  \Illuminate\Http\Request  $request
     * @param  CostConcept $cost
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $cost = CostoEspecial::find($id);
            $cost->updateData($request);
            return response(['message' => 'Edited successfully']);
        } catch (\Exception $e) {
            Log::error($e);
            return response([$e], 500);
        }
    }

    /**
     * Gets cost data from given id.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-04-12
     * @params
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $products = [];
        $cost = CostoEspecial::find($id);
        $rules = $cost->reglas_especiales_productos;
        foreach ($rules as $rule) {
            foreach ($rule->productos as $product) {
                array_push($products, $product);
            }
        }
        return response()->json(compact('cost'));
    }

    /**
     * Remove the specified resource from storage.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-04-12
     * @param  RuleSpecialCost $rule
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cost = CostoEspecial::find($id);
        $cost->toggleStatus();
        return response()->json(['exit' => 'Deleted successfully']);
    }

    /**
     * Return all settings to create special cost
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-03-31
     * @params
     * @return \Illuminate\Http\Response
     */
    public function getSettingsToCreateSpecialCost(){
        $prices = RuleSpecialCost::active()->select('Id_ReglaCostoEspecial', 'Descripcion')->get();
        $families = Family::active()->select('Id_Familia', 'Familia')->get();
        $brands = Brand::active()->select('Id_Marca', 'Marca')->get();
        $especialities = MedicalSpecialty::active()->select('Id_EspMed', 'Especialidad')->get();
        $categories = Category::active()->select('Id_Categoria', 'Categoria')->get();
        $conceptosCostos = CostConcept::selectOptions()->get();
        $basesCalculo = CalculatedCostBase::get(['Descripcion', 'Id_BaseCostoCalculado']);
        $basesCalculoPluck = CalculatedCostBase::pluck('Descripcion', 'Id_BaseCostoCalculado');

        return response()->json(compact(
            'prices',
            'families',
            'brands',
            'especialities',
            'categories',
            'conceptosCostos',
            'basesCalculo',
            'basesCalculoPluck'));
    }

    /**
     * search the products in the database.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-03-31
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function searchProducts(Request $request)
    {
        $costoEspecial = new CostoEspecial();
        $products = $costoEspecial->searchProducts($request);
        return response()->json(compact('products'));
    }

    /**
     * search the specific products in the database.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-04-30
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function searchSpecificProducts(Request $request)
    {
        $products =  Product::active()->where('Id_Prod1', 'LIKE', "%$request->search%")->ecommerce()->get();
        return response()->json(compact('products'));
    }

    /**
     * Return products to edit.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-04-13
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getProductsToEdit(Request $request){
        $productsToEdit = [];
        $requestJson = json_decode($request->products);
        foreach ($requestJson->products as $item) {
            array_push($productsToEdit, Article::where('Id_Prod1', '=', $item->product)->get(['Id_Prod1', 'Descrip1']));
        }
        return response()->json(['products' => $productsToEdit]);
    }
}
