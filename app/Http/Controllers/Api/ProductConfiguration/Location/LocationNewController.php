<?php

namespace App\Http\Controllers\Api\ProductConfiguration\Location;

use App\Rack;
use App\Nivel;
use App\Seccion;
use Illuminate\Http\Request;
use App\Traits\ApiResponserTrait;
use App\Http\Controllers\Controller;
use App\Http\Requests\ProductConfiguration\LocationRequest;
use App\Http\Requests\ProductConfiguration\SectionRequest;

class LocationNewController extends Controller
{
    use ApiResponserTrait;

    /**
     * Method construct, verify the permissions from user for access to the functions.
     * @auth Iván Morales <ivan.morales@nuvem.mx>
     * @date 2021-06-25
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permission:Location.view', ['only' => ['index', 'show', 'getSections']]);
        $this->middleware('permission:Location.create', ['only' => ['store']]);
        $this->middleware('permission:Location.edit', ['only' => ['update', 'storeNewLevel', 'deleteLevel']]);
        $this->middleware('permission:Location.delete', ['only' => ['destroy']]);
    }
    /**
     * Get all racks with status active
     * @author Iván Morales <ivan.morales@nuvem.mx>
     * @created 2021/06/17
     * @param
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $racks = Rack::active()->get();
        return $this->successResponse(compact('racks'), 200);
    }

    /**
     * Store a new rack and levels
     * @author Iván Morales <ivan.morales@nuvem.mx>
     * created 18/06/2021
     * @params LocationRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(LocationRequest $request)
    {
        $newRack = Rack::storeRack($request);
        return $this->successResponse(['rack' => $newRack], 200);
    }

    /**
     * Return the data of a rack and its levels actives
     * @author Iván Morales <ivan.morales@nuvem.mx>
     * created 18/06/2021
     * @params int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $rack = Rack::with('levelsActive')->findOrFail($id);
        return $this->successResponse(compact('rack'), 200);
    }

    /**
     * Update the data of a rack
     * @author Iván Morales <ivan.morales@nuvem.mx>
     * created 18/06/2021
     * @params LocationRequest $request, int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(LocationRequest $request, $id)
    {
        $rack = Rack::findOrFail($id);
        $rack->update(['Nombre' => $request->name]);
        return $this->successResponse(compact('rack'), 200);
    }

    /**
     * Change the status of a record
     * @author Iván Morales <ivan.morales@nuvem.mx>
     * @created 2021/06/17
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $rack = Rack::findOrFail($id);
        $modelsDeleted = $rack->deleteRack();
        return $this->successResponse([
            'message' => 'El rack, niveleles y secciones relacionados han sido dados de baja',
            'models_deleted' => $modelsDeleted,
        ], 200);
    }

    /**
     * Store a new level from a rack
     * @author Iván Morales <ivan.morales@nuvem.mx>
     * created 18/06/2021
     * @params Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function storeNewLevel(Request $request)
    {
        $data = $request->only('Nombre', 'zAE_Rak_Id');
        $newLevel = Nivel::create($data);
        return $this->successResponse(compact('newLevel'), 200);
    }

    /**
     * Change the status of a level
     * @author Iván Morales <ivan.morales@nuvem.mx>
     * created 18/06/2021
     * @params int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteLevel($id)
    {
        $level = Nivel::findOrFail($id);
        $modelsDeleted = $level->deleteLevel();
        $message = "El estatus fue cambiado exitosamente";
        return $this->successResponse(['message' => $message, 'models_deleted' => $modelsDeleted], 200);
    }

    /**
     * Get the rack, levels and sections.
     * @author Iván Morales <ivan.morales@nuvem.mx>
     * created 23/06/2021
     * @params int $rackId
     * @return \Illuminate\Http\JsonResponse
     */
    public function getSections($rackId)
    {
        $rack= Rack::active()->findOrFail($rackId);
        $rackClone = clone $rack;
        $levels = $rackClone->levelsActive;

        $levelsId = $levels->pluck('Id');
        $sections = Seccion::whereIn('zAE_Nivel_Id', $levelsId)->active()->orderedLevelDesc()->get();
        foreach ($sections as $section) {
            $sectionClone = clone $section;
            $section->level_name = $sectionClone->nivel ? $sectionClone->nivel->Nombre : null;
        }

        return $this->successResponse(compact('rack', 'levels', 'sections'), 200);
    }

    /**
     * Change the status of a section
     * @author Iván Morales <ivan.morales@nuvem.mx>
     * created 23/06/2021
     * @params int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteSection($id)
    {
        $section = Seccion::findOrFail($id);
        $section->toggleStatus();
        $message = "El estatus fue cambiado exitosamente";
        return $this->successResponse(compact('message', 'section'), 200);
    }

    /**
     * Store a sections for a level
     * @author Iván Morales <ivan.morales@nuvem.mx>
     * created 24/06/2021
     * @params SectionRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function storeSections(SectionRequest $request, $levelId)
    {
        $level = Nivel::findOrFail($levelId);
        $sections = $level->storeSections($request);
        return $this->successResponse(compact('sections'), 200);
    }
}
