<?php

namespace App\Http\Controllers\Api\ProductConfiguration\Location;

use App\Location;
use Illuminate\Http\Request;
use App\Traits\ApiResponserTrait;
use App\Http\Controllers\Controller;
use App\Http\Requests\LocationRequest;
use App\Repositories\LocationRepository;

class LocationController extends Controller
{
    use ApiResponserTrait;

    private $locationRepository;

    /**
     * Instance class for controller.
     * @auth Kareem Lorenzana <kareem.lorenzana@nuvem.mx>
     * @created 2021-05-27
     * @param LocationRepository $locationRepository
     * @return void
     */
    public function __construct(
        LocationRepository $locationRepository
    ){
        $this->locationRepository = $locationRepository;
    }

    /**
     * Display a listing of the resource.
     * @auth Kareem Lorenzana <kareem.lorenzana@nuvem.mx>
     * @date 2021-05-26
     * @param
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $locations = $this->locationRepository->getLocations();
        return response()->json(compact('locations'));
    }

    /**
     * save new location in database
     * @auth Kareem Lorenzana <kareem.lorenzana@nuvem.mx>
     * @date 2021-05-27
     * @param LocationRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(LocationRequest $request)
    {
        $location = $this->locationRepository->storeUpdateLocation($request, new Location());
        if($location instanceof Location){
            return $this->successResponse("Ubicación registrada correctamente", 201);
        }elseif($location){
            return $this->errorResponse($location,500);
        }
    }


    /**
     * update a location in database
     * @auth Kareem Lorenzana <kareem.lorenzana@nuvem.mx>
     * @date 2021-05-27
     * @params LocationRequest $request, Location $location
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(LocationRequest $request, Location $location)
    {
        $location = $this->locationRepository->storeUpdateLocation($request, $location);
        if($location instanceof Location){
            return $this->successResponse("Ubicación actualizada correctamente", 201);
        }elseif($location){
            return $this->errorResponse($location,500);
        }
    }

    /**
     * toggle status in database for delete method
     * @auth Kareem Lorenzana
     * @date 2021-05-27
     * @param Location $location
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Location $location)
    {
        $location->toggleStatus();
        return response()->json(['location' => $location->refresh()]);
    }

    /**
     * return all racks for select
     * @auth Kareem Lorenzana <kareem.lorenzana@nuvem.mx>
     * @date 2021-05-27
     * @param 
     * @return \Illuminate\Http\JsonResponse
     */
    public function getRacks()
    {
        $racks = $this->locationRepository->getRacks();
        return $this->successResponse($racks, 200);
    }
}
