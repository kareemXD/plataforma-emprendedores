<?php

namespace App\Http\Controllers\Api\ProductConfiguration\GlobalCostConfigurator;

use App\CostConcept;
use App\CalculatedCostBase;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use App\Http\Requests\CostConceptRequest;

class GlobalCostConfiguratorController extends Controller
{
    /**
     * Method construct, verify the permissions from user for access to the functions.
     * @auth Iván Morales <ivan.morales@nuvem.mx>
     * @date 2021-04-28
     */
    public function __construct()
    {
        //$this->middleware('permission:GlobalCostConfiguration.view|SpecialCostConfiguration.*|SpecialCostConfiguration.view|SpecialCostConfiguration.create|SpecialCostConfiguration.edit|SpecialCostConfiguration.delete')->only('index');
        //$this->middleware('permission:GlobalCostConfiguration.create')->only('store');
        $this->middleware('permission:GlobalCostConfiguration.edit')->only('update');
        $this->middleware('permission:GlobalCostConfiguration.delete')->only('destroy');
    }

    /**
     * Display a listing of the resource.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-03-31
     * @updated 27/04/2021 Iván Morales | ivan.morales@nuvem.mx
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $globalCosts = CostConcept::get();
        $bases = CalculatedCostBase::get();
        return response()->json(compact('globalCosts','bases'));
    }

    /**
     * Store a newly created resource in storage.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-03-31
     * @param  \App\Http\Requests\CostConceptRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(CostConceptRequest $request)
    {
        try {
            $data = CostConcept::getAttributesToUpdate($request);
            $data['Prioridad'] = 0;
            CostConcept::create($data);
            return response(['message' => 'Created successfully']);
        } catch (\Exception $e) {
            Log::error($e);
            return response([$e], 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @author José Vega <jose.vega@nuvem.mx>
     * @created 2020-04-06
     * @param  int  $id
     * @param  \Illuminate\Http\ServiceRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function update(CostConceptRequest $request,  $id)
    {
        try {
            $cost = CostConcept::find($id);
            $data = CostConcept::getAttributesToUpdate($request);
            $cost->update($data);
            return response(['message' => 'Update successfully']);
        } catch (\Exception $e) {
            Log::error($e);
            return response([$e], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @author José Vega <jose.vega@nuvem.mx>
     * @created 2021-04-06
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cost = CostConcept::find($id);
        $cost->toggleStatus();
        return response()->json(['cost' => 'successfully' ]);
    }
}
