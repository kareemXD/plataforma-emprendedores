<?php

namespace App\Http\Controllers\Api\ProductConfiguration\GlobalCostConfigurator;

use App\CostConcept;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;

class GlobalUtilityController extends Controller
{
    /**
     * Method construct, verify the permissions from user for access to the functions.
     * @auth Iván Morales <ivan.morales@nuvem.mx>
     * @date 2021-04-28
     */
    public function __construct()
    {
        $this->middleware('permission:GlobalUtilityConfiguration.create', ['only' => ['index', 'update']]);
        $this->middleware('permission:GlobalUtilityConfiguration.edit', ['only' => ['index', 'update']]);
    }

    /**
     * Display a listing of the resource.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-03-31
     * @param
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $globalUtility = CostConcept::firstOrCreate([
			'Descripcion' => 'Utilidad',
			'Tipo' => 'P',
			'Prioridad' => 1
		]);
        return response()->json(compact('globalUtility'));
    }

    /**
     * Update the specified resource in storage.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-03-31
     * @param  \Illuminate\Http\Request  $request
     * @param  CostConcept $cost
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CostConcept $cost)
    {
        try {
            $cost->update($request->all());
            return response(['message' => 'Updated successfully']);
        } catch (\Exception $e) {
            Log::error($e);
            return response([$e], 500);
        }
    }
}
