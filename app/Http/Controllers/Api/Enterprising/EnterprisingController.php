<?php

namespace App\Http\Controllers\Api\Enterprising;

use App\Marca;
use App\Familia;
use App\Categoria;
use App\Especialidad;
use Illuminate\Http\Request;
use App\Cliente as AppCliente;
use App\Traits\ApiResponserTrait;
use Illuminate\Support\Facades\DB;
use App\Models\MedicaDepot\Cliente;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use App\Models\MedicaDepot\ClientType;
use App\Models\MedicaDepot\CreditStatus;
use App\Models\MedicaDepot\Enterprising;
use App\Http\Controllers\Api\ApiController;
use App\Models\MedicaDepot\EnterprisingType;
use App\Http\Requests\Enterprising\EnterprisingRequest;

class EnterprisingController extends ApiController
{
    use ApiResponserTrait;
    /**
     * Method construct, verify the permissions from user for access to the functions.
     * @auth Iván Morales <ivan.morales@nuvem.mx>
     * @date 2021-05-20
     */
    public function __construct()
    {
        $this->middleware('permission:EnterprisingsList.view')->only('index');
        $this->middleware('permission:EnterprisingsList.delete')->only('destroy');
        $this->middleware('permission:EnterprisingsList.edit', ['only' => ['getEnterprisingInfo', 'updateEnterprising']]);
    }
    
    /**
     * Display a listing of the resource.
     * @auth Iván Morales <ivan.morales@nuvem.mx>
     * @date 2021-05-05
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $enterprisings = Cliente::where('eCommerce', 1)
            ->get([
                'Id_Cliente',
                'Razon_Soc',
                'Nombre_Com',
                'RFC',
                'Telefonos',
                'Tipo_Cliente',
                'eMail',
                'Director',
                'Email_Dir',
                'Observaciones',
                'Estatus',
                'eCommerce'
            ]);
        return response()->json(["enterprisings" => $enterprisings]);
    }

    /**
     * Change the current status.
     * @auth Iván Morales <ivan.morales@nuvem.mx>
     * @date 2021-05-07
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cliente = Cliente::findOrFail($id, [
            'Id_Cliente',
            'Razon_Soc',
            'Nombre_Com',
            'RFC',
            'Telefonos',
            'Tipo_Cliente',
            'eMail',
            'Director',
            'Email_Dir',
            'Observaciones',
            'Estatus',
            'eCommerce'
        ]);
        $cliente->toggleStatus();

        return response()->json(["Message" => "Estatus cambiado", "Enterprising" => $cliente]);
    }

    /* Get a information enterprising.
     * @auth Iván Morales <ivan.morales@nuvem.mx>
     * @date 2021-05-24
     * @param int $clientId
     * @return Illuminate\Http\Response
     */
    public function getEnterprisingInfo($clientId)
    {
        $clientData = Cliente::where('Id_Cliente', $clientId)->first([
            'Id_Cliente',
            'Razon_Soc',
            'Nombre_Com',
            'RFC',
            'Telefonos',
            'Tipo_Cliente',
            'Clasifica',
            'eMail',
            'Observaciones',
            'Estatus',
        ]);
        $informationConfig = Enterprising::where('Id_Cliente', $clientId)->first();
        $clientData->complement = $informationConfig;

        $clientTypes = ClientType::all();
        $enterprisingsTypes = EnterprisingType::get(['Id','Tipo']);
        $creditStatus = CreditStatus::get(['Id','Estatus']);
        return response()->json([
            "enterprising" => $clientData,
            "client_types" => $clientTypes,
            "enterprising_types" => $enterprisingsTypes,
            "credit_status" => $creditStatus
        ]);
    }

    /**
     * Update the information of from a clientID.
     * @auth Iván Morales <ivan.morales@nuvem.mx>
     * @date 2021-05-25
     * @param EnterprisingRequest $request, int $clientId
     * @return Illuminate\Http\Response
     */
    public function updateEnterprising(EnterprisingRequest $request, $clientId)
    {
        try {
            DB::connection('medicadepot')->beginTransaction();
            $enterprisingData = new \stdClass();
            $enterprising = Enterprising::firstOrCreate(['Id_Cliente' => $clientId]);
            $enterprising->updateEnterprising($request);
    
            $clientInfo = $request->only(['Razon_Soc', 'RFC', 'Telefonos', 'eMail', 'Observaciones', 'Tipo_Cliente', 'Clasifica', 'Estatus']);
            $client = Cliente::where('Id_Cliente', $clientId)->first();
            $client->update($clientInfo);
            $enterprisingData = $client;
            $enterprisingData->complement = $enterprising;
            DB::connection('medicadepot')->commit();
            return response()->json(["message" => "Registro actualizado", "enterprising" => $enterprisingData]);
        } catch (\Throwable $th) {
            DB::connection('medicadepot')->rollback();
            throw new \Exception($th);
        }
    }
    
    /**
     * Display a listing of the resource.
     * @auth Iván Morales <ivan.morales@nuvem.mx>
     * @date 2021-05-06
     * @param int $clientId
     * @return \Illuminate\Http\Response
     */
    public function getBranches($clientId)
    {
        $branches = collect(["id" => $clientId, "name" => "Sucursal Prueba", "type" => "Prueba", "address" => "Prueba", "status" => 1]);
        return response()->json(["branches" => [$branches]]);
    }
    /**
     *
     * Filters options
     * @author Giovanny González <giovanny.gonzalez@nuvem.mx>
     * created 12/05/2021
     * @params
     * @return \Illuminate\Http\JsonResponse
     */
    public function productAssigningFiltersOptions()
    {
        try {
            $brands = Marca::filterSelectOptions();
            $families = Familia::filterSelectOptions();
            $especialities = Especialidad::filterSelectOptions();
            $categories = Categoria::filterSelectOptions();
            return $this->successResponse(compact('brands', 'families', 'especialities', 'categories'), 200);
        } catch (\Exception $e) {
            Log::error($e);
            return $this->errorResponse($e->getMessage(), 500);
        }
    }
    /**
     * Product list an enterprising is allowed to sell
     *
     * @author Giovanny González <giovanny.gonzalez@nuvem.mx>
     * created 31/05/2021
     * @params App\Cliente $enterprising
     * @return \Illuminate\Http\JsonResponse
     *
     */
      public function asignedProducts(AppCliente $enterprising)
      {
        try {
            return $this->successResponse($enterprising->assignedProducts(), 200);
        } catch (\Exception $e) {
            Log::error($e);
            return $this->errorResponse($e->getMessage(), 500);
        }
      }
      /**
       * Updates enterprising's products exceptions
       *
       * @author Giovanny González <giovanny.gonzalez@nuvem.mx>
       * created 31/05/2021
       * @params number $enterprisingId
       * @return \Illuminate\Http\JsonResponse
       *
       */
      public function updateExceptions($enterprisingId)
      {
        try {
          $product = DB::connection('medicadepot')->table('zAE_ArticulosClientesExcepciones')
          ->where('Clientes_Id_Cliente', $enterprisingId)
          ->where('Articulos_Id_Prod1', request()->productId);
          
          if(request()->action == -1 && $product->first()) $product->delete();
          else DB::connection('medicadepot')->table('zAE_ArticulosClientesExcepciones')->insert([
            'Clientes_Id_Cliente' => $enterprisingId,
            'Articulos_Id_Prod1' => request()->productId
          ]);
        } catch (\Exception $e) {
            Log::error($e);
            return $this->errorResponse($e->getMessage(), 500);
        }
      }
}
