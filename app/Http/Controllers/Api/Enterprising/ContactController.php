<?php

namespace App\Http\Controllers\Api\Enterprising;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\MedicaDepot\ClientContact;
use App\Models\MedicaDepot\PositionContact;
use App\Models\MedicaDepot\ClientContactComplement;

class ContactController extends Controller
{
    /**
     * Method construct, verify the permissions from user for access to the functions.
     * @auth Iván Morales <ivan.morales@nuvem.mx>
     * @date 2021-05-26
     */
    public function __construct()
    {
        $this->middleware('permission:EnterprisingsList.edit', ['only' => ['index', 'store', 'update']]);
    }

    /**
     * Display a listing of the resource.
     * @auth Iván Morales <ivan.morales@nuvem.mx>
     * @date 2021-05-26
     * @param int $clientId
     * @return \Illuminate\Http\Response
     */
    public function index($clientId)
    {
        $contacts = ClientContact::where('Id_Cliente', $clientId)->get();
        $contactsResponse = collect();
        foreach ($contacts as $contact) {
            $contact->complement = ClientContactComplement::where([
                ['ClieContacto_Id_Cliente', $contact->Id_Cliente],
                ['ClieContacto_Id_Contacto', $contact->Id_Contacto]
            ])->first();
            $contact->Tipo_contacto = $contact->complement ? $contact->complement->Tipo_contacto : null;
            $contactsResponse->push($contact);
        }

        $positions = PositionContact::all();
        return response()->json(["contacts" => $contactsResponse, "positions" => $positions]);
    }

    /**
     * Store the data in DB
     * @auth Iván Morales <ivan.morales@nuvem.mx>
     * @date 2021-05-26
     * @param int $clientId, Request $request
     * @return \Illuminate\Http\Response
     */
    public function store($clientId, Request $request)
    {
        try {
            DB::connection('medicadepot')->beginTransaction();
            
            $idsContact = ClientContact::where('Id_Cliente', $clientId)->get(['Id_Contacto']);
            $valuesId = (count($idsContact) > 0) ? $idsContact->pluck('Id_Contacto') : collect();
            $valuesId = $valuesId->toArray();
            $nexId = count($valuesId) > 0 ? (max($valuesId) + 1) : 1;
    
            $dataContact = $request->only(['Nombre', 'Id_Puesto', 'EMail', 'Telefono1', 'TelMovil']);
            $dataContact['Id_Cliente'] = $clientId;
            $dataContact['Id_Contacto'] = $nexId;
            $newContact = ClientContact::create($dataContact);
            $dataContactComplement = $request->only(['Fecha_nacimiento', 'Tipo_contacto', 'Calle_numero', 'Colonia', 'Codigo_postal', 'Ciudad', 'Estado', 'Observaciones']);
            $dataContactComplement['ClieContacto_Id_Cliente'] = $clientId;
            $dataContactComplement['ClieContacto_Id_Contacto'] = $nexId;
            $newContactComplement = ClientContactComplement::create($dataContactComplement);
    
            $newContact->complement = $newContactComplement;
            DB::connection('medicadepot')->commit();
            return response()->json(["mesage" => "Registro creado", "contact" => $newContact]);
        } catch (\Throwable $th) {
            DB::connection('medicadepot')->rollback();
            throw new \Exception($th);
        }
    }

    /**
     * Update the data in DB
     * @auth Iván Morales <ivan.morales@nuvem.mx>
     * @date 2021-05-26
     * int $clientId, int $contactId, Request $request
     * @return \Illuminate\Http\Response
     */
    public function update($clientId, $contactId, Request $request)
    {
        try {
            DB::connection('medicadepot')->beginTransaction();
    
            $dataContact = $request->only(['Nombre', 'Id_Puesto', 'EMail', 'Telefono1', 'TelMovil']);
            $contact = ClientContact::where([['Id_Cliente', $clientId], ['Id_Contacto', $contactId]])->first();
            if($contact){
                $contact->fill($dataContact);
                $updatedContact = $contact->save();
                $dataContactComplement = $request->only(['Fecha_nacimiento', 'Tipo_contacto', 'Calle_numero', 'Colonia', 'Codigo_postal', 'Ciudad', 'Estado', 'Observaciones']);
                $contactComplement = ClientContactComplement::firstOrCreate(['ClieContacto_Id_Cliente' => $clientId, 'ClieContacto_Id_Contacto' => $contactId]);
                $contactComplement->fill($dataContactComplement);
                $contactComplement->save();
                DB::connection('medicadepot')->commit();
                return response()->json(["mesage" => "Registro actualizado", "contact" => $updatedContact]);
            }
            else{
                DB::connection('medicadepot')->rollback();
                return response()->json(["mesage" => "No se encontró ningún registro con el id del cliente y el id del contacto"], 403);
            }
        } catch (\Throwable $th) {
            DB::connection('medicadepot')->rollback();
            throw new \Exception($th);
        }
    }
}
