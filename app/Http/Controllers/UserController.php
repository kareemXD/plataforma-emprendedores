<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Requests\UserRequest;

class UserController extends Controller
{
    /**
     * UserController constructor.
     */
    public function __construct()
    {
        $this->middleware('permission:list_users', ['only' => ['index']]);
        $this->middleware('permission:create_users', ['only' => ['create', 'store']]);
        $this->middleware('permission:edit_users', ['only' => ['edit', 'update']]);
        $this->middleware('permission:delete_users', ['only' => ['destroy']]);
    }

    /**
     * @author Octavio Cornejo
     * @created 2020-04-22
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     * @throws \Exception
     */
    public function index()
    {
        if (request()->ajax() or request()->expectsJson())
            return User::datatable();
        return view('users.index');
    }

    /**
     * @author Octavio Cornejo
     * @created 2020-04-22
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $passConfirmed = true;
        return view('users.create', compact('passConfirmed'));
    }

    /**
     * @author Octavio Cornejo
     * @created 2020-04-22
     * @param UserRequest $request
     * @return $this
     */
    public function store(UserRequest $request)
    {
        User::create($request->all());
        return redirect('/users')->with(['message' => 'Guardado']);
    }

    /**
     * @author Octavio Cornejo
     * @created 2020-04-22
     * @param User $user
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(User $user)
    {
        $passConfirmed = false;
        return view('users.edit', compact('user', 'passConfirmed'));
    }

    /**
     * @author Octavio Cornejo
     * @created 2020-04-22
     * @param User $user
     * @param UserRequest $request
     * @return $this
     */
    public function update(User $user, UserRequest $request)
    {
        $user->update($request->all());
        return redirect('/users')->with(['message' => 'Actualizado']);
    }

    /**
     * @author Octavio Cornejo
     * @created 2020-04-22
     * @param User $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(User $user)
    {
        $user->toggleStatus();
        return response()->json('ok');
    }
}
