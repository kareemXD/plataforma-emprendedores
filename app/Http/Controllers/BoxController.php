<?php

namespace App\Http\Controllers;

use App\Repositories\BoxRepository;
use Illuminate\Http\Request;

class BoxController extends Controller
{

    private $boxRepository;
    
    public function __construct(
        BoxRepository $boxRepository
    ){
        $this->boxRepository = $boxRepository;
    }
    
    /**
     * Display a listing of the resource.
     *
     * @author Kareem Lorenzana <kareem.lorenzana@nuvem.mx>
     * @created 2021/04/19
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(request()->ajax())
            return $this->boxRepository->list();

        return view('box.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @author Benito Huerta <benito.huerta@nuvem.mx>
     * @created 2020/02/16
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('box.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @author Benito Huerta <benito.huerta@nuvem.mx>
     * @created 2020/02/16
     * @param  \App\Http\Requests\RuleSpecialCostRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RuleSpecialCostRequest $request)
    {
        RuleSpecialCost::create($request->all());

        return redirect()->route('box.index')->with(['message' => 'Caja Guardada  Correctamente']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  RuleSpecialCost $rule
     * @return \Illuminate\Http\Response
     *@author Benito Huerta <benito.huerta@nuvem.mx>
     * @created 2020/02/16
     */
    public function edit(RuleSpecialCost $rule)
    {
        return view('box.edit', compact('rule'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\RuleSpecialCostRequest  $request
     * @param  RuleSpecialCost $rule
     * @return \Illuminate\Http\Response
     * @author Benito Huerta <benito.huerta@nuvem.mx>
     * @created 2020/02/16
     */
    public function update(RuleSpecialCostRequest $request, RuleSpecialCost $rule)
    {
        $rule->update($request->all());
        $rule->refresh();

        return redirect()->route('box.index')->with(['message' => 'Caja actualizada correctamente']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  RuleSpecialCost $rule
     * @return \Illuminate\Http\Response
     * @author Benito Huerta <benito.huerta@nuvem.mx>
     * @created 2020/02/16
     */
    public function destroy(RuleSpecialCost $rule)
    {
        $rule->toggleStatus();
        return response()->json(['rule' => $rule->refresh()]);
    }
}
