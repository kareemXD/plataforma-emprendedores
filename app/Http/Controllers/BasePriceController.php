<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables as DT;

class BasePriceController extends Controller
{
    /**
     * Display a listing of the base prices.
     * @author daniel.cruz@losfra.dev
     * @created 2021-01-08
     * @return mixed
     */
    public function index()
    {
        $basePrices = DB::connection('medicadepot')->select('SET NOCOUNT ON ; EXEC SP_Costos_Emp');
        $attributes = array_keys((array)$basePrices[0]);
        foreach ($basePrices as $price => $value) {
            foreach ($value as $v => $z) {
                if (is_numeric($z)) {
                    $value->$v = number_format((float)$z, 2, '.', '');                    
                }
            }
        }

        if (request()->ajax() or request()->expectsJson()) {
            return DT::of($basePrices)
            ->addColumn('actions', function ($basePrice) {
                return view('base_prices.partials.buttons', ['basePrice' => $basePrice]);
            })
            ->rawColumns(['actions'])
            ->make(true);
        }
        return view('base_prices.index')->with('attributes', $attributes);
    }
}
