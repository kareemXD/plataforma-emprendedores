<?php

namespace App\Http\Controllers;

use App\Role;
use App\Permission;
use App\Http\Requests\RoleRequest;

class RoleController extends Controller
{
    /**
     * UserController constructor.
     */
    public function __construct()
    {
        $this->middleware('permission:list_roles', ['only' => ['index']]);
        $this->middleware('permission:create_roles', ['only' => ['create', 'store']]);
        $this->middleware('permission:edit_roles', ['only' => ['edit', 'update']]);
        $this->middleware('permission:delete_roles', ['only' => ['destroy']]);
    }

    /**
     * @author Octavio Cornejo
     * @created 2020-04-22
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     * @throws \Exception
     */
    public function index()
    {
        if (request()->ajax() or request()->expectsJson())
            return Role::datatable();

        $permissions = Permission::all();
        return view('roles.index', compact('permissions'));
    }

    /**
     * @author Octavio Cornejo
     * @created 2020-04-22
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('roles.create');
    }

    /**
     * @author Octavio Cornejo
     * @created 2020-04-22
     * @param RoleRequest $request
     * @return $this
     */
    public function store(RoleRequest $request)
    {
        Role::create($request->all());
        return redirect('/roles')->with(['message' => 'Guardado']);
    }

    /**
     * @author Octavio Cornejo
     * @created 2020-04-22
     * @param Role $role
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Role $role)
    {
        return view('roles.edit', compact('role'));
    }

    /**
     * @author Octavio Cornejo
     * @created 2020-04-22
     * @param Role $role
     * @param RoleRequest $request
     * @return $this
     */
    public function update(Role $role, RoleRequest $request)
    {
        $role->update($request->all());
        return redirect('/roles')->with(['message' => 'Actualizado']);
    }

    /**
     * @author Octavio Cornejo
     * @created 2020-04-22
     * @param Role $role
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Role $role)
    {
        $role->toggleStatus();
        return response()->json('ok');
    }
}
