<?php

namespace App\Http\Controllers;

use Auth;
use App\ShippingConfig;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class ShippingConfigController extends Controller
{
    /**
     * redirect index
     * @author  luis peña <luis.pena@nuvem.mx> 20/10/2020
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('shipping_config.index');
    }

    /**
     * store new  ShippingConfig
     * @author  luis peña <luis.pena@nuvem.mx> 20/10/2020
     * @param  \Illuminate\Http\Request  $request
     * @return ShippingConfig $shippingConfig
     */
    public function store(Request $request)
    {
        $shippingConfig = ShippingConfig::create($request->shipping_config);
        return $shippingConfig;
    }

    /**
     * update
     * @author  luis peña <luis.pena@nuvem.mx> 20/10/2020
     * @param ShippingConfig $shippings_config ,  \Illuminate\Http\Request  $request
     * @return null
     */
    public function update(ShippingConfig $shippings_config, Request $request)
    {
        $shippings_config->update($request->shipping_config);
        $shippings_config->save();
    }

    /**
     * change status
     * @author  luis peña <luis.pena@nuvem.mx> 20/10/2020
     * @param ShippingConfig $shippings_config
     * @return null
     */
    public function destroy(ShippingConfig $shippings_config)
    {
        $shippings_config->toggleStatus();
    }

    /**
     * get ShippingConfig
     * @author  luis peña <luis.pena@nuvem.mx> 20/10/2020
     * @param null
     * @return Illuminate\Database\Eloquent\Collection ShippingConfig
     */
    public function shippingsConfigUser(){
        return Auth::user()
            ->shippingConfig()
            ->orderBy('status', 'desc')
            ->orderBy('box_width', 'asc')
            ->get();
    }
}
