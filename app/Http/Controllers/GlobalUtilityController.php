<?php

namespace App\Http\Controllers;

use App\CostConcept;
use Illuminate\Http\Request;

class GlobalUtilityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @author Benito Huerta <benito.huerta@nuvem.mx>
     * @created 2020/02/17
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$globalUtility = CostConcept::firstOrCreate([
			'Descripcion' => 'Utilidad',
			'Tipo' => 'P',
			'Prioridad' => 1
		]);

        return view('global_utility.index', ['global_utility' => $globalUtility]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @author Benito Huerta <benito.huerta@nuvem.mx>
     * @created 2020/02/17
     * @param  \Illuminate\Http\Request  $request
     * @param  CostConcept $cost
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CostConcept $cost)
    {
        $this->validate($request, [
        	'Porcentaje' => 'required|int|min:0'
        ]);

        $cost->update($request->all());
        $cost->refresh();

        return redirect()->route('global-costs-configurator.index')->with(['message' => 'Utilidad actualizada correctamente']);
    }

}
