<?php

namespace App\Http\Requests;

use Illuminate\Support\Facades\Log;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class CostoEspecialRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'Descripcion' => 'required',
            'Id_ReglaCostoEspecial' => 'required|numeric',
            'reglas_especiales_productos' => 'bail|array|min:1',
            'reglas_especiales_productos.*.description' => 'bail|required_with:reglas_especiales_productos',
            'reglas_especiales_productos.*.type' => 'bail|required_with:reglas_especiales_productos',
            'reglas_especiales_productos_detalles' => 'bail|array|min:1',
        ];
    }

    /**
     *
     * Request validation rules custom messages
     * @author Giovanny González <giovanny.gonzalez@nuvem.mx>
     * created 16/03/2021
     * @params
     * @return array
     */
    public function messages()
    {
        return [
            'Descripcion.required' => 'El campo nombre es requerido',
            'Id_ReglaCostoEspecial.required' => 'El campo tipo de regla es requerido',
            'Id_ReglaCostoEspecial.numeric' => 'El campo tipo de regla debe ser numérico',
            'reglas_especiales_productos.*.type.required_with' => 'El campo tipo de cada precio especial es requerido',
            'reglas_especiales_productos.*.description.required_with' => 'El campo descripción de cada precio especial es requerido',
            'reglas_especiales_productos_detalles.min' => 'Se debe seleccionar al menos un producto'
        ];
    }

    /**
     *
     * Returns validation failures in json
     * @author Giovanny González <giovanny.gonzalez@nuvem.mx>
     * created 16/03/2021
     * @params Illuminate\Contracts\Validation\Validator $validator
     * @return Illuminate\Http\Exceptions\HttpResponseException
     */
    protected function failedValidation(Validator $validator) {
        throw new HttpResponseException(response()->json($validator->errors()->all(), 422));
    }
}
