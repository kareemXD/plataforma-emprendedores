<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class ShippingConfigurationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'Id_CostoEnvioTipo' => 'required|integer',
            'Descripcion' => 'required',
            'TiempoEstimadoEnvio' => 'required',
            'Tarifa' => 'required|numeric',
            'MontoMinimoEnvioGratuito' => 'nullable|required_if:AplicaEnvioGratuito,1|numeric'
        ];
    }
    /**
     *
     * Returns validation failures in json
     * @author Giovanny González <giovanny.gonzalez@nuvem.mx>
     * created 31/03/2021
     * @params Illuminate\Contracts\Validation\Validator $validator
     * @return Illuminate\Http\Exceptions\HttpResponseException
     */
    protected function failedValidation(Validator $validator) {
        throw new HttpResponseException(response()->json($validator->errors()->all(), 422));
    }
}
