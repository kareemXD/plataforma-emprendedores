<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LoginNegoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'Id_Usuario' => 'required|exists:medicadepot2.Usuarios,Id_Usuario',
            'Password' => 'required'
        ];
    }
}
