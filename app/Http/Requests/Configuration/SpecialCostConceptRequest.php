<?php

namespace App\Http\Requests\Configuration;

use Illuminate\Foundation\Http\FormRequest;

class SpecialCostConceptRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ConceptosCostos_Id' => 'required|numeric',
            'Grupo_Id' => 'required|numeric',
            'Tipo' => 'required|in:P,M,C',
            'Cantidad' => 'required|numeric',
            'BaseCostosCalculados_Id' => 'nullable|numeric'
        ];
    }
}
