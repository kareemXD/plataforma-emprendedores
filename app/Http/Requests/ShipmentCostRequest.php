<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ShipmentCostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules =  [
            'Descripcion' => 'required|string',
            'Id_CostoEnvioTipo' => 'required|exists:medicadepot.CostoEnvioTipo,Id_CostoEnvioTipo',
            'TiempoEstimadoMinimo' => 'required|numeric|max:99999999|min:1',
            'TiempoEstimadoMaximo' => 'required|numeric|max:99999999|min:1',
            'Tarifa' => 'required|numeric|max:99999999|min:0',
            'MontoMinimoEnvioGratuito' => 'required_if:AplicaEnvioGratuito,==,"1"|max:99999999|min:0'
        ];
           
        return $rules;
    }

  
}
