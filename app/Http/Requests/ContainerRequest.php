<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ContainerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     * @author Kareem Lorenzana <kareem.lorenzana@nuvem.mx>
     * @created 2021-05-28
     * @param
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * get validation rules for request
     * @author Kareem Lorenzana <kareem.lorenzana@nuvem.mx>
     * @created 2021-05-28
     * @updated Oscar Castellanos <oscar.castellanos@nuvem.mx> 17/06/2021
     * @param
     * @return array
     */
    public function rules()
    {
        return [
            "Descripcion" => "required|string|min:1|max:200",
            "Codigo_barras" => "required|numeric|min:1",
        ];
    }
}
