<?php

namespace App\Http\Requests;

use App\RuleSpecialCost;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class RuleSpecialCostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @author Benito Huerta <benito.huerta@nuvem.mx>
     * @created 2020/02/16
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @author Benito Huerta <benito.huerta@nuvem.mx>
     * @created 2020/02/16
     * @return array
     */
    public function rules()
    {
        return [
            RuleSpecialCost::DESCRIPCION => 'required|string',
            RuleSpecialCost::PRIORIDAD => [
                'required',
                Rule::unique(RuleSpecialCost::CONNECTION . '.' . RuleSpecialCost::TABLE, RuleSpecialCost::PRIORIDAD)
                    ->ignore($this->rule)
            ]
        ];
    }

    /**
     * Custom attributes names
     *
     * @author Benito Huerta <benito.huerta@nuvem.mx>
     * @created 2020/02/16
     * @return array
     */
    public function attributes()
    {
        return [
            RuleSpecialCost::DESCRIPCION => 'Descripción',
            RuleSpecialCost::PRIORIDAD => 'Prioridad'
        ];
    }

}
