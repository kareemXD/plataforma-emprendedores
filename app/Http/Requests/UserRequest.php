<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules =  [
            'name' => 'required|max:80',
            'last_name' => 'nullable|max:50',
            'second_last_name' => 'nullable|max:50',
        ];

        switch ($this->method()) {
            case "PUT":
            case "PATCH":
                $user = $this->route('user');
                $rules['email'] = "required|email|max:100|unique:users,email,{$user->id}";
                if (!is_null(request('password')) and !is_null(request('password_confirmation')))
                    $rules['password'] = 'min:8|max:20|confirmed';
                break;
            default:
                $rules['email'] = 'required|email|max:100|unique:users,email';
        }

        return $rules;
    }
}
