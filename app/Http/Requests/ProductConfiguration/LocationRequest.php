<?php

namespace App\Http\Requests\ProductConfiguration;

use Illuminate\Foundation\Http\FormRequest;

class LocationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     * @auth Iván Morales <ivan.morales@nuvem.mx>
     * @created 22/06/2021
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     * @auth Iván Morales <ivan.morales@nuvem.mx>
     * @created 22/06/2021
     * @return array
     */
    public function rules()
    {
        $rules = ['name' => 'required | string | max:50'];
        switch ($this->method()){
            case 'POST': 
                $rules['levels'] = 'required | array | min:1'; 
                $rules['levels.*.Nombre'] = 'required | string | max:45'; 
                break;
            case 'PUT' || 'PATCH': $rules['name'] = 'sometimes | required | string | max:50'; break;
        }

        return $rules;
    }
}
