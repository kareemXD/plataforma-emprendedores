<?php

namespace App\Http\Requests\ProductConfiguration;

use Illuminate\Foundation\Http\FormRequest;

class SectionRequest extends FormRequest
{
    
    /**
     * Determine if the user is authorized to make this request.
     * @auth Iván Morales <ivan.morales@nuvem.mx>
     * @created 25/06/2021
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     * @auth Iván Morales <ivan.morales@nuvem.mx>
     * @created 25/06/2021
     * @return array
     */
    public function rules()
    {
        $rules = [
            'sections' => 'required | array | min:1',
            'sections.*.Nombre' => 'required | string | max:45',
        ];

        return $rules;
    }
}
