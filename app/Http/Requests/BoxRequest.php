<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BoxRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules =  [
            'Descripcion' => 'required',
            'Alto' => 'required|numeric|max:99999999',
            'Ancho' => 'required|numeric|max:99999999',
            'Largo' => 'required|numeric|max:99999999',
            'Peso' => 'required|numeric|max:99999999'
        ];
           
        return $rules;
    }
}
