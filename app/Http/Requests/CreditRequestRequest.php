<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreditRequestRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          'Id_Cliente' => 'required',
          'Fecha_solicitud' => 'required',
          'Monto_a_incrementar' => 'required|numeric',
          'Monto_ponderado_maximo' => 'required|numeric',
        ];
    }
}
