<?php

namespace App\Http\Requests;

use App\CostConcept;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class CostConceptRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @author Benito Huerta <benito.huerta@nuvem.mx>
     * @created 2020/02/17
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @author Benito Huerta <benito.huerta@nuvem.mx>
     * @created 2020/02/17
     * @return array
     */
    public function rules()
    {
        return [
            CostConcept::DESCRIPCION => 'required|string|max:50',
            CostConcept::TIPO        => 'required|in:P,M,C',
            CostConcept::PORCENTAJE  => 'required_if:' . CostConcept::TIPO . ',P',
            CostConcept::MONTO       => 'required_if:' . CostConcept::TIPO . ',M,C', 
            CostConcept::ID_BASE_COSTO_CALCULADO => 'required_if:' . CostConcept::TIPO . ',C', 
        ];
    }

    /**
     * Custom attributes names
     *
     * @author Benito Huerta <benito.huerta@nuvem.mx>
     * @created 2020/02/17
     * @return array
     */
    public function attributes()
    {
        return [
            CostConcept::DESCRIPCION => 'Descripción',
            CostConcept::TIPO        => 'Tipo',
            CostConcept::PORCENTAJE  => 'Porcentaje',
            CostConcept::MONTO       => 'Monto',
            CostConcept::ID_BASE_COSTO_CALCULADO => 'Base de Cálculo'
        ];
    }

}
