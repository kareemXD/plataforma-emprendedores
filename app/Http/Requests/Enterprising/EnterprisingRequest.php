<?php

namespace App\Http\Requests\Enterprising;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class EnterprisingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     * @auth Iván Morales <ivan.morales@nuvem.mx>
     * @created 26/05/2021
     * @return array
     */
    public function rules()
    {
        return [
            'Nombre_tienda' => 'sometimes | required | max:80',
            'Razon_Soc' => 'sometimes | required | max:100',
            'Estatus' => 'sometimes | in:A,B,S',
            'RFC' => 'sometimes | required | max:13 | min:12',
            'Tipo_Cliente' => 'sometimes | required | max:20',
            'zAE_TipoEmprendedor_Id' => 'sometimes | required | numeric',
            'Calle_numero' => 'sometimes | required | max:100',
            'Colonia' => 'sometimes | required | max:100',
            'Codigo_Postal' => 'sometimes | required | max:10',
            'Ciudad' => 'sometimes | required | max:100',
            'Estado' => 'sometimes | required | max:45',
            'Clasifica' => 'sometimes | required | max:10',
            'Telefonos' => 'sometimes | required | max:30',
            'Celular' => 'sometimes | required | max:12 | min:10',
            'eMail' => 'sometimes | required | email | max:40',
            'Observaciones' => 'sometimes | required | max:200',
            'cerFile' => 'nullable | file | mimes:jpeg,jpg,png,pdf',
            'comDomFile' => 'nullable | file | mimes:jpeg,jpg,png,pdf',
            'ineAnFile' => 'nullable | file | mimes:jpeg,jpg,png,pdf',
            'ineRevFile' => 'nullable | file | mimes:jpeg,jpg,png,pdf',
            'actaFile' => 'nullable | file | mimes:jpeg,jpg,png,pdf',
            'comIngreFile' => 'nullable | file | mimes:jpeg,jpg,png,pdf',
            'poderIndeFile' => 'nullable | file | mimes:jpeg,jpg,png,pdf',
            'Poder_independiente' => 'sometimes | in:1,0',
            'Facebook' => 'sometimes | max:191',
            'Twitter' => 'sometimes | max:191',
            'Instagram' => 'sometimes | max:191',
            'zAE_EstatusCredito_Id' => 'sometimes | required | numeric',
            'Credito_actual' => 'sometimes | required | numeric | between:0,999999999999999.99',
            'Dias_credito' => 'sometimes | required | numeric | digits_between:0,9',
        ];
    }

    /**
     * Custom failed response validation with MessageBag
     *
     * @author  Iván Morales <ivan.morales@nuvem.mx>
     * @created 2021-05-21
     * @param  Illuminate\Contracts\Validation\Validator $validator
     * @return Illuminate\Http\Exceptions\HttpResponseException | \Illuminate\Http\Response
     */
    public function failedValidation(Validator $validator)
    {
        $response = response()->json([
            'statusCode' => 422,
            'message'    => 'Unprocessable Entity',
            'errors'     => $validator->errors()
        ], 422);

        throw new HttpResponseException($response);
    }
}
