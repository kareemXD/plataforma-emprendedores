<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LocationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "Descripcion" => "required|string|min:1|max:200",
            "Seccion" => "required|string|min:1|max:50",
            "Nivel" => "required|string|min:1|max:50",
            "zAE_Rak_Id" => "numeric|exists:medicadepot.zAE_Raks,Id"
        ];
    }
}
