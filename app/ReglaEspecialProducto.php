<?php

namespace App;

use App\ReglaEspecialProductoDetalle;
use Illuminate\Database\Eloquent\Model;

class ReglaEspecialProducto extends Model
{
    protected $connection = 'medicadepot';
    protected $table = 'ReglasEspecialesProductos';
    protected $primaryKey = 'Id_ReglaEspecialProducto';
    protected $guarded = ['Id_ReglaEspecialProducto'];
    public $timestamps = false;

    /**
     * ReglaEspecialProducto hasMany ReglaEspecialProductoDetalle
     * @author Giovanny González <giovanny.gonzalez@nuvem.mx>
     * created 16/03/2021
     * @params
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function productos()
    {
        return $this->hasMany(ReglaEspecialProductoDetalle::class, 'Id_ReglaEspecialProducto', 'Id_ReglaEspecialProducto');
    }
    /**
     *
     * Filling up default values on creating
     * @author Giovanny González <giovanny.gonzalez@nuvem.mx>
     * created 12/03/2021
     * @params
     * @return void
     */
    public static function boot()
    {
        parent::boot();
        self::creating(function(ReglaEspecialProducto $regla) {
            $regla->Estatus = 1;
        });
    }
}
