<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Configuration extends Model
{
    protected $fillable = [
        'id',
        'conekta_private_key',
        'conekta_public_key',
        'receipt_email',
        'receipt_phone_number',
        'legal_notice_of_privacy',
        'legal_terms_and_conditions',
        'pixel_facebook_tag',
        'google_analytics_tag',
        'facebook_chat_tag',
    ];

    /**
     * Upload a file to storage
     * @author Luis Pena  <luis.pena@nuvem.mx> | 05/11/2020
     * @param $file
     * @param $dir
     * @param $name
     * @optional $disk
     * @return $fullPath or null
     */
    public static function uploadFile($file, $dir, $name, $disk = 'local')
    {
        $filename = $name ."_". auth()->user()->id . "." . $file->getClientOriginalExtension();
        $path 	  = "/" . $dir;
        $fullpath = $dir . "/" . $filename;

        $upload = \Storage::disk($disk)->putFileAs(
            'public' . $path,
            $file,
            $filename
        );

        return $upload ? $fullpath : null;
    }

    /**
     * Update taxes config
     * @author Luis Pena  <luis.pena@nuvem.mx> | 05/11/2020
     * @param array $configTax
     * @param configuration $configuration
     */
    public function updateDigitalTaxes($configTax, $configuration){
        ConfigDigitalTax::updateOrCreate(
            ['configuration_id' => $configuration->id],
            [
                'iva_without_rfc' => $configTax['iva_without_rfc'],
                'iva_with_rfc' => $configTax['iva_with_rfc'],
                'irs_without_rfc' => $configTax['irs_without_rfc']
            ]
        );

    }
}
