<?php

namespace App\Models\Log;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    protected $connection = 'mysql';
    protected $table = 'logs';
    public $timestamps    = false;

    /**
    * Relation morphTo.
    *
    * @author José Vega <jose.vega@nuvem.mx>
    * @created 2020-04-26
    * @param
    * @return \Illuminate\Database\Eloquent\Relations\morphOne
    */
    public function loggable()
    {
        return $this->morphTo();
    }

    /**
    * Relation belongsTo with the User model (the user who created it returns).
    *
    * @author José Vega <jose.vega@nuvem.mx>
    * @created 2020-04-28
    * @param
    * @return \Illuminate\Database\Eloquent\Relations\belongsTo
    */
    public function userCreated()
    {
        return $this->belongsTo(User::class, 'created_by');
    }
    
    /**
    * Relation belongsTo with the User model (the user who updated it returns).
    *
    * @author José Vega <jose.vega@nuvem.mx>
    * @created 2020-04-28
    * @param
    * @return \Illuminate\Database\Eloquent\Relations\belongsTo
    */
    public function userUpdated()
    {
        return $this->belongsTo(User::class, 'updated_by');
    }
    
    /**
    * Relation belongsTo with the User model (the user who deleted it returns).
    *
    * @author José Vega <jose.vega@nuvem.mx>
    * @created 2020-04-28
    * @param
    * @return \Illuminate\Database\Eloquent\Relations\belongsTo
    */
    public function userDeleted()
    {
        return $this->belongsTo(User::class, 'deleted_by');
    }


}
