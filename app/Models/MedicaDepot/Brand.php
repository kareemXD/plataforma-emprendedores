<?php

namespace App\Models\MedicaDepot;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    protected $connection = 'medicadepot';
    protected $table = 'Marcas';
    protected $primaryKey = 'Id_Marca';
    protected $guarded = ['Id_Marca'];
    public $timestamps = false;
    protected $fillable = ['Marca', 'Fabricante', 'Activo'];

    /**
     * Active brands query.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-04-05
     * @param  \App\Http\Requests\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function scopeActive($query){
        return $query->where('Activo','=',1);
    }
}
