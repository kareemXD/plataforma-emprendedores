<?php

namespace App\Models\MedicaDepot;

use Illuminate\Database\Eloquent\Model;

class PositionContact extends Model
{
    protected $connection = 'medicadepot';
    protected $table = 'Puestos';
}
