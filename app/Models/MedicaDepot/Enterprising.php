<?php

namespace App\Models\Medicadepot;

use App\Traits\LogTrait;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Enterprising extends Model
{
    use LogTrait;
    protected $connection = 'medicadepot';
    protected $table = 'zAE_Emprendedores';
    protected $primaryKey = "Id";
    public $timestamps = false;
    protected $casts = [
        'Id_Cliente' => 'string',
        'Poder_independiente' => 'integer',
    ];

    protected $fillable = [
        'Id_Cliente',
        'zAE_TipoEmprendedor_Id', 
        'Calle_numero',
        'Colonia',
        'Codigo_Postal',
        'Ciudad',
        'Estado',
        'Nombre_tienda',
        'Facebook',
        'Twitter',
        'Instagram',
        'Celular',
        'Certificado_RFC',
        'Comprobante_domicilio_fiscal',
        'INE_anverso',
        'INE_reverso',
        'Acta_constitutiva',
        'Comprobante_ingreso',
        'Poder_independiente',
        'Archivo_poder_independiente',
        'Dias_credito',
        'zAE_EstatusCredito_Id',
        'Credito_actual',
    ];

    /**
     * Upload a file.
     * @auth Iván Morales <ivan.morales@nuvem.mx>
     * @date 2021-05-24
     * @param File $file, String $dir, String $name, String $actualFile
     * @return String
     */
    public function uploadFile($file, $dir, $name, $actualFile)
    { 
        $dir = str_replace(' ', '_', $dir);
        $nameUploaded = "{$name}_{$dir}_".\Str::random(3).".{$file->getClientOriginalExtension()}";
        $uploadedFileLogo = Storage::disk('enterprisings')
            ->putFileAs($dir, $file, $nameUploaded);
        if($uploadedFileLogo != "{$dir}/{$nameUploaded}"){
            throw new \Exception("No se subió correctamente el archivo {$name}");
        }
        //Delete the previous file
        if($actualFile){
            Storage::disk('enterprisings')->delete($actualFile);
        }
        return $uploadedFileLogo;
    }

    /**
     * Update the information of general config.
     * @auth Iván Morales <ivan.morales@nuvem.mx>
     * @date 2021-05-24
     * @param Request $request
     * @return Illuminate\Database\Eloquent\Model
     */
    public function updateEnterprising(Request $request)
    {
        $data = $request->all();
        if($request->hasFile('cerFile')){ 
            $uploadedFileCer = self::uploadFile($request->file('cerFile'),$this->Id_Cliente,'cerRFC',$this->Certificado_RFC);
            $data['Certificado_RFC'] = $uploadedFileCer;
        }
        if($request->hasFile('comDomFile')){ 
            $uploadedFileComDom = self::uploadFile($request->file('comDomFile'),$this->Id_Cliente,'comprobante_domicilio',$this->Comprobante_domicilio_fiscal);
            $data['Comprobante_domicilio_fiscal'] = $uploadedFileComDom;
        }
        if($request->hasFile('ineAnFile')){ 
            $uploadedFileIneAn = self::uploadFile($request->file('ineAnFile'),$this->Id_Cliente,'ine_anverso',$this->INE_anverso);
            $data['INE_anverso'] = $uploadedFileIneAn;
        }
        if($request->hasFile('ineRevFile')){ 
            $uploadedFileIneRev = self::uploadFile($request->file('ineRevFile'),$this->Id_Cliente,'ine_reverso',$this->INE_reverso);
            $data['INE_reverso'] = $uploadedFileIneRev;
        }
        if($request->hasFile('actaFile')){ 
            $uploadedFileActa = self::uploadFile($request->file('actaFile'),$this->Id_Cliente,'acta_constitutiva',$this->Acta_constitutiva);
            $data['Acta_constitutiva'] = $uploadedFileActa;
        }
        if($request->hasFile('comIngreFile')){ 
            $uploadedComIngreFile = self::uploadFile($request->file('comIngreFile'),$this->Id_Cliente,'comprobante_ingresos',$this->Comprobante_ingreso);
            $data['Comprobante_ingreso'] = $uploadedComIngreFile;
        }
        if($request->hasFile('poderIndeFile')){ 
            $uploadedFilePoder = self::uploadFile($request->file('poderIndeFile'),$this->Id_Cliente,'poder_independiente',$this->Archivo_poder_independiente);
            $data['Archivo_poder_independiente'] = $uploadedFilePoder;
        }
        $data['Dias_credito'] = floor($request->Dias_credito);
        $this->update($data);
        return $this;
    }
}
