<?php

namespace App\Models\MedicaDepot;

use App\Traits\LogTrait;
use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    use LogTrait;
    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;

    protected $connection = 'medicadepot';
    protected $table = 'Clientes';
    protected $primaryKey = 'Id_Cliente';
    protected $guarded = ['Id_Cliente'];
    protected $casts = [
        'Id_Cliente' => 'string'
    ];
    public $timestamps = false;

    protected $fillable = [
        'eMail',
        'Razon_Soc',
        'RFC',
        'Telefonos',
        'Observaciones',
        'Tipo_Cliente',
        'Clasifica',
        'Estatus'
    ];
    /**
     * Change the status of client
     *
     * @author Iván Morales <ivan.morales@nuvem.mx>
     * @created 2021/05/07
     * @return void
     */
    public function toggleStatus()
    {
        $this->eCommerce = ($this->eCommerce == self::STATUS_ACTIVE) ? self::STATUS_INACTIVE : self::STATUS_ACTIVE;
        $this->save();
        if(!$this->log()->exists()){
            $log = new Log;
            $log->deleted_by = auth()->id();
            $log->deleted_at_date = now();
            $this->log()->save($log);
        }else{
            $this->log()->update([
                'deleted_by' => auth()->id(),
                'deleted_at_date' => now()]);
        } 
    }
}
