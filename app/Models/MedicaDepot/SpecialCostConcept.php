<?php

namespace App\Models\MedicaDepot;

use App\CostConcept;
use App\Traits\LogTrait;
use App\Models\MedicaDepot\Group;
use Illuminate\Database\Eloquent\Model;

class SpecialCostConcept extends Model
{
    use LogTrait;
    
    protected $connection = 'medicadepot';
    protected $table = 'ConceptosCostosEspeciales';
    protected $primaryKey ="Id";
    protected $guarded = ['Id'];
    public $timestamps = false;

    protected $fillable = [
        'ConceptosCostos_Id', 'Grupo_Id', 'Tipo', 'Cantidad', 'BaseCostosCalculados_Id'
    ];

    protected $appends = [
        'type_name', 'global_concept_name'
    ];

    /**
     * Relation belongsTo to CostConcept
     * @author José Vega <jose.vega@nuvem.mx>
     * @created 2021-05-03
     * @param 
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function costConcept()
    {
        return $this->belongsTo(CostConcept::class, 'ConceptosCostos_Id');
    }

    /**
     * Relation belongsTo to group
     * @author José Vega <jose.vega@nuvem.mx>
     * @created 2021-05-04
     * @param 
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function group()
    {
        return $this->belongsTo(Group::class, 'Grupo_Id');
    }

    /**
     * Active concept costs query.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-05-03
     * @param  \App\Http\Requests\Request  $query
     * @return \Illuminate\Http\Response
     */
    public function scopeActive($query){
        return $query->where('Estatus','=',1);
    }

    /**
     * Get the name of the type
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-05-04
     * @param 
     * @return String
     */
    public function getTypeNameAttribute()
    {
        $namesTypes = [
            "M" => "Monto",
            "P" => "Porcentaje",
            "C" => "Calculado"
        ];

        return $namesTypes[$this->Tipo];
    }

    /**
     * Get the global concept name of the relation
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-05-04
     * @param 
     * @return String
     */
    public function getGlobalConceptNameAttribute()
    {
        return $this->costConcept->Descripcion;
    }
}
