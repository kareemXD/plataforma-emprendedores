<?php

namespace App\Models\MedicaDepot;
use Illuminate\Database\Eloquent\Model;

class Box extends Model
{
    protected $connection = 'medicadepot';
    protected $table = 'zAE_Cajas';
    protected $primaryKey ="Id";
    protected $guarded = ['Id'];
    public $timestamps = false;

    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    protected $fillable = [
        'Descripcion', 'Ancho', 'Alto', 'Largo', 'Peso', 'Estatus'
    ];


    public function scopeActive($query){
        return $query->where('Estatus',self::STATUS_ACTIVE);
    }

    /**
     * Change the model status
     *
     * @author Kareem Lorenzana <kareem.lorenzana@nuvem.mx>
     * @created 2021/04/26
     * @return void
     */
    public function toggleStatus()
    {
        $this->Estatus = $this->Estatus ? self::STATUS_INACTIVE : self::STATUS_ACTIVE;
        $this->save();
    }

}