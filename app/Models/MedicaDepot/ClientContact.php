<?php

namespace App\Models\MedicaDepot;

use App\Traits\LogTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use App\Traits\HasCompositePrimaryKeyTrait;

class ClientContact extends Model
{
    use HasCompositePrimaryKeyTrait;
    protected $connection = 'medicadepot';
    protected $table = 'ClieContactos';
    public $timestamps = false;
    protected $primaryKey = array('Id_Cliente', 'Id_Contacto');
    protected $casts = [
        'Id_Cliente' => 'string',
        'Id_Contacto' => 'integer',
    ];

    protected $fillable = [
        'Id_Cliente',
        'Id_Contacto', 
        'Nombre',
        'Id_Puesto',
        'EMail',
        'Telefono1',
        'TelMovil',
    ];
}
