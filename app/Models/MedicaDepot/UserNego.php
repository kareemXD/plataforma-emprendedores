<?php

namespace App\Models\MedicaDepot;

use App\User;
use Illuminate\Http\Request;
use App\Http\Requests\LoginNegoRequest;
use Illuminate\Database\Eloquent\Model;

class UserNego extends Model
{
    protected $connection = 'medicadepot2';
    protected $table = 'Usuarios';
    protected $primaryKey = 'Id_Usuario';
    protected $guarded = ['Id_Usuario'];
    public $timestamps = false;

    protected $casts = [
        'Id_Usuario' => 'string'
    ];

    /**
     * Returns user nego data and check if user exist in local db
     * @author Jose Vega
     * @created 2020-05-20
     * @param LoginNegoRequest $request
     * @return UserNego
     */
    public function existInDb(LoginNegoRequest $request){
        $userNego = $this::where(['Id_Usuario' => $request->Id_Usuario, 'Password' => $request->Password])->first();
        $userNego->Password = bcrypt($request->Password);

        if($userNego != null){
            $user = User::where('id_user_nego', $request->Id_Usuario)->first();
            $user != null ? $userNego->localDb = true : $userNego->localDb = false;
        }
        return $userNego;
    }

    /**
     * Update local user nego data from nego db
     * @author Jose Vega
     * @created 2020-05-27
     * @param Request $request
     * @return void
     */
    public function updateUserNegoData(Request $request){
        $user = User::where('email', '=', $request->email)->orWhere('id_user_nego', '=', $request->email)->firstOrFail();
        $user->id_user_nego != null ? $isNego = true : $isNego =false;
        if($isNego){
            $userNego = $this::where('Id_Usuario', '=', $user->id_user_nego)->firstOrFail();
            $user->Password = bcrypt($userNego->Password);
            $user->save();
        }
    }
}
