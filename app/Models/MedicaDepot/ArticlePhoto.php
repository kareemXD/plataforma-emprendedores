<?php

namespace App\Models\MedicaDepot;

use App\Product;
use Illuminate\Database\Eloquent\Model;

class ArticlePhoto extends Model
{
    protected $connection = 'medicadepot';
    protected $table = 'ArtFotos';
    public $timestamps = false;
    protected $fillable = ['Id_Prod1', 'Foto'];

    protected $casts = [
        'Id_Prod1' => 'string'
    ];
    
    public function product()
    {
        return $this->belongsTo(Product::class, 'Id_Prod1', 'Id_Prod1');
    }
}
