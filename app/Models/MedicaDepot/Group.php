<?php

namespace App\Models\MedicaDepot;

use App\Product;
use App\Traits\LogTrait;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use App\Models\MedicaDepot\SpecialCostConcept;

class Group extends Model
{
    use LogTrait;

    protected $connection = 'medicadepot';
    protected $table = 'Grupos';
    protected $primaryKey ="Id";
    protected $guarded = ['Id'];
    public $timestamps = false;

    protected $fillable = [
        'Nombre', 'Fecha_inicio', 'Fecha_fin', 'Prioridad'
    ];

    protected $appends = [
        'number_of_products'
    ];

    protected $hidden = ['pivot'];

    /**
     * Relation hasMany to SpecialCostConcept
     * @author José Vega <jose.vega@nuvem.mx>
     * @created 2021-05-04
     * @param
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function specialCostConcepts()
    {
        return $this->hasMany(SpecialCostConcept::class, 'Grupo_Id');
    }

    /**
     * Relation belongsToMany to Product
     * @author José Vega <jose.vega@nuvem.mx>
     * @created 2021-05-04
     * @param
     * @return \Illuminate\Database\Eloquent\Relations\belongsToMany
     */
    public function products()
    {
        return $this->belongsToMany(Product::class, 'ArticulosGrupos', 'Grupo_Id', 'Articulos_Id_Prod1');
    }

    /**
     * Active concept groups query.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-05-05
     * @param  \App\Http\Requests\Request  $query
     * @return \Illuminate\Http\Response
     */
    public function scopeActive($query){
        return $query->where('Estatus','=',1);
    }

    /**
     * Relates a group of products (articles) to a group.
     * @author José Vega <jose.vega@nuvem.mx>
     * @created 2021-05-04
     * @param  Request  $request
     * @return void
     */
    public function addProducts(Request $request)
    {
        $productsIds = [];
        foreach ($request->products as $product) {
            array_push($productsIds, $product['Id_Prod1']);
        }
        $this->products()->attach($productsIds);
    }

    /**
     * Get the number of products of a group
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-05-05
     * @param
     * @return int
     */
    public function getNumberOfProductsAttribute()
    {
        return $this->products()->count();
    }

    /**
     *
     * @auth José Vega <jose.vega@nuvem.mx>
     * @created 2021/05/06
     * @param Request $request
     */
    public function detachProducts(Request $request)
    {
        $productsToDelete = [];
        $requestJson = json_decode($request->products);
        foreach ($requestJson->products as $product) {
            array_push($productsToDelete, $product->Id_Prod1);
        }
        $this->products()->detach($productsToDelete);
    }
}
