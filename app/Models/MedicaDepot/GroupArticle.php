<?php

namespace App\Models\MedicaDepot;

use Illuminate\Database\Eloquent\Model;

class GroupArticle extends Model
{
    protected $connection = 'medicadepot';
    protected $table = 'ArticulosGrupos';
    public $timestamps = false;

    protected $fillable = [
        'Articulos_Id_Prod1', 'Grupo_Id'
    ];
}
