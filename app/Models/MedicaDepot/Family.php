<?php

namespace App\Models\MedicaDepot;

use Illuminate\Database\Eloquent\Model;

class Family extends Model
{
    protected $connection = 'medicadepot';
    protected $table = 'Familias';
    protected $primaryKey = 'Id_Familia';
    protected $guarded = ['Id_Familia'];
    public $timestamps = false;
    protected $fillable = ['Familia', 'Id_Categoria', 'Imagen', 'Activo'];

    /**
     * Active families query.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-04-05
     * @param  \App\Http\Requests\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function scopeActive($query){
        return $query->where('Activo','=',1);
    }
}
