<?php

namespace App\Models\MedicaDepot;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $connection = 'medicadepot';
    protected $table = 'Articulos';
    protected $primaryKey = 'Id_Prod1';
    protected $guarded = ['Id_Prod1'];
    public $timestamps = false;

    protected $casts = [
        'Id_Prod1' => 'string'
    ];

    /**
     * Active families query.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-04-05
     * @param  \App\Http\Requests\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function scopeActive($query){
        return $query->where('Activo','=',1);
    }
}
