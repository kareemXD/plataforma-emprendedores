<?php

namespace App\Models\MedicaDepot;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $connection = 'medicadepot';
    protected $table = 'Categorias';
    protected $primaryKey = 'Id_Categoria';
    protected $guarded = ['Id_Categoria'];
    public $timestamps = false;
    protected $fillable = ['Categoria', 'Ap_Presenta', 'Ap_Padecim', 'Ap_Substanc', 'Ap_Medicamento', 'Activo'];

    /**
     * Active families query.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-04-05
     * @param  \App\Http\Requests\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function scopeActive($query){
        return $query->where('Activo','=',1);
    }
}
