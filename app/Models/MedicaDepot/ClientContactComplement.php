<?php

namespace App\Models\MedicaDepot;

use App\Traits\LogTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use App\Traits\HasCompositePrimaryKeyTrait;

class ClientContactComplement extends Model
{
    use HasCompositePrimaryKeyTrait;
    protected $connection = 'medicadepot';
    protected $table = 'zE_Contactos';
    public $timestamps = false;
    protected $primaryKey = array('ClieContacto_Id_Cliente', 'ClieContacto_Id_Contacto');
    public $incrementing = false;
    protected $casts = [
        'ClieContacto_Id_Cliente' => 'string',
    ];

    protected $fillable = [
        'ClieContacto_Id_Cliente',
        'ClieContacto_Id_Contacto',
        'Tipo_contacto', 
        'Fecha_nacimiento',
        'Calle_numero',
        'Colonia',
        'Codigo_postal',
        'Ciudad',
        'Estado',
        'Observaciones',
    ];

    /**
     * Set the keys for a save update query.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function setKeysForSaveQuery(Builder $query)
    {
        $keys = $this->getKeyName();
        if(!is_array($keys)){
            return parent::setKeysForSaveQuery($query);
        }

        foreach($keys as $keyName){
            $query->where($keyName, '=', $this->getKeyForSaveQuery($keyName));
        }

        return $query;
    }

    /**
     * Get the primary key value for a save query.
     *
     * @param mixed $keyName
     * @return mixed
     */
    protected function getKeyForSaveQuery($keyName = null)
    {
        if(is_null($keyName)){
            $keyName = $this->getKeyName();
        }

        if (isset($this->original[$keyName])) {
            return $this->original[$keyName];
        }

        return $this->getAttribute($keyName);
    }
}
