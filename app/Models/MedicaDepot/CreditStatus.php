<?php

namespace App\Models\MedicaDepot;

use Illuminate\Database\Eloquent\Model;

class CreditStatus extends Model
{
    protected $connection = 'medicadepot';
    protected $table = 'zE_EstatusSolicitudTransacciones';
}
