<?php

namespace App\Models\MedicaDepot;

use Illuminate\Database\Eloquent\Model;

class MedicalSpecialty extends Model
{
    protected $connection = 'medicadepot';
    protected $table = 'Especialidades_Medicas';
    protected $primaryKey = 'Id_EspMed';
    protected $guarded = ['Id_EspMed'];
    public $timestamps = false;
    protected $fillable = ['Especialidad', 'Activo'];

    /**
     * Active specialties query.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-04-05
     * @param  \App\Http\Requests\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function scopeActive($query){
        return $query->where('Activo','=',1);
    }
}
