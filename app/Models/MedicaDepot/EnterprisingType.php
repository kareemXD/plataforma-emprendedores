<?php

namespace App\Models\MedicaDepot;

use Illuminate\Database\Eloquent\Model;

class EnterprisingType extends Model
{
    protected $connection = 'medicadepot';
    protected $table = 'zE_TiposEmprendedores';
}
