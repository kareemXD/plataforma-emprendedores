<?php

namespace App\Models\MedicaDepot;

use App\Traits\LogTrait;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Configuration extends Model
{
    use LogTrait;
    
    protected $connection = 'medicadepot';
    protected $table = 'zAP_ConfGeneral';
    protected $primaryKey = 'Id';
    protected $guarded = ['Id'];
    public $timestamps = false;
    protected $fillable = [
        'AmbienteActivo', 
        'LlavePublicaProduccion', 
        'LlavePrivadaProduccion',
        'LlavePublicaPruebas',
        'LlavePrivadaPruebas',
        'ClaveApi',
        'ArchivoCertificadoSelloFiscalCer',
        'ArchivoCertificadoSelloFiscalKey',
        'PassCertificadoSelloFiscalKey',
        'UsoCFDI',
        'RFC',
        'RazonSocial',
        'CalleNum',
        'Colonia',
        'CP',
        'Ciudad',
        'Estado',
        'Logotipo',
        'CorreoFacturacionEmprendedores',
        'CorreoPlataforma',
        'CorreoAtencionEmprendedores',
        'CorreoNotificaciones',
        'NumeroAtencionEmprendedores',
        'CodigoSeguimientoPixel',
        'CodigoSeguimientoGoogleADS',
        'CodigoSeguimientoGoogleAnalytics',
        'CodigoChatFacebook',
        'Estatus',
        'zAE_ConfiguracionSkydropx_Id'
    ];

    /**
     *
     * Filling up default values on creating.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-04-29
     * @params
     * @return void
     */
    public static function boot()
    {
        parent::boot();
        self::creating(function(Configuration $configuration) {
            $configuration->Estatus = 1;
        });
    }

    /**
     *
     * Returns the mapped data depending on the type of data sent: platform, billing, general or ads.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-04-29
     * @params
     * @return void
     */
    public function mapOutData(Request $request)
    {
        switch ($request->dataType) {
            case 'platform':
                return [
                    'AmbienteActivo' => $request->activeEnvironment,
                    'LlavePublicaProduccion' => $request->productivePublicKey != null ? $request->productivePublicKey : null,
                    'LlavePrivadaProduccion' => $request->productivePrivateKey != null ? $request->productivePrivateKey : null,
                    'LlavePublicaPruebas' => $request->testPublicKey != null ? $request->testPublicKey : null,
                    'LlavePrivadaPruebas' => $request->testPrivateKey != null ? $request->testPrivateKey : null,
                    'ClaveApi' => $request->apiKey != null ? $request->apiKey : null
                ];
                break;
            case 'billing':
                $files = $this->saveBillingFiles($request);
                return [
                    'ArchivoCertificadoSelloFiscalCer' => $files[0] != null ? $files[0] : null,
                    'ArchivoCertificadoSelloFiscalKey' => $files[1] != null ? $files[1] : null,
                    'PassCertificadoSelloFiscalKey' => $request->keyPassword != null ? $request->keyPassword : null, 
                    'UsoCFDI' => $request->cfdi != null ? $request->cfdi : null,
                    'RFC' => $request->rfc != null ? $request->rfc : null,
                    'RazonSocial' => $request->socialReason != null ? $request->socialReason : null,
                    'CalleNum' => $request->streetAndNumber != null ? $request->streetAndNumber : null,
                    'Colonia' => $request->suburb != null ? $request->suburb : null,
                    'CP' => $request->cp != null ? $request->cp : null,
                    'Ciudad' => $request->town != null ? $request->town : null,
                    'Estado' => $request->state != null ? $request->state : null
                ];
                break;
            case 'general':
                $logo = $this->saveLogo($request);
                return [
                    'Logotipo' => $logo != null ? $logo : null,
                    'CorreoFacturacionEmprendedores' => $request->billingEmail != null ? $request->billingEmail : null,
                    'CorreoPlataforma' => $request->platformEmail != null ? $request->platformEmail : null,
                    'CorreoAtencionEmprendedores' => $request->entrepreneurServiceEmail != null ? $request->entrepreneurServiceEmail : null,
                    'CorreoNotificaciones' => $request->notificationsEmail != null ? $request->notificationsEmail : null,
                    'NumeroAtencionEmprendedores' => $request->entrepreneurServiceTel != null ? $request->entrepreneurServiceTel : null,
                ];
                break; 
                
            case 'ads':
                return [
                    'CodigoSeguimientoPixel' => $request->pixelCode != null ? $request->pixelCode : null,
                    'CodigoSeguimientoGoogleADS' => $request->googleAdsCode != null ? $request->googleAdsCode : null,
                    'CodigoSeguimientoGoogleAnalytics' => $request->googleAnalythicsCode != null ? $request->googleAnalythicsCode : null,
                    'CodigoChatFacebook' => $request->chatFacebookCode != null ? $request->chatFacebookCode : null,
                ];
                break;     
            
            default:
            break;
        }
    }

    /**
     * Save or update the billing files.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-04-29
     * @param  \Illuminate\Http\Request  $request
     * @return Array
     */
    public function saveBillingFiles(Request $request)
    {   
        $files = [];
        if(is_null($this->ArchivoCertificadoSelloFiscalCer) && is_null($this->ArchivoCertificadoSelloFiscalKey)){
            if($request->exists('keyFile') && $request->exists('cerFile')){
                $cerFileName = Storage::disk('configurations')->putFile(null, $request->cerFile);
                $keyFileName = Storage::disk('configurations')->putFile(null, $request->keyFile);
                array_push($files, '../storage/configurations/'.$cerFileName, '../storage/configurations/'.$keyFileName);
                return $files;
            }else{
                return array_push($files, null, null);
            }
        }else{
            if(is_null($request->keyFile) && is_null($request->cerFile)){
                array_push($files, $this->ArchivoCertificadoSelloFiscalCer, $this->ArchivoCertificadoSelloFiscalKey);
                return $files;
            }else if(is_null($request->keyFile) && !is_null($request->cerFile)){
                $cerFileName = explode('/', $this->ArchivoCertificadoSelloFiscalCer)[3];
                Storage::disk('configurations')->delete($cerFileName);
                $cerFileName = Storage::disk('configurations')->putFile(null, $request->cerFile);
                array_push($files, '../storage/configurations/'.$cerFileName, $this->ArchivoCertificadoSelloFiscalKey);
                return $files;
            }else if(!is_null($request->keyFile) && is_null($request->cerFile)){
                $keyFileName = explode('/', $this->ArchivoCertificadoSelloFiscalKey)[3];
                Storage::disk('configurations')->delete($keyFileName);
                $keyFileName = Storage::disk('configurations')->putFile(null, $request->keyFile);
                array_push($files, $this->ArchivoCertificadoSelloFiscalCer, '../storage/configurations/'.$keyFileName);
                return $files;
            }else{
                $cerFileName = explode('/', $this->ArchivoCertificadoSelloFiscalCer)[3];
                $keyFileName = explode('/', $this->ArchivoCertificadoSelloFiscalKey)[3];
                Storage::disk('configurations')->delete($cerFileName);
                Storage::disk('configurations')->delete($keyFileName);
                $cerFileName = Storage::disk('configurations')->putFile(null, $request->cerFile);
                $keyFileName = Storage::disk('configurations')->putFile(null, $request->keyFile);
                array_push($files, '../storage/configurations/'.$cerFileName, '../storage/configurations/'.$keyFileName);
                return $files;
            }
        }
    }

    /**
     * Save or update the general data logo.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-04-29
     * @param  \Illuminate\Http\Request  $request
     * @return string
     */
    public function saveLogo(Request $request)
    {   
        $logoFileName = '';
        if(is_null($this->Logotipo)){
            if($request->exists('logo')){
                $logoFileName = Storage::disk('configurations')->putFile(null, $request->logo);
                return '../storage/configurations/'.$logoFileName;
            }else{
                return null;
            }
        }else{
            if(is_null($request->logo)){
                return $this->Logotipo;
            }else{
                $logoFileName = explode('/', $this->Logotipo)[3];
                Storage::disk('configurations')->delete($logoFileName);
                $logoFileName = Storage::disk('configurations')->putFile(null, $request->logo);
                return '../storage/configurations/'.$logoFileName;
            }
        }
    }
}
