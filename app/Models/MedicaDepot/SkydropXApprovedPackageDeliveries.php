<?php

namespace App\Models\MedicaDepot;

use Illuminate\Database\Eloquent\Relations\Pivot;

class SkydropXApprovedPackageDeliveries extends Pivot {

    const CONNECTION  = 'medicadepot';
    const TABLE       = 'zAE_PaqueteriasAprobadas';

    const SKYDROPX_ID = 'zAE_ConfiguracionSkydropx_Id';
    const PACKAGE_DELIVERY_ID = 'zAE_Paqueteria_Id';

    protected $connection = self::CONNECTION;
    protected $table      = self::TABLE;

     /**
     * The attributes that are mass assignable.
     * 
     * @author Benito Huerta <benito.huerta@nuvem.mx>
     * @created 2021/06/11
     * @var array
     */
    protected $fillable = [
        self::SKYDROPX_ID,
        self::PACKAGE_DELIVERY_ID,
    ];
}