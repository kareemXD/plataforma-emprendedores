<?php

namespace App\Models\MedicaDepot;

use Illuminate\Database\Eloquent\Model;

class SkydropXPackageDeliveries extends Model {

    const CONNECTION  = 'medicadepot';
    const TABLE       = 'zAE_Paqueterias';
    const PRIMARYKEY  = 'Id';

    const NOMBRE = 'Nombre';

    protected $connection = self::CONNECTION;
    protected $table      = self::TABLE;
    protected $primaryKey = self::PRIMARYKEY;
    public $timestamps    = false;

     /**
     * The attributes that are mass assignable.
     * 
     * @author Benito Huerta <benito.huerta@nuvem.mx>
     * @created 2021/06/11
     * @var array
     */
    protected $fillable = [
        self::NOMBRE
    ];

    /**
     * Relation BelongsToMany with SkydropXConfiguration
     *
     * @author Benito Huerta <benito.huerta@nuvem.mx>
     * @created 11/06/2021
     * @return Illuminate\Database\Eloquent\BelongsToMany
     */
    public function skydropXConfiguration() 
    {
        return $this->belongsToMany(
                                    SkydropXConfiguration::class, 
                                    SkydropXApprovedPackageDeliveries::TABLE, 
                                    SkydropXApprovedPackageDeliveries::PACKAGE_DELIVERY_ID, 
                                    SkydropXApprovedPackageDeliveries::SKYDROPX_ID
                                )
                            ->using(SkydropXApprovedPackageDeliveries::class);
    }
}