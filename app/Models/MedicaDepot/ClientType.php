<?php

namespace App\Models\MedicaDepot;

use Illuminate\Database\Eloquent\Model;

class ClientType extends Model
{
    protected $connection = 'medicadepot';
    protected $table = 'TipoCliente';
}
