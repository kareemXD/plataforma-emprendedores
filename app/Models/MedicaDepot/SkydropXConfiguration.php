<?php

namespace App\Models\MedicaDepot;

use Illuminate\Database\Eloquent\Model;

class SkydropXConfiguration extends Model {

    const CONNECTION  = 'medicadepot';
    const TABLE       = 'zAE_ConfiguracionSkydropx';
    const PRIMARYKEY  = 'Id';

    const AMBIENTE_ACTUAL = 'AmbienteActual';
    const CLAVE_API = 'ClaveApi';
    const ESTATUS = 'Estatus';

    protected $connection = self::CONNECTION;
    protected $table      = self::TABLE;
    protected $primaryKey = self::PRIMARYKEY;
    public $timestamps    = false;

     /**
     * The attributes that are mass assignable.
     * 
     * @author Benito Huerta <benito.huerta@nuvem.mx>
     * @created 2021/06/11
     * @var array
     */
    protected $fillable = [
        self::AMBIENTE_ACTUAL,
        self::CLAVE_API,
        self::ESTATUS,
    ];

    /**
     * Get the configuration with Approved Package Deliveries
     * 
     * @author Benito Huerta <benito.huerta@nuvem.mx>
     * @created 2021/06/11
     * @return Illuminate\Database\Eloquent\QueryBuilder
     */
    public function scopeWithApprovedPackageDeliveries($query, $id)
    {
        return $query->with('approvedPackageDeliveries')
                    ->where(self::PRIMARYKEY, $id);
    }

    /**
     * Relation BelongsToMany with SkydropXPackageDeliveries
     *
     * @author Benito Huerta <benito.huerta@nuvem.mx>
     * @created 11/06/2021
     * @return Illuminate\Database\Eloquent\BelongsToMany
     */
    public function approvedPackageDeliveries()
    {
        return $this->belongsToMany(
                                    SkydropXPackageDeliveries::class, 
                                    SkydropXApprovedPackageDeliveries::TABLE, 
                                    SkydropXApprovedPackageDeliveries::SKYDROPX_ID, 
                                    SkydropXApprovedPackageDeliveries::PACKAGE_DELIVERY_ID
                                )
                                ->using(SkydropXApprovedPackageDeliveries::class);
    }
}