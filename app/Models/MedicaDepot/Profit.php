<?php

namespace App\Models\MedicaDepot;

use App\Product;
use Illuminate\Database\Eloquent\Model;

class Profit extends Model
{
    protected $connection = 'medicadepot';
    protected $table = 'zEC_Articulos';
    public $timestamps = false;
    protected $fillable = ['Id_Prod1', 'Utilidad'];

    /**
     * Relation belongsTo to Product
     * @author José Vega <jose.vega@nuvem.mx>
     * @created 2021/04/19
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
