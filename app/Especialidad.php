<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Especialidad extends Model
{
    protected $connection = 'medicadepot';
    protected $table = 'Especialidades_Medicas';
    public $timestamps = false;

    /**
     *
     * select options
     * @author Giovanny González <giovanny.gonzalez@nuvem.mx>
     * created 13/05/2021
     * @params
     * @return void
     */
    public static function filterSelectOptions() {
        $allOption = new Especialidad();
        $allOption->Id_EspMed = -1;
        $allOption->Especialidad = 'TODAS';
        return self::select('Id_EspMed', 'Especialidad')->orderBy('Especialidad')->get()
        ->prepend($allOption);
    }
}
