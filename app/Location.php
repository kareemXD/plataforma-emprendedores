<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    protected $connection = 'medicadepot';
    protected $table = 'zAE_Ubicaciones';
    protected $primaryKey = 'Id';
    protected $fillable = [
        'Descripcion', 'Seccion', 'Nivel', 'zAE_Rak_Id', 'Estatus'
    ];
    
    public $timestamps = false;

    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    /**
     * ShipmentCost belongsTo Rack
     * @author Kareem Lorenzana <kareem.lorenzana@nuvem.mx>
     * created 2021-05-27
     * @params
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function rack()
    {
        return $this->belongsTo(Rack::class, 'zAE_Rak_Id');
    }

    /**
     * get active locations
     * @author Kareem Lorenzana <@kareem.lorenzana@nuvem.mx>
     * @created 2021-05-27
     * @param  $query
     * @return $query
     */
    public function scopeActive($query){
        return $query->where('Estatus','=',self::STATUS_ACTIVE);
    }

    /**
     *
     * Filling up default values on creating
     * @author Kareem Lorenzana <kareem.lorenzana@nuvem.mx>
     * created 2021-05-27
     * @params
     * @return void
     */
    public static function boot()
    {
        parent::boot();
        self::creating(function(Location $location) {
            $location->Estatus = self::STATUS_ACTIVE;
        });
    }

     /**
     * Toggles status
     * @author Kareem Lorenzana <@kareem.lorenzana@nuvem.mx>
     * @created 2020-05-27
     * @param  
     * @return void
     **/
    public function toggleStatus()
    {
        $this->Estatus = $this->Estatus ? self::STATUS_INACTIVE : self::STATUS_ACTIVE;
        $this->save();
    }

}
