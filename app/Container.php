<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Container extends Model
{
    protected $connection = 'medicadepot';
    protected $table = 'zAE_Contenedores';
    protected $primaryKey = 'Id';
    protected $fillable = [
        'Descripcion', 'Estatus','Codigo_barras'
    ];
    
    public $timestamps = false;

    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    /**
     * get active containers
     * @author Kareem Lorenzana <@kareem.lorenzana@nuvem.mx>
     * @created 2021-05-28
     * @param  $query
     * @return $query
     */
    public function scopeActive($query){
        return $query->where('Estatus','=',self::STATUS_ACTIVE);
    }

    /**
     *
     * Filling up default values on creating
     * @author Kareem Lorenzana <kareem.lorenzana@nuvem.mx>
     * created 2021-05-28
     * @params
     * @return void
     */
    public static function boot()
    {
        parent::boot();
        self::creating(function(Container $container) {
            $container->Estatus = self::STATUS_ACTIVE;
        });
    }

     /**
     * Toggles status
     * @author Kareem Lorenzana <@kareem.lorenzana@nuvem.mx>
     * @created 2020-05-28
     * @param  
     * @return void
     **/
    public function toggleStatus()
    {
        $this->Estatus = $this->Estatus ? self::STATUS_INACTIVE : self::STATUS_ACTIVE;
        $this->save();
    }
}
