<?php

use Illuminate\Support\Facades\Log;
Route::middleware('auth:sanctum')->get('/user', 'Api\Sanctum\SanctumController@show');

Route::post('login', 'Api\AuthController@login');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');
Route::post('user-nego-exists', 'Api\AuthNegoController@userExists');
Route::post('user-nego-validates', 'Api\AuthNegoController@isUserNego');

Route::group([
    'namespace' => 'Auth',
    'middleware' => 'api',
    'prefix' => 'password'
], function () {
    Route::post('create', 'PasswordResetController@create');
    Route::get('find/{token}', 'PasswordResetController@find');
    Route::post('reset', 'PasswordResetController@reset');
});

Route::group(['as' => 'api.', 'middleware' => 'auth:api'], function () {
    Route::post('logout', 'Api\AuthController@logout');
    Route::get('my-profile', 'Api\AuthController@myProfile');


    Route::apiResources([
        'users' => 'Api\User\UserController',
        'roles' => 'Api\Role\RoleController',
    ]);
});

Route::group(['prefix' => 'v1', 'middleware' => 'auth:api'], function() {
    Route::apiResource('/articles', 'Api\Article\ArticleController')->except(['create']);
});
Route::group([
    'middleware' => 'verify.token',
    'name' => 'medicadepotapi',
    'prefix' => 'medicadepotapi'
], function () {
    Route::get('categories', 'Api\MedicaDepot\CategoryController@index');
    Route::get('presentations', 'Api\MedicaDepot\PresentationController@index');
    Route::get('substances', 'Api\MedicaDepot\SubstanceController@index');
    Route::get('sufferings', 'Api\MedicaDepot\SufferingController@index');
});

Route::post('products/{product}', 'Api\ProductConfiguration\Product\ProductController@update');
Route::get('products-filter','Api\ProductConfiguration\Product\ProductController@filterProduct');
Route::get('products-special-filters', 'Api\ProductConfiguration\Product\ProductController@searchProducts');
Route::apiResources([
    'users' => 'Api\User\UserController',
    'roles' => 'Api\Role\RoleController',
    'products' => 'Api\ProductConfiguration\Product\ProductController',
    'global-costs-configurators' => 'Api\ProductConfiguration\GlobalCostConfigurator\GlobalCostConfiguratorController',
    'special-costs-configurators' => 'Api\ProductConfiguration\SpecialCostConfigurator\SpecialCostConfiguratorController',
    'view-cost-entrepreneurs' => 'Api\ProductConfiguration\ViewCostEntrepreneurial\ViewCostEntrepreneurialController',
    'enterprisings' => 'Api\Enterprising\EnterprisingController',
]);

Route::get('/products/add-location/get-rack-select-options', 'Api\ProductConfiguration\Product\ProductController@getRackSelectsOptions');
Route::get('/products/add-location/rack/{rack}/get-nivel-select-options', 'Api\ProductConfiguration\Product\ProductController@getNivelSelectsOptions');
Route::get('/products/add-location/nivel/{nivel}/get-seccion-select-options', 'Api\ProductConfiguration\Product\ProductController@getSeccionSelectsOptions');

/**ENTERPRISINGS**/
Route::group(['prefix' => 'enterprisings', 'middleware' => 'auth', 'namespace' => 'Api'], function () {
  Route::get('/products-assigning/filters-options', 'Enterprising\EnterprisingController@productAssigningFiltersOptions');
  Route::post('/{enterprising}/products-assigning', 'Enterprising\EnterprisingController@asignedProducts');
  Route::post('/{enterprisingId}/products-assigning/update-exceptions', 'Enterprising\EnterprisingController@updateExceptions');
});

Route::get('/enterprisings/contacts/{clientId}', 'Api\Enterprising\ContactController@index');
Route::get('/enterprisings/branches/{clientId}', 'Api\Enterprising\EnterprisingController@getBranches');
Route::get('/enterprising-info/{clientId}', 'Api\Enterprising\EnterprisingController@getEnterprisingInfo')->name('enterprising-info');
Route::post('/update-enterprising/{clientId}', 'Api\Enterprising\EnterprisingController@updateEnterprising')->name('update-enterprising');
Route::post('/store-contact/{clientId}', 'Api\Enterprising\ContactController@store')->name('store-contact');
Route::post('/update-contact/{clientId}/{contactId}', 'Api\Enterprising\ContactController@update')->name('update-contact');

Route::get('export-costs', 'Api\ProductConfiguration\ViewCostEntrepreneurial\ViewCostEntrepreneurialController@exportCosts');

Route::resource('rules-configurators', 'Api\ProductConfiguration\RuleConfigurator\RuleConfiguratorController', [
    'parameters' => [
        'rules-configurators' => 'rule'
    ]
]);

Route::resource('global-utilities', 'Api\ProductConfiguration\GlobalCostConfigurator\GlobalUtilityController', [
    'parameters' => [
        'global-utilities' => 'cost'
    ],
    'only' => [
        'index',
        'update'
    ]
]);

Route::get('special-costs-settings', 'Api\ProductConfiguration\SpecialCostConfigurator\SpecialCostConfiguratorController@getSettingsToCreateSpecialCost');
Route::get('special-costs-products', 'Api\ProductConfiguration\SpecialCostConfigurator\SpecialCostConfiguratorController@searchProducts');
Route::get('special-specific-products', 'Api\ProductConfiguration\SpecialCostConfigurator\SpecialCostConfiguratorController@searchSpecificProducts');
Route::get('special-costs-products-edit', 'Api\ProductConfiguration\SpecialCostConfigurator\SpecialCostConfiguratorController@getProductsToEdit');
Route::get('rules-configurators-priority', 'Api\ProductConfiguration\RuleConfigurator\RuleConfiguratorController@existPriority');

Route::group(['prefix' => 'roles'], function() {
    Route::get('/{role}/permissions', 'Api\Role\RolePermissionController@index');
    Route::post('/{role}/sync-permissions', 'Api\Role\RolePermissionController@sync');
});

Route::group(['prefix' => 'configurations'], function() {
    Route::resource('special-costs-groups', 'Api\Configuration\GroupController', [
        'parameters' => [
            'special-costs-groups' => 'group'
        ],
    ]);
    Route::resource('special-costs-concepts', 'Api\Configuration\SpecialCostConceptController', [
        'parameters' => [
            'special-costs-concepts' => 'specialCostConcept'
        ],
    ]);
    Route::post('special-costs-groups-articles', 'Api\Configuration\GroupController@addProductsToGroup');
    Route::get('special-costs-groups-concepts/{group}', 'Api\Configuration\GroupController@getProductsFromGroup');
    Route::get('special-costs-groups-edit/{group}', 'Api\Configuration\GroupController@getProductsToEdit');
    Route::delete('special-costs-groups-removes-articles/{group}', 'Api\Configuration\GroupController@detachProducts');
});

Route::group(['prefix' => 'product', 'middleware' => 'auth'], function() {
    Route::get('suppliers', 'Api\ProductConfiguration\ProductConfigurationController@listOfSuppliers');
    Route::get('categories', 'Api\ProductConfiguration\ProductConfigurationController@listOfCategories');
    Route::get('families', 'Api\ProductConfiguration\ProductConfigurationController@listOfFamilies');
    Route::get('marks', 'Api\ProductConfiguration\ProductConfigurationController@listOfMarks');
    Route::get('specialties', 'Api\ProductConfiguration\ProductConfigurationController@listOfSpecialties');
    Route::get('status', 'Api\ProductConfiguration\ProductConfigurationController@listOfStatus');
    Route::get('countries', 'Api\ProductConfiguration\ProductConfigurationController@listOfCountries');
    Route::get('packaging', 'Api\ProductConfiguration\ProductConfigurationController@listOfPackaging');
    Route::post('profit/{product}', 'Api\ProductConfiguration\Product\ProductController@storeProfit');
    Route::put('profit/{product}', 'Api\ProductConfiguration\Product\ProductController@updateProfit');
    Route::get('search-product', 'Api\ProductConfiguration\Product\ProductController@searchProduct');
});

Route::apiResource(
    'shipping-configurations', 'Api\ShippingConfiguration\ShippingConfigurationController', [
    'parameters' => [
        'shipping_configuration' => 'shippingConfiguration'
    ]
])
->except(['create', 'show', 'edit']);

Route::apiResource('boxes', 'Api\ShippingConfiguration\Box\BoxController', [
    'parameters' => [
        'boxes' => 'box'
    ]
])->except('create', 'edit','show');

Route::apiResource('shipment-cost-configurations', 'Api\ProductConfiguration\ShipmentCostConfiguration\ShipmentCostConfigurationController', [
    'parameters' => [
        'shipment_cost_configuration' => 'shipmentCostConfiguration'
    ]
])->except(['create', 'show', 'edit']);

Route::group(['prefix' => 'shipment-cost-configuration'], function() {
    Route::get('shipment-cost-types', 'Api\ProductConfiguration\ShipmentCostConfiguration\ShipmentCostConfigurationController@listShipmentCostTypes');
});

Route::resource('configurations', 'Api\Administration\Configuration\ConfigurationController', [
    'middleware' => 'auth'
]);

Route::group(['middleware' => 'auth', 'prefix' => 'credit-requests', 'namespace' => 'Api'], function () {
  Route::get('/enterprisings/{enterprising}', 'CreditRequestController@enterprisingRequests');
  Route::get('/enterprisings/{enterprising}/export', 'CreditRequestController@enterprisingRequestsExport');
  Route::apiResource('', 'CreditRequestController', [
    'only' => ['index']
  ]);
});

Route::apiResource('locations', 'Api\ProductConfiguration\Location\LocationController',[
    'parameters' => [
        'locations' => 'location'
    ]
])->except(['create', 'show', 'edit']);

Route::get('locations/racks','Api\ProductConfiguration\Location\LocationController@getRacks')->name('api.locations.racks');

Route::apiResource('containers', 'Api\ProductConfiguration\Container\ContainerController',[
    'parameters' => [
        'containers' => 'container'
    ]
])->except(['create', 'show', 'edit']);

Route::group(['prefix' => 'picking', 'namespace' => 'Api', 'middleware' => 'auth'], function () {
    Route::patch('/incomplete-assortment-reason', 'PickingController@incompleteAssortment');
    Route::patch('/turn-order-to-packing/{orderId}', 'PickingController@turnOrderToPacking');
    Route::patch('/products-replaced', 'PickingController@productsReplaced');
    Route::apiResource('/', 'PickingController', [
        'only' => ['index', 'show', 'store'],
        'parameters' => ['' => 'orderId']
    ]);
});

Route::apiResource('locations-new', 'Api\ProductConfiguration\Location\LocationNewController')->except(['create', 'edit']);
Route::post('locations-new/store-new-level', 'Api\ProductConfiguration\Location\LocationNewController@storeNewLevel')->name('locations.store-level');
Route::delete('locations-new/delete-level/{id}', 'Api\ProductConfiguration\Location\LocationNewController@deleteLevel')->name('locations.delete-level');
Route::get('locations-new/sections/{rackId}', 'Api\ProductConfiguration\Location\LocationNewController@getSections')->name('locations.get-sections');
Route::delete('locations-new/sections/{id}', 'Api\ProductConfiguration\Location\LocationNewController@deleteSection')->name('locations.delete-sections');
Route::post('locations-new/sections/{levelId}', 'Api\ProductConfiguration\Location\LocationNewController@storeSections')->name('locations.store-sections');

Route::group(['prefix' => 'packing', 'namespace' => 'Api', 'middleware' => 'auth'], function () {
    Route::apiResource('/', 'PackingController', [
        'only' => ['index'],
        'parameters' => ['' => 'orderId']
    ]);
});


