<?php

use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\Api\ShippingConfiguration\Box\BoxController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('login', [HomeController::class, 'index']);
Route::get('/', function(){
    return view('home');
})->middleware('auth');

Auth::routes();

Route::middleware(['auth'])->group(function () {

    Route::get('/shippings-config-user','ShippingConfigController@shippingsConfigUser');

    Route::resource('/shippings-config','ShippingConfigController')->only(['index','store','update','destroy']);
    //Route::resource('configurations', 'ConfigurationController',['only' => ['update','index']]);

    /*
    * Configuration Special Prices Routes
    */
    Route::name('special_prices.')->group(function () {
        Route::get('/special_prices', [App\Http\Controllers\SpecialPricesController::class, 'index'])->name('index');
        Route::post('/special_prices/show', [App\Http\Controllers\SpecialPricesController::class, 'show'])->name('show');
        Route::post('/special_prices/store', [App\Http\Controllers\SpecialPricesController::class, 'store'])->name('store');
        Route::get('/special_prices/create', [App\Http\Controllers\SpecialPricesController::class, 'create'])->name('create');
        Route::post('/special_prices/delete', [App\Http\Controllers\SpecialPricesController::class, 'delete'])->name('delete');
        Route::post('/special_prices/destroy', [App\Http\Controllers\SpecialPricesController::class, 'destroy'])->name('destroy');
        Route::post('/special_prices/search', [App\Http\Controllers\SpecialPricesController::class, 'searchProducts'])->name('search');
    });

    /*Base prices Routes
    *
    */
    Route::name('base-prices.')->group(function () {
        Route::get('/base-prices', [App\Http\Controllers\BasePriceController::class, 'index'])->name('index');
    });

    Route::resource('rules-configurator', 'RuleConfiguratorController', [
        'parameters' => [
            'rules-configurator' => 'rule'
        ],
        'except' => [
            'show'
        ]
    ]);

    Route::resource('global-costs-configurator', 'GlobalCostConfiguratorController', [
        'parameters' => [
            'global-costs-configurator' => 'cost'
        ],
        'except' => [
            'show'
        ]
    ]);

    Route::resource('global-utility', 'GlobalUtilityController', [
        'parameters' => [
            'global-utility' => 'cost'
        ],
        'only' => [
            'index',
            'update'
        ]
    ]);
    
    
    Route::resource('box', 'BoxController', [
        'except' => [
            'show'
        ]
    ]);

    Route::resource('products', 'ProductController', [
        'except' => [
            'index'
        ]
    ]);

});


Route::get('{any}', [HomeController::class, 'index'])->where('any', '.*');
