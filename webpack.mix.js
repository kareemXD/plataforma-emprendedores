const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css')
    .sass('resources/sass/nuvem.scss', 'public/css')
    .js('resources/js/products/index.js', 'public/js/products/index.js')
    .js('resources/js/rules_configurator/index.js', 'public/js/rules_configurator/index.js')
    .js('resources/js/global_costs_configurator/index.js', 'public/js/global_costs_configurator/index.js')
    .js('resources/js/global_costs_configurator/create_edit.js', 'public/js/global_costs_configurator/create_edit.js')
    .vue();
