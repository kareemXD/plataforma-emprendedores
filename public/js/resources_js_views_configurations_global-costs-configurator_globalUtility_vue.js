(self["webpackChunk"] = self["webpackChunk"] || []).push([["resources_js_views_configurations_global-costs-configurator_globalUtility_vue"],{

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/configurations/global-costs-configurator/globalUtility.vue?vue&type=script&lang=js&":
/*!********************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/configurations/global-costs-configurator/globalUtility.vue?vue&type=script&lang=js& ***!
  \********************************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var vuelidate__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vuelidate */ "./node_modules/vuelidate/lib/index.js");
/* harmony import */ var vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vuelidate/lib/validators */ "./node_modules/vuelidate/lib/validators/index.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  name: "globalUtility",
  mounted: function mounted() {
    this.getGlobalUtility();
  },
  mixins: [vuelidate__WEBPACK_IMPORTED_MODULE_0__.validationMixin],
  validations: {
    currentGlobalUtility: {
      Porcentaje: {
        required: vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_1__.required
      }
    }
  },
  data: function data() {
    return {
      submitStatus: "",
      currentGlobalUtility: {
        AplicaDeManeraGlobal: null,
        Descripcion: null,
        EsVisibleEnCostoEspecial: null,
        Estatus: null,
        Id_BaseCostoCalculado: null,
        Id_new: null,
        Monto: null,
        Prioridad: null,
        Tipo: null,
        Porcentaje: "",
        Id_ConceptoCosto: null
      }
    };
  },
  methods: {
    /**
     * Get global utility from api.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-03-31
     * @param
     * @return void
     */
    getGlobalUtility: function getGlobalUtility() {
      var _this = this;

      this.loading = true;
      axios.get("/api/global-utilities").then(function (result) {
        _this.currentGlobalUtility = result.data.globalUtility;
      });
    },

    /**
     * Allow call edit method if data is valid.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-03-31
     * @param
     * @return void
     */
    submit: function submit() {
      var _this2 = this;

      this.$v.$touch();

      if (this.$v.$invalid) {
        this.submitStatus = "ERROR";
      } else if (!this.$v.$invalid) {
        this.edit(this.currentGlobalUtility);
      } else {
        this.submitStatus = "PENDING";
        setTimeout(function () {
          _this2.submitStatus = "OK";
        }, 500);
      }
    },

    /**
     * Edit the percentage of global utility.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-03-31
     * @param
     * @return void
     */
    edit: function edit(data) {
      var _this3 = this;

      axios.put("/api/global-utilities/" + this.currentGlobalUtility.Id_ConceptoCosto, data).then(function (result) {
        swal2.fire("Editado", "Se editaron los datos correctamente", "success");
      })["catch"](function (error) {
        swal2.fire({
          icon: "error",
          title: "Oops...",
          text: "Algo salió mal, vuelve a intentarlo!"
        });
      })["finally"](function () {
        _this3.$router.push({
          name: 'global-costs-configurators'
        });
      });
    },

    /**
     * Redirect to index rules configurator.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-03-31
     * @param
     * @return void
     */
    cancel: function cancel() {
      this.$router.push({
        name: 'global-costs-configurators'
      });
    }
  },
  computed: {
    /**
     * Evaluate if an input is valid.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-03-31
     * @param
     * @return array
     */
    percentageErrors: function percentageErrors() {
      var errors = [];
      if (!this.$v.currentGlobalUtility.Porcentaje.$dirty) return errors;
      !this.$v.currentGlobalUtility.Porcentaje.required && errors.push("El campo es requerido");
      return errors;
    }
  }
});

/***/ }),

/***/ "./resources/js/views/configurations/global-costs-configurator/globalUtility.vue":
/*!***************************************************************************************!*\
  !*** ./resources/js/views/configurations/global-costs-configurator/globalUtility.vue ***!
  \***************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _globalUtility_vue_vue_type_template_id_443dfaad___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./globalUtility.vue?vue&type=template&id=443dfaad& */ "./resources/js/views/configurations/global-costs-configurator/globalUtility.vue?vue&type=template&id=443dfaad&");
/* harmony import */ var _globalUtility_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./globalUtility.vue?vue&type=script&lang=js& */ "./resources/js/views/configurations/global-costs-configurator/globalUtility.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! !../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */
;
var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__.default)(
  _globalUtility_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__.default,
  _globalUtility_vue_vue_type_template_id_443dfaad___WEBPACK_IMPORTED_MODULE_0__.render,
  _globalUtility_vue_vue_type_template_id_443dfaad___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/configurations/global-costs-configurator/globalUtility.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/js/views/configurations/global-costs-configurator/globalUtility.vue?vue&type=script&lang=js&":
/*!****************************************************************************************************************!*\
  !*** ./resources/js/views/configurations/global-costs-configurator/globalUtility.vue?vue&type=script&lang=js& ***!
  \****************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_globalUtility_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./globalUtility.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/configurations/global-costs-configurator/globalUtility.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_globalUtility_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__.default); 

/***/ }),

/***/ "./resources/js/views/configurations/global-costs-configurator/globalUtility.vue?vue&type=template&id=443dfaad&":
/*!**********************************************************************************************************************!*\
  !*** ./resources/js/views/configurations/global-costs-configurator/globalUtility.vue?vue&type=template&id=443dfaad& ***!
  \**********************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_globalUtility_vue_vue_type_template_id_443dfaad___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   "staticRenderFns": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_globalUtility_vue_vue_type_template_id_443dfaad___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_globalUtility_vue_vue_type_template_id_443dfaad___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./globalUtility.vue?vue&type=template&id=443dfaad& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/configurations/global-costs-configurator/globalUtility.vue?vue&type=template&id=443dfaad&");


/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/configurations/global-costs-configurator/globalUtility.vue?vue&type=template&id=443dfaad&":
/*!*************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/configurations/global-costs-configurator/globalUtility.vue?vue&type=template&id=443dfaad& ***!
  \*************************************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render),
/* harmony export */   "staticRenderFns": () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "v-card",
    { staticClass: "p-3" },
    [
      _c("v-card-title", [_c("b", [_vm._v("Utilidad Global")])]),
      _vm._v(" "),
      _c("v-card-text", [
        _c(
          "form",
          { attrs: { "data-app": "" } },
          [
            _c(
              "v-row",
              [
                _c(
                  "v-col",
                  { attrs: { cols: "6", md: "4" } },
                  [
                    _c("h6", [_vm._v("Porcentaje de Utilidad")]),
                    _vm._v(" "),
                    _c("v-text-field", {
                      attrs: {
                        "error-messages": _vm.percentageErrors,
                        placeholder: "0.00%",
                        type: "number",
                        min: "0",
                        outlined: "",
                        required: "",
                        dense: ""
                      },
                      on: {
                        input: function($event) {
                          return _vm.$v.currentGlobalUtility.Porcentaje.$touch()
                        },
                        blur: function($event) {
                          return _vm.$v.currentGlobalUtility.Porcentaje.$touch()
                        }
                      },
                      model: {
                        value: _vm.currentGlobalUtility.Porcentaje,
                        callback: function($$v) {
                          _vm.$set(_vm.currentGlobalUtility, "Porcentaje", $$v)
                        },
                        expression: "currentGlobalUtility.Porcentaje"
                      }
                    })
                  ],
                  1
                )
              ],
              1
            ),
            _vm._v(" "),
            _c(
              "v-card-actions",
              { staticClass: "mt-5 d-block" },
              [
                _c("form-action-buttons", {
                  on: { close: _vm.cancel, save: _vm.submit }
                })
              ],
              1
            )
          ],
          1
        )
      ])
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ })

}]);