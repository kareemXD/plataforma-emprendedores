(self["webpackChunk"] = self["webpackChunk"] || []).push([["resources_js_views_product_configuration_global_costs_configurator_create_vue"],{

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product_configuration/global_costs_configurator/create.vue?vue&type=script&lang=js&":
/*!********************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product_configuration/global_costs_configurator/create.vue?vue&type=script&lang=js& ***!
  \********************************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var vuelidate__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vuelidate */ "./node_modules/vuelidate/lib/index.js");
/* harmony import */ var vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vuelidate/lib/validators */ "./node_modules/vuelidate/lib/validators/index.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  name: "CreateGlobalCost",
  props: {
    title: {
      type: String,
      "default": "Alta de Costo Global"
    },
    editCost: {
      type: Boolean,
      "default": false,
      required: false
    },
    showCost: {
      type: Boolean,
      "default": false,
      required: false
    },
    inputsStatus: {
      type: Boolean,
      "default": false,
      required: false
    },
    costToShow: {
      type: Object,
      "default": null,
      required: false
    },
    basesFromShow: {
      type: Array,
      "default": null,
      required: false
    }
  },
  mounted: function mounted() {
    this.bases = this.$route.params.bases;

    if (this.$route.params.item != null) {
      this.currentGlobalCost.Id_ConceptoCosto = this.$route.params.item.Id_ConceptoCosto;
      this.currentGlobalCost.Descripcion = this.$route.params.item.Descripcion;
      this.currentGlobalCost.Tipo = this.$route.params.item.Tipo;
      this.currentGlobalCost.Porcentaje = this.$route.params.item.Porcentaje;
      this.currentGlobalCost.Monto = this.$route.params.item.Monto;
      this.currentGlobalCost.Id_BaseCostoCalculado = this.$route.params.item.Id_BaseCostoCalculado;
      this.checkInputs();
      if (this.currentGlobalCost.Porcentaje != null) this.currentGlobalCost.Monto = this.currentGlobalCost.Porcentaje;
      if (this.currentGlobalCost.Id_BaseCostoCalculado != null) this.currentGlobalCost.Id_BaseCostoCalculado = parseInt(this.$route.params.item.Id_BaseCostoCalculado);
    } else if (this.costToShow != null) {
      this.bases = this.basesFromShow;
      this.currentGlobalCost.Descripcion = this.costToShow.Descripcion;
      this.currentGlobalCost.Tipo = this.costToShow.Tipo;
      this.currentGlobalCost.Porcentaje = this.costToShow.Porcentaje;
      this.currentGlobalCost.Monto = this.costToShow.Monto;
      this.currentGlobalCost.Id_BaseCostoCalculado = this.costToShow.Id_BaseCostoCalculado;
      this.checkInputs();
      if (this.currentGlobalCost.Porcentaje != null) this.currentGlobalCost.Monto = this.currentGlobalCost.Porcentaje;

      if (this.currentGlobalCost.Id_BaseCostoCalculado != null) {
        this.currentGlobalCost.Id_BaseCostoCalculado = parseInt(this.costToShow.Id_BaseCostoCalculado);
      }
    }
  },
  mixins: [vuelidate__WEBPACK_IMPORTED_MODULE_0__.validationMixin],
  validations: {
    currentGlobalCost: {
      Descripcion: {
        required: vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_1__.required
      },
      Tipo: {
        required: vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_1__.required
      },
      Monto: {
        required: vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_1__.required
      }
    }
  },
  data: function data() {
    return {
      submitStatus: "",
      showBases: false,
      showPercentage: false,
      bases: [],
      currentGlobalCost: {
        Id_ConceptoCosto: null,
        Descripcion: "",
        Tipo: "",
        Porcentaje: null,
        Monto: null,
        Id_BaseCostoCalculado: null
      }
    };
  },
  methods: {
    /**
     * Allows do submit if data is valid.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-03-31
     * @param
     * @return void
     */
    submit: function submit() {
      var _this = this;

      this.$v.$touch();

      if (this.$v.$invalid) {
        this.submitStatus = "ERROR";
      } else if (!this.$v.$invalid) {
        switch (this.currentGlobalCost.Tipo) {
          case "P":
            this.currentGlobalCost.Porcentaje = this.currentGlobalCost.Monto;
            this.currentGlobalCost.Id_BaseCostoCalculado = null;
            break;

          case "M":
            this.currentGlobalCost.Porcentaje = null;
            this.currentGlobalCost.Id_BaseCostoCalculado = null;
            break;

          case "C":
            this.currentGlobalCost.Porcentaje = null;
            break;
        }

        this.editCost != true ? this.post(this.currentGlobalCost) : this.edit(this.currentGlobalCost);
      } else {
        this.submitStatus = "PENDING";
        setTimeout(function () {
          _this.submitStatus = "OK";
        }, 500);
      }
    },

    /**
     * Post a new cost.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-03-31
     * @param Object data
     * @return void
     */
    post: function post(data) {
      var _this2 = this;

      axios.post("/api/global-costs-configurators", data).then(function (result) {
        swal2.fire("Creado", "Se agregaron los datos correctamente", "success");
      })["catch"](function (error) {
        swal2.fire({
          icon: "error",
          title: "Oops...",
          text: "Algo salió mal, vuelve a intentarlo!"
        });
      })["finally"](function () {
        _this2.$router.push({
          name: "global-costs-configurators"
        });
      });
    },

    /**
     * Update a cost.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-04-06
     * @param Object data
     * @return void
     */
    edit: function edit(data) {
      var _this3 = this;

      axios.put("/api/global-costs-configurators/" + data.Id_ConceptoCosto, data).then(function (result) {
        swal2.fire("Editado", "Se editó el costo correctamente", "success");

        _this3.$router.push({
          name: "global-costs-configurators"
        });
      })["catch"](function (error) {
        swal2.fire({
          icon: "error",
          title: "Oops...",
          text: "Algo salió mal, vuelve a intentarlo!"
        });
      });
    },

    /**
     * Redirect to index rules configurator.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-03-31
     * @param
     * @return void
     */
    cancel: function cancel() {
      this.showCost ? this.$emit("dialogStatus", false) : this.$router.push({
        name: "global-costs-configurators"
      });
    },

    /**
     * Enables and disables inputs depending on the type.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-03-31
     * @param
     * @return void
     */
    checkInputs: function checkInputs() {
      switch (this.currentGlobalCost.Tipo) {
        case "P":
          this.showBases = false;
          this.showPercentage = true;
          break;

        case "M":
          this.showBases = false;
          this.showPercentage = false;
          break;

        case "C":
          this.showBases = true;
          this.showPercentage = false;
          break;
      }
    }
  },
  computed: {
    /**
     * Evaluate if an input is valid.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-03-31
     * @param
     * @return array
     */
    descriptionErrors: function descriptionErrors() {
      var errors = [];
      if (!this.$v.currentGlobalCost.Descripcion.$dirty) return errors;
      !this.$v.currentGlobalCost.Descripcion.required && errors.push("El campo es requerido");
      return errors;
    },

    /**
     * Evaluate if an input is valid.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-03-31
     * @param
     * @return array
     */
    typeErrors: function typeErrors() {
      var errors = [];
      if (!this.$v.currentGlobalCost.Tipo.$dirty) return errors;
      !this.$v.currentGlobalCost.Tipo.required && errors.push("El campo es requerido");
      return errors;
    },

    /**
     * Evaluate if an input is valid.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-03-31
     * @param
     * @return array
     */
    amountErrors: function amountErrors() {
      var errors = [];
      if (!this.$v.currentGlobalCost.Monto.$dirty) return errors;
      !this.$v.currentGlobalCost.Monto.required && errors.push("El campo es requerido");
      return errors;
    }
  },
  watch: {
    /**
     * Asign a new value to priceSelected.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-02-18
     * @param any val
     * @return void
     */
    costToShow: function costToShow(val) {
      this.currentGlobalCost = val;
      this.checkInputs();
      if (this.currentGlobalCost.Porcentaje != null) this.currentGlobalCost.Monto = this.currentGlobalCost.Porcentaje;
      if (this.currentGlobalCost.Id_BaseCostoCalculado != null) this.currentGlobalCost.Id_BaseCostoCalculado = parseInt(this.currentGlobalCost.Id_BaseCostoCalculado);
    }
  }
});

/***/ }),

/***/ "./resources/js/views/product_configuration/global_costs_configurator/create.vue":
/*!***************************************************************************************!*\
  !*** ./resources/js/views/product_configuration/global_costs_configurator/create.vue ***!
  \***************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _create_vue_vue_type_template_id_6e920bee___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./create.vue?vue&type=template&id=6e920bee& */ "./resources/js/views/product_configuration/global_costs_configurator/create.vue?vue&type=template&id=6e920bee&");
/* harmony import */ var _create_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./create.vue?vue&type=script&lang=js& */ "./resources/js/views/product_configuration/global_costs_configurator/create.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! !../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */
;
var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__.default)(
  _create_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__.default,
  _create_vue_vue_type_template_id_6e920bee___WEBPACK_IMPORTED_MODULE_0__.render,
  _create_vue_vue_type_template_id_6e920bee___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/product_configuration/global_costs_configurator/create.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/js/views/product_configuration/global_costs_configurator/create.vue?vue&type=script&lang=js&":
/*!****************************************************************************************************************!*\
  !*** ./resources/js/views/product_configuration/global_costs_configurator/create.vue?vue&type=script&lang=js& ***!
  \****************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_create_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./create.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product_configuration/global_costs_configurator/create.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_create_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__.default); 

/***/ }),

/***/ "./resources/js/views/product_configuration/global_costs_configurator/create.vue?vue&type=template&id=6e920bee&":
/*!**********************************************************************************************************************!*\
  !*** ./resources/js/views/product_configuration/global_costs_configurator/create.vue?vue&type=template&id=6e920bee& ***!
  \**********************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_create_vue_vue_type_template_id_6e920bee___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   "staticRenderFns": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_create_vue_vue_type_template_id_6e920bee___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_create_vue_vue_type_template_id_6e920bee___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./create.vue?vue&type=template&id=6e920bee& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product_configuration/global_costs_configurator/create.vue?vue&type=template&id=6e920bee&");


/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product_configuration/global_costs_configurator/create.vue?vue&type=template&id=6e920bee&":
/*!*************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product_configuration/global_costs_configurator/create.vue?vue&type=template&id=6e920bee& ***!
  \*************************************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render),
/* harmony export */   "staticRenderFns": () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "v-card",
    { staticClass: "p-3" },
    [
      _c("v-card-title", [_c("b", [_vm._v(_vm._s(_vm.title) + " ")])]),
      _vm._v(" "),
      _c("v-card-text", [
        _c(
          "form",
          { attrs: { "data-app": "" } },
          [
            _c(
              "v-row",
              [
                _c(
                  "v-col",
                  { attrs: { cols: "6", md: "12" } },
                  [
                    _c("h6", [_vm._v("Descripción")]),
                    _vm._v(" "),
                    _c("v-text-field", {
                      attrs: {
                        "error-messages": _vm.descriptionErrors,
                        placeholder: "Descripción",
                        readonly: _vm.inputsStatus,
                        outlined: "",
                        required: "",
                        dense: ""
                      },
                      on: {
                        input: function($event) {
                          return _vm.$v.currentGlobalCost.Descripcion.$touch()
                        },
                        blur: function($event) {
                          return _vm.$v.currentGlobalCost.Descripcion.$touch()
                        }
                      },
                      model: {
                        value: _vm.currentGlobalCost.Descripcion,
                        callback: function($$v) {
                          _vm.$set(_vm.currentGlobalCost, "Descripcion", $$v)
                        },
                        expression: "currentGlobalCost.Descripcion"
                      }
                    })
                  ],
                  1
                )
              ],
              1
            ),
            _vm._v(" "),
            _c(
              "v-row",
              [
                _c(
                  "v-radio-group",
                  {
                    attrs: {
                      "error-messages": _vm.typeErrors,
                      readonly: _vm.inputsStatus,
                      row: ""
                    },
                    on: {
                      change: _vm.checkInputs,
                      input: function($event) {
                        return _vm.$v.currentGlobalCost.Tipo.$touch()
                      },
                      blur: function($event) {
                        return _vm.$v.currentGlobalCost.Tipo.$touch()
                      }
                    },
                    model: {
                      value: _vm.currentGlobalCost.Tipo,
                      callback: function($$v) {
                        _vm.$set(_vm.currentGlobalCost, "Tipo", $$v)
                      },
                      expression: "currentGlobalCost.Tipo"
                    }
                  },
                  [
                    _c(
                      "v-col",
                      {
                        staticStyle: { "margin-right": "40px" },
                        attrs: { cols: "6", md: "3" }
                      },
                      [
                        _c("h6", [_vm._v("Tipo")]),
                        _vm._v(" "),
                        _c("v-radio", {
                          attrs: {
                            label: "Porcentaje",
                            color: "#018085",
                            value: "P"
                          }
                        })
                      ],
                      1
                    ),
                    _vm._v(" "),
                    _c(
                      "v-col",
                      {
                        staticStyle: { "padding-top": "40px" },
                        attrs: { cols: "6", md: "3" }
                      },
                      [
                        _c("v-radio", {
                          attrs: {
                            label: "Monto",
                            color: "#018085",
                            value: "M"
                          }
                        })
                      ],
                      1
                    ),
                    _vm._v(" "),
                    _c(
                      "v-col",
                      {
                        staticStyle: { "padding-top": "40px" },
                        attrs: { cols: "6", md: "3" }
                      },
                      [
                        _c("v-radio", {
                          attrs: {
                            label: "Calculado",
                            color: "#018085",
                            value: "C"
                          }
                        })
                      ],
                      1
                    )
                  ],
                  1
                ),
                _vm._v(" "),
                _c(
                  "v-col",
                  {
                    directives: [
                      {
                        name: "show",
                        rawName: "v-show",
                        value: _vm.showBases,
                        expression: "showBases"
                      }
                    ],
                    staticStyle: { "padding-top": "50px" },
                    attrs: { cols: "6", md: "3" }
                  },
                  [
                    _c("v-select", {
                      attrs: {
                        items: _vm.bases,
                        "item-text": "Descripcion",
                        "item-value": "Id_BaseCostoCalculado",
                        placeholder: "Base de cálculo",
                        readonly: _vm.inputsStatus,
                        outlined: "",
                        required: "",
                        dense: ""
                      },
                      model: {
                        value: _vm.currentGlobalCost.Id_BaseCostoCalculado,
                        callback: function($$v) {
                          _vm.$set(
                            _vm.currentGlobalCost,
                            "Id_BaseCostoCalculado",
                            $$v
                          )
                        },
                        expression: "currentGlobalCost.Id_BaseCostoCalculado"
                      }
                    })
                  ],
                  1
                )
              ],
              1
            ),
            _vm._v(" "),
            _c(
              "v-row",
              [
                _c(
                  "v-col",
                  { attrs: { cols: "6", md: "2" } },
                  [
                    _c(
                      "h6",
                      {
                        directives: [
                          {
                            name: "show",
                            rawName: "v-show",
                            value: !_vm.showPercentage,
                            expression: "!showPercentage"
                          }
                        ]
                      },
                      [_vm._v("Cantidad")]
                    ),
                    _vm._v(" "),
                    _c(
                      "h6",
                      {
                        directives: [
                          {
                            name: "show",
                            rawName: "v-show",
                            value: _vm.showPercentage,
                            expression: "showPercentage"
                          }
                        ]
                      },
                      [_vm._v("Porcentaje")]
                    ),
                    _vm._v(" "),
                    _c("v-text-field", {
                      attrs: {
                        "error-messages": _vm.amountErrors,
                        placeholder: "0.00",
                        readonly: _vm.inputsStatus,
                        type: "number",
                        min: "0",
                        step: "0.01",
                        outlined: "",
                        required: "",
                        dense: ""
                      },
                      on: {
                        input: function($event) {
                          return _vm.$v.currentGlobalCost.Monto.$touch()
                        },
                        blur: function($event) {
                          return _vm.$v.currentGlobalCost.Monto.$touch()
                        }
                      },
                      model: {
                        value: _vm.currentGlobalCost.Monto,
                        callback: function($$v) {
                          _vm.$set(_vm.currentGlobalCost, "Monto", $$v)
                        },
                        expression: "currentGlobalCost.Monto"
                      }
                    })
                  ],
                  1
                )
              ],
              1
            ),
            _vm._v(" "),
            _c(
              "v-card-actions",
              { staticClass: "mt-5 d-block" },
              [
                _c("form-action-buttons", {
                  attrs: { hideSaveBtn: _vm.showCost },
                  on: { close: _vm.cancel, save: _vm.submit }
                })
              ],
              1
            )
          ],
          1
        )
      ])
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ })

}]);