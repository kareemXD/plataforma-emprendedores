(self["webpackChunk"] = self["webpackChunk"] || []).push([["resources_js_views_product-configuration_products_index_vue"],{

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/products/index.vue?vue&type=script&lang=js&":
/*!**************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/products/index.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _components_chipStatus__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../components/chipStatus */ "./resources/js/components/chipStatus.vue");
/* harmony import */ var _partials_Utility__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./partials/Utility */ "./resources/js/views/product-configuration/products/partials/Utility.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  created: function created() {
    this.getProducts();
  },
  mounted: function mounted() {
    this.getSettings();
  },
  components: {
    ChipStatus: _components_chipStatus__WEBPACK_IMPORTED_MODULE_0__.default,
    Utility: _partials_Utility__WEBPACK_IMPORTED_MODULE_1__.default
  },
  data: function data() {
    return {
      brands: [],
      families: [],
      specialties: [],
      categories: [],
      weight: ['<', '>', '='],
      volumen: ['<', '>', '='],
      filters: {
        brand: 0,
        family: 0,
        especiality: 0,
        category: 0,
        weight: '',
        weight_t: '',
        volumen: '',
        volumen_t: ''
      },
      filter: null,
      search: "",
      dialog: false,
      productId: null,
      dialogUtility: false,
      dialogDelete: false,
      editedIndex: -1,
      editedItem: {},
      page: 1,
      pageCount: 0,
      totalItems: 0,
      itemsPerPage: 10,
      loading: false,
      productToDelete: null,
      headers: [{
        text: "Id Producto",
        value: "Id_Prod1",
        align: 'start',
        "class": 'absolute-sort-arrow'
      }, {
        text: "Descripción",
        value: "Descrip1",
        align: 'start',
        "class": 'absolute-sort-arrow'
      }, {
        text: "Nivel",
        value: "Niveles",
        align: 'start',
        "class": 'absolute-sort-arrow'
      }, {
        text: "Empaque",
        value: "id_Empaque1",
        align: 'start',
        "class": 'absolute-sort-arrow'
      }, {
        text: "Unidad",
        value: "Unidad_N2",
        align: 'start',
        "class": 'absolute-sort-arrow'
      }, {
        text: "Estatus",
        value: "Activo",
        align: 'start',
        "class": 'absolute-sort-arrow'
      }, {
        text: "Acciones",
        value: "actions",
        align: 'start',
        "class": 'absolute-sort-arrow'
      }],
      products: [],
      types: ['E-commerce', 'Sin codigo de barras', 'Todos'],
      resetProducts: false
    };
  },
  watch: {
    page: function page(val) {
      if (val % 5 === 0) {
        this.getProducts(this.page + 1);
      }
    }
  },
  methods: {
    /**
    * Filters table.
    * @auth Oscar Castellanos
    * @date 2021-06-11
    * @param Object params
    * @return void
    */
    filterData: function filterData() {
      var _this = this;

      this.loading = true;
      var params = {};
      if (this.filter) params.filter = this.filter;
      axios.get('/api/products-filter', {
        params: params
      }).then(function (res) {
        _this.totalItems = res.data.products.total;
        _this.products = res.data.products.data;
      })["catch"](function (error) {
        swal2.fire({
          icon: "error",
          title: "Oops...",
          text: "Algo salió mal, vuelve a intentarlo!" + error
        });
      }).then(function () {
        return _this.loading = false;
      });
    },

    /**
     * Fill selects.
     * @auth Oscar Castellanos
     * @date 2021-06-25
     * @param Object params
     * @return void
     */
    getSettings: function getSettings() {
      var _this2 = this;

      axios.get("/api/special-costs-settings").then(function (result) {
        _this2.brands = result.data.brands;
        _this2.families = result.data.families;
        _this2.specialties = result.data.especialities;
        _this2.categories = result.data.categories;
      })["catch"](function (error) {
        swal2.fire({
          icon: "error",
          title: "Oops...",
          text: "Algo salió mal, vuelve a intentarlo!"
        });
      });
    },

    /**
     * Allows you to filter products from api.
     * @auth Oscar Castellanos
     * @date 2021-06-25
     * @param
     * @return void
     */
    filterProducts: function filterProducts() {
      var _this3 = this;

      this.loading = true;
      this.search = "";
      axios.get("/api/products-special-filters", {
        params: {
          filters: this.filters
        }
      }).then(function (result) {
        _this3.products = result.data.products;
      })["catch"](function (error) {
        swal2.fire({
          icon: "error",
          title: "Oops...",
          text: "Algo salió mal, vuelve a intentarlo!"
        });
      })["finally"](function () {
        _this3.loading = false;
        _this3.selectAll = false;
      });
    },

    /**
    * Clean the filters.
    * @auth José Vega <jose.vega@nuvem.mx>
    * @date 2021-05-04
    * @param
    * @return void
    */
    cleanFilters: function cleanFilters() {
      this.filters.brand = 0;
      this.filters.family = 0;
      this.filters.especiality = 0;
      this.filters.category = 0;
      this.filters.weight = 0;
      this.filters.weight_t = "";
      this.filters.volumen = 0;
      this.filters.volumen_t = "";
      this.selectAll = false;
    },

    /**
     * Redirect to create product component.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-03-25
     * @param
     * @return void
     */
    createProduct: function createProduct() {
      this.$router.push({
        name: 'create-products'
      });
    },

    /**
     * Redirect to create product component.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-03-25
     * @param
     * @return void
     */
    showProduct: function showProduct(item) {
      this.$router.push({
        name: 'show-products',
        params: {
          item: item
        }
      });
    },

    /**
     * Redirect to create product component.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-03-25
     * @param
     * @return void
     */
    editProduct: function editProduct(item) {
      this.$router.push({
        name: 'edit-products',
        params: {
          item: item
        }
      });
    },

    /**
     * Open Show component.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-04-07
     * @param Object item
     * @return void
     */
    openUtility: function openUtility(item) {
      this.dialogUtility = true;
      this.$refs.sendProductId.reciveProductId(item.Id_Prod1, item.profit != null ? item.profit.Utilidad : null);
    },

    /**
     * Get dialog status from Show component.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-04-07
     * @param boolean value
     * @return void
     */
    getDialogUtilityStatus: function getDialogUtilityStatus(value) {
      this.dialogUtility = value[0];

      if (value[1]) {
        this.resetProducts = true;
        this.getProducts();
      }
    },

    /**
     * Get products and fill date table from api products.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-03-25
     * @param
     * @return void
     */
    getProducts: function getProducts() {
      var _this4 = this;

      var page = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 1;
      this.loading = true;

      if (this.resetProducts) {
        this.products = [];
      }

      axios.get("/api/products?page=" + page).then(function (result) {
        _this4.totalItems = result.data.products.total;
        result.data.products.data.forEach(function (product) {
          _this4.products.push(product);
        });
        _this4.resetProducts = false;
      })["catch"](function (error) {
        swal2.fire({
          icon: "error",
          title: "Oops...",
          text: "Algo salió mal, vuelve a intentarlo!" + error
        });
      })["finally"](function () {
        _this4.loading = false;
      });
    },

    /**
     * Get products and fill date table from api products.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-03-25
     * @param
     * @return void
     */
    searchProducts: function searchProducts(data) {
      var _this5 = this;

      this.loading = true;
      axios.get("/api/product/search-product", {
        params: {
          search: data
        }
      }).then(function (result) {
        _this5.products = [];
        _this5.totalItems = Object.keys(result.data.products).length;
        result.data.products.forEach(function (product) {
          _this5.products.push(product);
        });
      })["catch"](function (error) {
        swal2.fire({
          icon: "error",
          title: "Oops...",
          text: "Algo salió mal, vuelve a intentarlo!" + error
        });
      })["finally"](function () {
        _this5.loading = false;
      });
    },

    /**
     * Open delete dialog.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-04-07
     * @param Object item
     * @return void
     */
    openDeleteDialog: function openDeleteDialog(item) {
      this.dialogDelete = true;
      this.productToDelete = item.Id_Prod1;
    },

    /**
     * Closes the delete dialog.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-04-06
     * @param
     * @return void
     */
    closeDelete: function closeDelete() {
      this.dialogDelete = false;
      this.productToDelete = null;
      this.resetProducts = true;
      this.getProducts();
      swal2.fire("Estatus cambiado", "Se cambió el estatus correctamente", "success");
    },

    /**
     * Change status of a item.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-04-07
     * @param
     * @return void
     */
    deleteItem: function deleteItem() {
      var _this6 = this;

      axios["delete"]("/api/products/" + this.productToDelete).then(function (result) {
        _this6.closeDelete();
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/products/partials/Utility.vue?vue&type=script&lang=js&":
/*!*************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/products/partials/Utility.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var vuelidate__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vuelidate */ "./node_modules/vuelidate/lib/index.js");
/* harmony import */ var vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vuelidate/lib/validators */ "./node_modules/vuelidate/lib/validators/index.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  name: "Utility",
  props: ["dialog"],
  mixins: [vuelidate__WEBPACK_IMPORTED_MODULE_0__.validationMixin],
  validations: {
    newProductProfit: {
      required: vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_1__.required
    }
  },
  components: {},
  mounted: function mounted() {
    this.getMinimalProfit();
  },
  data: function data() {
    return {
      currentMinimalProfit: null,
      currentProductProfit: null,
      newProductProfit: null,
      currentProductId: null
    };
  },
  methods: {
    /**
     * Allows do submit if data is valid.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-03-26
     * @param
     * @return void
     */
    submit: function submit() {
      this.$v.$touch();

      if (!this.$v.$invalid) {
        this.currentProductProfit != null ? this.edit({
          newProductProfit: this.newProductProfit
        }, this.currentProductId) : this.post({
          newProductProfit: this.newProductProfit
        }, this.currentProductId);
      }
    },

    /**
     * Emit a boolean value to close the current dialog.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-04-14
     * @param
     * @return void
     */
    cancel: function cancel() {
      this.newProductProfit = null;
      this.$v.$reset();
      this.$emit("dialogStatus", [false, false]);
    },

    /**
     * Get the product id from index component and calls the getCurrentProfit function.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-04-19
     * @param
     * @return void
     */
    reciveProductId: function reciveProductId(productId, profit) {
      this.currentProductProfit = profit;
      this.currentProductId = productId;
    },

    /**
     * Get the minimal profit.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-04-19
     * @param
     * @return void
     */
    getMinimalProfit: function getMinimalProfit() {
      var _this = this;

      axios.get("/api/global-utilities").then(function (result) {
        _this.currentMinimalProfit = result.data.globalUtility.Porcentaje;
      });
    },

    /**
     * Post a new profit.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-03-31
     * @param Object data
     * @return void
     */
    post: function post(data, productId) {
      var _this2 = this;

      axios.post("/api/product/profit/" + productId, data).then(function (result) {
        _this2.newProductProfit = null;

        _this2.$v.$reset();

        _this2.$emit("dialogStatus", [false, true]);

        swal2.fire("Creado", "Se agregaron los datos correctamente", "success");
      })["catch"](function (error) {
        swal2.fire({
          icon: "error",
          title: "Oops...",
          text: "Algo salió mal, vuelve a intentarlo!"
        });
      });
    },

    /**
     * Edit the current profit.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-04-19
     * @param
     * @return void
     */
    edit: function edit(data, productId) {
      var _this3 = this;

      axios.put("/api/product/profit/" + productId, data).then(function (result) {
        _this3.newProductProfit = null;

        _this3.$v.$reset();

        _this3.$emit("dialogStatus", [false, true]);

        swal2.fire("Editado", "Se editaron los datos correctamente", "success");
      })["catch"](function (error) {
        swal2.fire({
          icon: "error",
          title: "Oops...",
          text: "Algo salió mal, vuelve a intentarlo!"
        });
      });
    }
  },
  computed: {
    /**
     * Evaluate if an input is valid.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-04-19
     * @param
     * @return array
     */
    profitErrors: function profitErrors() {
      var errors = [];
      if (!this.$v.newProductProfit.$dirty) return errors;
      !this.$v.newProductProfit.required && errors.push("El campo es requerido");
      return errors;
    }
  }
});

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/products/index.vue?vue&type=style&index=0&lang=css&":
/*!************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/products/index.vue?vue&type=style&index=0&lang=css& ***!
  \************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ ((module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../../../node_modules/css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js");
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0__);
// Imports

var ___CSS_LOADER_EXPORT___ = _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0___default()(function(i){return i[1]});
// Module
___CSS_LOADER_EXPORT___.push([module.id, "\n.data-table-style{\n    margin: 10px;\n}\n.v-data-table-header > tr > .absolute-sort-arrow > i {\n  position: absolute;\n}\n", ""]);
// Exports
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (___CSS_LOADER_EXPORT___);


/***/ }),

/***/ "./resources/js/views/product-configuration/products/index.vue":
/*!*********************************************************************!*\
  !*** ./resources/js/views/product-configuration/products/index.vue ***!
  \*********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _index_vue_vue_type_template_id_1ccae186___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./index.vue?vue&type=template&id=1ccae186& */ "./resources/js/views/product-configuration/products/index.vue?vue&type=template&id=1ccae186&");
/* harmony import */ var _index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./index.vue?vue&type=script&lang=js& */ "./resources/js/views/product-configuration/products/index.vue?vue&type=script&lang=js&");
/* harmony import */ var _index_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./index.vue?vue&type=style&index=0&lang=css& */ "./resources/js/views/product-configuration/products/index.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! !../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");



;


/* normalize component */

var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__.default)(
  _index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__.default,
  _index_vue_vue_type_template_id_1ccae186___WEBPACK_IMPORTED_MODULE_0__.render,
  _index_vue_vue_type_template_id_1ccae186___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/product-configuration/products/index.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/js/views/product-configuration/products/partials/Utility.vue":
/*!********************************************************************************!*\
  !*** ./resources/js/views/product-configuration/products/partials/Utility.vue ***!
  \********************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _Utility_vue_vue_type_template_id_08112098___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Utility.vue?vue&type=template&id=08112098& */ "./resources/js/views/product-configuration/products/partials/Utility.vue?vue&type=template&id=08112098&");
/* harmony import */ var _Utility_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Utility.vue?vue&type=script&lang=js& */ "./resources/js/views/product-configuration/products/partials/Utility.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! !../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */
;
var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__.default)(
  _Utility_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__.default,
  _Utility_vue_vue_type_template_id_08112098___WEBPACK_IMPORTED_MODULE_0__.render,
  _Utility_vue_vue_type_template_id_08112098___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/product-configuration/products/partials/Utility.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/js/views/product-configuration/products/index.vue?vue&type=script&lang=js&":
/*!**********************************************************************************************!*\
  !*** ./resources/js/views/product-configuration/products/index.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./index.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/products/index.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__.default); 

/***/ }),

/***/ "./resources/js/views/product-configuration/products/partials/Utility.vue?vue&type=script&lang=js&":
/*!*********************************************************************************************************!*\
  !*** ./resources/js/views/product-configuration/products/partials/Utility.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Utility_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./Utility.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/products/partials/Utility.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Utility_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__.default); 

/***/ }),

/***/ "./resources/js/views/product-configuration/products/index.vue?vue&type=template&id=1ccae186&":
/*!****************************************************************************************************!*\
  !*** ./resources/js/views/product-configuration/products/index.vue?vue&type=template&id=1ccae186& ***!
  \****************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_1ccae186___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   "staticRenderFns": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_1ccae186___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_1ccae186___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./index.vue?vue&type=template&id=1ccae186& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/products/index.vue?vue&type=template&id=1ccae186&");


/***/ }),

/***/ "./resources/js/views/product-configuration/products/partials/Utility.vue?vue&type=template&id=08112098&":
/*!***************************************************************************************************************!*\
  !*** ./resources/js/views/product-configuration/products/partials/Utility.vue?vue&type=template&id=08112098& ***!
  \***************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Utility_vue_vue_type_template_id_08112098___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   "staticRenderFns": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Utility_vue_vue_type_template_id_08112098___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Utility_vue_vue_type_template_id_08112098___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./Utility.vue?vue&type=template&id=08112098& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/products/partials/Utility.vue?vue&type=template&id=08112098&");


/***/ }),

/***/ "./resources/js/views/product-configuration/products/index.vue?vue&type=style&index=0&lang=css&":
/*!******************************************************************************************************!*\
  !*** ./resources/js/views/product-configuration/products/index.vue?vue&type=style&index=0&lang=css& ***!
  \******************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_node_modules_css_loader_dist_cjs_js_clonedRuleSet_10_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_10_0_rules_0_use_2_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-style-loader/index.js!../../../../../node_modules/css-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[1]!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[2]!../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./index.vue?vue&type=style&index=0&lang=css& */ "./node_modules/vue-style-loader/index.js!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/products/index.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_vue_style_loader_index_js_node_modules_css_loader_dist_cjs_js_clonedRuleSet_10_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_10_0_rules_0_use_2_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_node_modules_css_loader_dist_cjs_js_clonedRuleSet_10_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_10_0_rules_0_use_2_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ var __WEBPACK_REEXPORT_OBJECT__ = {};
/* harmony reexport (unknown) */ for(const __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_node_modules_css_loader_dist_cjs_js_clonedRuleSet_10_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_10_0_rules_0_use_2_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== "default") __WEBPACK_REEXPORT_OBJECT__[__WEBPACK_IMPORT_KEY__] = () => _node_modules_vue_style_loader_index_js_node_modules_css_loader_dist_cjs_js_clonedRuleSet_10_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_10_0_rules_0_use_2_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__[__WEBPACK_IMPORT_KEY__]
/* harmony reexport (unknown) */ __webpack_require__.d(__webpack_exports__, __WEBPACK_REEXPORT_OBJECT__);


/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/products/index.vue?vue&type=template&id=1ccae186&":
/*!*******************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/products/index.vue?vue&type=template&id=1ccae186& ***!
  \*******************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render),
/* harmony export */   "staticRenderFns": () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "v-card",
    { staticClass: "p-3", attrs: { "data-app": "" } },
    [
      _c("Utility", {
        ref: "sendProductId",
        attrs: { dialog: _vm.dialogUtility },
        on: { dialogStatus: _vm.getDialogUtilityStatus }
      }),
      _vm._v(" "),
      _c("dialog-delete", {
        attrs: { status: _vm.dialogDelete },
        on: {
          cancel: function($event) {
            _vm.dialogDelete = false
          },
          confirm: _vm.deleteItem
        }
      }),
      _vm._v(" "),
      _c("v-data-table", {
        staticClass: "elevation-1",
        attrs: {
          headers: _vm.headers,
          items: _vm.products,
          search: _vm.search,
          page: _vm.page,
          "items-per-page": _vm.itemsPerPage,
          loading: _vm.loading,
          "hide-default-footer": "",
          "no-data-text": "No hay ningún registro",
          "no-results-text": "No se encontraron coincidencias",
          "loading-text": "Cargando... Porfavor espere"
        },
        on: {
          "update:page": function($event) {
            _vm.page = $event
          },
          "page-count": function($event) {
            _vm.pageCount = $event
          }
        },
        scopedSlots: _vm._u(
          [
            {
              key: "top",
              fn: function() {
                return [
                  _c(
                    "datatable-header",
                    {
                      attrs: {
                        title: "Listado de productos",
                        "disable-creation": false
                      },
                      on: {
                        searching: function($event) {
                          return _vm.searchProducts($event)
                        },
                        open: _vm.createProduct
                      }
                    },
                    [
                      _c("v-select", {
                        staticClass: "ml-5",
                        attrs: {
                          items: _vm.types,
                          placeholder: "Filtrar por",
                          outlined: "",
                          dense: "",
                          attach: "",
                          "hide-details": "",
                          clearable: "",
                          loading: _vm.loading,
                          "item-text": "type",
                          "item-value": "type"
                        },
                        on: {
                          input: function($event) {
                            return _vm.filterData()
                          }
                        },
                        model: {
                          value: _vm.filter,
                          callback: function($$v) {
                            _vm.filter = $$v
                          },
                          expression: "filter"
                        }
                      })
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "v-row",
                    [
                      _c(
                        "v-col",
                        { attrs: { cols: "6", md: "2" } },
                        [
                          _c("h6", [_vm._v("Marca")]),
                          _vm._v(" "),
                          _c("v-select", {
                            attrs: {
                              placeholder: "Marca",
                              items: _vm.brands,
                              attach: "",
                              "item-text": "Marca",
                              "item-value": "Id_Marca",
                              outlined: "",
                              dense: ""
                            },
                            model: {
                              value: _vm.filters.brand,
                              callback: function($$v) {
                                _vm.$set(_vm.filters, "brand", $$v)
                              },
                              expression: "filters.brand"
                            }
                          })
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "v-col",
                        { attrs: { cols: "6", md: "2" } },
                        [
                          _c("h6", [_vm._v("Familia")]),
                          _vm._v(" "),
                          _c("v-select", {
                            attrs: {
                              placeholder: "Familia",
                              items: _vm.families,
                              attach: "",
                              "item-text": "Familia",
                              "item-value": "Id_Familia",
                              outlined: "",
                              dense: ""
                            },
                            model: {
                              value: _vm.filters.family,
                              callback: function($$v) {
                                _vm.$set(_vm.filters, "family", $$v)
                              },
                              expression: "filters.family"
                            }
                          })
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "v-col",
                        { attrs: { cols: "6", md: "2" } },
                        [
                          _c("h6", [_vm._v("Especialidad")]),
                          _vm._v(" "),
                          _c("v-select", {
                            attrs: {
                              placeholder: "Especialidad",
                              items: _vm.specialties,
                              attach: "",
                              "item-text": "Especialidad",
                              "item-value": "Id_EspMed",
                              outlined: "",
                              dense: ""
                            },
                            model: {
                              value: _vm.filters.especiality,
                              callback: function($$v) {
                                _vm.$set(_vm.filters, "especiality", $$v)
                              },
                              expression: "filters.especiality"
                            }
                          })
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "v-col",
                        { attrs: { cols: "6", md: "2" } },
                        [
                          _c("h6", [_vm._v("Categoría")]),
                          _vm._v(" "),
                          _c("v-select", {
                            attrs: {
                              placeholder: "Categoría",
                              items: _vm.categories,
                              attach: "",
                              "item-text": "Categoria",
                              "item-value": "Id_Categoria",
                              outlined: "",
                              dense: ""
                            },
                            model: {
                              value: _vm.filters.category,
                              callback: function($$v) {
                                _vm.$set(_vm.filters, "category", $$v)
                              },
                              expression: "filters.category"
                            }
                          })
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "v-col",
                        { attrs: { cols: "6", md: "2" } },
                        [
                          _c("h6", [_vm._v("Peso")]),
                          _vm._v(" "),
                          _c("v-select", {
                            attrs: {
                              placeholder: "Peso",
                              items: _vm.weight,
                              attach: "",
                              "item-text": "Peso",
                              "item-value": "Id_Peso",
                              outlined: "",
                              dense: ""
                            },
                            model: {
                              value: _vm.filters.weight,
                              callback: function($$v) {
                                _vm.$set(_vm.filters, "weight", $$v)
                              },
                              expression: "filters.weight"
                            }
                          }),
                          _vm._v(" "),
                          _c("v-text-field", {
                            attrs: {
                              placeholder: "Peso_t",
                              "item-text": "Peso",
                              "item-value": "Id_Peso_t",
                              outlined: "",
                              dense: ""
                            },
                            model: {
                              value: _vm.filters.weight_t,
                              callback: function($$v) {
                                _vm.$set(_vm.filters, "weight_t", $$v)
                              },
                              expression: "filters.weight_t"
                            }
                          })
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "v-col",
                        { attrs: { cols: "6", md: "2" } },
                        [
                          _c("h6", [_vm._v("Volumen")]),
                          _vm._v(" "),
                          _c("v-select", {
                            attrs: {
                              placeholder: "Volumen",
                              items: _vm.volumen,
                              attach: "",
                              "item-text": "Volumen",
                              "item-value": "Id_Volumen",
                              outlined: "",
                              dense: ""
                            },
                            model: {
                              value: _vm.filters.volumen,
                              callback: function($$v) {
                                _vm.$set(_vm.filters, "volumen", $$v)
                              },
                              expression: "filters.volumen"
                            }
                          }),
                          _vm._v(" "),
                          _c("v-text-field", {
                            attrs: {
                              placeholder: "Volumen_t",
                              "item-text": "Volumen",
                              "item-value": "Id_volumen_t",
                              outlined: "",
                              dense: ""
                            },
                            model: {
                              value: _vm.filters.volumen_t,
                              callback: function($$v) {
                                _vm.$set(_vm.filters, "volumen_t", $$v)
                              },
                              expression: "filters.volumen_t"
                            }
                          })
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "v-col",
                        {
                          staticStyle: { "align-self": "center" },
                          attrs: { cols: "6", md: "2" }
                        },
                        [
                          _c(
                            "v-btn",
                            {
                              attrs: { block: "", color: "primary", text: "" },
                              on: { click: _vm.filterProducts }
                            },
                            [_vm._v("\n        Buscar\n      ")]
                          )
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "v-col",
                        {
                          staticStyle: { "align-self": "center" },
                          attrs: { cols: "6", md: "2" }
                        },
                        [
                          _c(
                            "v-btn",
                            {
                              attrs: { block: "", color: "primary", text: "" },
                              on: { click: _vm.cleanFilters }
                            },
                            [_vm._v("\n        Limpiar\n      ")]
                          )
                        ],
                        1
                      )
                    ],
                    1
                  )
                ]
              },
              proxy: true
            },
            {
              key: "item.Activo",
              fn: function(ref) {
                var item = ref.item
                return [_c("chip-status", { attrs: { item: item } })]
              }
            },
            {
              key: "item.actions",
              fn: function(ref) {
                var item = ref.item
                return [
                  _c(
                    "v-tooltip",
                    {
                      attrs: { top: "", attach: "" },
                      scopedSlots: _vm._u(
                        [
                          {
                            key: "activator",
                            fn: function(ref) {
                              var on = ref.on
                              var attrs = ref.attrs
                              return [
                                _c(
                                  "v-icon",
                                  _vm._g(
                                    _vm._b(
                                      {
                                        staticClass: "mr-2",
                                        attrs: { small: "", color: "primary" },
                                        on: {
                                          click: function($event) {
                                            return _vm.editProduct(item)
                                          }
                                        }
                                      },
                                      "v-icon",
                                      attrs,
                                      false
                                    ),
                                    on
                                  ),
                                  [
                                    _vm._v(
                                      "\n                    mdi-pencil\n                "
                                    )
                                  ]
                                )
                              ]
                            }
                          }
                        ],
                        null,
                        true
                      )
                    },
                    [_vm._v(" "), _c("span", [_vm._v("Editar")])]
                  ),
                  _vm._v(" "),
                  _c(
                    "v-tooltip",
                    {
                      attrs: { top: "", attach: "" },
                      scopedSlots: _vm._u(
                        [
                          {
                            key: "activator",
                            fn: function(ref) {
                              var on = ref.on
                              var attrs = ref.attrs
                              return [
                                _c(
                                  "v-icon",
                                  _vm._g(
                                    _vm._b(
                                      {
                                        staticClass: "mr-2",
                                        attrs: { small: "", color: "primary" },
                                        on: {
                                          click: function($event) {
                                            return _vm.showProduct(item)
                                          }
                                        }
                                      },
                                      "v-icon",
                                      attrs,
                                      false
                                    ),
                                    on
                                  ),
                                  [
                                    _vm._v(
                                      "\n                    mdi-eye\n                "
                                    )
                                  ]
                                )
                              ]
                            }
                          }
                        ],
                        null,
                        true
                      )
                    },
                    [_vm._v(" "), _c("span", [_vm._v("Mostrar")])]
                  ),
                  _vm._v(" "),
                  _c(
                    "v-tooltip",
                    {
                      attrs: { top: "", attach: "" },
                      scopedSlots: _vm._u(
                        [
                          {
                            key: "activator",
                            fn: function(ref) {
                              var on = ref.on
                              var attrs = ref.attrs
                              return [
                                _c(
                                  "v-icon",
                                  _vm._g(
                                    _vm._b(
                                      {
                                        attrs: { small: "", color: "primary" },
                                        on: {
                                          click: function($event) {
                                            return _vm.openDeleteDialog(item)
                                          }
                                        }
                                      },
                                      "v-icon",
                                      attrs,
                                      false
                                    ),
                                    on
                                  ),
                                  [
                                    _vm._v(
                                      "\n                    mdi-delete\n                "
                                    )
                                  ]
                                )
                              ]
                            }
                          }
                        ],
                        null,
                        true
                      )
                    },
                    [_vm._v(" "), _c("span", [_vm._v("Eliminar")])]
                  ),
                  _vm._v(" "),
                  _c(
                    "v-tooltip",
                    {
                      attrs: { top: "", attach: "" },
                      scopedSlots: _vm._u(
                        [
                          {
                            key: "activator",
                            fn: function(ref) {
                              var on = ref.on
                              var attrs = ref.attrs
                              return [
                                _c(
                                  "v-icon",
                                  _vm._g(
                                    _vm._b(
                                      {
                                        staticClass: "mr-2",
                                        attrs: { small: "", color: "primary" },
                                        on: {
                                          click: function($event) {
                                            return _vm.openUtility(item)
                                          }
                                        }
                                      },
                                      "v-icon",
                                      attrs,
                                      false
                                    ),
                                    on
                                  ),
                                  [
                                    _vm._v(
                                      "\n                    mdi-currency-usd\n                "
                                    )
                                  ]
                                )
                              ]
                            }
                          }
                        ],
                        null,
                        true
                      )
                    },
                    [_vm._v(" "), _c("span", [_vm._v("Utilidad")])]
                  )
                ]
              }
            }
          ],
          null,
          true
        )
      }),
      _vm._v(" "),
      _c("pagination", {
        attrs: {
          last: _vm.pageCount,
          current: _vm.page,
          itemsPerPage: _vm.itemsPerPage,
          total: _vm.totalItems
        },
        on: {
          input: function($event) {
            _vm.page = $event
          }
        }
      })
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/products/partials/Utility.vue?vue&type=template&id=08112098&":
/*!******************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/products/partials/Utility.vue?vue&type=template&id=08112098& ***!
  \******************************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render),
/* harmony export */   "staticRenderFns": () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "text-center" },
    [
      _c(
        "v-dialog",
        {
          attrs: { persistent: "", "max-width": "800px" },
          model: {
            value: _vm.dialog,
            callback: function($$v) {
              _vm.dialog = $$v
            },
            expression: "dialog"
          }
        },
        [
          _c(
            "v-card",
            [
              _c(
                "v-card-text",
                { staticClass: "pb-0 pt-4" },
                [
                  _c(
                    "v-row",
                    { staticClass: "flex-column" },
                    [
                      _c(
                        "v-col",
                        { attrs: { cols: "6", md: "6" } },
                        [
                          _c("h6", { staticClass: "mt-2" }, [
                            _vm._v("Utilidad actual")
                          ]),
                          _vm._v(" "),
                          _c("v-text-field", {
                            staticClass: "show-v-text-field",
                            attrs: {
                              readonly: "",
                              disabled: "",
                              placeholder: "%",
                              outlined: "",
                              dense: ""
                            },
                            model: {
                              value: _vm.currentProductProfit,
                              callback: function($$v) {
                                _vm.currentProductProfit = $$v
                              },
                              expression: "currentProductProfit"
                            }
                          })
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "v-col",
                        { attrs: { cols: "6", md: "6" } },
                        [
                          _c("h6", { staticClass: "mt-2" }, [
                            _vm._v("Utilidad mínima")
                          ]),
                          _vm._v(" "),
                          _c("v-text-field", {
                            staticClass: "show-v-text-field",
                            attrs: {
                              readonly: "",
                              disabled: "",
                              placeholder: "%",
                              outlined: "",
                              dense: ""
                            },
                            model: {
                              value: _vm.currentMinimalProfit,
                              callback: function($$v) {
                                _vm.currentMinimalProfit = $$v
                              },
                              expression: "currentMinimalProfit"
                            }
                          })
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "v-col",
                        { attrs: { cols: "6", md: "6" } },
                        [
                          _c("h6", { staticClass: "mt-2" }, [
                            _vm._v("Nueva utilidad")
                          ]),
                          _vm._v(" "),
                          _c("v-text-field", {
                            attrs: {
                              "error-messages": _vm.profitErrors,
                              placeholder: "%",
                              type: "number",
                              min: "0",
                              step: "0.01",
                              outlined: "",
                              dense: ""
                            },
                            on: {
                              input: function($event) {
                                return _vm.$v.newProductProfit.$touch()
                              },
                              blur: function($event) {
                                return _vm.$v.newProductProfit.$touch()
                              }
                            },
                            model: {
                              value: _vm.newProductProfit,
                              callback: function($$v) {
                                _vm.newProductProfit = $$v
                              },
                              expression: "newProductProfit"
                            }
                          })
                        ],
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "v-container",
                { staticClass: "d-flex flex-column mb-6" },
                [
                  _c(
                    "v-card-actions",
                    { staticClass: "mt-5 d-block" },
                    [
                      _c("form-action-buttons", {
                        on: { close: _vm.cancel, save: _vm.submit }
                      })
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-style-loader/index.js!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/products/index.vue?vue&type=style&index=0&lang=css&":
/*!*****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-style-loader/index.js!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/products/index.vue?vue&type=style&index=0&lang=css& ***!
  \*****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(/*! !!../../../../../node_modules/css-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[1]!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[2]!../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./index.vue?vue&type=style&index=0&lang=css& */ "./node_modules/css-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/products/index.vue?vue&type=style&index=0&lang=css&");
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.id, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var add = __webpack_require__(/*! !../../../../../node_modules/vue-style-loader/lib/addStylesClient.js */ "./node_modules/vue-style-loader/lib/addStylesClient.js").default
var update = add("de084c6e", content, false, {});
// Hot Module Replacement
if(false) {}

/***/ })

}]);