(self["webpackChunk"] = self["webpackChunk"] || []).push([["resources_js_views_product_configuration_rules_configurator_edit_vue"],{

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product_configuration/rules_configurator/edit.vue?vue&type=script&lang=js&":
/*!***********************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product_configuration/rules_configurator/edit.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _partials_Inputs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./partials/Inputs */ "./resources/js/views/product_configuration/rules_configurator/partials/Inputs.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  name: "EditRuleConfigurator",
  components: {
    Inputs: _partials_Inputs__WEBPACK_IMPORTED_MODULE_0__.default
  },
  data: function data() {
    return {
      submitStatus: "",
      currentRule: {
        Descripcion: "",
        Prioridad: null
      }
    };
  },
  methods: {
    /**
     * Redirect to index rules configurator.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-03-31
     * @param
     * @return void
     */
    submit: function submit() {
      this.$refs.submit.verifyPriority();
    },

    /**
     * Redirect to index rules configurator.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-03-31
     * @param
     * @return void
     */
    cancel: function cancel() {
      this.$router.push({
        name: "rules-configurators"
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product_configuration/rules_configurator/partials/Inputs.vue?vue&type=script&lang=js&":
/*!**********************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product_configuration/rules_configurator/partials/Inputs.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var vuelidate__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vuelidate */ "./node_modules/vuelidate/lib/index.js");
/* harmony import */ var vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vuelidate/lib/validators */ "./node_modules/vuelidate/lib/validators/index.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  name: "Inputs",
  props: {
    ruleToShow: {
      type: Object,
      "default": null
    },
    inputsStatus: {
      type: Boolean,
      "default": false
    }
  },
  mixins: [vuelidate__WEBPACK_IMPORTED_MODULE_0__.validationMixin],
  validations: {
    currentRule: {
      Descripcion: {
        required: vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_1__.required
      },
      Prioridad: {
        required: vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_1__.required
      }
    }
  },
  mounted: function mounted() {
    if (this.$route.params.item != null) {
      this.currentRule = this.$route.params.item;
      this.previousRulePriority = this.$route.params.item.Prioridad;
    } else if (this.ruleToShow != null) {
      this.currentRule = this.ruleToShow;
    }
  },
  data: function data() {
    return {
      submitStatus: "",
      previousRulePriority: null,
      currentRule: {
        Descripcion: "",
        Prioridad: null
      }
    };
  },
  methods: {
    /**
     * Allows call post or edit method if data is valid.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-03-31
     * @param
     * @return void
     */
    submit: function submit() {
      var _this = this;

      this.$v.$touch();

      if (this.$v.$invalid) {
        this.submitStatus = "ERROR";
      } else if (!this.$v.$invalid) {
        this.$route.params.item != null ? this.edit(this.currentRule) : this.post(this.currentRule);
      } else {
        this.submitStatus = "PENDING";
        setTimeout(function () {
          _this.submitStatus = "OK";
        }, 500);
      }
    },

    /**
     * Post a new rule.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-03-31
     * @param
     * @return void
     */
    post: function post(data) {
      var _this2 = this;

      axios.post("/api/rules-configurators", data).then(function (result) {
        swal2.fire("Creado", "Se agregaron los datos correctamente", "success");
      })["catch"](function (error) {
        swal2.fire({
          icon: "error",
          title: "Oops...",
          text: "Algo salió mal, vuelve a intentarlo!"
        });
      })["finally"](function () {
        _this2.$router.push({
          name: "rules-configurators"
        });
      });
    },

    /**
     * Update a rule.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-04-07
     * @param
     * @return void
     */
    edit: function edit(data) {
      var _this3 = this;

      axios.put("/api/rules-configurators/" + data.Id_ReglaCostoEspecial, data).then(function (result) {
        swal2.fire("Editado", "Se editó la regla correctamente", "success");

        _this3.$router.push({
          name: "rules-configurators"
        });
      })["catch"](function (error) {
        swal2.fire({
          icon: "error",
          title: "Oops...",
          text: "Algo salió mal, vuelve a intentarlo!"
        });
      });
    },

    /**
     * Check if current priority already exists in bd and allows or does not allow call submit method.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-04-07
     * @param
     * @return void
     */
    verifyPriority: function verifyPriority() {
      var _this4 = this;

      var priority = {
        priority: this.currentRule.Prioridad
      };

      if (this.$route.params.item != null && this.previousRulePriority === this.currentRule.Prioridad) {
        this.submit();
      } else {
        axios.get("/api/rules-configurators-priority", {
          params: {
            priority: priority
          }
        }).then(function (result) {
          if (result.data.priority != true) {
            _this4.submit();
          } else {
            swal2.fire({
              icon: "error",
              title: "Oops...",
              text: "La prioridad seleccionada ya fue dada de alta anteriormente, intente con otra."
            });
          }
        })["catch"](function (error) {
          swal2.fire({
            icon: "error",
            title: "Oops...",
            text: "Algo salió mal, vuelve a intentarlo!"
          });
        });
      }
    }
  },
  computed: {
    /**
     * Evaluate if an input is valid.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-03-31
     * @param
     * @return array
     */
    descriptionErrors: function descriptionErrors() {
      var errors = [];
      if (!this.$v.currentRule.Descripcion.$dirty) return errors;
      !this.$v.currentRule.Descripcion.required && errors.push("El campo es requerido");
      return errors;
    },

    /**
     * Evaluate if an input is valid.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-03-31
     * @param
     * @return array
     */
    priorityErrors: function priorityErrors() {
      var errors = [];
      if (!this.$v.currentRule.Prioridad.$dirty) return errors;
      !this.$v.currentRule.Prioridad.required && errors.push("El campo es requerido");
      return errors;
    }
  },
  watch: {
    /**
     * Asign a new value to currentRule.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-04-08
     * @param Object newVal
     * @return void
     */
    ruleToShow: function ruleToShow(newVal) {
      this.currentRule = newVal;
    }
  }
});

/***/ }),

/***/ "./resources/js/views/product_configuration/rules_configurator/edit.vue":
/*!******************************************************************************!*\
  !*** ./resources/js/views/product_configuration/rules_configurator/edit.vue ***!
  \******************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _edit_vue_vue_type_template_id_57c23604___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./edit.vue?vue&type=template&id=57c23604& */ "./resources/js/views/product_configuration/rules_configurator/edit.vue?vue&type=template&id=57c23604&");
/* harmony import */ var _edit_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./edit.vue?vue&type=script&lang=js& */ "./resources/js/views/product_configuration/rules_configurator/edit.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! !../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */
;
var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__.default)(
  _edit_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__.default,
  _edit_vue_vue_type_template_id_57c23604___WEBPACK_IMPORTED_MODULE_0__.render,
  _edit_vue_vue_type_template_id_57c23604___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/product_configuration/rules_configurator/edit.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/js/views/product_configuration/rules_configurator/partials/Inputs.vue":
/*!*****************************************************************************************!*\
  !*** ./resources/js/views/product_configuration/rules_configurator/partials/Inputs.vue ***!
  \*****************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _Inputs_vue_vue_type_template_id_2db0b8c8___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Inputs.vue?vue&type=template&id=2db0b8c8& */ "./resources/js/views/product_configuration/rules_configurator/partials/Inputs.vue?vue&type=template&id=2db0b8c8&");
/* harmony import */ var _Inputs_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Inputs.vue?vue&type=script&lang=js& */ "./resources/js/views/product_configuration/rules_configurator/partials/Inputs.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! !../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */
;
var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__.default)(
  _Inputs_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__.default,
  _Inputs_vue_vue_type_template_id_2db0b8c8___WEBPACK_IMPORTED_MODULE_0__.render,
  _Inputs_vue_vue_type_template_id_2db0b8c8___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/product_configuration/rules_configurator/partials/Inputs.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/js/views/product_configuration/rules_configurator/edit.vue?vue&type=script&lang=js&":
/*!*******************************************************************************************************!*\
  !*** ./resources/js/views/product_configuration/rules_configurator/edit.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_edit_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./edit.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product_configuration/rules_configurator/edit.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_edit_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__.default); 

/***/ }),

/***/ "./resources/js/views/product_configuration/rules_configurator/partials/Inputs.vue?vue&type=script&lang=js&":
/*!******************************************************************************************************************!*\
  !*** ./resources/js/views/product_configuration/rules_configurator/partials/Inputs.vue?vue&type=script&lang=js& ***!
  \******************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Inputs_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./Inputs.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product_configuration/rules_configurator/partials/Inputs.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Inputs_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__.default); 

/***/ }),

/***/ "./resources/js/views/product_configuration/rules_configurator/edit.vue?vue&type=template&id=57c23604&":
/*!*************************************************************************************************************!*\
  !*** ./resources/js/views/product_configuration/rules_configurator/edit.vue?vue&type=template&id=57c23604& ***!
  \*************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_edit_vue_vue_type_template_id_57c23604___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   "staticRenderFns": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_edit_vue_vue_type_template_id_57c23604___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_edit_vue_vue_type_template_id_57c23604___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./edit.vue?vue&type=template&id=57c23604& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product_configuration/rules_configurator/edit.vue?vue&type=template&id=57c23604&");


/***/ }),

/***/ "./resources/js/views/product_configuration/rules_configurator/partials/Inputs.vue?vue&type=template&id=2db0b8c8&":
/*!************************************************************************************************************************!*\
  !*** ./resources/js/views/product_configuration/rules_configurator/partials/Inputs.vue?vue&type=template&id=2db0b8c8& ***!
  \************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Inputs_vue_vue_type_template_id_2db0b8c8___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   "staticRenderFns": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Inputs_vue_vue_type_template_id_2db0b8c8___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Inputs_vue_vue_type_template_id_2db0b8c8___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./Inputs.vue?vue&type=template&id=2db0b8c8& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product_configuration/rules_configurator/partials/Inputs.vue?vue&type=template&id=2db0b8c8&");


/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product_configuration/rules_configurator/edit.vue?vue&type=template&id=57c23604&":
/*!****************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product_configuration/rules_configurator/edit.vue?vue&type=template&id=57c23604& ***!
  \****************************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render),
/* harmony export */   "staticRenderFns": () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "v-card",
    { staticClass: "p-3" },
    [
      _c("v-card-title", [_c("b", [_vm._v("Edición de regla")])]),
      _vm._v(" "),
      _c(
        "v-card-text",
        [
          _c(
            "v-card-actions",
            { staticClass: "mt-5 d-block" },
            [
              _c("Inputs", { ref: "submit" }),
              _vm._v(" "),
              _c("form-action-buttons", {
                on: { close: _vm.cancel, save: _vm.submit }
              })
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product_configuration/rules_configurator/partials/Inputs.vue?vue&type=template&id=2db0b8c8&":
/*!***************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product_configuration/rules_configurator/partials/Inputs.vue?vue&type=template&id=2db0b8c8& ***!
  \***************************************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render),
/* harmony export */   "staticRenderFns": () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "form",
    { attrs: { "data-app": "" } },
    [
      _c(
        "v-row",
        [
          _c(
            "v-col",
            { attrs: { cols: "6", md: "12" } },
            [
              _c("h6", [_vm._v("Descripción de la regla")]),
              _vm._v(" "),
              _c("v-text-field", {
                attrs: {
                  "error-messages": _vm.descriptionErrors,
                  placeholder: "Descripción",
                  readonly: _vm.inputsStatus,
                  outlined: "",
                  required: "",
                  dense: ""
                },
                on: {
                  input: function($event) {
                    return _vm.$v.currentRule.Descripcion.$touch()
                  },
                  blur: function($event) {
                    return _vm.$v.currentRule.Descripcion.$touch()
                  }
                },
                model: {
                  value: _vm.currentRule.Descripcion,
                  callback: function($$v) {
                    _vm.$set(_vm.currentRule, "Descripcion", $$v)
                  },
                  expression: "currentRule.Descripcion"
                }
              })
            ],
            1
          )
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "v-row",
        [
          _c(
            "v-col",
            { attrs: { cols: "6", md: "2" } },
            [
              _c("h6", [_vm._v("Prioridad")]),
              _vm._v(" "),
              _c("v-text-field", {
                attrs: {
                  "error-messages": _vm.priorityErrors,
                  placeholder: "0",
                  readonly: _vm.inputsStatus,
                  type: "number",
                  min: "1",
                  outlined: "",
                  required: "",
                  dense: ""
                },
                on: {
                  input: function($event) {
                    return _vm.$v.currentRule.Prioridad.$touch()
                  },
                  blur: function($event) {
                    return _vm.$v.currentRule.Prioridad.$touch()
                  }
                },
                model: {
                  value: _vm.currentRule.Prioridad,
                  callback: function($$v) {
                    _vm.$set(_vm.currentRule, "Prioridad", $$v)
                  },
                  expression: "currentRule.Prioridad"
                }
              })
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ })

}]);