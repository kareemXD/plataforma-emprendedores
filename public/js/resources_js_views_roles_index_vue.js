(self["webpackChunk"] = self["webpackChunk"] || []).push([["resources_js_views_roles_index_vue"],{

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/roles/index.vue?vue&type=script&lang=js&":
/*!*************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/roles/index.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _store__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../store */ "./resources/js/store/index.js");
/* harmony import */ var _Layout_Components_LayoutWrapper_vue__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../Layout/Components/LayoutWrapper.vue */ "./resources/js/Layout/Components/LayoutWrapper.vue");
/* harmony import */ var _permissions_vue__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./permissions.vue */ "./resources/js/views/roles/permissions.vue");
/* harmony import */ var _components_pagination2_vue__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../components/pagination2.vue */ "./resources/js/components/pagination2.vue");
/* harmony import */ var _components_label_vue__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../components/label.vue */ "./resources/js/components/label.vue");
/* harmony import */ var _components_formActionButtons_vue__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../components/formActionButtons.vue */ "./resources/js/components/formActionButtons.vue");
/* harmony import */ var _components_datatableHeader_vue__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../components/datatableHeader.vue */ "./resources/js/components/datatableHeader.vue");
/* harmony import */ var vuelidate__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! vuelidate */ "./node_modules/vuelidate/lib/index.js");
/* harmony import */ var vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! vuelidate/lib/validators */ "./node_modules/vuelidate/lib/validators/index.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//









/**
 * Validates if the item has a unique value.
 * @auth Octavio Cornejo
 * @date 2021-02-22
 * @param String value
 * @param Object item
 * @return boolean
 */

var isUnique = function isUnique(value, item) {
  return item.isUnique;
};

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  components: {
    "layout-wrapper": _Layout_Components_LayoutWrapper_vue__WEBPACK_IMPORTED_MODULE_1__.default,
    "permissions-table": _permissions_vue__WEBPACK_IMPORTED_MODULE_2__.default,
    "pagination": _components_pagination2_vue__WEBPACK_IMPORTED_MODULE_3__.default,
    "n-label": _components_label_vue__WEBPACK_IMPORTED_MODULE_4__.default,
    "form-action-buttons": _components_formActionButtons_vue__WEBPACK_IMPORTED_MODULE_5__.default,
    "datatable-header": _components_datatableHeader_vue__WEBPACK_IMPORTED_MODULE_6__.default
  },
  mixins: [vuelidate__WEBPACK_IMPORTED_MODULE_7__.validationMixin],
  validations: {
    editedItem: {
      name: {
        required: vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_8__.required,
        maxLength: (0,vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_8__.maxLength)(50),
        isUnique: isUnique
      },
      description: {
        required: vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_8__.required,
        maxLength: (0,vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_8__.maxLength)(100)
      }
    }
  },
  data: function data() {
    return {
      page: 1,
      pageCount: 0,
      itemsPerPage: 10,
      icon: "pe-7s-key",
      search: '',
      dialog: false,
      dialogDelete: false,
      loading: false,
      dialogPermissions: false,
      valid: true,
      editedIndex: -1,
      submitStatus: null,
      errorMessage: '',
      show1: false,
      snackbar: false,
      textSnackbar: '',
      editionBlocked: false,
      editedItem: {
        name: '',
        description: '',
        status: 1,
        isUnique: true
      },
      defaultItem: {
        name: '',
        description: '',
        status: 1,
        isUnique: true
      },
      headers: [{
        text: 'Id',
        align: 'start',
        sortable: true,
        value: 'id'
      }, {
        text: 'Nombre',
        sortable: true,
        value: 'name'
      }, {
        text: 'Descripción',
        value: 'description'
      }, {
        text: 'Estatus',
        value: 'status'
      }, {
        text: 'Acciones',
        value: 'actions',
        sortable: false
      }],
      roles: []
    };
  },
  mounted: function mounted() {
    this.getData();
    this.editedItem.isUnique = true;
  },
  watch: {
    dialog: function dialog(val) {
      val || this.close();
    },
    dialogDelete: function dialogDelete(val) {
      val || this.closeDelete();
    },
    'editedItem.name': function editedItemName() {
      if (!this.editedItem.isUnique) this.editedItem.isUnique = true;
    },
    dialogPermissions: function dialogPermissions(val) {
      val || this.closePermissions();
    }
  },
  computed: {
    /**
    * Get global permissions of current user.
    * @auth José Vega
    * @date 2021-05-07
    * @param
    * @return Array
    */
    permissions: function permissions() {
      return _store__WEBPACK_IMPORTED_MODULE_0__.default.state.permissions;
    },

    /**
     * Sets the form title.
     * @auth Octavio Cornejo
     * @date 2021-02-17
     * @param
     * @return String
     */
    formTitle: function formTitle() {
      if (this.editionBlocked) return 'Detalles Rol';
      return this.editedIndex === -1 ? 'Alta de Rol ' : 'Editar Rol';
    },

    /**
     * Validates if the form submit is pending.
     * @auth Octavio Cornejo
     * @date 2021-02-17
     * @param
     * @return boolean
     */
    submitIsPending: function submitIsPending() {
      return this.submitStatus == 'PENDING';
    },

    /**
     * Validates if the form submit has error.
     * @auth Octavio Cornejo
     * @date 2021-02-17
     * @param
     * @return boolean
     */
    submitHasError: function submitHasError() {
      return this.submitStatus == 'ERROR';
    },

    /**
     * Validates name input requirements.
     * @auth Octavio Cornejo
     * @date 2021-02-17
     * @param
     * @return array errors
     */
    nameErrors: function nameErrors() {
      var errors = [];
      !this.$v.editedItem.name.required;
      if (!this.editedItem.isUnique) !this.editedItem.isUnique && errors.push('Este valor ya se encuentra registrado.');
      if (!this.$v.editedItem.name.$dirty) return errors;
      !this.$v.editedItem.name.maxLength && errors.push('No puede ser mayor a la longitud estipulada.');
      !this.$v.editedItem.name.required && errors.push('Este campo es requerido');
      !this.$v.editedItem.name.isUnique && errors.push('Este valor ya se encuentra registrado.');
      return errors;
    },

    /**
     * Validates input requirements.
     * @auth Octavio Cornejo
     * @date 2021-02-17
     * @param
     * @return array errors
     */
    descriptionErrors: function descriptionErrors() {
      var errors = [];
      if (!this.$v.editedItem.description.$dirty) return errors;
      !this.$v.editedItem.description.maxLength && errors.push('No puede ser mayor a la longitud estipulada.');
      !this.$v.editedItem.description.required && errors.push('Este campo es requerido');
      return errors;
    }
  },
  methods: {
    /**
     * Gets all activated rows.
     * @auth Octavio Cornejo
     * @date 2021-02-17
     * @param
     * @return void
     */
    getData: function getData() {
      var _this = this;

      this.loading = true;
      axios.get('/api/roles').then(function (res) {
        _this.roles = res.data.data;
      })["catch"](function (er) {
        console.warn(er.response);
      }).then(function () {
        return _this.loading = false;
      });
    },

    /**
     * Sets te selected item as editItem to further actions.
     * @auth Octavio Cornejo
     * @date 2021-02-17
     * @param object item
     * @return void
     */
    editItem: function editItem(item) {
      this.editedIndex = this.roles.indexOf(item);
      this.editedItem = Object.assign({}, item);
      this.dialog = true;
      this.editionBlocked = false;
    },

    /**
     * Sets selected item as editItem to further actions.
     * @auth Octavio Cornejo
     * @date 2021-02-17
     * @param object item
     * @return void
     */
    deleteItem: function deleteItem(item) {
      this.editedIndex = this.roles.indexOf(item);
      this.editedItem = Object.assign({}, item);
      this.dialogDelete = true;
    },

    /**
     * Shows permissions dialog.
     * @auth Octavio Cornejo
     * @date 2021-02-17
     * @param object item
     * @return void
     */
    showPermissions: function showPermissions(item) {
      this.editedIndex = this.roles.indexOf(item);
      this.editedItem = Object.assign({}, item);
      this.dialogPermissions = true;
    },

    /**
     * Request to delete the selected item.
     * @auth Octavio Cornejo
     * @date 2021-02-17
     * @param
     * @return void
     */
    deleteItemConfirm: function deleteItemConfirm() {
      var _this2 = this;

      var url = "/api/roles/".concat(this.editedItem.id);
      axios["delete"](url).then(function (res) {
        swal2.fire('Eliminado', 'Se eliminó el registro correctamente', 'success');

        _this2.roles.splice(_this2.editedIndex, 1);

        _this2.closeDelete();
      })["catch"](function (er) {
        _this2.submitStatus = 'ERROR';
        _this2.errorMessage = 'Ocurrió un error. Intente en otro momento.';
      });
    },

    /**
     * Closes and restart the form.
     * @auth Octavio Cornejo
     * @date 2021-02-17
     * @param
     * @return void
     */
    close: function close() {
      var _this3 = this;

      this.$v.$reset();
      this.dialog = false;
      this.submitStatus = null;
      this.$nextTick(function () {
        _this3.editedItem = Object.assign({}, _this3.defaultItem);
        _this3.editedIndex = -1;
      });
    },

    /**
     * Closes and restart the delete dialog.
     * @auth Octavio Cornejo
     * @date 2021-02-17
     * @param
     * @return void
     */
    closeDelete: function closeDelete() {
      var _this4 = this;

      this.dialogDelete = false;
      this.$nextTick(function () {
        _this4.editedItem = Object.assign({}, _this4.defaultItem);
        _this4.editedIndex = -1;
      });
    },
    closePermissions: function closePermissions() {
      var _this5 = this;

      this.dialogPermissions = false;
      this.$nextTick(function () {
        _this5.editedItem = Object.assign({}, _this5.defaultItem);
        _this5.editedIndex = -1;
      });
    },

    /**
     * Validates the form and then saves/updates the given data.
     * @auth Octavio Cornejo
     * @date 2021-02-17
     * @param
     * @return void
     */
    save: function save() {
      this.$v.$touch();

      if (this.$v.$invalid) {
        this.submitStatus = 'ERROR';
        this.errorMessage = 'Es necesario corregir los campos con error.';
      } else {
        this.submitStatus = 'PENDING';

        if (this.editedIndex > -1) {
          this.saveEdited();
        } else {
          this.saveNew();
        }
      }
    },

    /**
     * Saves a new register with the given data.
     * @auth Octavio Cornejo
     * @date 2021-02-17
     * @param
     * @return void
     */
    saveNew: function saveNew() {
      var _this6 = this;

      axios.post('/api/roles', this.editedItem).then(function (res) {
        _this6.submitStatus = null;
        _this6.editedItem.id = res.data.data.id;

        _this6.roles.push(_this6.editedItem);

        _this6.close();

        swal2.fire('Guardado', 'Se guardó el registro correctamente', 'success');
      })["catch"](this.handleError);
    },

    /**
     * Updates a new register with the given data.
     * @auth Octavio Cornejo
     * @date 2021-02-17
     * @param
     * @return void
     */
    saveEdited: function saveEdited() {
      var _this7 = this;

      var url = "/api/roles/".concat(this.editedItem.id);
      axios.put(url, this.editedItem).then(function (res) {
        _this7.submitStatus = null;
        Object.assign(_this7.roles[_this7.editedIndex], _this7.editedItem);

        _this7.close();

        swal2.fire('Actualizado', 'Se actualizó el registro correctamente', 'success');
      })["catch"](this.handleError);
    },

    /**
     * Opens the formulary.
     * @auth Octavio Cornejo
     * @date 2021-02-17
     * @param
     * @return void
     */
    openDialog: function openDialog() {
      this.dialog = true;
      this.editionBlocked = false;
    },

    /**
     * Gets the status name.
     * @auth Octavio Cornejo
     * @date 2021-02-19
     * @param Object item
     * @return String
     */
    getStatusName: function getStatusName(item) {
      return item.status ? 'Activo' : 'Inactivo';
    },

    /**
     * Gets status color.
     * @auth Octavio Cornejo
     * @date 2021-02-19
     * @param Object item
     * @return String
     */
    getStatusColor: function getStatusColor(item) {
      return item.status ? 'success' : 'red';
    },

    /**
     * Handle errors from failed requests.
     * @auth Octavio Cornejo
     * @date 2021-02-25
     * @param Object er
     * @return void
     */
    handleError: function handleError(er) {
      var res = er.response;
      console.error(res);
      this.submitStatus = 'ERROR';
      this.errorMessage = 'Ocurrió un error. Intente en otro momento.';
      if (res.status >= 500) return;
      this.errorMessage = res.data.message;

      for (var i in res.data.errors) {
        this.editedItem.isUnique = false;
      }
    },

    /**
     * Shows the snackbar
     * @auth Octavio Cornejo
     * @date 2021-02-24
     * @param String text
     * @return void
     */
    showSnackbar: function showSnackbar(text) {
      this.snackbar = true;
      this.textSnackbar = text;
    },

    /**
     * Show details vie.
     * @auth Octavio Cornejo
     * @date 2021-03-09
     * @param Object item
     * @return void
     */
    viewItem: function viewItem(item) {
      this.editedIndex = this.roles.indexOf(item);
      this.editedItem = Object.assign({}, item);
      this.dialog = true;
      this.editionBlocked = true;
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/roles/permissions.vue?vue&type=script&lang=js&":
/*!*******************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/roles/permissions.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  props: {
    role: Object,
    dialog2: Boolean
  },
  data: function data() {
    return {
      submitStatus: null,
      headers: [{
        text: 'Módulo',
        align: 'start',
        value: 'perm_display_name'
      }, {
        text: 'Todos',
        sortable: false,
        value: 'all'
      }, {
        text: 'Crear',
        sortable: false,
        value: 'create'
      }, {
        text: 'Editar',
        sortable: false,
        value: 'edit'
      }, {
        text: 'Consulta',
        sortable: false,
        value: 'view'
      }, {
        text: 'Eliminar',
        sortable: false,
        value: 'delete'
      }],
      permissions: []
    };
  },
  mounted: function mounted() {
    this.getAvailablePermissions();
  },
  watch: {
    dialog2: function dialog2(val) {
      if (val) this.getAvailablePermissions();
    }
  },
  computed: {
    /**
     * Validates if the form submit is pending.
     * @auth Octavio Cornejo
     * @date 2021-02-17
     * @param
     * @return boolean
     */
    submitIsPending: function submitIsPending() {
      return this.submitStatus == 'PENDING';
    },

    /**
     * Validates if the form submit has error.
     * @auth Octavio Cornejo
     * @date 2021-02-17
     * @param
     * @return boolean
     */
    submitHasError: function submitHasError() {
      return this.submitStatus == 'ERROR';
    }
  },
  methods: {
    /**
     * Saves the permissions status and relates to selected role.
     * @auth Octavio Cornejo
     * @date 2021-02-24
     * @param
     * @return void
     */
    save: function save() {
      var _this = this;

      this.submitStatus = 'PENDING';
      var url = "/api/roles/".concat(this.role.id, "/sync-permissions");
      axios.post(url, this.permissions).then(function (res) {
        _this.close();

        _this.submitStatus = null;

        _this.$emit('showSnackbar', 'Los permisos fueron asignados');
      })["catch"](function (er) {
        return _this.handleError;
      });
    },

    /**
     * Closes the opened dialog.
     * @auth Octavio Cornejo
     * @date 2021-02-24
     * @param
     * @return void
     */
    close: function close() {
      this.$emit('closeDialog');
    },

    /**
     * Get all available permissions with the status of each permission related to the selected role.
     * @auth Octavio Cornejo
     * @date 2021-02-24
     * @param
     * @return void
     */
    getAvailablePermissions: function getAvailablePermissions() {
      var _this2 = this;

      var url = "/api/roles/".concat(this.role.id, "/permissions");
      axios.get(url).then(function (res) {
        _this2.permissions = res.data.permissions;
      })["catch"](function (er) {
        return _this2.handleError;
      });
    },

    /**
     * Changes the status of permissions depending on 'all' input value.
     * @auth Octavio Cornejo
     * @date 2021-02-23
     * @param Object item
     * @return void
     */
    checkChanged: function checkChanged(item) {
      if (item.all) {
        item.create = true;
        item.edit = true;
        item.view = true;
        item["delete"] = true;
      } else {
        item.create = false;
        item.edit = false;
        item.view = false;
        item["delete"] = false;
      }
    },

    /**
     * Changes the status of 'all' depending on other input value.
     * @auth Oscar Castellanos
     * @date 2021-06-11
     * @param Object item
     * @return void
     */
    checkChange: function checkChange(item) {
      if (item.create == false || !item.edit == false || !item.view == false || !item["delete"] == false) {
        item.all = false;
      }
    },

    /**
     * Handles the error responses.
     * @auth Octavio Cornejo
     * @date 2021-02-24
     * @param Object er
     * @return void
     */
    handleError: function handleError(er) {
      var res = er.response;
      console.error(res);
      this.submitStatus = 'ERROR';
      this.errorMessage = 'Ocurrió un error. Intente en otro momento.';
      if (res.status >= 500) return;
      this.errorMessage = res.data.message;
    }
  }
});

/***/ }),

/***/ "./resources/js/views/roles/index.vue":
/*!********************************************!*\
  !*** ./resources/js/views/roles/index.vue ***!
  \********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _index_vue_vue_type_template_id_0014ba5e___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./index.vue?vue&type=template&id=0014ba5e& */ "./resources/js/views/roles/index.vue?vue&type=template&id=0014ba5e&");
/* harmony import */ var _index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./index.vue?vue&type=script&lang=js& */ "./resources/js/views/roles/index.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! !../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */
;
var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__.default)(
  _index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__.default,
  _index_vue_vue_type_template_id_0014ba5e___WEBPACK_IMPORTED_MODULE_0__.render,
  _index_vue_vue_type_template_id_0014ba5e___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/roles/index.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/js/views/roles/permissions.vue":
/*!**************************************************!*\
  !*** ./resources/js/views/roles/permissions.vue ***!
  \**************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _permissions_vue_vue_type_template_id_78be1c43___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./permissions.vue?vue&type=template&id=78be1c43& */ "./resources/js/views/roles/permissions.vue?vue&type=template&id=78be1c43&");
/* harmony import */ var _permissions_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./permissions.vue?vue&type=script&lang=js& */ "./resources/js/views/roles/permissions.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! !../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */
;
var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__.default)(
  _permissions_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__.default,
  _permissions_vue_vue_type_template_id_78be1c43___WEBPACK_IMPORTED_MODULE_0__.render,
  _permissions_vue_vue_type_template_id_78be1c43___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/roles/permissions.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/js/views/roles/index.vue?vue&type=script&lang=js&":
/*!*********************************************************************!*\
  !*** ./resources/js/views/roles/index.vue?vue&type=script&lang=js& ***!
  \*********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./index.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/roles/index.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__.default); 

/***/ }),

/***/ "./resources/js/views/roles/permissions.vue?vue&type=script&lang=js&":
/*!***************************************************************************!*\
  !*** ./resources/js/views/roles/permissions.vue?vue&type=script&lang=js& ***!
  \***************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_permissions_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./permissions.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/roles/permissions.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_permissions_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__.default); 

/***/ }),

/***/ "./resources/js/views/roles/index.vue?vue&type=template&id=0014ba5e&":
/*!***************************************************************************!*\
  !*** ./resources/js/views/roles/index.vue?vue&type=template&id=0014ba5e& ***!
  \***************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_0014ba5e___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   "staticRenderFns": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_0014ba5e___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_0014ba5e___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./index.vue?vue&type=template&id=0014ba5e& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/roles/index.vue?vue&type=template&id=0014ba5e&");


/***/ }),

/***/ "./resources/js/views/roles/permissions.vue?vue&type=template&id=78be1c43&":
/*!*********************************************************************************!*\
  !*** ./resources/js/views/roles/permissions.vue?vue&type=template&id=78be1c43& ***!
  \*********************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_permissions_vue_vue_type_template_id_78be1c43___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   "staticRenderFns": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_permissions_vue_vue_type_template_id_78be1c43___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_permissions_vue_vue_type_template_id_78be1c43___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./permissions.vue?vue&type=template&id=78be1c43& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/roles/permissions.vue?vue&type=template&id=78be1c43&");


/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/roles/index.vue?vue&type=template&id=0014ba5e&":
/*!******************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/roles/index.vue?vue&type=template&id=0014ba5e& ***!
  \******************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render),
/* harmony export */   "staticRenderFns": () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { attrs: { "data-app": "" } },
    [
      _vm.dialog
        ? _c(
            "layout-wrapper",
            { attrs: { transition: "scroll-y-transition" } },
            [
              _c(
                "v-card",
                { staticClass: "p-3" },
                [
                  _c("v-card-title", [
                    _c("span", { staticClass: "headline" }, [
                      _vm._v(_vm._s(_vm.formTitle))
                    ])
                  ]),
                  _vm._v(" "),
                  _c(
                    "v-card-text",
                    [
                      _vm.submitHasError
                        ? _c(
                            "v-alert",
                            {
                              attrs: {
                                dismissible: "",
                                elevation: "5",
                                text: "",
                                type: "error"
                              }
                            },
                            [
                              _vm._v(
                                "\n                    " +
                                  _vm._s(_vm.errorMessage) +
                                  "\n                "
                              )
                            ]
                          )
                        : _vm._e(),
                      _vm._v(" "),
                      _c(
                        "v-form",
                        {
                          ref: "form",
                          attrs: { "lazy-validation": "" },
                          model: {
                            value: _vm.valid,
                            callback: function($$v) {
                              _vm.valid = $$v
                            },
                            expression: "valid"
                          }
                        },
                        [
                          _c(
                            "v-row",
                            [
                              _c(
                                "v-col",
                                { staticClass: "py-0", attrs: { cols: "12" } },
                                [
                                  _c("n-label", {
                                    attrs: {
                                      text: "Nombre del Rol",
                                      required: "",
                                      error: _vm.$v.editedItem.name.$error
                                    }
                                  }),
                                  _vm._v(" "),
                                  _c("v-text-field", {
                                    attrs: {
                                      readonly: _vm.editionBlocked,
                                      outlined: "",
                                      dense: "",
                                      placeholder: "Nombre del Rol",
                                      "error-messages": _vm.nameErrors,
                                      counter: 50,
                                      required: ""
                                    },
                                    on: {
                                      input: function($event) {
                                        return _vm.$v.editedItem.name.$touch()
                                      },
                                      blur: function($event) {
                                        return _vm.$v.editedItem.name.$touch()
                                      }
                                    },
                                    model: {
                                      value: _vm.editedItem.name,
                                      callback: function($$v) {
                                        _vm.$set(_vm.editedItem, "name", $$v)
                                      },
                                      expression: "editedItem.name"
                                    }
                                  })
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _c(
                                "v-col",
                                { staticClass: "py-0", attrs: { cols: "12" } },
                                [
                                  _c("n-label", {
                                    attrs: {
                                      text: "Descripción del Rol",
                                      required: "",
                                      error:
                                        _vm.$v.editedItem.description.$error
                                    }
                                  }),
                                  _vm._v(" "),
                                  _c("v-text-field", {
                                    attrs: {
                                      readonly: _vm.editionBlocked,
                                      outlined: "",
                                      dense: "",
                                      placeholder: "Descripción del Rol",
                                      "error-messages": _vm.descriptionErrors,
                                      counter: 100,
                                      required: ""
                                    },
                                    on: {
                                      input: function($event) {
                                        return _vm.$v.editedItem.description.$touch()
                                      },
                                      blur: function($event) {
                                        return _vm.$v.editedItem.description.$touch()
                                      }
                                    },
                                    model: {
                                      value: _vm.editedItem.description,
                                      callback: function($$v) {
                                        _vm.$set(
                                          _vm.editedItem,
                                          "description",
                                          $$v
                                        )
                                      },
                                      expression: "editedItem.description"
                                    }
                                  })
                                ],
                                1
                              )
                            ],
                            1
                          )
                        ],
                        1
                      )
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "v-card-actions",
                    [
                      _c("form-action-buttons", {
                        attrs: {
                          hideSaveBtn: _vm.editionBlocked,
                          isLoading: _vm.submitIsPending
                        },
                        on: { close: _vm.close, save: _vm.save }
                      })
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          )
        : _c(
            "layout-wrapper",
            { attrs: { transition: "scroll-y-transition" } },
            [
              _c(
                "v-card",
                { staticClass: "p-3" },
                [
                  _c(
                    "v-dialog",
                    {
                      attrs: { "max-width": "65%" },
                      model: {
                        value: _vm.dialogPermissions,
                        callback: function($$v) {
                          _vm.dialogPermissions = $$v
                        },
                        expression: "dialogPermissions"
                      }
                    },
                    [
                      _c(
                        "v-card",
                        [
                          _c("v-card-title", { staticClass: "headline" }, [
                            _vm._v(
                              "\n                        Asignación de permisos: "
                            ),
                            _c("b", [_vm._v(_vm._s(_vm.editedItem.name))])
                          ]),
                          _vm._v(" "),
                          _c(
                            "v-card-text",
                            [
                              _c("permissions-table", {
                                attrs: {
                                  role: _vm.editedItem,
                                  dialog2: _vm.dialogPermissions
                                },
                                on: {
                                  closeDialog: _vm.closePermissions,
                                  showSnackbar: _vm.showSnackbar
                                }
                              })
                            ],
                            1
                          )
                        ],
                        1
                      )
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "v-dialog",
                    {
                      attrs: { "max-width": "500px" },
                      model: {
                        value: _vm.dialogDelete,
                        callback: function($$v) {
                          _vm.dialogDelete = $$v
                        },
                        expression: "dialogDelete"
                      }
                    },
                    [
                      _c(
                        "v-card",
                        [
                          _c(
                            "v-card-title",
                            {
                              staticClass: "headline",
                              staticStyle: { "justify-content": "center" }
                            },
                            [_vm._v("¿Estás seguro de eliminar este registro?")]
                          ),
                          _vm._v(" "),
                          _c(
                            "v-card-actions",
                            [
                              _c("v-spacer"),
                              _vm._v(" "),
                              _c(
                                "v-btn",
                                {
                                  attrs: { color: "blue darken-1", text: "" },
                                  on: { click: _vm.closeDelete }
                                },
                                [_vm._v("Cancelar")]
                              ),
                              _vm._v(" "),
                              _c(
                                "v-btn",
                                {
                                  attrs: {
                                    outlined: "",
                                    color: "red",
                                    text: ""
                                  },
                                  on: { click: _vm.deleteItemConfirm }
                                },
                                [_vm._v("Sí, eliminalo")]
                              ),
                              _vm._v(" "),
                              _c("v-spacer")
                            ],
                            1
                          )
                        ],
                        1
                      )
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c("v-data-table", {
                    staticClass: "elevation-1",
                    attrs: {
                      headers: _vm.headers,
                      items: _vm.roles,
                      "item-key": "id",
                      search: _vm.search,
                      page: _vm.page,
                      "items-per-page": _vm.itemsPerPage,
                      "hide-default-footer": "",
                      "no-data-text": "No hay ningún registro",
                      "no-results-text": "No se encontraron coincidencias",
                      loading: _vm.loading,
                      "loading-text": "Cargando... Porfavor espere"
                    },
                    on: {
                      "update:page": function($event) {
                        _vm.page = $event
                      },
                      "page-count": function($event) {
                        _vm.pageCount = $event
                      }
                    },
                    scopedSlots: _vm._u([
                      {
                        key: "top",
                        fn: function() {
                          return [
                            _c("datatable-header", {
                              attrs: {
                                title: "Roles",
                                "disable-creation": !_vm.permissions.includes(
                                  "Roles.create"
                                )
                              },
                              on: {
                                searching: function($event) {
                                  _vm.search = $event
                                },
                                open: _vm.openDialog
                              }
                            })
                          ]
                        },
                        proxy: true
                      },
                      {
                        key: "item.status",
                        fn: function(ref) {
                          var item = ref.item
                          return [_c("chip-status", { attrs: { item: item } })]
                        }
                      },
                      {
                        key: "item.actions",
                        fn: function(ref) {
                          var item = ref.item
                          return [
                            _c(
                              "v-tooltip",
                              {
                                attrs: { top: "" },
                                scopedSlots: _vm._u(
                                  [
                                    {
                                      key: "activator",
                                      fn: function(ref) {
                                        var on = ref.on
                                        var attrs = ref.attrs
                                        return [
                                          _vm.permissions.includes("Roles.edit")
                                            ? _c(
                                                "v-icon",
                                                _vm._g(
                                                  _vm._b(
                                                    {
                                                      staticClass: "mr-2",
                                                      attrs: {
                                                        small: "",
                                                        color: "primary"
                                                      },
                                                      on: {
                                                        click: function(
                                                          $event
                                                        ) {
                                                          return _vm.editItem(
                                                            item
                                                          )
                                                        }
                                                      }
                                                    },
                                                    "v-icon",
                                                    attrs,
                                                    false
                                                  ),
                                                  on
                                                ),
                                                [
                                                  _vm._v(
                                                    "\n                                mdi-pencil\n                            "
                                                  )
                                                ]
                                              )
                                            : _vm._e()
                                        ]
                                      }
                                    }
                                  ],
                                  null,
                                  true
                                )
                              },
                              [_vm._v(" "), _c("span", [_vm._v("Editar")])]
                            ),
                            _vm._v(" "),
                            _c(
                              "v-tooltip",
                              {
                                attrs: { top: "" },
                                scopedSlots: _vm._u(
                                  [
                                    {
                                      key: "activator",
                                      fn: function(ref) {
                                        var on = ref.on
                                        var attrs = ref.attrs
                                        return [
                                          _vm.permissions.includes("Roles.edit")
                                            ? _c(
                                                "v-icon",
                                                _vm._g(
                                                  _vm._b(
                                                    {
                                                      attrs: {
                                                        small: "",
                                                        color: "primary"
                                                      },
                                                      on: {
                                                        click: function(
                                                          $event
                                                        ) {
                                                          return _vm.showPermissions(
                                                            item
                                                          )
                                                        }
                                                      }
                                                    },
                                                    "v-icon",
                                                    attrs,
                                                    false
                                                  ),
                                                  on
                                                ),
                                                [
                                                  _vm._v(
                                                    "\n                                mdi-lock\n                            "
                                                  )
                                                ]
                                              )
                                            : _vm._e()
                                        ]
                                      }
                                    }
                                  ],
                                  null,
                                  true
                                )
                              },
                              [_vm._v(" "), _c("span", [_vm._v("Permisos")])]
                            ),
                            _vm._v(" "),
                            _c(
                              "v-tooltip",
                              {
                                attrs: { top: "" },
                                scopedSlots: _vm._u(
                                  [
                                    {
                                      key: "activator",
                                      fn: function(ref) {
                                        var on = ref.on
                                        var attrs = ref.attrs
                                        return [
                                          _vm.permissions.includes("Roles.view")
                                            ? _c(
                                                "v-icon",
                                                _vm._g(
                                                  _vm._b(
                                                    {
                                                      attrs: {
                                                        small: "",
                                                        color: "primary"
                                                      },
                                                      on: {
                                                        click: function(
                                                          $event
                                                        ) {
                                                          return _vm.viewItem(
                                                            item
                                                          )
                                                        }
                                                      }
                                                    },
                                                    "v-icon",
                                                    attrs,
                                                    false
                                                  ),
                                                  on
                                                ),
                                                [
                                                  _vm._v(
                                                    "\n                                mdi-eye\n                            "
                                                  )
                                                ]
                                              )
                                            : _vm._e()
                                        ]
                                      }
                                    }
                                  ],
                                  null,
                                  true
                                )
                              },
                              [_vm._v(" "), _c("span", [_vm._v("Mostrar")])]
                            ),
                            _vm._v(" "),
                            _c(
                              "v-tooltip",
                              {
                                attrs: { top: "" },
                                scopedSlots: _vm._u(
                                  [
                                    {
                                      key: "activator",
                                      fn: function(ref) {
                                        var on = ref.on
                                        var attrs = ref.attrs
                                        return [
                                          _vm.permissions.includes(
                                            "Roles.delete"
                                          )
                                            ? _c(
                                                "v-icon",
                                                _vm._g(
                                                  _vm._b(
                                                    {
                                                      attrs: {
                                                        small: "",
                                                        color: "primary"
                                                      },
                                                      on: {
                                                        click: function(
                                                          $event
                                                        ) {
                                                          return _vm.deleteItem(
                                                            item
                                                          )
                                                        }
                                                      }
                                                    },
                                                    "v-icon",
                                                    attrs,
                                                    false
                                                  ),
                                                  on
                                                ),
                                                [
                                                  _vm._v(
                                                    "\n                                mdi-delete\n                            "
                                                  )
                                                ]
                                              )
                                            : _vm._e()
                                        ]
                                      }
                                    }
                                  ],
                                  null,
                                  true
                                )
                              },
                              [_vm._v(" "), _c("span", [_vm._v("Eliminar")])]
                            )
                          ]
                        }
                      }
                    ])
                  }),
                  _vm._v(" "),
                  _c("pagination", {
                    attrs: {
                      last: _vm.pageCount,
                      current: _vm.page,
                      itemsPerPage: _vm.itemsPerPage,
                      total: this.roles.length
                    },
                    on: {
                      input: function($event) {
                        _vm.page = $event
                      }
                    }
                  })
                ],
                1
              )
            ],
            1
          ),
      _vm._v(" "),
      _c(
        "v-snackbar",
        {
          attrs: { absolute: "", right: "", top: "", color: "success" },
          scopedSlots: _vm._u([
            {
              key: "action",
              fn: function(ref) {
                var attrs = ref.attrs
                return [
                  _c(
                    "v-btn",
                    _vm._b(
                      {
                        attrs: { color: "red", icon: "" },
                        on: {
                          click: function($event) {
                            _vm.snackbar = false
                          }
                        }
                      },
                      "v-btn",
                      attrs,
                      false
                    ),
                    [
                      _c("v-icon", [
                        _vm._v(
                          "\n                    mdi-close\n                "
                        )
                      ])
                    ],
                    1
                  )
                ]
              }
            }
          ]),
          model: {
            value: _vm.snackbar,
            callback: function($$v) {
              _vm.snackbar = $$v
            },
            expression: "snackbar"
          }
        },
        [_vm._v("\n        " + _vm._s(_vm.textSnackbar) + "\n        ")]
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/roles/permissions.vue?vue&type=template&id=78be1c43&":
/*!************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/roles/permissions.vue?vue&type=template&id=78be1c43& ***!
  \************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render),
/* harmony export */   "staticRenderFns": () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c("v-data-table", {
        staticClass: "elevation-1",
        attrs: {
          headers: _vm.headers,
          items: _vm.permissions,
          "items-per-page": 10,
          "hide-default-footer": "",
          loading: _vm.submitIsPending,
          "loading-text": "Cargando... Porfavor espere"
        },
        scopedSlots: _vm._u([
          {
            key: "item.all",
            fn: function(ref) {
              var item = ref.item
              return [
                _c("v-checkbox", {
                  attrs: { color: "success" },
                  on: {
                    click: function($event) {
                      return _vm.checkChanged(item)
                    }
                  },
                  model: {
                    value: item.all,
                    callback: function($$v) {
                      _vm.$set(item, "all", $$v)
                    },
                    expression: "item.all"
                  }
                })
              ]
            }
          },
          {
            key: "item.create",
            fn: function(ref) {
              var item = ref.item
              return [
                _c("v-checkbox", {
                  attrs: { color: "success" },
                  on: {
                    click: function($event) {
                      return _vm.checkChange(item)
                    }
                  },
                  model: {
                    value: item.create,
                    callback: function($$v) {
                      _vm.$set(item, "create", $$v)
                    },
                    expression: "item.create"
                  }
                })
              ]
            }
          },
          {
            key: "item.edit",
            fn: function(ref) {
              var item = ref.item
              return [
                _c("v-checkbox", {
                  attrs: { color: "success" },
                  on: {
                    click: function($event) {
                      return _vm.checkChange(item)
                    }
                  },
                  model: {
                    value: item.edit,
                    callback: function($$v) {
                      _vm.$set(item, "edit", $$v)
                    },
                    expression: "item.edit"
                  }
                })
              ]
            }
          },
          {
            key: "item.view",
            fn: function(ref) {
              var item = ref.item
              return [
                _c("v-checkbox", {
                  attrs: { color: "success" },
                  on: {
                    click: function($event) {
                      return _vm.checkChange(item)
                    }
                  },
                  model: {
                    value: item.view,
                    callback: function($$v) {
                      _vm.$set(item, "view", $$v)
                    },
                    expression: "item.view"
                  }
                })
              ]
            }
          },
          {
            key: "item.delete",
            fn: function(ref) {
              var item = ref.item
              return [
                _c("v-checkbox", {
                  attrs: { color: "success" },
                  on: {
                    click: function($event) {
                      return _vm.checkChange(item)
                    }
                  },
                  model: {
                    value: item.delete,
                    callback: function($$v) {
                      _vm.$set(item, "delete", $$v)
                    },
                    expression: "item.delete"
                  }
                })
              ]
            }
          }
        ])
      }),
      _vm._v(" "),
      _c("form-action-buttons", {
        attrs: { isLoading: _vm.submitIsPending },
        on: { close: _vm.close, save: _vm.save }
      })
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ })

}]);