(self["webpackChunk"] = self["webpackChunk"] || []).push([["resources_js_views_product-configuration_global-costs-configurator_index_vue"],{

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/global-costs-configurator/create.vue?vue&type=script&lang=js&":
/*!********************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/global-costs-configurator/create.vue?vue&type=script&lang=js& ***!
  \********************************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var vuelidate__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vuelidate */ "./node_modules/vuelidate/lib/index.js");
/* harmony import */ var vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vuelidate/lib/validators */ "./node_modules/vuelidate/lib/validators/index.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  name: "CreateGlobalCost",
  props: {
    title: {
      type: String,
      "default": "Alta de Costo Global"
    },
    editCost: {
      type: Boolean,
      "default": false,
      required: false
    },
    showCost: {
      type: Boolean,
      "default": false,
      required: false
    },
    inputsStatus: {
      type: Boolean,
      "default": false,
      required: false
    },
    costToShow: {
      type: Object,
      "default": null,
      required: false
    },
    basesFromShow: {
      type: Array,
      "default": null,
      required: false
    }
  },
  mounted: function mounted() {
    this.bases = this.$route.params.bases;

    if (this.$route.params.item != null) {
      this.currentGlobalCost.Id_ConceptoCosto = this.$route.params.item.Id_ConceptoCosto;
      this.currentGlobalCost.Descripcion = this.$route.params.item.Descripcion;
      this.currentGlobalCost.Tipo = this.$route.params.item.Tipo;
      this.currentGlobalCost.Porcentaje = this.$route.params.item.Porcentaje;
      this.currentGlobalCost.Monto = this.$route.params.item.Monto;
      this.currentGlobalCost.Id_BaseCostoCalculado = this.$route.params.item.Id_BaseCostoCalculado;
      this.checkInputs();
      if (this.currentGlobalCost.Porcentaje != null) this.currentGlobalCost.Monto = this.currentGlobalCost.Porcentaje;
      if (this.currentGlobalCost.Id_BaseCostoCalculado != null) this.currentGlobalCost.Id_BaseCostoCalculado = parseInt(this.$route.params.item.Id_BaseCostoCalculado);
    } else if (this.costToShow != null) {
      this.bases = this.basesFromShow;
      this.currentGlobalCost.Descripcion = this.costToShow.Descripcion;
      this.currentGlobalCost.Tipo = this.costToShow.Tipo;
      this.currentGlobalCost.Porcentaje = this.costToShow.Porcentaje;
      this.currentGlobalCost.Monto = this.costToShow.Monto;
      this.currentGlobalCost.Id_BaseCostoCalculado = this.costToShow.Id_BaseCostoCalculado;
      this.checkInputs();
      if (this.currentGlobalCost.Porcentaje != null) this.currentGlobalCost.Monto = this.currentGlobalCost.Porcentaje;

      if (this.currentGlobalCost.Id_BaseCostoCalculado != null) {
        this.currentGlobalCost.Id_BaseCostoCalculado = parseInt(this.costToShow.Id_BaseCostoCalculado);
      }
    }
  },
  mixins: [vuelidate__WEBPACK_IMPORTED_MODULE_0__.validationMixin],
  validations: {
    currentGlobalCost: {
      Descripcion: {
        required: vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_1__.required
      },
      Tipo: {
        required: vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_1__.required
      },
      Monto: {
        required: vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_1__.required
      }
    }
  },
  data: function data() {
    return {
      submitStatus: "",
      showBases: false,
      showPercentage: false,
      bases: [],
      currentGlobalCost: {
        Id_ConceptoCosto: null,
        Descripcion: "",
        Tipo: "",
        Porcentaje: null,
        Monto: null,
        Id_BaseCostoCalculado: null
      }
    };
  },
  methods: {
    /**
     * Allows do submit if data is valid.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-03-31
     * @param
     * @return void
     */
    submit: function submit() {
      var _this = this;

      this.$v.$touch();

      if (this.$v.$invalid) {
        this.submitStatus = "ERROR";
      } else if (!this.$v.$invalid) {
        switch (this.currentGlobalCost.Tipo) {
          case "P":
            this.currentGlobalCost.Porcentaje = this.currentGlobalCost.Monto;
            this.currentGlobalCost.Id_BaseCostoCalculado = null;
            break;

          case "M":
            this.currentGlobalCost.Porcentaje = null;
            this.currentGlobalCost.Id_BaseCostoCalculado = null;
            break;

          case "C":
            this.currentGlobalCost.Porcentaje = null;
            break;
        }

        this.editCost != true ? this.post(this.currentGlobalCost) : this.edit(this.currentGlobalCost);
      } else {
        this.submitStatus = "PENDING";
        setTimeout(function () {
          _this.submitStatus = "OK";
        }, 500);
      }
    },

    /**
     * Post a new cost.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-03-31
     * @param Object data
     * @return void
     */
    post: function post(data) {
      var _this2 = this;

      axios.post("/api/global-costs-configurators", data).then(function (result) {
        swal2.fire("Creado", "Se agregaron los datos correctamente", "success");
      })["catch"](function (error) {
        swal2.fire({
          icon: "error",
          title: "Oops...",
          text: "Algo salió mal, vuelve a intentarlo!"
        });
      })["finally"](function () {
        _this2.$router.push({
          name: "global-costs-configurators"
        });
      });
    },

    /**
     * Update a cost.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-04-06
     * @param Object data
     * @return void
     */
    edit: function edit(data) {
      var _this3 = this;

      axios.put("/api/global-costs-configurators/" + data.Id_ConceptoCosto, data).then(function (result) {
        swal2.fire("Editado", "Se editó el costo correctamente", "success");

        _this3.$router.push({
          name: "global-costs-configurators"
        });
      })["catch"](function (error) {
        swal2.fire({
          icon: "error",
          title: "Oops...",
          text: "Algo salió mal, vuelve a intentarlo!"
        });
      });
    },

    /**
     * Redirect to index rules configurator.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-03-31
     * @param
     * @return void
     */
    cancel: function cancel() {
      this.showCost ? this.$emit("dialogStatus", false) : this.$router.push({
        name: "global-costs-configurators"
      });
    },

    /**
     * Enables and disables inputs depending on the type.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-03-31
     * @param
     * @return void
     */
    checkInputs: function checkInputs() {
      switch (this.currentGlobalCost.Tipo) {
        case "P":
          this.showBases = false;
          this.showPercentage = true;
          break;

        case "M":
          this.showBases = false;
          this.showPercentage = false;
          break;

        case "C":
          this.showBases = true;
          this.showPercentage = false;
          break;
      }
    }
  },
  computed: {
    /**
     * Evaluate if an input is valid.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-03-31
     * @param
     * @return array
     */
    descriptionErrors: function descriptionErrors() {
      var errors = [];
      if (!this.$v.currentGlobalCost.Descripcion.$dirty) return errors;
      !this.$v.currentGlobalCost.Descripcion.required && errors.push("El campo es requerido");
      return errors;
    },

    /**
     * Evaluate if an input is valid.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-03-31
     * @param
     * @return array
     */
    typeErrors: function typeErrors() {
      var errors = [];
      if (!this.$v.currentGlobalCost.Tipo.$dirty) return errors;
      !this.$v.currentGlobalCost.Tipo.required && errors.push("El campo es requerido");
      return errors;
    },

    /**
     * Evaluate if an input is valid.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-03-31
     * @param
     * @return array
     */
    amountErrors: function amountErrors() {
      var errors = [];
      if (!this.$v.currentGlobalCost.Monto.$dirty) return errors;
      !this.$v.currentGlobalCost.Monto.required && errors.push("El campo es requerido");
      return errors;
    }
  },
  watch: {
    /**
     * Asign a new value to priceSelected.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-02-18
     * @param any val
     * @return void
     */
    costToShow: function costToShow(val) {
      this.currentGlobalCost = val;
      this.checkInputs();
      if (this.currentGlobalCost.Porcentaje != null) this.currentGlobalCost.Monto = this.currentGlobalCost.Porcentaje;
      if (this.currentGlobalCost.Id_BaseCostoCalculado != null) this.currentGlobalCost.Id_BaseCostoCalculado = parseInt(this.currentGlobalCost.Id_BaseCostoCalculado);
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/global-costs-configurator/index.vue?vue&type=script&lang=js&":
/*!*******************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/global-costs-configurator/index.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _components_chipStatus__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../components/chipStatus */ "./resources/js/components/chipStatus.vue");
/* harmony import */ var _show__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./show */ "./resources/js/views/product-configuration/global-costs-configurator/show.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  mounted: function mounted() {
    this.getGlobalCosts();
  },
  components: {
    ChipStatus: _components_chipStatus__WEBPACK_IMPORTED_MODULE_0__.default,
    Show: _show__WEBPACK_IMPORTED_MODULE_1__.default
  },
  data: function data() {
    return {
      search: "",
      dialog: false,
      productId: null,
      dialogDelete: false,
      dialogShow: false,
      globalCostToDelete: null,
      page: 1,
      pageCount: 0,
      totalItems: 0,
      itemsPerPage: 10,
      loading: false,
      globalCostToShow: {},
      headers: [{
        text: "Nombre",
        value: "Descripcion"
      }, {
        text: "Tipo",
        value: "Tipo"
      }, {
        text: "Porcentaje",
        value: "Porcentaje"
      }, {
        text: "Monto",
        value: "Monto"
      }, {
        text: "Estatus",
        value: "Estatus"
      }, {
        text: "Acciones",
        value: "actions"
      }],
      globalCosts: [],
      bases: []
    };
  },
  methods: {
    /**
     * Get global costs from api.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-03-31
     * @param
     * @return void
     */
    getGlobalCosts: function getGlobalCosts() {
      var _this = this;

      this.loading = true;
      axios.get("/api/global-costs-configurators").then(function (result) {
        _this.globalCosts = result.data.globalCosts;
        _this.bases = result.data.bases;
      })["catch"](function (error) {
        swal2.fire({
          icon: "error",
          title: "Oops...",
          text: "Algo salió mal, vuelve a intentarlo!"
        });
      })["finally"](function () {
        _this.loading = false;
      });
    },

    /**
     * Redirect to create global cost component.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-03-31
     * @param
     * @return void
     */
    openCreateGlobalCost: function openCreateGlobalCost() {
      this.$router.push({
        name: "global-costs-configurators-create",
        params: {
          bases: this.bases
        }
      });
    },

    /**
     * Redirect to create global cost component.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-03-31
     * @param
     * @return void
     */
    openEditGlobalCost: function openEditGlobalCost(item) {
      this.$router.push({
        name: "global-costs-configurators-edit",
        params: {
          bases: this.bases,
          item: item
        }
      });
    },

    /**
     * Redirect to global utility component.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-03-31
     * @param
     * @return void
     */
    openUtility: function openUtility() {
      this.$router.push({
        name: "global-costs-configurators-utility"
      });
    },

    /**
     * Open delete dialog.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-04-06
     * @param Object item
     * @return void
     */
    openDeleteDialog: function openDeleteDialog(item) {
      this.dialogDelete = true;
      this.globalCostToDelete = item.Id_ConceptoCosto;
    },

    /**
     * Open Show component.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-04-06
     * @param Object item
     * @return void
     */
    goToShowGlobalCost: function goToShowGlobalCost(item) {
      this.$refs.sendGlobalCost.reciveGlobalCost(item, this.bases);
      this.dialogShow = true;
    },

    /**
     * Get dialog status from Show component.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-04-06
     * @param boolean value
     * @return void
     */
    getDialogShowStatus: function getDialogShowStatus(value) {
      this.dialogShow = value;
    },

    /**
     * Closes the delete dialog and calls to getGlobalCosts.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-04-06
     * @param
     * @return void
     */
    closeDelete: function closeDelete() {
      this.dialogDelete = false;
      this.globalCostToDelete = null;
      this.getGlobalCosts();
    },

    /**
     * Changes status of a item.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-04-06
     * @param object item
     * @return void
     */
    deleteItem: function deleteItem() {
      var _this2 = this;

      axios["delete"]("/api/global-costs-configurators/" + this.globalCostToDelete).then(function (result) {
        _this2.closeDelete();

        swal2.fire("Estatus cambiado", "Se cambió el estatus correctamente", "success");
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/global-costs-configurator/show.vue?vue&type=script&lang=js&":
/*!******************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/global-costs-configurator/show.vue?vue&type=script&lang=js& ***!
  \******************************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _create__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./create */ "./resources/js/views/product-configuration/global-costs-configurator/create.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  name: "Show",
  props: ["dialog", "currentGlobalCost"],
  components: {
    CreateGlobalCost: _create__WEBPACK_IMPORTED_MODULE_0__.default
  },
  data: function data() {
    return {
      globalCost: {},
      bases: []
    };
  },
  methods: {
    /**
     * Get dialog status from Inputs component.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-04-06
     * @param boolean value
     * @return void
     */
    getDialogShowStatus: function getDialogShowStatus(value) {
      this.$emit("dialogStatus", value);
    },

    /**
     * Receives a global cost from index component.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-04-06
     * @param Object globalCost
     * @param Array bases
     * @return void
     */
    reciveGlobalCost: function reciveGlobalCost(globalCost, bases) {
      this.globalCost = globalCost;
      this.bases = bases;
    }
  }
});

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/global-costs-configurator/index.vue?vue&type=style&index=0&lang=css&":
/*!*****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/global-costs-configurator/index.vue?vue&type=style&index=0&lang=css& ***!
  \*****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ ((module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../../../node_modules/css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js");
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0__);
// Imports

var ___CSS_LOADER_EXPORT___ = _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0___default()(function(i){return i[1]});
// Module
___CSS_LOADER_EXPORT___.push([module.id, "\n.data-table-style {\n  margin: 10px;\n}\n", ""]);
// Exports
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (___CSS_LOADER_EXPORT___);


/***/ }),

/***/ "./node_modules/style-loader/dist/cjs.js!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/global-costs-configurator/index.vue?vue&type=style&index=0&lang=css&":
/*!*********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader/dist/cjs.js!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/global-costs-configurator/index.vue?vue&type=style&index=0&lang=css& ***!
  \*********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !../../../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ "./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js");
/* harmony import */ var _node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _node_modules_css_loader_dist_cjs_js_clonedRuleSet_10_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_10_0_rules_0_use_2_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! !!../../../../../node_modules/css-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[1]!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[2]!../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./index.vue?vue&type=style&index=0&lang=css& */ "./node_modules/css-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/global-costs-configurator/index.vue?vue&type=style&index=0&lang=css&");

            

var options = {};

options.insert = "head";
options.singleton = false;

var update = _node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0___default()(_node_modules_css_loader_dist_cjs_js_clonedRuleSet_10_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_10_0_rules_0_use_2_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_1__.default, options);



/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_css_loader_dist_cjs_js_clonedRuleSet_10_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_10_0_rules_0_use_2_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_1__.default.locals || {});

/***/ }),

/***/ "./resources/js/views/product-configuration/global-costs-configurator/create.vue":
/*!***************************************************************************************!*\
  !*** ./resources/js/views/product-configuration/global-costs-configurator/create.vue ***!
  \***************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _create_vue_vue_type_template_id_3d83bdbb___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./create.vue?vue&type=template&id=3d83bdbb& */ "./resources/js/views/product-configuration/global-costs-configurator/create.vue?vue&type=template&id=3d83bdbb&");
/* harmony import */ var _create_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./create.vue?vue&type=script&lang=js& */ "./resources/js/views/product-configuration/global-costs-configurator/create.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! !../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */
;
var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__.default)(
  _create_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__.default,
  _create_vue_vue_type_template_id_3d83bdbb___WEBPACK_IMPORTED_MODULE_0__.render,
  _create_vue_vue_type_template_id_3d83bdbb___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/product-configuration/global-costs-configurator/create.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/js/views/product-configuration/global-costs-configurator/index.vue":
/*!**************************************************************************************!*\
  !*** ./resources/js/views/product-configuration/global-costs-configurator/index.vue ***!
  \**************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _index_vue_vue_type_template_id_61b08d3a___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./index.vue?vue&type=template&id=61b08d3a& */ "./resources/js/views/product-configuration/global-costs-configurator/index.vue?vue&type=template&id=61b08d3a&");
/* harmony import */ var _index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./index.vue?vue&type=script&lang=js& */ "./resources/js/views/product-configuration/global-costs-configurator/index.vue?vue&type=script&lang=js&");
/* harmony import */ var _index_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./index.vue?vue&type=style&index=0&lang=css& */ "./resources/js/views/product-configuration/global-costs-configurator/index.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! !../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");



;


/* normalize component */

var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__.default)(
  _index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__.default,
  _index_vue_vue_type_template_id_61b08d3a___WEBPACK_IMPORTED_MODULE_0__.render,
  _index_vue_vue_type_template_id_61b08d3a___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/product-configuration/global-costs-configurator/index.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/js/views/product-configuration/global-costs-configurator/show.vue":
/*!*************************************************************************************!*\
  !*** ./resources/js/views/product-configuration/global-costs-configurator/show.vue ***!
  \*************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _show_vue_vue_type_template_id_acb22608___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./show.vue?vue&type=template&id=acb22608& */ "./resources/js/views/product-configuration/global-costs-configurator/show.vue?vue&type=template&id=acb22608&");
/* harmony import */ var _show_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./show.vue?vue&type=script&lang=js& */ "./resources/js/views/product-configuration/global-costs-configurator/show.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! !../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */
;
var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__.default)(
  _show_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__.default,
  _show_vue_vue_type_template_id_acb22608___WEBPACK_IMPORTED_MODULE_0__.render,
  _show_vue_vue_type_template_id_acb22608___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/product-configuration/global-costs-configurator/show.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/js/views/product-configuration/global-costs-configurator/create.vue?vue&type=script&lang=js&":
/*!****************************************************************************************************************!*\
  !*** ./resources/js/views/product-configuration/global-costs-configurator/create.vue?vue&type=script&lang=js& ***!
  \****************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_create_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./create.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/global-costs-configurator/create.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_create_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__.default); 

/***/ }),

/***/ "./resources/js/views/product-configuration/global-costs-configurator/index.vue?vue&type=script&lang=js&":
/*!***************************************************************************************************************!*\
  !*** ./resources/js/views/product-configuration/global-costs-configurator/index.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./index.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/global-costs-configurator/index.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__.default); 

/***/ }),

/***/ "./resources/js/views/product-configuration/global-costs-configurator/show.vue?vue&type=script&lang=js&":
/*!**************************************************************************************************************!*\
  !*** ./resources/js/views/product-configuration/global-costs-configurator/show.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_show_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./show.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/global-costs-configurator/show.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_show_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__.default); 

/***/ }),

/***/ "./resources/js/views/product-configuration/global-costs-configurator/index.vue?vue&type=style&index=0&lang=css&":
/*!***********************************************************************************************************************!*\
  !*** ./resources/js/views/product-configuration/global-costs-configurator/index.vue?vue&type=style&index=0&lang=css& ***!
  \***********************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_dist_cjs_js_node_modules_css_loader_dist_cjs_js_clonedRuleSet_10_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_10_0_rules_0_use_2_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/style-loader/dist/cjs.js!../../../../../node_modules/css-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[1]!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[2]!../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./index.vue?vue&type=style&index=0&lang=css& */ "./node_modules/style-loader/dist/cjs.js!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/global-costs-configurator/index.vue?vue&type=style&index=0&lang=css&");


/***/ }),

/***/ "./resources/js/views/product-configuration/global-costs-configurator/create.vue?vue&type=template&id=3d83bdbb&":
/*!**********************************************************************************************************************!*\
  !*** ./resources/js/views/product-configuration/global-costs-configurator/create.vue?vue&type=template&id=3d83bdbb& ***!
  \**********************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_create_vue_vue_type_template_id_3d83bdbb___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   "staticRenderFns": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_create_vue_vue_type_template_id_3d83bdbb___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_create_vue_vue_type_template_id_3d83bdbb___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./create.vue?vue&type=template&id=3d83bdbb& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/global-costs-configurator/create.vue?vue&type=template&id=3d83bdbb&");


/***/ }),

/***/ "./resources/js/views/product-configuration/global-costs-configurator/index.vue?vue&type=template&id=61b08d3a&":
/*!*********************************************************************************************************************!*\
  !*** ./resources/js/views/product-configuration/global-costs-configurator/index.vue?vue&type=template&id=61b08d3a& ***!
  \*********************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_61b08d3a___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   "staticRenderFns": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_61b08d3a___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_61b08d3a___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./index.vue?vue&type=template&id=61b08d3a& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/global-costs-configurator/index.vue?vue&type=template&id=61b08d3a&");


/***/ }),

/***/ "./resources/js/views/product-configuration/global-costs-configurator/show.vue?vue&type=template&id=acb22608&":
/*!********************************************************************************************************************!*\
  !*** ./resources/js/views/product-configuration/global-costs-configurator/show.vue?vue&type=template&id=acb22608& ***!
  \********************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_show_vue_vue_type_template_id_acb22608___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   "staticRenderFns": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_show_vue_vue_type_template_id_acb22608___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_show_vue_vue_type_template_id_acb22608___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./show.vue?vue&type=template&id=acb22608& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/global-costs-configurator/show.vue?vue&type=template&id=acb22608&");


/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/global-costs-configurator/create.vue?vue&type=template&id=3d83bdbb&":
/*!*************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/global-costs-configurator/create.vue?vue&type=template&id=3d83bdbb& ***!
  \*************************************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render),
/* harmony export */   "staticRenderFns": () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "v-card",
    { staticClass: "p-3" },
    [
      _c("v-card-title", [_c("b", [_vm._v(_vm._s(_vm.title) + " ")])]),
      _vm._v(" "),
      _c("v-card-text", [
        _c(
          "form",
          { attrs: { "data-app": "" } },
          [
            _c(
              "v-row",
              [
                _c(
                  "v-col",
                  { attrs: { cols: "6", md: "12" } },
                  [
                    _c("h6", [_vm._v("Descripción")]),
                    _vm._v(" "),
                    _c("v-text-field", {
                      attrs: {
                        "error-messages": _vm.descriptionErrors,
                        placeholder: "Descripción",
                        readonly: _vm.inputsStatus,
                        outlined: "",
                        required: "",
                        dense: ""
                      },
                      on: {
                        input: function($event) {
                          return _vm.$v.currentGlobalCost.Descripcion.$touch()
                        },
                        blur: function($event) {
                          return _vm.$v.currentGlobalCost.Descripcion.$touch()
                        }
                      },
                      model: {
                        value: _vm.currentGlobalCost.Descripcion,
                        callback: function($$v) {
                          _vm.$set(_vm.currentGlobalCost, "Descripcion", $$v)
                        },
                        expression: "currentGlobalCost.Descripcion"
                      }
                    })
                  ],
                  1
                )
              ],
              1
            ),
            _vm._v(" "),
            _c(
              "v-row",
              [
                _c(
                  "v-radio-group",
                  {
                    attrs: {
                      "error-messages": _vm.typeErrors,
                      readonly: _vm.inputsStatus,
                      row: ""
                    },
                    on: {
                      change: _vm.checkInputs,
                      input: function($event) {
                        return _vm.$v.currentGlobalCost.Tipo.$touch()
                      },
                      blur: function($event) {
                        return _vm.$v.currentGlobalCost.Tipo.$touch()
                      }
                    },
                    model: {
                      value: _vm.currentGlobalCost.Tipo,
                      callback: function($$v) {
                        _vm.$set(_vm.currentGlobalCost, "Tipo", $$v)
                      },
                      expression: "currentGlobalCost.Tipo"
                    }
                  },
                  [
                    _c(
                      "v-col",
                      {
                        staticStyle: { "margin-right": "40px" },
                        attrs: { cols: "6", md: "3" }
                      },
                      [
                        _c("h6", [_vm._v("Tipo")]),
                        _vm._v(" "),
                        _c("v-radio", {
                          attrs: {
                            label: "Porcentaje",
                            color: "#018085",
                            value: "P"
                          }
                        })
                      ],
                      1
                    ),
                    _vm._v(" "),
                    _c(
                      "v-col",
                      {
                        staticStyle: { "padding-top": "40px" },
                        attrs: { cols: "6", md: "3" }
                      },
                      [
                        _c("v-radio", {
                          attrs: {
                            label: "Monto",
                            color: "#018085",
                            value: "M"
                          }
                        })
                      ],
                      1
                    ),
                    _vm._v(" "),
                    _c(
                      "v-col",
                      {
                        staticStyle: { "padding-top": "40px" },
                        attrs: { cols: "6", md: "3" }
                      },
                      [
                        _c("v-radio", {
                          attrs: {
                            label: "Calculado",
                            color: "#018085",
                            value: "C"
                          }
                        })
                      ],
                      1
                    )
                  ],
                  1
                ),
                _vm._v(" "),
                _c(
                  "v-col",
                  {
                    directives: [
                      {
                        name: "show",
                        rawName: "v-show",
                        value: _vm.showBases,
                        expression: "showBases"
                      }
                    ],
                    staticStyle: { "padding-top": "50px" },
                    attrs: { cols: "6", md: "3" }
                  },
                  [
                    _c("v-select", {
                      attrs: {
                        items: _vm.bases,
                        "item-text": "Descripcion",
                        "item-value": "Id_BaseCostoCalculado",
                        placeholder: "Base de cálculo",
                        readonly: _vm.inputsStatus,
                        outlined: "",
                        required: "",
                        dense: ""
                      },
                      model: {
                        value: _vm.currentGlobalCost.Id_BaseCostoCalculado,
                        callback: function($$v) {
                          _vm.$set(
                            _vm.currentGlobalCost,
                            "Id_BaseCostoCalculado",
                            $$v
                          )
                        },
                        expression: "currentGlobalCost.Id_BaseCostoCalculado"
                      }
                    })
                  ],
                  1
                )
              ],
              1
            ),
            _vm._v(" "),
            _c(
              "v-row",
              [
                _c(
                  "v-col",
                  { attrs: { cols: "6", md: "2" } },
                  [
                    _c(
                      "h6",
                      {
                        directives: [
                          {
                            name: "show",
                            rawName: "v-show",
                            value: !_vm.showPercentage,
                            expression: "!showPercentage"
                          }
                        ]
                      },
                      [_vm._v("Cantidad")]
                    ),
                    _vm._v(" "),
                    _c(
                      "h6",
                      {
                        directives: [
                          {
                            name: "show",
                            rawName: "v-show",
                            value: _vm.showPercentage,
                            expression: "showPercentage"
                          }
                        ]
                      },
                      [_vm._v("Porcentaje")]
                    ),
                    _vm._v(" "),
                    _c("v-text-field", {
                      attrs: {
                        "error-messages": _vm.amountErrors,
                        placeholder: "0.00",
                        readonly: _vm.inputsStatus,
                        type: "number",
                        min: "0",
                        step: "0.01",
                        outlined: "",
                        required: "",
                        dense: ""
                      },
                      on: {
                        input: function($event) {
                          return _vm.$v.currentGlobalCost.Monto.$touch()
                        },
                        blur: function($event) {
                          return _vm.$v.currentGlobalCost.Monto.$touch()
                        }
                      },
                      model: {
                        value: _vm.currentGlobalCost.Monto,
                        callback: function($$v) {
                          _vm.$set(_vm.currentGlobalCost, "Monto", $$v)
                        },
                        expression: "currentGlobalCost.Monto"
                      }
                    })
                  ],
                  1
                )
              ],
              1
            ),
            _vm._v(" "),
            _c(
              "v-card-actions",
              { staticClass: "mt-5 d-block" },
              [
                _c("form-action-buttons", {
                  attrs: { hideSaveBtn: _vm.showCost },
                  on: { close: _vm.cancel, save: _vm.submit }
                })
              ],
              1
            )
          ],
          1
        )
      ])
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/global-costs-configurator/index.vue?vue&type=template&id=61b08d3a&":
/*!************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/global-costs-configurator/index.vue?vue&type=template&id=61b08d3a& ***!
  \************************************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render),
/* harmony export */   "staticRenderFns": () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "v-card",
    { staticClass: "p-3", attrs: { "data-app": "" } },
    [
      _c("Show", {
        ref: "sendGlobalCost",
        attrs: {
          dialog: _vm.dialogShow,
          currentGlobalCost: _vm.globalCostToShow
        },
        on: { dialogStatus: _vm.getDialogShowStatus }
      }),
      _vm._v(" "),
      _c("dialog-delete", {
        attrs: { status: _vm.dialogDelete },
        on: {
          cancel: function($event) {
            _vm.dialogDelete = false
          },
          confirm: _vm.deleteItem
        }
      }),
      _vm._v(" "),
      _c("v-data-table", {
        staticClass: "elevation-1",
        attrs: {
          headers: _vm.headers,
          items: _vm.globalCosts,
          search: _vm.search,
          page: _vm.page,
          "items-per-page": _vm.itemsPerPage,
          loading: _vm.loading,
          "hide-default-footer": "",
          "no-data-text": "No hay ningún registro",
          "no-results-text": "No se encontraron coincidencias",
          "loading-text": "Cargando... Porfavor espere"
        },
        on: {
          "update:page": function($event) {
            _vm.page = $event
          },
          "page-count": function($event) {
            _vm.pageCount = $event
          }
        },
        scopedSlots: _vm._u(
          [
            {
              key: "top",
              fn: function() {
                return [
                  _c("datatable-header", {
                    attrs: {
                      title: "Costos Globales",
                      "disable-creation": false,
                      hideCenterButton: false
                    },
                    on: {
                      searching: function($event) {
                        _vm.search = $event
                      },
                      open: _vm.openCreateGlobalCost,
                      openUtility: _vm.openUtility
                    }
                  })
                ]
              },
              proxy: true
            },
            {
              key: "item.Estatus",
              fn: function(ref) {
                var item = ref.item
                return [_c("chip-status", { attrs: { item: item } })]
              }
            },
            {
              key: "item.actions",
              fn: function(ref) {
                var item = ref.item
                return [
                  _c("action-buttons", {
                    attrs: { item: item },
                    on: {
                      edit: function($event) {
                        return _vm.openEditGlobalCost(item)
                      },
                      view: _vm.goToShowGlobalCost,
                      delete: _vm.openDeleteDialog
                    }
                  })
                ]
              }
            }
          ],
          null,
          true
        )
      }),
      _vm._v(" "),
      _c("pagination", {
        attrs: {
          last: _vm.pageCount,
          current: _vm.page,
          itemsPerPage: _vm.itemsPerPage,
          total: this.globalCosts.length
        },
        on: {
          input: function($event) {
            _vm.page = $event
          }
        }
      })
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/global-costs-configurator/show.vue?vue&type=template&id=acb22608&":
/*!***********************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/global-costs-configurator/show.vue?vue&type=template&id=acb22608& ***!
  \***********************************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render),
/* harmony export */   "staticRenderFns": () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "text-center" },
    [
      _c(
        "v-dialog",
        {
          attrs: { persistent: "", "max-width": "800px" },
          model: {
            value: _vm.dialog,
            callback: function($$v) {
              _vm.dialog = $$v
            },
            expression: "dialog"
          }
        },
        [
          _c("CreateGlobalCost", {
            attrs: {
              title: "Información de costo global",
              inputsStatus: true,
              showCost: true,
              costToShow: _vm.globalCost,
              basesFromShow: _vm.bases
            },
            on: { dialogStatus: _vm.getDialogShowStatus }
          })
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ })

}]);