(self["webpackChunk"] = self["webpackChunk"] || []).push([["resources_js_views_product-configuration_locations_new_rackDistribution_vue"],{

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/locations_new/partials/modalCreateDistribution.vue?vue&type=script&lang=js&":
/*!**********************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/locations_new/partials/modalCreateDistribution.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var vuelidate__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vuelidate */ "./node_modules/vuelidate/lib/index.js");
/* harmony import */ var vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vuelidate/lib/validators */ "./node_modules/vuelidate/lib/validators/index.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  name: 'modalCreateDistribution',
  mixins: [vuelidate__WEBPACK_IMPORTED_MODULE_0__.validationMixin],
  validations: {
    levelId: {
      required: vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_1__.required
    },
    sectionName: {
      required: vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_1__.required
    }
  },
  props: {
    itemsLevel: {
      type: Array,
      "default": function _default() {
        return [];
      }
    }
  },
  data: function data() {
    return {
      isSubmit: false,
      sectionName: null,
      sections: [],
      levelId: null
    };
  },
  methods: {
    /**
     * Emit a event for close the modal
     * @auth Iván Morales <ivan.morales@nuvem.mx>
     * @date 2021-06-24
     * @param
     * @return void
     */
    cancel: function cancel() {
      this.sectionName = null;
      this.sections = [];
      this.levelId = null;
      this.$v.levelId.$reset();
      this.$v.sectionName.$reset();
      this.$emit('closeModal', true);
    },

    /**
     * Execute a axios request for store the sections
     * @auth Iván Morales <ivan.morales@nuvem.mx>
     * @date 2021-06-24
     * @param
     * @return void
     */
    saveSections: function saveSections() {
      var _this = this;

      this.$v.levelId.$touch();

      if (this.$v.levelId.$invalid) {
        return false;
      }

      if (this.sections.length == 0) {
        swal2.fire("Secciones", "Debe agregar al menos una sección", "warning");
        return false;
      }

      this.isSubmit = true;
      var route = "/api/locations-new/sections/".concat(this.levelId);
      axios.post(route, {
        sections: this.sections
      }).then(function (result) {
        _this.isSubmit = false;
        _this.sectionName = null;
        _this.sections = [];
        _this.levelId = null;

        _this.$v.sectionName.$reset();

        _this.$v.levelId.$reset();

        _this.$emit('closeModal', true);

        _this.$emit('sectionsSaved', true);
      })["catch"](function (error) {
        _this.isSubmit = false;
        swal2.fire({
          icon: "error",
          title: "Oops...",
          text: "Algo salió mal, vuelve a intentarlo!"
        });
        console.log({
          error: error
        });
      });
    },

    /**
     * Add a section to array
     * @auth Iván Morales <ivan.morales@nuvem.mx>
     * @date 2021-06-24
     * @param
     * @return void
     */
    addSection: function addSection() {
      this.$v.sectionName.$touch();

      if (this.$v.sectionName.$invalid) {
        return false;
      }

      this.sections.push({
        Nombre: this.sectionName
      });
      this.sectionName = null;
      this.$v.sectionName.$reset();
    },

    /**
     * Remove a section from array
     * @auth Iván Morales <ivan.morales@nuvem.mx>
     * @date 2021-06-24
     * @param int index, int idsection
     * @return void
     */
    removeSection: function removeSection(index) {
      this.sections.splice(index, 1);
    }
  },

  /**
  * get the message errors for fields.
  * @auth Iván Morales <ivan.morales@nuvem.mx>
  * @date 2021-06-24
  * @param
  * @return Array
  */
  computed: {
    /**
    * get the message errors for fields.
    * @auth Iván Morales <ivan.morales@nuvem.mx>
    * @date 2021-06-18
    * @param
    * @return Array
    */
    //BEGIN ERRORS FIELDS
    levelIdErrors: function levelIdErrors() {
      var errors = [];
      if (!this.$v.levelId.$dirty) return errors;
      !this.$v.levelId.required && errors.push("Este campo es requerido");
      return errors;
    },
    sectionNameErrors: function sectionNameErrors() {
      var errors = [];
      if (!this.$v.sectionName.$dirty) return errors;
      !this.$v.sectionName.required && errors.push("Este campo es requerido");
      return errors;
    } //END ERRORS FIELDS

  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/locations_new/rackDistribution.vue?vue&type=script&lang=js&":
/*!******************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/locations_new/rackDistribution.vue?vue&type=script&lang=js& ***!
  \******************************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _store__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../store */ "./resources/js/store/index.js");
/* harmony import */ var _partials_modalCreateDistribution__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./partials/modalCreateDistribution */ "./resources/js/views/product-configuration/locations_new/partials/modalCreateDistribution.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  components: {
    ModalCreateDistribution: _partials_modalCreateDistribution__WEBPACK_IMPORTED_MODULE_1__.default
  },
  mounted: function mounted() {
    this.idRack = this.$route.params.id;
    this.getSections();
    this.permissionsUser = _store__WEBPACK_IMPORTED_MODULE_0__.default.state.permissions;
    var nameRoute = this.$route.name;
    this.isOnlyShow = nameRoute == 'location-distributions-show';
    if (!this.isOnlyShow && (this.permissionsUser.includes('Location.create') || this.permissionsUser.includes('Location.edit'))) this.isEditable = true;
  },
  data: function data() {
    return {
      search: "",
      dialog: false,
      dialogDelete: false,
      dialogShow: false,
      permissionsUser: [],
      page: 1,
      pageCount: 0,
      totalItems: 0,
      itemsPerPage: 10,
      loading: false,
      sections: [],
      itemDelete: {},
      headers: [{
        text: "Nivel",
        value: "level_name"
      }, {
        text: "Sección",
        value: "Nombre"
      }, {
        text: "Acciones",
        value: "actions",
        sortable: false
      }],
      amounts: [{
        id: 10,
        name: '10'
      }, {
        id: 50,
        name: '50'
      }, {
        id: 100,
        name: '100'
      }],
      rackName: null,
      idRack: null,
      isDeleting: false,
      levelsForSelect: [],
      modalDistribution: false,
      isOnlyShow: false,
      isEditable: false
    };
  },
  methods: {
    /**
     * Get sections from api.
     * @auth Iván Morales <ivan.morales@nuvem.mx>
     * @date 2021-06-23
     * @param
     * @return void
     */
    getSections: function getSections() {
      var _this = this;

      this.loading = true;
      axios.get("/api/locations-new/sections/" + this.idRack).then(function (result) {
        _this.loading = false;
        _this.sections = result.data.sections;
        _this.rackName = result.data.rack ? result.data.rack.Nombre : null;
        _this.levelsForSelect = result.data.levels;
      })["catch"](function (error) {
        swal2.fire({
          icon: "error",
          title: "Oops...",
          text: "Algo salió mal, vuelve a intentarlo!"
        });
        console.log({
          error: error
        });
      });
    },

    /**
     * Open the delete dialog confirm
     * @author Iván Morales <ivan.morales@nuvem.mx>
     * @created 2021/06/23
     * @param Object item
     * @return \Illuminate\Http\JsonResponse
     */
    openDeleteDialog: function openDeleteDialog(item) {
      this.itemDelete = item;
      this.dialogDelete = true;
    },

    /**
     * Send a request to api for change the status.
     * @auth Iván Morales | ivan.morales@nuvem.mx
     * @date 2021/06/23
     * @param
     * @return void
     */
    deleteItem: function deleteItem() {
      var _this2 = this;

      this.isDeleting = true;
      var url = "/api/locations-new/sections/".concat(this.itemDelete.Id);
      axios["delete"](url).then(function (res) {
        _this2.isDeleting = false;
        _this2.dialogDelete = false;
        swal2.fire('Eliminado', 'Estatus cambiado correctamente', 'success');

        _this2.getSections();
      })["catch"](function (err) {
        _this2.isDeleting = false;
        swal2.fire('Error', 'Ocurrió un error, intente mas tarde', 'error');
        console.log(err);
      });
    },

    /**
     * Method when sectionsSaved from child is emited, refresh the data
     * @auth Iván Morales | ivan.morales@nuvem.mx
     * @date 2021/06/25
     * @param
     * @return void
     */
    sectionsSaved: function sectionsSaved() {
      swal2.fire('Éxito', 'Secciones almacenadas correctamente', 'success');
      this.getSections();
    },

    /**
     * Redirects to 2 previous routes
     * @auth Iván Morales | ivan.morales@nuvem.mx
     * @date 2021/06/24
     * @param
     * @return void
     */
    endDistribution: function endDistribution() {
      this.$router.go(-2);
    }
  }
});

/***/ }),

/***/ "./resources/js/views/product-configuration/locations_new/partials/modalCreateDistribution.vue":
/*!*****************************************************************************************************!*\
  !*** ./resources/js/views/product-configuration/locations_new/partials/modalCreateDistribution.vue ***!
  \*****************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _modalCreateDistribution_vue_vue_type_template_id_158c1056___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./modalCreateDistribution.vue?vue&type=template&id=158c1056& */ "./resources/js/views/product-configuration/locations_new/partials/modalCreateDistribution.vue?vue&type=template&id=158c1056&");
/* harmony import */ var _modalCreateDistribution_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./modalCreateDistribution.vue?vue&type=script&lang=js& */ "./resources/js/views/product-configuration/locations_new/partials/modalCreateDistribution.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! !../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */
;
var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__.default)(
  _modalCreateDistribution_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__.default,
  _modalCreateDistribution_vue_vue_type_template_id_158c1056___WEBPACK_IMPORTED_MODULE_0__.render,
  _modalCreateDistribution_vue_vue_type_template_id_158c1056___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/product-configuration/locations_new/partials/modalCreateDistribution.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/js/views/product-configuration/locations_new/rackDistribution.vue":
/*!*************************************************************************************!*\
  !*** ./resources/js/views/product-configuration/locations_new/rackDistribution.vue ***!
  \*************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _rackDistribution_vue_vue_type_template_id_fb8d12be___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./rackDistribution.vue?vue&type=template&id=fb8d12be& */ "./resources/js/views/product-configuration/locations_new/rackDistribution.vue?vue&type=template&id=fb8d12be&");
/* harmony import */ var _rackDistribution_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./rackDistribution.vue?vue&type=script&lang=js& */ "./resources/js/views/product-configuration/locations_new/rackDistribution.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! !../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */
;
var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__.default)(
  _rackDistribution_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__.default,
  _rackDistribution_vue_vue_type_template_id_fb8d12be___WEBPACK_IMPORTED_MODULE_0__.render,
  _rackDistribution_vue_vue_type_template_id_fb8d12be___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/product-configuration/locations_new/rackDistribution.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/js/views/product-configuration/locations_new/partials/modalCreateDistribution.vue?vue&type=script&lang=js&":
/*!******************************************************************************************************************************!*\
  !*** ./resources/js/views/product-configuration/locations_new/partials/modalCreateDistribution.vue?vue&type=script&lang=js& ***!
  \******************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_modalCreateDistribution_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./modalCreateDistribution.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/locations_new/partials/modalCreateDistribution.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_modalCreateDistribution_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__.default); 

/***/ }),

/***/ "./resources/js/views/product-configuration/locations_new/rackDistribution.vue?vue&type=script&lang=js&":
/*!**************************************************************************************************************!*\
  !*** ./resources/js/views/product-configuration/locations_new/rackDistribution.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_rackDistribution_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./rackDistribution.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/locations_new/rackDistribution.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_rackDistribution_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__.default); 

/***/ }),

/***/ "./resources/js/views/product-configuration/locations_new/partials/modalCreateDistribution.vue?vue&type=template&id=158c1056&":
/*!************************************************************************************************************************************!*\
  !*** ./resources/js/views/product-configuration/locations_new/partials/modalCreateDistribution.vue?vue&type=template&id=158c1056& ***!
  \************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_modalCreateDistribution_vue_vue_type_template_id_158c1056___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   "staticRenderFns": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_modalCreateDistribution_vue_vue_type_template_id_158c1056___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_modalCreateDistribution_vue_vue_type_template_id_158c1056___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./modalCreateDistribution.vue?vue&type=template&id=158c1056& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/locations_new/partials/modalCreateDistribution.vue?vue&type=template&id=158c1056&");


/***/ }),

/***/ "./resources/js/views/product-configuration/locations_new/rackDistribution.vue?vue&type=template&id=fb8d12be&":
/*!********************************************************************************************************************!*\
  !*** ./resources/js/views/product-configuration/locations_new/rackDistribution.vue?vue&type=template&id=fb8d12be& ***!
  \********************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_rackDistribution_vue_vue_type_template_id_fb8d12be___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   "staticRenderFns": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_rackDistribution_vue_vue_type_template_id_fb8d12be___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_rackDistribution_vue_vue_type_template_id_fb8d12be___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./rackDistribution.vue?vue&type=template&id=fb8d12be& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/locations_new/rackDistribution.vue?vue&type=template&id=fb8d12be&");


/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/locations_new/partials/modalCreateDistribution.vue?vue&type=template&id=158c1056&":
/*!***************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/locations_new/partials/modalCreateDistribution.vue?vue&type=template&id=158c1056& ***!
  \***************************************************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render),
/* harmony export */   "staticRenderFns": () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "v-card",
    { staticClass: "p-3" },
    [
      _c("v-card-title", [_c("b", [_vm._v("Agregar distribución")])]),
      _vm._v(" "),
      _c(
        "v-card-text",
        [
          _c(
            "v-form",
            { attrs: { "data-app": "" } },
            [
              _c(
                "v-row",
                [
                  _c(
                    "v-col",
                    { staticClass: "py-0", attrs: { cols: "12", lg: "12" } },
                    [
                      _c("n-label", {
                        attrs: {
                          text: "Elige un nivel al que agregas secciones"
                        }
                      }),
                      _vm._v(" "),
                      _c("v-select", {
                        attrs: {
                          attach: "",
                          outlined: "",
                          dense: "",
                          placeholder: "Seleccione",
                          items: _vm.itemsLevel,
                          "item-text": "Nombre",
                          "item-value": "Id",
                          "error-messages": _vm.levelIdErrors
                        },
                        on: {
                          input: function($event) {
                            return _vm.$v.levelId.$touch()
                          },
                          blur: function($event) {
                            return _vm.$v.levelId.$touch()
                          }
                        },
                        model: {
                          value: _vm.levelId,
                          callback: function($$v) {
                            _vm.levelId = $$v
                          },
                          expression: "levelId"
                        }
                      })
                    ],
                    1
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "v-row",
                [
                  _c(
                    "v-col",
                    { staticClass: "py-0", attrs: { cols: "9", lg: "8" } },
                    [
                      _c("n-label", {
                        attrs: { text: "Sección para el nivel" }
                      }),
                      _vm._v(" "),
                      _c("v-text-field", {
                        attrs: {
                          outlined: "",
                          dense: "",
                          placeholder: "Ingrese nombre o número de sección",
                          maxlength: "45",
                          "error-messages": _vm.sectionNameErrors
                        },
                        on: {
                          input: function($event) {
                            return _vm.$v.sectionName.$touch()
                          },
                          blur: function($event) {
                            return _vm.$v.sectionName.$touch()
                          }
                        },
                        model: {
                          value: _vm.sectionName,
                          callback: function($$v) {
                            _vm.sectionName = $$v
                          },
                          expression: "sectionName"
                        }
                      })
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "v-col",
                    { staticClass: "py-0", attrs: { cols: "3", lg: "4" } },
                    [
                      _c("n-label", { attrs: { text: "Agregar sección" } }),
                      _vm._v(" "),
                      _c(
                        "v-btn",
                        {
                          staticClass: "mx-2",
                          attrs: {
                            fab: "",
                            dark: "",
                            small: "",
                            color: "success"
                          },
                          on: {
                            click: function($event) {
                              return _vm.addSection()
                            }
                          }
                        },
                        [
                          _c("v-icon", { attrs: { dark: "" } }, [
                            _vm._v(
                              "\n                            mdi-plus\n                        "
                            )
                          ])
                        ],
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              ),
              _vm._v(" "),
              _vm.sections.length > 0
                ? _c(
                    "v-row",
                    [
                      _c(
                        "v-col",
                        { staticClass: "py-0", attrs: { cols: "9", lg: "8" } },
                        [_c("b", [_vm._v("Secciones")])]
                      ),
                      _vm._v(" "),
                      _c(
                        "v-col",
                        { staticClass: "py-0", attrs: { cols: "3", lg: "4" } },
                        [_c("b", [_vm._v("Eliminar")])]
                      )
                    ],
                    1
                  )
                : _vm._e(),
              _vm._v(" "),
              _vm._l(_vm.sections, function(section, index) {
                return _c(
                  "v-row",
                  { key: index },
                  [
                    _c(
                      "v-col",
                      { staticClass: "py-0", attrs: { cols: "9", lg: "8" } },
                      [
                        _vm._v(
                          "\n                    " +
                            _vm._s(section.Nombre) +
                            "\n                "
                        )
                      ]
                    ),
                    _vm._v(" "),
                    _c(
                      "v-col",
                      { staticClass: "py-0", attrs: { cols: "3", lg: "4" } },
                      [
                        _c(
                          "v-icon",
                          {
                            attrs: { color: "success" },
                            on: {
                              click: function($event) {
                                return _vm.removeSection(index)
                              }
                            }
                          },
                          [
                            _vm._v(
                              "\n                        mdi-delete\n                    "
                            )
                          ]
                        )
                      ],
                      1
                    )
                  ],
                  1
                )
              }),
              _vm._v(" "),
              _c(
                "v-card-actions",
                { staticClass: "mt-5 d-block" },
                [
                  _c(
                    "v-row",
                    [
                      _c(
                        "v-col",
                        { attrs: { cols: "12", lg: "12" } },
                        [
                          _c(
                            "v-btn",
                            {
                              attrs: {
                                block: "",
                                color: "primary",
                                loading: _vm.isSubmit
                              },
                              on: { click: _vm.saveSections }
                            },
                            [
                              _vm._v(
                                "\n                            Terminar seleccionado del nivel\n                        "
                              )
                            ]
                          )
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "v-col",
                        { attrs: { cols: "12", lg: "12" } },
                        [
                          _c(
                            "v-btn",
                            {
                              attrs: {
                                block: "",
                                color: "primary",
                                outlined: ""
                              },
                              on: { click: _vm.cancel }
                            },
                            [
                              _vm._v(
                                "\n                            Cancelar\n                        "
                              )
                            ]
                          )
                        ],
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              )
            ],
            2
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/locations_new/rackDistribution.vue?vue&type=template&id=fb8d12be&":
/*!***********************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/locations_new/rackDistribution.vue?vue&type=template&id=fb8d12be& ***!
  \***********************************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render),
/* harmony export */   "staticRenderFns": () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "v-card",
    { staticClass: "px-3", attrs: { "data-app": "" } },
    [
      _vm.loading
        ? _c(
            "v-row",
            [
              _c("v-progress-circular", {
                staticClass: "progress-circular-color",
                staticStyle: { margin: "0px auto" },
                attrs: { indeterminate: "" }
              })
            ],
            1
          )
        : _c(
            "div",
            [
              _c("dialog-delete", {
                attrs: { status: _vm.dialogDelete, loading: _vm.isDeleting },
                on: {
                  cancel: function($event) {
                    _vm.dialogDelete = false
                  },
                  confirm: _vm.deleteItem
                }
              }),
              _vm._v(" "),
              _c(
                "v-dialog",
                {
                  attrs: { "max-width": "500px", persistent: "" },
                  model: {
                    value: _vm.modalDistribution,
                    callback: function($$v) {
                      _vm.modalDistribution = $$v
                    },
                    expression: "modalDistribution"
                  }
                },
                [
                  _c("ModalCreateDistribution", {
                    attrs: { itemsLevel: _vm.levelsForSelect },
                    on: {
                      closeModal: function($event) {
                        _vm.modalDistribution = false
                      },
                      sectionsSaved: _vm.sectionsSaved
                    }
                  })
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "v-row",
                [
                  _c(
                    "v-col",
                    { staticClass: "py-0", attrs: { cols: "12", lg: "12" } },
                    [
                      _c("n-label", { attrs: { text: "Rack" } }),
                      _vm._v(" "),
                      _c("v-text-field", {
                        attrs: {
                          outlined: "",
                          dense: "",
                          disabled: true,
                          maxlength: "50"
                        },
                        model: {
                          value: _vm.rackName,
                          callback: function($$v) {
                            _vm.rackName = $$v
                          },
                          expression: "rackName"
                        }
                      })
                    ],
                    1
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c("v-data-table", {
                staticClass: "elevation-1",
                attrs: {
                  headers: _vm.headers,
                  items: _vm.sections,
                  search: _vm.search,
                  page: _vm.page,
                  "items-per-page": _vm.itemsPerPage,
                  loading: _vm.loading,
                  "hide-default-footer": "",
                  "no-data-text": "No hay ningún registro",
                  "no-results-text": "No se encontraron coincidencias",
                  "loading-text": "Cargando... Porfavor espere"
                },
                on: {
                  "update:page": function($event) {
                    _vm.page = $event
                  },
                  "page-count": function($event) {
                    _vm.pageCount = $event
                  }
                },
                scopedSlots: _vm._u(
                  [
                    {
                      key: "top",
                      fn: function() {
                        return [
                          _c("datatable-header", {
                            attrs: {
                              title: "Distribución del rack",
                              "disable-creation": !_vm.isEditable
                            },
                            on: {
                              searching: function($event) {
                                _vm.search = $event
                              },
                              open: function($event) {
                                _vm.modalDistribution = true
                              }
                            }
                          }),
                          _vm._v(" "),
                          _c(
                            "v-row",
                            { staticStyle: { "margin-left": "17px" } },
                            [
                              _c(
                                "v-col",
                                {
                                  staticStyle: {
                                    "align-self": "center",
                                    "padding-right": "0px",
                                    "max-width": "80px"
                                  },
                                  attrs: { cols: "6", md: "1" }
                                },
                                [_c("p", [_vm._v("Mostrar")])]
                              ),
                              _vm._v(" "),
                              _c(
                                "v-col",
                                {
                                  staticStyle: {
                                    "padding-left": "0px",
                                    "max-width": "95px"
                                  },
                                  attrs: { cols: "6", md: "2" }
                                },
                                [
                                  _c(
                                    "div",
                                    [
                                      _c("v-select", {
                                        attrs: {
                                          items: _vm.amounts,
                                          "item-text": "name",
                                          "item-value": "id",
                                          placeholder: "Elige una opción",
                                          outlined: "",
                                          dense: "",
                                          attach: "",
                                          auto: ""
                                        },
                                        model: {
                                          value: _vm.itemsPerPage,
                                          callback: function($$v) {
                                            _vm.itemsPerPage = $$v
                                          },
                                          expression: "itemsPerPage"
                                        }
                                      })
                                    ],
                                    1
                                  )
                                ]
                              ),
                              _vm._v(" "),
                              _c(
                                "v-col",
                                {
                                  staticStyle: {
                                    "align-self": "center",
                                    "padding-left": "0px"
                                  },
                                  attrs: { cols: "6", md: "1" }
                                },
                                [_c("p", [_vm._v("Registros")])]
                              )
                            ],
                            1
                          )
                        ]
                      },
                      proxy: true
                    },
                    {
                      key: "item.actions",
                      fn: function(ref) {
                        var item = ref.item
                        return [
                          _c("action-buttons", {
                            attrs: {
                              item: item,
                              editDisabled: true,
                              showDisabled: true,
                              deleteDisabled: !_vm.isEditable
                            },
                            on: { delete: _vm.openDeleteDialog }
                          })
                        ]
                      }
                    }
                  ],
                  null,
                  true
                )
              }),
              _vm._v(" "),
              _c("pagination", {
                attrs: {
                  last: _vm.pageCount,
                  current: _vm.page,
                  itemsPerPage: _vm.itemsPerPage,
                  total: _vm.sections.length
                },
                on: {
                  input: function($event) {
                    _vm.page = $event
                  }
                }
              }),
              _vm._v(" "),
              _c(
                "v-card-actions",
                { staticClass: "d-block" },
                [
                  _c(
                    "v-row",
                    [
                      _c(
                        "v-col",
                        {
                          staticStyle: { margin: "0px auto" },
                          attrs: { cols: "12", lg: "4" }
                        },
                        [
                          _c(
                            "v-btn",
                            {
                              attrs: { block: "", color: "primary" },
                              on: { click: _vm.endDistribution }
                            },
                            [
                              _vm._v(
                                "\n                        Terminar distribución del rack\n                    "
                              )
                            ]
                          )
                        ],
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ })

}]);