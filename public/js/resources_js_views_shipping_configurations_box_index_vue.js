(self["webpackChunk"] = self["webpackChunk"] || []).push([["resources_js_views_shipping_configurations_box_index_vue"],{

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/shipping_configurations/box/index.vue?vue&type=script&lang=js&":
/*!***********************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/shipping_configurations/box/index.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _components_chipStatus__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../components/chipStatus */ "./resources/js/components/chipStatus.vue");
/* harmony import */ var _show__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./show */ "./resources/js/views/shipping_configurations/box/show.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  created: function created() {
    this.getBoxes();
  },
  components: {
    ChipStatus: _components_chipStatus__WEBPACK_IMPORTED_MODULE_0__.default,
    Show: _show__WEBPACK_IMPORTED_MODULE_1__.default
  },
  data: function data() {
    return {
      boxesStatus: [{
        text: "Activo",
        value: 1
      }, {
        text: "Inactivo",
        value: 0
      }, {
        text: "Todos",
        value: 2
      }],
      estatusFilter: 1,
      search: "",
      dialog: false,
      productId: null,
      dialogDelete: false,
      dialogShow: false,
      boxToDelete: null,
      page: 1,
      pageCount: 0,
      totalItems: 0,
      itemsPerPage: 10,
      loading: false,
      headers: [{
        text: "Id",
        value: "Id"
      }, {
        text: "Descripción",
        value: "Descripcion"
      }, {
        text: "Alto",
        value: "Alto"
      }, {
        text: "Ancho",
        value: "Ancho"
      }, {
        text: "Largo",
        value: "Largo"
      }, {
        text: "Estatus",
        value: "Estatus"
      }, {
        text: "Acciones",
        value: "actions"
      }],
      boxes: []
    };
  },
  methods: {
    /**
     * Get products and fill date table from boxes api.
     * @auth Kareem Lorenzana <kareem.lorenzana@nuvem.mx>
     * @date 2021-04-19
     * @param
     * @return void
     */
    getBoxes: function getBoxes() {
      var _this = this;

      this.loading = true;
      var params = {};
      if (this.estatusFilter != null && (this.estatusFilter == 1 || this.estatusFilter == 0 || this.estatusFilter == 2)) params.estatusFilter = this.estatusFilter;
      axios.get("/api/boxes", {
        params: params
      }).then(function (result) {
        _this.boxes = result.data.boxes;
      })["catch"](function (error) {
        swal2.fire({
          icon: "error",
          title: "Oops...",
          text: "Algo salió mal, vuelve a intentarlo!"
        });
      })["finally"](function () {
        _this.loading = false;
      });
    },

    /**
     * Redirect to create box component.
     * @auth Kareem Lorenzana  <kareem.lorenzana@nuvem.mx>
     * @date 2021-04-19
     * @param
     * @return void
     */
    openCreateBox: function openCreateBox() {
      this.$router.push({
        name: 'boxes-create'
      });
    },

    /**
     * Redirect to edit box component.
     * @auth Kareem Lorenzana <kareem.lorenzana@nuvem.mx>
     * @date 2021-04-19
     * @param Object item
     * @return void
     */
    openEditBox: function openEditBox(item) {
      this.$router.push({
        name: "boxes-edit",
        params: {
          item: item
        }
      });
    },

    /**
     * Open delete dialog.
     * @auth Kareem Lorenzana <kareem.lorenzana@nuvem.mx>
     * @date 2021-04-19
     * @update 2021-04-26
     * @param Object item
     * @return void
     */
    openDeleteDialog: function openDeleteDialog(item) {
      this.dialogDelete = true;
      this.boxToDelete = item.Id;
    },

    /**
     * Closes the delete dialog.
     * @auth Kareem Lorenzana <kareem.lorenzana@nuvem.mx>
     * @date 2021-04-19
     * @param
     * @return void
     */
    closeDelete: function closeDelete() {
      this.dialogDelete = false;
      this.boxToDelete = null;
      this.getBoxes();
    },

    /**
     * Change status of a item.
     * @auth Kareem Lorenzana <kareem.lorenzana@nuvem.mx>
     * @date 2021-04-19
     * @param 
     * @return void
     */
    deleteItem: function deleteItem() {
      var _this2 = this;

      axios["delete"]("/api/boxes/" + this.boxToDelete).then(function (result) {
        _this2.closeDelete();

        swal2.fire("Caja Eliminada", "Se Eliminó la Caja correctamente", "success");
      });
    },

    /**
     * Open Show component.
     * @auth Kareem Lorenzana <kareem.lorenzana@nuvem.mx>
     * @date 2021-04-19
     * @param Object item
     * @return void
     */
    goToShowBox: function goToShowBox(item) {
      this.$refs.sendBox.reciveBox(item);
      this.dialogShow = true;
    },

    /**
     * Get dialog status from Show component.
     * @auth Kareem Lorenzana <kareem.lorenzana@nuvem.mx>
     * @date 2021-04-19
     * @param boolean value
     * @return void
     */
    getDialogShowStatus: function getDialogShowStatus(value) {
      this.dialogShow = value;
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/shipping_configurations/box/partials/Inputs.vue?vue&type=script&lang=js&":
/*!*********************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/shipping_configurations/box/partials/Inputs.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var vuelidate__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vuelidate */ "./node_modules/vuelidate/lib/index.js");
/* harmony import */ var vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vuelidate/lib/validators */ "./node_modules/vuelidate/lib/validators/index.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  name: "Inputs",
  props: {
    boxToShow: {
      type: Object,
      "default": null
    },
    inputsStatus: {
      type: Boolean,
      "default": false
    }
  },
  mixins: [vuelidate__WEBPACK_IMPORTED_MODULE_0__.validationMixin],
  validations: {
    currentBox: {
      Descripcion: {
        required: vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_1__.required
      },
      Ancho: {
        required: vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_1__.required,
        decimal: vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_1__.decimal,
        maxLength: (0,vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_1__.maxLength)(10)
      },
      Alto: {
        required: vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_1__.required,
        decimal: vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_1__.decimal,
        maxLength: (0,vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_1__.maxLength)(10)
      },
      Largo: {
        required: vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_1__.required,
        decimal: vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_1__.decimal,
        maxLength: (0,vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_1__.maxLength)(10)
      },
      Peso: {
        required: vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_1__.required,
        decimal: vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_1__.decimal,
        maxLength: (0,vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_1__.maxLength)(10)
      }
    }
  },
  mounted: function mounted() {
    if (this.$route.params.item != null) {
      this.currentBox = this.$route.params.item;
      console.log(this.$route.params.item);
      this.previousRulePriority = this.$route.params.item.Prioridad;
    } else if (this.boxToShow != null) {
      this.currentBox = this.boxToShow;
    }
  },
  data: function data() {
    return {
      submitStatus: "",
      previousRulePriority: null,
      currentBox: {
        Descripcion: "",
        Alto: "",
        Ancho: "",
        Largo: "",
        Peso: "",
        Estatus: true
      }
    };
  },
  methods: {
    /**
     * Allows call post or edit method if data is valid.
     * @auth Kareem Lorenzana <kareem.lorenzana@nuvem.mx>
     * @date 2021-04-19
     * @param
     * @return void
     */
    submit: function submit() {
      var _this = this;

      this.$v.$touch();

      if (this.$v.$invalid) {
        this.submitStatus = "ERROR";
      } else if (!this.$v.$invalid) {
        this.$route.params.item != null ? this.edit(this.currentBox) : this.post(this.currentBox);
      } else {
        this.submitStatus = "PENDING";
        setTimeout(function () {
          _this.submitStatus = "OK";
        }, 500);
      }
    },

    /**
     * Post a new Box.
     * @auth Kareem Lorenzana <kareem.lorenzana@nuvem.mx>
     * @date 2021-04-19
     * @param
     * @return void
     */
    post: function post(data) {
      var _this2 = this;

      axios.post("/api/boxes", data).then(function (result) {
        swal2.fire("Creado", "Se agregaron los datos correctamente", "success");
      })["catch"](function (error) {
        swal2.fire({
          icon: "error",
          title: "Oops...",
          text: "Algo salió mal, vuelve a intentarlo!"
        });
      })["finally"](function () {
        _this2.$router.push({
          name: "boxes"
        });
      });
    },

    /**
     * Update a box.
     * @auth Kareem Lorenzana <kareem.lorenzana@nuvem.mx>
     * @date 2021-04-19
     * @param
     * @return void
     */
    edit: function edit(data) {
      var _this3 = this;

      axios.put("/api/boxes/" + data.Id, data).then(function (result) {
        swal2.fire("Editado", "Se editó la caja correctamente", "success");

        _this3.$router.push({
          name: "boxes"
        });
      })["catch"](function (error) {
        swal2.fire({
          icon: "error",
          title: "Oops...",
          text: "Algo salió mal, vuelve a intentarlo!"
        });
      });
    }
  },
  computed: {
    /**
     * Evaluate if an input is valid.
     * @auth Kareem Lorenzana <kareem.lorenzana@nuvem.mx>
     * @date 2021-04-26
     * @param
     * @return array
     */
    descriptionErrors: function descriptionErrors() {
      var errors = [];
      if (!this.$v.currentBox.Descripcion.$dirty) return errors;
      !this.$v.currentBox.Descripcion.required && errors.push("El campo es requerido");
      return errors;
    },

    /**
     * Evaluate if an input is valid.
     * @auth Kareem Lorenzana <kareem.lorenzana@nuvem.mx>
     * @date 2021-04-26
     * @param
     * @return array
     */
    highErrors: function highErrors() {
      var errors = [];
      if (!this.$v.currentBox.Alto.$dirty) return errors;
      !this.$v.currentBox.Alto.required && errors.push("El campo es requerido");
      !this.$v.currentBox.Alto.decimal && errors.push("El campo debe ser numérico");
      !this.$v.currentBox.Alto.maxLength && errors.push("El campo no debe pasar los 10 dígitos");
      return errors;
    },

    /**
     * Evaluate if an input is valid.
     * @auth Kareem Lorenzana <kareem.lorenzana@nuvem.mx>
     * @date 2021-04-26
     * @param
     * @return array
     */
    widthErrors: function widthErrors() {
      var errors = [];
      if (!this.$v.currentBox.Ancho.$dirty) return errors;
      !this.$v.currentBox.Ancho.required && errors.push("El campo es requerido");
      !this.$v.currentBox.Ancho.decimal && errors.push("El campo debe ser numérico");
      !this.$v.currentBox.Ancho.maxLength && errors.push("El campo no debe pasar los 10 dígitos");
      return errors;
    },

    /**
     * Evaluate if an input is valid.
     * @auth Kareem Lorenzana <kareem.lorenzana@nuvem.mx>
     * @date 2021-04-26
     * @param
     * @return array
     */
    largeErrors: function largeErrors() {
      var errors = [];
      if (!this.$v.currentBox.Largo.$dirty) return errors;
      !this.$v.currentBox.Largo.required && errors.push("El campo es requerido");
      !this.$v.currentBox.Largo.decimal && errors.push("El campo debe ser numérico");
      !this.$v.currentBox.Largo.maxLength && errors.push("El campo no debe pasar los 10 dígitos");
      return errors;
    },

    /**
     * Evaluate if an input is valid.
     * @auth Kareem Lorenzana <kareem.lorenzana@nuvem.mx>
     * @date 2021-04-26
     * @updated 2021-06-10 | Benito Huerta <benito.huerta@nuvem.mx>   
     * @param
     * @return array
     */
    weightErrors: function weightErrors() {
      var errors = [];
      if (!this.$v.currentBox.Peso.$dirty) return errors;
      !this.$v.currentBox.Peso.required && errors.push("El campo es requerido");
      !this.$v.currentBox.Peso.decimal && errors.push("El campo es numérico");
      !this.$v.currentBox.Peso.maxLength && errors.push("El campo no debe pasar los 10 dígitos");
      return errors;
    }
  },
  watch: {
    /**
     * Asign a new value to currentBox.
     * @auth Kareem Lorenzana <kareem.lorenzana@nuvem.mx>
     * @date 2021-04-26
     * @param Object newVal
     * @return void
     */
    boxToShow: function boxToShow(newVal) {
      this.currentBox = newVal;
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/shipping_configurations/box/show.vue?vue&type=script&lang=js&":
/*!**********************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/shipping_configurations/box/show.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _partials_Inputs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./partials/Inputs */ "./resources/js/views/shipping_configurations/box/partials/Inputs.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  name: "Show",
  props: ["dialog"],
  components: {
    Inputs: _partials_Inputs__WEBPACK_IMPORTED_MODULE_0__.default
  },
  data: function data() {
    return {
      box: {}
    };
  },
  methods: {
    /**
     * Redirect to index Boxes
     * @auth Kareem Lorenzana <kareem.lorenzana@nuvem.mx>
     * @date 2021-04-26
     * @param
     * @return void
     */
    cancel: function cancel() {
      this.$emit("dialogStatus", false);
    },

    /**
     * Receives a box from index component.
     * @auth JKareem Lorenzana <kareem.lorenzana@nuvem.mx>
     * @date 2021-04-26
     * @param Object box
     * @return void
     */
    reciveBox: function reciveBox(box) {
      this.box = box;
    }
  }
});

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/shipping_configurations/box/index.vue?vue&type=style&index=0&lang=css&":
/*!*********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/shipping_configurations/box/index.vue?vue&type=style&index=0&lang=css& ***!
  \*********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ ((module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../../../node_modules/css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js");
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0__);
// Imports

var ___CSS_LOADER_EXPORT___ = _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0___default()(function(i){return i[1]});
// Module
___CSS_LOADER_EXPORT___.push([module.id, "\n.v-chip.success, .v-btn:not(.v-btn--outlined).success, .v-progress-linear .v-progress-linear__background.primary{\nbackground:#018085 !important\n}\n", ""]);
// Exports
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (___CSS_LOADER_EXPORT___);


/***/ }),

/***/ "./resources/js/views/shipping_configurations/box/index.vue":
/*!******************************************************************!*\
  !*** ./resources/js/views/shipping_configurations/box/index.vue ***!
  \******************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _index_vue_vue_type_template_id_3b1fac3e___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./index.vue?vue&type=template&id=3b1fac3e& */ "./resources/js/views/shipping_configurations/box/index.vue?vue&type=template&id=3b1fac3e&");
/* harmony import */ var _index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./index.vue?vue&type=script&lang=js& */ "./resources/js/views/shipping_configurations/box/index.vue?vue&type=script&lang=js&");
/* harmony import */ var _index_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./index.vue?vue&type=style&index=0&lang=css& */ "./resources/js/views/shipping_configurations/box/index.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! !../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");



;


/* normalize component */

var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__.default)(
  _index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__.default,
  _index_vue_vue_type_template_id_3b1fac3e___WEBPACK_IMPORTED_MODULE_0__.render,
  _index_vue_vue_type_template_id_3b1fac3e___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/shipping_configurations/box/index.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/js/views/shipping_configurations/box/partials/Inputs.vue":
/*!****************************************************************************!*\
  !*** ./resources/js/views/shipping_configurations/box/partials/Inputs.vue ***!
  \****************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _Inputs_vue_vue_type_template_id_4c0bdfdc___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Inputs.vue?vue&type=template&id=4c0bdfdc& */ "./resources/js/views/shipping_configurations/box/partials/Inputs.vue?vue&type=template&id=4c0bdfdc&");
/* harmony import */ var _Inputs_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Inputs.vue?vue&type=script&lang=js& */ "./resources/js/views/shipping_configurations/box/partials/Inputs.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! !../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */
;
var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__.default)(
  _Inputs_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__.default,
  _Inputs_vue_vue_type_template_id_4c0bdfdc___WEBPACK_IMPORTED_MODULE_0__.render,
  _Inputs_vue_vue_type_template_id_4c0bdfdc___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/shipping_configurations/box/partials/Inputs.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/js/views/shipping_configurations/box/show.vue":
/*!*****************************************************************!*\
  !*** ./resources/js/views/shipping_configurations/box/show.vue ***!
  \*****************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _show_vue_vue_type_template_id_41c7b2c1___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./show.vue?vue&type=template&id=41c7b2c1& */ "./resources/js/views/shipping_configurations/box/show.vue?vue&type=template&id=41c7b2c1&");
/* harmony import */ var _show_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./show.vue?vue&type=script&lang=js& */ "./resources/js/views/shipping_configurations/box/show.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! !../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */
;
var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__.default)(
  _show_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__.default,
  _show_vue_vue_type_template_id_41c7b2c1___WEBPACK_IMPORTED_MODULE_0__.render,
  _show_vue_vue_type_template_id_41c7b2c1___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/shipping_configurations/box/show.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/js/views/shipping_configurations/box/index.vue?vue&type=script&lang=js&":
/*!*******************************************************************************************!*\
  !*** ./resources/js/views/shipping_configurations/box/index.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./index.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/shipping_configurations/box/index.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__.default); 

/***/ }),

/***/ "./resources/js/views/shipping_configurations/box/partials/Inputs.vue?vue&type=script&lang=js&":
/*!*****************************************************************************************************!*\
  !*** ./resources/js/views/shipping_configurations/box/partials/Inputs.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Inputs_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./Inputs.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/shipping_configurations/box/partials/Inputs.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Inputs_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__.default); 

/***/ }),

/***/ "./resources/js/views/shipping_configurations/box/show.vue?vue&type=script&lang=js&":
/*!******************************************************************************************!*\
  !*** ./resources/js/views/shipping_configurations/box/show.vue?vue&type=script&lang=js& ***!
  \******************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_show_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./show.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/shipping_configurations/box/show.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_show_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__.default); 

/***/ }),

/***/ "./resources/js/views/shipping_configurations/box/index.vue?vue&type=template&id=3b1fac3e&":
/*!*************************************************************************************************!*\
  !*** ./resources/js/views/shipping_configurations/box/index.vue?vue&type=template&id=3b1fac3e& ***!
  \*************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_3b1fac3e___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   "staticRenderFns": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_3b1fac3e___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_3b1fac3e___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./index.vue?vue&type=template&id=3b1fac3e& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/shipping_configurations/box/index.vue?vue&type=template&id=3b1fac3e&");


/***/ }),

/***/ "./resources/js/views/shipping_configurations/box/partials/Inputs.vue?vue&type=template&id=4c0bdfdc&":
/*!***********************************************************************************************************!*\
  !*** ./resources/js/views/shipping_configurations/box/partials/Inputs.vue?vue&type=template&id=4c0bdfdc& ***!
  \***********************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Inputs_vue_vue_type_template_id_4c0bdfdc___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   "staticRenderFns": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Inputs_vue_vue_type_template_id_4c0bdfdc___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Inputs_vue_vue_type_template_id_4c0bdfdc___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./Inputs.vue?vue&type=template&id=4c0bdfdc& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/shipping_configurations/box/partials/Inputs.vue?vue&type=template&id=4c0bdfdc&");


/***/ }),

/***/ "./resources/js/views/shipping_configurations/box/show.vue?vue&type=template&id=41c7b2c1&":
/*!************************************************************************************************!*\
  !*** ./resources/js/views/shipping_configurations/box/show.vue?vue&type=template&id=41c7b2c1& ***!
  \************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_show_vue_vue_type_template_id_41c7b2c1___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   "staticRenderFns": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_show_vue_vue_type_template_id_41c7b2c1___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_show_vue_vue_type_template_id_41c7b2c1___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./show.vue?vue&type=template&id=41c7b2c1& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/shipping_configurations/box/show.vue?vue&type=template&id=41c7b2c1&");


/***/ }),

/***/ "./resources/js/views/shipping_configurations/box/index.vue?vue&type=style&index=0&lang=css&":
/*!***************************************************************************************************!*\
  !*** ./resources/js/views/shipping_configurations/box/index.vue?vue&type=style&index=0&lang=css& ***!
  \***************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_node_modules_css_loader_dist_cjs_js_clonedRuleSet_10_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_10_0_rules_0_use_2_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-style-loader/index.js!../../../../../node_modules/css-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[1]!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[2]!../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./index.vue?vue&type=style&index=0&lang=css& */ "./node_modules/vue-style-loader/index.js!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/shipping_configurations/box/index.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_vue_style_loader_index_js_node_modules_css_loader_dist_cjs_js_clonedRuleSet_10_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_10_0_rules_0_use_2_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_node_modules_css_loader_dist_cjs_js_clonedRuleSet_10_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_10_0_rules_0_use_2_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ var __WEBPACK_REEXPORT_OBJECT__ = {};
/* harmony reexport (unknown) */ for(const __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_node_modules_css_loader_dist_cjs_js_clonedRuleSet_10_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_10_0_rules_0_use_2_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== "default") __WEBPACK_REEXPORT_OBJECT__[__WEBPACK_IMPORT_KEY__] = () => _node_modules_vue_style_loader_index_js_node_modules_css_loader_dist_cjs_js_clonedRuleSet_10_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_10_0_rules_0_use_2_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__[__WEBPACK_IMPORT_KEY__]
/* harmony reexport (unknown) */ __webpack_require__.d(__webpack_exports__, __WEBPACK_REEXPORT_OBJECT__);


/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/shipping_configurations/box/index.vue?vue&type=template&id=3b1fac3e&":
/*!****************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/shipping_configurations/box/index.vue?vue&type=template&id=3b1fac3e& ***!
  \****************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render),
/* harmony export */   "staticRenderFns": () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { attrs: { "data-app": "" } },
    [
      _c(
        "v-card",
        { staticClass: "p-3", attrs: { "data-app": "" } },
        [
          _c("Show", {
            ref: "sendBox",
            attrs: { dialog: _vm.dialogShow },
            on: { dialogStatus: _vm.getDialogShowStatus }
          }),
          _vm._v(" "),
          _c("dialog-delete", {
            attrs: { status: _vm.dialogDelete },
            on: {
              cancel: function($event) {
                _vm.dialogDelete = false
              },
              confirm: _vm.deleteItem
            }
          }),
          _vm._v(" "),
          _c("v-data-table", {
            staticClass: "elevation-1",
            attrs: {
              headers: _vm.headers,
              items: _vm.boxes,
              search: _vm.search,
              page: _vm.page,
              "items-per-page": _vm.itemsPerPage,
              loading: _vm.loading,
              "hide-default-footer": "",
              "no-data-text": "No hay ningún registro",
              "no-results-text": "No se encontraron coincidencias",
              "loading-text": "Cargando... Porfavor espere"
            },
            on: {
              "update:page": function($event) {
                _vm.page = $event
              },
              "page-count": function($event) {
                _vm.pageCount = $event
              }
            },
            scopedSlots: _vm._u(
              [
                {
                  key: "top",
                  fn: function() {
                    return [
                      _c(
                        "datatable-header",
                        {
                          attrs: { title: "Cajas", "disable-creation": false },
                          on: {
                            searching: function($event) {
                              _vm.search = $event
                            },
                            open: _vm.openCreateBox
                          }
                        },
                        [
                          _c(
                            "v-container",
                            { attrs: { fluid: "" } },
                            [
                              _c("v-select", {
                                staticClass: "ml-5",
                                attrs: {
                                  items: _vm.boxesStatus,
                                  placeholder: "Filtrar por estatus",
                                  outlined: "",
                                  dense: "",
                                  "hide-details": "",
                                  clearable: "",
                                  loading: _vm.loading,
                                  "item-text": "text",
                                  "item-value": "value"
                                },
                                on: {
                                  input: function($event) {
                                    return _vm.getBoxes()
                                  }
                                },
                                model: {
                                  value: _vm.estatusFilter,
                                  callback: function($$v) {
                                    _vm.estatusFilter = $$v
                                  },
                                  expression: "estatusFilter"
                                }
                              })
                            ],
                            1
                          )
                        ],
                        1
                      )
                    ]
                  },
                  proxy: true
                },
                {
                  key: "item.Estatus",
                  fn: function(ref) {
                    var item = ref.item
                    return [_c("chip-status", { attrs: { item: item } })]
                  }
                },
                {
                  key: "item.actions",
                  fn: function(ref) {
                    var item = ref.item
                    return [
                      _c("action-buttons", {
                        attrs: { item: item },
                        on: {
                          edit: function($event) {
                            return _vm.openEditBox(item)
                          },
                          view: _vm.goToShowBox,
                          delete: _vm.openDeleteDialog
                        }
                      })
                    ]
                  }
                }
              ],
              null,
              true
            )
          }),
          _vm._v(" "),
          _c("pagination", {
            attrs: {
              last: _vm.pageCount,
              current: _vm.page,
              itemsPerPage: _vm.itemsPerPage,
              total: this.boxes.length
            },
            on: {
              input: function($event) {
                _vm.page = $event
              }
            }
          })
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/shipping_configurations/box/partials/Inputs.vue?vue&type=template&id=4c0bdfdc&":
/*!**************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/shipping_configurations/box/partials/Inputs.vue?vue&type=template&id=4c0bdfdc& ***!
  \**************************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render),
/* harmony export */   "staticRenderFns": () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "form",
    { attrs: { "data-app": "" } },
    [
      _c(
        "v-row",
        [
          _c(
            "v-col",
            { attrs: { cols: "6", md: "12" } },
            [
              _c("h6", [_vm._v("Descripción")]),
              _vm._v(" "),
              _c("v-text-field", {
                attrs: {
                  "error-messages": _vm.descriptionErrors,
                  placeholder: "Descripción",
                  readonly: _vm.inputsStatus,
                  outlined: "",
                  required: "",
                  dense: ""
                },
                on: {
                  input: function($event) {
                    return _vm.$v.currentBox.Descripcion.$touch()
                  },
                  blur: function($event) {
                    return _vm.$v.currentBox.Descripcion.$touch()
                  }
                },
                model: {
                  value: _vm.currentBox.Descripcion,
                  callback: function($$v) {
                    _vm.$set(_vm.currentBox, "Descripcion", $$v)
                  },
                  expression: "currentBox.Descripcion"
                }
              })
            ],
            1
          )
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "v-row",
        [
          _c(
            "v-col",
            { attrs: { cols: "6", md: "12" } },
            [
              _c("h6", [_vm._v("Alto")]),
              _vm._v(" "),
              _c("v-text-field", {
                attrs: {
                  "error-messages": _vm.highErrors,
                  placeholder: "Alto en CM",
                  readonly: _vm.inputsStatus,
                  outlined: "",
                  required: "",
                  dense: ""
                },
                on: {
                  input: function($event) {
                    return _vm.$v.currentBox.Alto.$touch()
                  },
                  blur: function($event) {
                    return _vm.$v.currentBox.Alto.$touch()
                  }
                },
                model: {
                  value: _vm.currentBox.Alto,
                  callback: function($$v) {
                    _vm.$set(_vm.currentBox, "Alto", $$v)
                  },
                  expression: "currentBox.Alto"
                }
              })
            ],
            1
          )
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "v-row",
        [
          _c(
            "v-col",
            { attrs: { cols: "6", md: "12" } },
            [
              _c("h6", [_vm._v("Ancho")]),
              _vm._v(" "),
              _c("v-text-field", {
                attrs: {
                  "error-messages": _vm.widthErrors,
                  placeholder: "Ancho en CM",
                  readonly: _vm.inputsStatus,
                  outlined: "",
                  required: "",
                  dense: ""
                },
                on: {
                  input: function($event) {
                    return _vm.$v.currentBox.Ancho.$touch()
                  },
                  blur: function($event) {
                    return _vm.$v.currentBox.Ancho.$touch()
                  }
                },
                model: {
                  value: _vm.currentBox.Ancho,
                  callback: function($$v) {
                    _vm.$set(_vm.currentBox, "Ancho", $$v)
                  },
                  expression: "currentBox.Ancho"
                }
              })
            ],
            1
          )
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "v-row",
        [
          _c(
            "v-col",
            { attrs: { cols: "6", md: "12" } },
            [
              _c("h6", [_vm._v("Largo")]),
              _vm._v(" "),
              _c("v-text-field", {
                attrs: {
                  "error-messages": _vm.largeErrors,
                  placeholder: "Largo en CM",
                  readonly: _vm.inputsStatus,
                  outlined: "",
                  required: "",
                  dense: ""
                },
                on: {
                  input: function($event) {
                    return _vm.$v.currentBox.Largo.$touch()
                  },
                  blur: function($event) {
                    return _vm.$v.currentBox.Largo.$touch()
                  }
                },
                model: {
                  value: _vm.currentBox.Largo,
                  callback: function($$v) {
                    _vm.$set(_vm.currentBox, "Largo", $$v)
                  },
                  expression: "currentBox.Largo"
                }
              })
            ],
            1
          )
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "v-row",
        [
          _c(
            "v-col",
            { attrs: { cols: "6", md: "12" } },
            [
              _c("h6", [_vm._v("Peso Máximo")]),
              _vm._v(" "),
              _c("v-text-field", {
                attrs: {
                  "error-messages": _vm.weightErrors,
                  placeholder: "Peso Máximo",
                  readonly: _vm.inputsStatus,
                  outlined: "",
                  required: "",
                  dense: ""
                },
                on: {
                  input: function($event) {
                    return _vm.$v.currentBox.Peso.$touch()
                  },
                  blur: function($event) {
                    return _vm.$v.currentBox.Peso.$touch()
                  }
                },
                model: {
                  value: _vm.currentBox.Peso,
                  callback: function($$v) {
                    _vm.$set(_vm.currentBox, "Peso", $$v)
                  },
                  expression: "currentBox.Peso"
                }
              })
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/shipping_configurations/box/show.vue?vue&type=template&id=41c7b2c1&":
/*!***************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/shipping_configurations/box/show.vue?vue&type=template&id=41c7b2c1& ***!
  \***************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render),
/* harmony export */   "staticRenderFns": () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "text-center" },
    [
      _c(
        "v-dialog",
        {
          attrs: { persistent: "", "max-width": "800px" },
          model: {
            value: _vm.dialog,
            callback: function($$v) {
              _vm.dialog = $$v
            },
            expression: "dialog"
          }
        },
        [
          _c(
            "v-card",
            [
              _c("v-card-title", [
                _c("span", { staticClass: "headline" }, [
                  _vm._v("Información de la Caja")
                ])
              ]),
              _vm._v(" "),
              _c(
                "v-card-text",
                [
                  _c("Inputs", {
                    attrs: { boxToShow: _vm.box, inputsStatus: true }
                  })
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "v-container",
                { staticClass: "d-flex flex-column mb-6" },
                [
                  _c(
                    "v-card-actions",
                    { staticClass: "mt-5 d-block" },
                    [
                      _c("form-action-buttons", {
                        attrs: { hideSaveBtn: true },
                        on: { close: _vm.cancel, save: false }
                      })
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-style-loader/index.js!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/shipping_configurations/box/index.vue?vue&type=style&index=0&lang=css&":
/*!**************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-style-loader/index.js!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/shipping_configurations/box/index.vue?vue&type=style&index=0&lang=css& ***!
  \**************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(/*! !!../../../../../node_modules/css-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[1]!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[2]!../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./index.vue?vue&type=style&index=0&lang=css& */ "./node_modules/css-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/shipping_configurations/box/index.vue?vue&type=style&index=0&lang=css&");
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.id, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var add = __webpack_require__(/*! !../../../../../node_modules/vue-style-loader/lib/addStylesClient.js */ "./node_modules/vue-style-loader/lib/addStylesClient.js").default
var update = add("68494d32", content, false, {});
// Hot Module Replacement
if(false) {}

/***/ })

}]);