$('.your-class').slick({
    // infinite: true,
    infinite: true,
    // arrow: true,
    slidesToShow: 3,
    slidesToScroll: 3,
    responsive: [
        {
            breakpoint: 768,
            settings: {
                arrows: false,
                centerMode: true,
                centerPadding: '40px',
                slidesToShow: 3
            }
        },
        {
            breakpoint: 480,
            settings: {
                arrows: false,
                centerMode: true,
                centerPadding: '40px',
                slidesToShow: 1
            }
        }
    ]
});

$(document).ready(function () {
    $('.responsive').slick({
        // dots: true,
        infinite: true,
        arrows: false,
        speed: 2000,
        autoplay: true,
        fade: true,
        // cssEase: 'linear'
    });
});

