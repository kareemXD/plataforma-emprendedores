(self["webpackChunk"] = self["webpackChunk"] || []).push([["resources_js_views_shipping_configurations_box_edit_vue"],{

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/shipping_configurations/box/edit.vue?vue&type=script&lang=js&":
/*!**********************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/shipping_configurations/box/edit.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _partials_Inputs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./partials/Inputs */ "./resources/js/views/shipping_configurations/box/partials/Inputs.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  name: "EditBox",
  components: {
    Inputs: _partials_Inputs__WEBPACK_IMPORTED_MODULE_0__.default
  },
  data: function data() {
    return {
      submitStatus: "",
      currentRule: {
        description: "",
        high: "",
        width: "",
        "long": "",
        weight: ""
      }
    };
  },
  methods: {
    /**
     * Redirect to index boxes.
     * @auth Kareem Lorenzana <kareem.lorenzana@nuvem.mx>
     * @date 2021-04-26
     * @param
     * @return void
     */
    submit: function submit() {
      this.$refs.submit.submit();
    },

    /**
     * Redirect to index boxes.
     * @auth Kareem Lorenzana <kareem.lorenzana@nuvem.mx>
     * @date 2021-04-26
     * @param
     * @return void
     */
    cancel: function cancel() {
      this.$router.push({
        name: "boxes"
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/shipping_configurations/box/partials/Inputs.vue?vue&type=script&lang=js&":
/*!*********************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/shipping_configurations/box/partials/Inputs.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var vuelidate__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vuelidate */ "./node_modules/vuelidate/lib/index.js");
/* harmony import */ var vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vuelidate/lib/validators */ "./node_modules/vuelidate/lib/validators/index.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  name: "Inputs",
  props: {
    boxToShow: {
      type: Object,
      "default": null
    },
    inputsStatus: {
      type: Boolean,
      "default": false
    }
  },
  mixins: [vuelidate__WEBPACK_IMPORTED_MODULE_0__.validationMixin],
  validations: {
    currentBox: {
      Descripcion: {
        required: vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_1__.required
      },
      Ancho: {
        required: vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_1__.required,
        decimal: vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_1__.decimal,
        maxLength: (0,vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_1__.maxLength)(10)
      },
      Alto: {
        required: vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_1__.required,
        decimal: vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_1__.decimal,
        maxLength: (0,vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_1__.maxLength)(10)
      },
      Largo: {
        required: vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_1__.required,
        decimal: vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_1__.decimal,
        maxLength: (0,vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_1__.maxLength)(10)
      },
      Peso: {
        required: vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_1__.required,
        decimal: vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_1__.decimal,
        maxLength: (0,vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_1__.maxLength)(10)
      }
    }
  },
  mounted: function mounted() {
    if (this.$route.params.item != null) {
      this.currentBox = this.$route.params.item;
      console.log(this.$route.params.item);
      this.previousRulePriority = this.$route.params.item.Prioridad;
    } else if (this.boxToShow != null) {
      this.currentBox = this.boxToShow;
    }
  },
  data: function data() {
    return {
      submitStatus: "",
      previousRulePriority: null,
      currentBox: {
        Descripcion: "",
        Alto: "",
        Ancho: "",
        Largo: "",
        Peso: "",
        Estatus: true
      }
    };
  },
  methods: {
    /**
     * Allows call post or edit method if data is valid.
     * @auth Kareem Lorenzana <kareem.lorenzana@nuvem.mx>
     * @date 2021-04-19
     * @param
     * @return void
     */
    submit: function submit() {
      var _this = this;

      this.$v.$touch();

      if (this.$v.$invalid) {
        this.submitStatus = "ERROR";
      } else if (!this.$v.$invalid) {
        this.$route.params.item != null ? this.edit(this.currentBox) : this.post(this.currentBox);
      } else {
        this.submitStatus = "PENDING";
        setTimeout(function () {
          _this.submitStatus = "OK";
        }, 500);
      }
    },

    /**
     * Post a new Box.
     * @auth Kareem Lorenzana <kareem.lorenzana@nuvem.mx>
     * @date 2021-04-19
     * @param
     * @return void
     */
    post: function post(data) {
      var _this2 = this;

      axios.post("/api/boxes", data).then(function (result) {
        swal2.fire("Creado", "Se agregaron los datos correctamente", "success");
      })["catch"](function (error) {
        swal2.fire({
          icon: "error",
          title: "Oops...",
          text: "Algo salió mal, vuelve a intentarlo!"
        });
      })["finally"](function () {
        _this2.$router.push({
          name: "boxes"
        });
      });
    },

    /**
     * Update a box.
     * @auth Kareem Lorenzana <kareem.lorenzana@nuvem.mx>
     * @date 2021-04-19
     * @param
     * @return void
     */
    edit: function edit(data) {
      var _this3 = this;

      axios.put("/api/boxes/" + data.Id, data).then(function (result) {
        swal2.fire("Editado", "Se editó la caja correctamente", "success");

        _this3.$router.push({
          name: "boxes"
        });
      })["catch"](function (error) {
        swal2.fire({
          icon: "error",
          title: "Oops...",
          text: "Algo salió mal, vuelve a intentarlo!"
        });
      });
    }
  },
  computed: {
    /**
     * Evaluate if an input is valid.
     * @auth Kareem Lorenzana <kareem.lorenzana@nuvem.mx>
     * @date 2021-04-26
     * @param
     * @return array
     */
    descriptionErrors: function descriptionErrors() {
      var errors = [];
      if (!this.$v.currentBox.Descripcion.$dirty) return errors;
      !this.$v.currentBox.Descripcion.required && errors.push("El campo es requerido");
      return errors;
    },

    /**
     * Evaluate if an input is valid.
     * @auth Kareem Lorenzana <kareem.lorenzana@nuvem.mx>
     * @date 2021-04-26
     * @param
     * @return array
     */
    highErrors: function highErrors() {
      var errors = [];
      if (!this.$v.currentBox.Alto.$dirty) return errors;
      !this.$v.currentBox.Alto.required && errors.push("El campo es requerido");
      !this.$v.currentBox.Alto.decimal && errors.push("El campo debe ser numérico");
      !this.$v.currentBox.Alto.maxLength && errors.push("El campo no debe pasar los 10 dígitos");
      return errors;
    },

    /**
     * Evaluate if an input is valid.
     * @auth Kareem Lorenzana <kareem.lorenzana@nuvem.mx>
     * @date 2021-04-26
     * @param
     * @return array
     */
    widthErrors: function widthErrors() {
      var errors = [];
      if (!this.$v.currentBox.Ancho.$dirty) return errors;
      !this.$v.currentBox.Ancho.required && errors.push("El campo es requerido");
      !this.$v.currentBox.Ancho.decimal && errors.push("El campo debe ser numérico");
      !this.$v.currentBox.Ancho.maxLength && errors.push("El campo no debe pasar los 10 dígitos");
      return errors;
    },

    /**
     * Evaluate if an input is valid.
     * @auth Kareem Lorenzana <kareem.lorenzana@nuvem.mx>
     * @date 2021-04-26
     * @param
     * @return array
     */
    largeErrors: function largeErrors() {
      var errors = [];
      if (!this.$v.currentBox.Largo.$dirty) return errors;
      !this.$v.currentBox.Largo.required && errors.push("El campo es requerido");
      !this.$v.currentBox.Largo.decimal && errors.push("El campo debe ser numérico");
      !this.$v.currentBox.Largo.maxLength && errors.push("El campo no debe pasar los 10 dígitos");
      return errors;
    },

    /**
     * Evaluate if an input is valid.
     * @auth Kareem Lorenzana <kareem.lorenzana@nuvem.mx>
     * @date 2021-04-26
     * @updated 2021-06-10 | Benito Huerta <benito.huerta@nuvem.mx>   
     * @param
     * @return array
     */
    weightErrors: function weightErrors() {
      var errors = [];
      if (!this.$v.currentBox.Peso.$dirty) return errors;
      !this.$v.currentBox.Peso.required && errors.push("El campo es requerido");
      !this.$v.currentBox.Peso.decimal && errors.push("El campo es numérico");
      !this.$v.currentBox.Peso.maxLength && errors.push("El campo no debe pasar los 10 dígitos");
      return errors;
    }
  },
  watch: {
    /**
     * Asign a new value to currentBox.
     * @auth Kareem Lorenzana <kareem.lorenzana@nuvem.mx>
     * @date 2021-04-26
     * @param Object newVal
     * @return void
     */
    boxToShow: function boxToShow(newVal) {
      this.currentBox = newVal;
    }
  }
});

/***/ }),

/***/ "./resources/js/views/shipping_configurations/box/edit.vue":
/*!*****************************************************************!*\
  !*** ./resources/js/views/shipping_configurations/box/edit.vue ***!
  \*****************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _edit_vue_vue_type_template_id_95121164___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./edit.vue?vue&type=template&id=95121164& */ "./resources/js/views/shipping_configurations/box/edit.vue?vue&type=template&id=95121164&");
/* harmony import */ var _edit_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./edit.vue?vue&type=script&lang=js& */ "./resources/js/views/shipping_configurations/box/edit.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! !../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */
;
var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__.default)(
  _edit_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__.default,
  _edit_vue_vue_type_template_id_95121164___WEBPACK_IMPORTED_MODULE_0__.render,
  _edit_vue_vue_type_template_id_95121164___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/shipping_configurations/box/edit.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/js/views/shipping_configurations/box/partials/Inputs.vue":
/*!****************************************************************************!*\
  !*** ./resources/js/views/shipping_configurations/box/partials/Inputs.vue ***!
  \****************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _Inputs_vue_vue_type_template_id_4c0bdfdc___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Inputs.vue?vue&type=template&id=4c0bdfdc& */ "./resources/js/views/shipping_configurations/box/partials/Inputs.vue?vue&type=template&id=4c0bdfdc&");
/* harmony import */ var _Inputs_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Inputs.vue?vue&type=script&lang=js& */ "./resources/js/views/shipping_configurations/box/partials/Inputs.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! !../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */
;
var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__.default)(
  _Inputs_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__.default,
  _Inputs_vue_vue_type_template_id_4c0bdfdc___WEBPACK_IMPORTED_MODULE_0__.render,
  _Inputs_vue_vue_type_template_id_4c0bdfdc___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/shipping_configurations/box/partials/Inputs.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/js/views/shipping_configurations/box/edit.vue?vue&type=script&lang=js&":
/*!******************************************************************************************!*\
  !*** ./resources/js/views/shipping_configurations/box/edit.vue?vue&type=script&lang=js& ***!
  \******************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_edit_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./edit.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/shipping_configurations/box/edit.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_edit_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__.default); 

/***/ }),

/***/ "./resources/js/views/shipping_configurations/box/partials/Inputs.vue?vue&type=script&lang=js&":
/*!*****************************************************************************************************!*\
  !*** ./resources/js/views/shipping_configurations/box/partials/Inputs.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Inputs_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./Inputs.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/shipping_configurations/box/partials/Inputs.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Inputs_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__.default); 

/***/ }),

/***/ "./resources/js/views/shipping_configurations/box/edit.vue?vue&type=template&id=95121164&":
/*!************************************************************************************************!*\
  !*** ./resources/js/views/shipping_configurations/box/edit.vue?vue&type=template&id=95121164& ***!
  \************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_edit_vue_vue_type_template_id_95121164___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   "staticRenderFns": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_edit_vue_vue_type_template_id_95121164___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_edit_vue_vue_type_template_id_95121164___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./edit.vue?vue&type=template&id=95121164& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/shipping_configurations/box/edit.vue?vue&type=template&id=95121164&");


/***/ }),

/***/ "./resources/js/views/shipping_configurations/box/partials/Inputs.vue?vue&type=template&id=4c0bdfdc&":
/*!***********************************************************************************************************!*\
  !*** ./resources/js/views/shipping_configurations/box/partials/Inputs.vue?vue&type=template&id=4c0bdfdc& ***!
  \***********************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Inputs_vue_vue_type_template_id_4c0bdfdc___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   "staticRenderFns": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Inputs_vue_vue_type_template_id_4c0bdfdc___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Inputs_vue_vue_type_template_id_4c0bdfdc___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./Inputs.vue?vue&type=template&id=4c0bdfdc& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/shipping_configurations/box/partials/Inputs.vue?vue&type=template&id=4c0bdfdc&");


/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/shipping_configurations/box/edit.vue?vue&type=template&id=95121164&":
/*!***************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/shipping_configurations/box/edit.vue?vue&type=template&id=95121164& ***!
  \***************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render),
/* harmony export */   "staticRenderFns": () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "v-card",
    { staticClass: "p-3" },
    [
      _c("v-card-title", [_c("b", [_vm._v("Edición de caja")])]),
      _vm._v(" "),
      _c(
        "v-card-text",
        [
          _c(
            "v-card-actions",
            { staticClass: "mt-5 d-block" },
            [
              _c("Inputs", { ref: "submit" }),
              _vm._v(" "),
              _c("form-action-buttons", {
                on: { close: _vm.cancel, save: _vm.submit }
              })
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/shipping_configurations/box/partials/Inputs.vue?vue&type=template&id=4c0bdfdc&":
/*!**************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/shipping_configurations/box/partials/Inputs.vue?vue&type=template&id=4c0bdfdc& ***!
  \**************************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render),
/* harmony export */   "staticRenderFns": () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "form",
    { attrs: { "data-app": "" } },
    [
      _c(
        "v-row",
        [
          _c(
            "v-col",
            { attrs: { cols: "6", md: "12" } },
            [
              _c("h6", [_vm._v("Descripción")]),
              _vm._v(" "),
              _c("v-text-field", {
                attrs: {
                  "error-messages": _vm.descriptionErrors,
                  placeholder: "Descripción",
                  readonly: _vm.inputsStatus,
                  outlined: "",
                  required: "",
                  dense: ""
                },
                on: {
                  input: function($event) {
                    return _vm.$v.currentBox.Descripcion.$touch()
                  },
                  blur: function($event) {
                    return _vm.$v.currentBox.Descripcion.$touch()
                  }
                },
                model: {
                  value: _vm.currentBox.Descripcion,
                  callback: function($$v) {
                    _vm.$set(_vm.currentBox, "Descripcion", $$v)
                  },
                  expression: "currentBox.Descripcion"
                }
              })
            ],
            1
          )
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "v-row",
        [
          _c(
            "v-col",
            { attrs: { cols: "6", md: "12" } },
            [
              _c("h6", [_vm._v("Alto")]),
              _vm._v(" "),
              _c("v-text-field", {
                attrs: {
                  "error-messages": _vm.highErrors,
                  placeholder: "Alto en CM",
                  readonly: _vm.inputsStatus,
                  outlined: "",
                  required: "",
                  dense: ""
                },
                on: {
                  input: function($event) {
                    return _vm.$v.currentBox.Alto.$touch()
                  },
                  blur: function($event) {
                    return _vm.$v.currentBox.Alto.$touch()
                  }
                },
                model: {
                  value: _vm.currentBox.Alto,
                  callback: function($$v) {
                    _vm.$set(_vm.currentBox, "Alto", $$v)
                  },
                  expression: "currentBox.Alto"
                }
              })
            ],
            1
          )
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "v-row",
        [
          _c(
            "v-col",
            { attrs: { cols: "6", md: "12" } },
            [
              _c("h6", [_vm._v("Ancho")]),
              _vm._v(" "),
              _c("v-text-field", {
                attrs: {
                  "error-messages": _vm.widthErrors,
                  placeholder: "Ancho en CM",
                  readonly: _vm.inputsStatus,
                  outlined: "",
                  required: "",
                  dense: ""
                },
                on: {
                  input: function($event) {
                    return _vm.$v.currentBox.Ancho.$touch()
                  },
                  blur: function($event) {
                    return _vm.$v.currentBox.Ancho.$touch()
                  }
                },
                model: {
                  value: _vm.currentBox.Ancho,
                  callback: function($$v) {
                    _vm.$set(_vm.currentBox, "Ancho", $$v)
                  },
                  expression: "currentBox.Ancho"
                }
              })
            ],
            1
          )
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "v-row",
        [
          _c(
            "v-col",
            { attrs: { cols: "6", md: "12" } },
            [
              _c("h6", [_vm._v("Largo")]),
              _vm._v(" "),
              _c("v-text-field", {
                attrs: {
                  "error-messages": _vm.largeErrors,
                  placeholder: "Largo en CM",
                  readonly: _vm.inputsStatus,
                  outlined: "",
                  required: "",
                  dense: ""
                },
                on: {
                  input: function($event) {
                    return _vm.$v.currentBox.Largo.$touch()
                  },
                  blur: function($event) {
                    return _vm.$v.currentBox.Largo.$touch()
                  }
                },
                model: {
                  value: _vm.currentBox.Largo,
                  callback: function($$v) {
                    _vm.$set(_vm.currentBox, "Largo", $$v)
                  },
                  expression: "currentBox.Largo"
                }
              })
            ],
            1
          )
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "v-row",
        [
          _c(
            "v-col",
            { attrs: { cols: "6", md: "12" } },
            [
              _c("h6", [_vm._v("Peso Máximo")]),
              _vm._v(" "),
              _c("v-text-field", {
                attrs: {
                  "error-messages": _vm.weightErrors,
                  placeholder: "Peso Máximo",
                  readonly: _vm.inputsStatus,
                  outlined: "",
                  required: "",
                  dense: ""
                },
                on: {
                  input: function($event) {
                    return _vm.$v.currentBox.Peso.$touch()
                  },
                  blur: function($event) {
                    return _vm.$v.currentBox.Peso.$touch()
                  }
                },
                model: {
                  value: _vm.currentBox.Peso,
                  callback: function($$v) {
                    _vm.$set(_vm.currentBox, "Peso", $$v)
                  },
                  expression: "currentBox.Peso"
                }
              })
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ })

}]);