(self["webpackChunk"] = self["webpackChunk"] || []).push([["resources_js_views_shipping_configurations_index_vue"],{

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/shipping_configurations/index.vue?vue&type=script&lang=js&":
/*!*******************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/shipping_configurations/index.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm.js");
/* harmony import */ var _store__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../store */ "./resources/js/store/index.js");
/* harmony import */ var _views_Layouts_Components_PageTitle_vue__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../views/Layouts/Components/PageTitle.vue */ "./resources/js/views/Layouts/Components/PageTitle.vue");
/* harmony import */ var _Layout_Components_LayoutWrapper_vue__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../Layout/Components/LayoutWrapper.vue */ "./resources/js/Layout/Components/LayoutWrapper.vue");
/* harmony import */ var _components_pagination2_vue__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../components/pagination2.vue */ "./resources/js/components/pagination2.vue");
/* harmony import */ var _components_label_vue__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../components/label.vue */ "./resources/js/components/label.vue");
/* harmony import */ var _components_formActionButtons_vue__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../components/formActionButtons.vue */ "./resources/js/components/formActionButtons.vue");
/* harmony import */ var _components_datatableHeader_vue__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../components/datatableHeader.vue */ "./resources/js/components/datatableHeader.vue");
/* harmony import */ var _components_chipStatus_vue__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../components/chipStatus.vue */ "./resources/js/components/chipStatus.vue");
/* harmony import */ var _components_datatableActionButtons_vue__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../components/datatableActionButtons.vue */ "./resources/js/components/datatableActionButtons.vue");
/* harmony import */ var _components_dialogDelete_vue__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../components/dialogDelete.vue */ "./resources/js/components/dialogDelete.vue");
/* harmony import */ var vuelidate__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! vuelidate */ "./node_modules/vuelidate/lib/index.js");
/* harmony import */ var vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! vuelidate/lib/validators */ "./node_modules/vuelidate/lib/validators/index.js");
/* harmony import */ var v_mask__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! v-mask */ "./node_modules/v-mask/dist/v-mask.esm.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//














vue__WEBPACK_IMPORTED_MODULE_11__.default.use(v_mask__WEBPACK_IMPORTED_MODULE_10__.default);
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  components: {
    PageTitle: _views_Layouts_Components_PageTitle_vue__WEBPACK_IMPORTED_MODULE_1__.default,
    "layout-wrapper": _Layout_Components_LayoutWrapper_vue__WEBPACK_IMPORTED_MODULE_2__.default,
    "pagination": _components_pagination2_vue__WEBPACK_IMPORTED_MODULE_3__.default,
    "n-label": _components_label_vue__WEBPACK_IMPORTED_MODULE_4__.default,
    "form-action-buttons": _components_formActionButtons_vue__WEBPACK_IMPORTED_MODULE_5__.default,
    "datatable-header": _components_datatableHeader_vue__WEBPACK_IMPORTED_MODULE_6__.default,
    'chip-status': _components_chipStatus_vue__WEBPACK_IMPORTED_MODULE_7__.default,
    'action-buttons': _components_datatableActionButtons_vue__WEBPACK_IMPORTED_MODULE_8__.default,
    'dialog-delete': _components_dialogDelete_vue__WEBPACK_IMPORTED_MODULE_9__.default
  },
  mixins: [vuelidate__WEBPACK_IMPORTED_MODULE_12__.validationMixin],
  validations: {
    editedItem: {
      Id_CostoEnvioTipo: {
        required: vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_13__.required
      },
      Descripcion: {
        required: vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_13__.required,
        maxLength: (0,vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_13__.maxLength)(80)
      },
      TiempoEstimadoEnvio: {
        required: vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_13__.required,
        maxLength: (0,vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_13__.maxLength)(10)
      },
      Tarifa: {
        required: vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_13__.required
      },
      MontoMinimoEnvioGratuito: {
        required: (0,vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_13__.requiredIf)(function () {
          return this.editedItem.AplicaEnvioGratuito === '1';
        })
      }
    }
  },
  data: function data() {
    return {
      roleFilter: null,
      page: 1,
      pageCount: 0,
      itemsPerPage: 10,
      icon: "pe-7s-users",
      search: '',
      loading: false,
      dialog: false,
      dialogDelete: false,
      valid: true,
      editedIndex: -1,
      submitStatus: null,
      errorMessage: '',
      show1: false,
      editionBlocked: false,
      Id_CostoEnvioTipos: [],
      editedItem: {
        Id_CostoEnvio: null,
        Id_CostoEnvioTipo: null,
        Descripcion: '',
        TiempoEstimadoEnvio: null,
        Tarifa: null,
        AplicaEnvioGratuito: '0',
        MontoMinimoEnvioGratuito: ''
      },
      defaultItem: {
        Id_CostoEnvio: null,
        Id_CostoEnvioTipo: null,
        Descripcion: '',
        TiempoEstimadoEnvio: '',
        Tarifa: '',
        AplicaEnvioGratuito: '0',
        MontoMinimoEnvioGratuito: ''
      },
      headers: [{
        text: 'Id',
        align: 'start',
        sortable: true,
        value: 'Id_CostoEnvio'
      }, {
        text: 'Nombre',
        sortable: true,
        value: 'Descripcion'
      }, {
        text: 'Tiempo estimado',
        value: 'TiempoEstimadoEnvio'
      }, {
        text: 'Tarifa',
        value: 'Tarifa'
      }, {
        text: 'Estatus',
        value: 'status'
      }, {
        text: 'Acciones',
        value: 'actions',
        sortable: false
      }],
      shippingConfigurations: []
    };
  },
  mounted: function mounted() {
    this.getDataTableEntries();
  },
  computed: {
    /**
     * Gets users permissions
     *
     * @author Giovanny González <giovanny.gonzalez@nuvem.mx>
     * created 01/04/2021
     * @params
     * @return array
     *
     */
    permissions: function permissions() {
      return _store__WEBPACK_IMPORTED_MODULE_0__.default.state.permissions;
    },

    /**
     * Gets formTitle based on editedIndex
     *
     * @author Giovanny González <giovanny.gonzalez@nuvem.mx>
     * created 01/04/2021
     * @params
     * @return string
     *
     */
    formTitle: function formTitle() {
      return this.editedIndex === -1 ? 'Alta de costos de envío' : 'Editar costo de envío';
    },

    /**
     * Inserts Id_CostoEnvioTipo error to errors array
     *
     * @author Giovanny González <giovanny.gonzalez@nuvem.mx>
     * created 01/04/2021
     * @params
     * @return array
     *
     */
    Id_CostoEnvioTipoErrors: function Id_CostoEnvioTipoErrors() {
      var errors = [];
      if (!this.$v.editedItem.Id_CostoEnvioTipo.$dirty) return errors;
      !this.$v.editedItem.Id_CostoEnvioTipo.required && errors.push('Este campo es requerido');
      return errors;
    },

    /**
     * Inserts Descripcion error to errors array
     *
     * @author Giovanny González <giovanny.gonzalez@nuvem.mx>
     * created 01/04/2021
     * @params
     * @return array
     *
     */
    nameErrors: function nameErrors() {
      var errors = [];
      if (!this.$v.editedItem.Descripcion.$dirty) return errors;
      !this.$v.editedItem.Descripcion.required && errors.push('Este campo es requerido');
      return errors;
    },

    /**
     * Inserts TiempoEstimadoEnvio error to errors array
     *
     * @author Giovanny González <giovanny.gonzalez@nuvem.mx>
     * created 01/04/2021
     * @params
     * @return array
     *
     */
    TiempoEstimadoEnvioErrors: function TiempoEstimadoEnvioErrors() {
      var errors = [];
      if (!this.$v.editedItem.TiempoEstimadoEnvio.$dirty) return errors;
      !this.$v.editedItem.TiempoEstimadoEnvio.required && errors.push('Este campo es requerido');
      return errors;
    },

    /**
     * Inserts Tarifa error to errors array
     *
     * @author Giovanny González <giovanny.gonzalez@nuvem.mx>
     * created 01/04/2021
     * @params
     * @return array
     *
     */
    TarifaErrors: function TarifaErrors() {
      var errors = [];
      if (!this.$v.editedItem.Tarifa.$dirty) return errors;
      !this.$v.editedItem.Tarifa.required && errors.push('Este campo es requerido');
      return errors;
    },

    /**
     * Inserts MontoMinimoEnvioGratuito error to errors array
     *
     * @author Giovanny González <giovanny.gonzalez@nuvem.mx>
     * created 01/04/2021
     * @params
     * @return array
     *
     */
    MontoMinimoEnvioGratuitoErrors: function MontoMinimoEnvioGratuitoErrors() {
      var errors = [];
      if (!this.$v.editedItem.MontoMinimoEnvioGratuito.$dirty) return errors;
      !this.$v.editedItem.MontoMinimoEnvioGratuito.required && errors.push('Este campo es requerido');
      return errors;
    },

    /**
     *
     * Determines if submitStauts is pending
     * @author Giovanny González <giovanny.gonzalez@nuvem.mx>
     * created 01/04/2021
     * @params
     * @return boolean
     *
     */
    submitIsPending: function submitIsPending() {
      return this.submitStatus == 'PENDING';
    },

    /**
     *
     * Determines if submit has error
     * @author Giovanny González <giovanny.gonzalez@nuvem.mx>
     * created 01/04/2021
     * @params
     * @return boolean
     *
     */
    submitHasError: function submitHasError() {
      return this.submitStatus == 'ERROR';
    }
  },
  watch: {
    dialog: function dialog(val) {
      val || this.close();
    },
    dialogDelete: function dialogDelete(val) {
      val || this.closeDelete();
    }
  },
  methods: {
    /**
     * Gets shipping configurations list
     * @author Giovanny González <giovanny.gonzalez@nuvem.mx>
     * @date 01/04/2021
     * @params
     * @return void
     */
    getDataTableEntries: function getDataTableEntries() {
      var _this = this;

      this.loading = true;
      axios.get('/api/shipping-configurations').then(function (res) {
        _this.Id_CostoEnvioTipos = res.data.tiposCostosEnvio;
        _this.shippingConfigurations = res.data.shippingConfigurations;
      })["catch"](function (er) {
        console.warn(er.response);
      }).then(function () {
        return _this.loading = false;
      });
    },

    /**
     * Gets the status name.
     * @author Giovanny González <giovanny.gonzalez@nuvem.mx>
     * @date 01/04/2021
     * @param Object item
     * @return string
     */
    getStatusName: function getStatusName(item) {
      return item.status ? 'Activo' : 'Inactivo';
    },

    /**
     * Gets status color.
     * @author Giovanny González <giovanny.gonzalez@nuvem.mx>
     * @date 01/04/2021
     * @param Object item
     * @return String
     */
    getStatusColor: function getStatusColor(item) {
      return item.status ? 'success' : 'red';
    },

    /**
     * Closes form dialog
     *
     * @author Giovanny González <giovanny.gonzalez@nuvem.mx>
     * @date 01/04/2021
     * @params
     * @return void
     *
     */
    close: function close() {
      var _this2 = this;

      this.$v.$reset();
      this.dialog = false;
      this.submitStatus = null;
      this.$nextTick(function () {
        _this2.editedItem = Object.assign({}, _this2.defaultItem);
        _this2.editedIndex = -1;
      });
    },

    /**
     * Saves a new or an existent item
     *
     * @author Giovanny González <giovanny.gonzalez@nuvem.mx>
     * created 01/04/2021
     * @params
     * @return void
     *
     */
    save: function save() {
      this.$v.$touch();

      if (this.$v.$invalid) {
        this.submitStatus = 'ERROR';
        this.errorMessage = 'Es necesario corregir los campos con error.';
      } else {
        this.submitStatus = 'PENDING';

        if (this.editedIndex > -1) {
          this.saveEdited();
        } else {
          this.saveNew();
        }
      }
    },

    /**
     * Saves a new item
     *
     * @author Giovanny González <giovanny.gonzalez@nuvem.mx>
     * created 01/04/2021
     * @params
     * @return void
     *
     */
    saveNew: function saveNew() {
      var _this3 = this;

      axios.post('/api/shipping-configurations', this.editedItem).then(function (res) {
        _this3.submitStatus = null;
        _this3.editedItem.Id_CostoEnvio = res.data.model.Id_CostoEnvio;
        _this3.editedItem.status = 1;

        _this3.shippingConfigurations.push(_this3.editedItem);

        _this3.close();

        swal2.fire('Guardado', 'Se guardó el registro correctamente', 'success');
      })["catch"](this.handleError);
    },

    /**
     * Saves an existent item
     *
     * @author Giovanny González <giovanny.gonzalez@nuvem.mx>
     * created 01/04/2021
     * @params
     * @return void
     *
     */
    saveEdited: function saveEdited() {
      var _this4 = this;

      axios.put("/api/shipping-configurations/".concat(this.editedItem.Id_CostoEnvio), this.editedItem).then(function (res) {
        _this4.submitStatus = null;
        Object.assign(_this4.shippingConfigurations[_this4.editedIndex], _this4.editedItem);

        _this4.close();

        swal2.fire('Actualizado', 'Se actualizó el registro correctamente', 'success');
      })["catch"](this.handleError);
    },

    /**
     * opens edit modal to edit a model
     *
     * @author Giovanny González <giovanny.gonzalez@nuvem.mx>
     * created 01/04/2021
     * @params
     * @return void
     *
     */
    editItem: function editItem(item) {
      this.editedIndex = this.shippingConfigurations.indexOf(item);
      Object.assign(this.editedItem, this.shippingConfigurations[this.editedIndex]);
      this.dialog = true;
      this.editionBlocked = false;
    },

    /**
     * opens view modal to edit a model
     *
     * @author Giovanny González <giovanny.gonzalez@nuvem.mx>
     * created 01/04/2021
     * @params
     * @return void
     *
     */
    viewItem: function viewItem(item) {
      this.editedIndex = this.shippingConfigurations.indexOf(item);
      this.editedItem = Object.assign({}, item);
      this.dialog = true;
      this.editionBlocked = true;
    },

    /**
     * opens delete modal
     *
     * @author Giovanny González <giovanny.gonzalez@nuvem.mx>
     * created 01/04/2021
     * @params
     * @return void
     *
     */
    openDeleteDialog: function openDeleteDialog(item) {
      this.editedIndex = this.shippingConfigurations.indexOf(item);
      this.editedItem = Object.assign({}, item);
      this.dialogDelete = true;
    },

    /**
     * Closes delete modal
     *
     * @author Giovanny González <giovanny.gonzalez@nuvem.mx>
     * created 01/04/2021
     * @params
     * @return void
     *
     */
    closeDelete: function closeDelete() {
      var _this5 = this;

      this.dialogDelete = false;
      this.$nextTick(function () {
        _this5.editedItem = Object.assign({}, _this5.defaultItem);
        _this5.editedIndex = -1;
      });
    },

    /**
     * confirms item delete
     *
     * @author Giovanny González <giovanny.gonzalez@nuvem.mx>
     * created 01/04/2021
     * @params
     * @return void
     *
     */
    deleteItemConfirm: function deleteItemConfirm() {
      var _this6 = this;

      var url = "/api/shipping-configurations/".concat(this.editedItem.Id_CostoEnvio);
      axios["delete"](url).then(function (res) {
        swal2.fire('Eliminado', 'Se eliminó el registro correctamente', 'success');

        _this6.shippingConfigurations.splice(_this6.editedIndex, 1);

        _this6.closeDelete();
      })["catch"](function (er) {
        _this6.submitStatus = 'ERROR';
        _this6.errorMessage = 'Ocurrió un error. Intente en otro momento.';
      });
    },

    /**
     * Handles request error
     *
     * @author Giovanny González <giovanny.gonzalez@nuvem.mx>
     * created 01/04/2021
     * @params
     * @return void
     *
     */
    handleError: function handleError(er) {
      var res = er.response;
      this.submitStatus = 'ERROR';
      this.errorMessage = 'Ocurrió un error. Intente en otro momento.';
      if (res.status >= 500) return;
      this.errorMessage = res.data.message;

      for (var i in res.data.errors) {
        this.editedItem.isUnique = false;
      }
    }
  }
});

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/shipping_configurations/index.vue?vue&type=style&index=0&lang=css&":
/*!*****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/shipping_configurations/index.vue?vue&type=style&index=0&lang=css& ***!
  \*****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ ((module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../../node_modules/css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js");
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0__);
// Imports

var ___CSS_LOADER_EXPORT___ = _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0___default()(function(i){return i[1]});
// Module
___CSS_LOADER_EXPORT___.push([module.id, "\n.v-input--checkbox .v-icon.success--text {\n    background-color: #f8f9fa !important;\n    color: #A6C236 !important;\n}\n", ""]);
// Exports
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (___CSS_LOADER_EXPORT___);


/***/ }),

/***/ "./resources/js/views/shipping_configurations/index.vue":
/*!**************************************************************!*\
  !*** ./resources/js/views/shipping_configurations/index.vue ***!
  \**************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _index_vue_vue_type_template_id_f9fd90fc___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./index.vue?vue&type=template&id=f9fd90fc& */ "./resources/js/views/shipping_configurations/index.vue?vue&type=template&id=f9fd90fc&");
/* harmony import */ var _index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./index.vue?vue&type=script&lang=js& */ "./resources/js/views/shipping_configurations/index.vue?vue&type=script&lang=js&");
/* harmony import */ var _index_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./index.vue?vue&type=style&index=0&lang=css& */ "./resources/js/views/shipping_configurations/index.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! !../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");



;


/* normalize component */

var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__.default)(
  _index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__.default,
  _index_vue_vue_type_template_id_f9fd90fc___WEBPACK_IMPORTED_MODULE_0__.render,
  _index_vue_vue_type_template_id_f9fd90fc___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/shipping_configurations/index.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/js/views/shipping_configurations/index.vue?vue&type=script&lang=js&":
/*!***************************************************************************************!*\
  !*** ./resources/js/views/shipping_configurations/index.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./index.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/shipping_configurations/index.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__.default); 

/***/ }),

/***/ "./resources/js/views/shipping_configurations/index.vue?vue&type=template&id=f9fd90fc&":
/*!*********************************************************************************************!*\
  !*** ./resources/js/views/shipping_configurations/index.vue?vue&type=template&id=f9fd90fc& ***!
  \*********************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_f9fd90fc___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   "staticRenderFns": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_f9fd90fc___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_f9fd90fc___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./index.vue?vue&type=template&id=f9fd90fc& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/shipping_configurations/index.vue?vue&type=template&id=f9fd90fc&");


/***/ }),

/***/ "./resources/js/views/shipping_configurations/index.vue?vue&type=style&index=0&lang=css&":
/*!***********************************************************************************************!*\
  !*** ./resources/js/views/shipping_configurations/index.vue?vue&type=style&index=0&lang=css& ***!
  \***********************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_node_modules_css_loader_dist_cjs_js_clonedRuleSet_10_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_10_0_rules_0_use_2_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-style-loader/index.js!../../../../node_modules/css-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[1]!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[2]!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./index.vue?vue&type=style&index=0&lang=css& */ "./node_modules/vue-style-loader/index.js!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/shipping_configurations/index.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_vue_style_loader_index_js_node_modules_css_loader_dist_cjs_js_clonedRuleSet_10_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_10_0_rules_0_use_2_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_node_modules_css_loader_dist_cjs_js_clonedRuleSet_10_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_10_0_rules_0_use_2_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ var __WEBPACK_REEXPORT_OBJECT__ = {};
/* harmony reexport (unknown) */ for(const __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_node_modules_css_loader_dist_cjs_js_clonedRuleSet_10_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_10_0_rules_0_use_2_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== "default") __WEBPACK_REEXPORT_OBJECT__[__WEBPACK_IMPORT_KEY__] = () => _node_modules_vue_style_loader_index_js_node_modules_css_loader_dist_cjs_js_clonedRuleSet_10_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_10_0_rules_0_use_2_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__[__WEBPACK_IMPORT_KEY__]
/* harmony reexport (unknown) */ __webpack_require__.d(__webpack_exports__, __WEBPACK_REEXPORT_OBJECT__);


/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/shipping_configurations/index.vue?vue&type=template&id=f9fd90fc&":
/*!************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/shipping_configurations/index.vue?vue&type=template&id=f9fd90fc& ***!
  \************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render),
/* harmony export */   "staticRenderFns": () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { attrs: { "data-app": "" } },
    [
      _c(
        "v-dialog",
        {
          attrs: { width: "50%" },
          model: {
            value: _vm.dialog,
            callback: function($$v) {
              _vm.dialog = $$v
            },
            expression: "dialog"
          }
        },
        [
          _c(
            "v-card",
            { staticClass: "p-3" },
            [
              _c("v-card-title", [
                _c("span", { staticClass: "headline" }, [
                  _vm._v(_vm._s(_vm.formTitle))
                ])
              ]),
              _vm._v(" "),
              _c(
                "v-card-text",
                [
                  _vm.submitHasError
                    ? _c(
                        "v-alert",
                        {
                          attrs: {
                            dismissible: "",
                            elevation: "5",
                            text: "",
                            type: "error"
                          }
                        },
                        [
                          _vm._v(
                            "\n                    " +
                              _vm._s(_vm.errorMessage) +
                              "\n                "
                          )
                        ]
                      )
                    : _vm._e(),
                  _vm._v(" "),
                  _c(
                    "v-form",
                    {
                      ref: "form",
                      attrs: { "lazy-validation": "" },
                      model: {
                        value: _vm.valid,
                        callback: function($$v) {
                          _vm.valid = $$v
                        },
                        expression: "valid"
                      }
                    },
                    [
                      _c(
                        "v-row",
                        [
                          _c(
                            "v-col",
                            { staticClass: "py-0", attrs: { cols: "12" } },
                            [
                              _c("n-label", {
                                attrs: {
                                  text: "Selecciona como se calcula el envío",
                                  required: "",
                                  error:
                                    _vm.$v.editedItem.Id_CostoEnvioTipo.$error
                                }
                              }),
                              _vm._v(" "),
                              _c("v-select", {
                                attrs: {
                                  placeholder: "Seleccionar...",
                                  outlined: "",
                                  dense: "",
                                  items: _vm.Id_CostoEnvioTipos,
                                  "item-text": "name",
                                  "item-value": "value",
                                  required: "",
                                  readonly: _vm.editionBlocked,
                                  "error-messages": _vm.Id_CostoEnvioTipoErrors
                                },
                                on: {
                                  change: function($event) {
                                    return _vm.$v.editedItem.Id_CostoEnvioTipo.$touch()
                                  }
                                },
                                model: {
                                  value: _vm.editedItem.Id_CostoEnvioTipo,
                                  callback: function($$v) {
                                    _vm.$set(
                                      _vm.editedItem,
                                      "Id_CostoEnvioTipo",
                                      $$v
                                    )
                                  },
                                  expression: "editedItem.Id_CostoEnvioTipo"
                                }
                              })
                            ],
                            1
                          ),
                          _vm._v(" "),
                          _c(
                            "v-col",
                            { staticClass: "py-0", attrs: { cols: "12" } },
                            [
                              _c("n-label", {
                                attrs: {
                                  text: "Nombre de la opción de envío",
                                  required: "",
                                  error: _vm.$v.editedItem.Descripcion.$error
                                }
                              }),
                              _vm._v(" "),
                              _c("v-text-field", {
                                attrs: {
                                  outlined: "",
                                  dense: "",
                                  "error-messages": _vm.nameErrors,
                                  counter: 80,
                                  required: "",
                                  readonly: _vm.editionBlocked
                                },
                                on: {
                                  input: function($event) {
                                    return _vm.$v.editedItem.Descripcion.$touch()
                                  },
                                  blur: function($event) {
                                    return _vm.$v.editedItem.Descripcion.$touch()
                                  }
                                },
                                model: {
                                  value: _vm.editedItem.Descripcion,
                                  callback: function($$v) {
                                    _vm.$set(_vm.editedItem, "Descripcion", $$v)
                                  },
                                  expression: "editedItem.Descripcion"
                                }
                              })
                            ],
                            1
                          ),
                          _vm._v(" "),
                          _c(
                            "v-col",
                            { staticClass: "py-0", attrs: { cols: "12" } },
                            [
                              _c("n-label", {
                                attrs: {
                                  text: "Tiempo estimado de envío",
                                  required: "",
                                  error:
                                    _vm.$v.editedItem.TiempoEstimadoEnvio.$error
                                }
                              }),
                              _vm._v(" "),
                              _c("v-text-field", {
                                attrs: {
                                  outlined: "",
                                  dense: "",
                                  "error-messages":
                                    _vm.TiempoEstimadoEnvioErrors,
                                  counter: 10,
                                  required: "",
                                  readonly: _vm.editionBlocked
                                },
                                on: {
                                  input: function($event) {
                                    return _vm.$v.editedItem.TiempoEstimadoEnvio.$touch()
                                  },
                                  blur: function($event) {
                                    return _vm.$v.editedItem.TiempoEstimadoEnvio.$touch()
                                  }
                                },
                                model: {
                                  value: _vm.editedItem.TiempoEstimadoEnvio,
                                  callback: function($$v) {
                                    _vm.$set(
                                      _vm.editedItem,
                                      "TiempoEstimadoEnvio",
                                      $$v
                                    )
                                  },
                                  expression: "editedItem.TiempoEstimadoEnvio"
                                }
                              })
                            ],
                            1
                          ),
                          _vm._v(" "),
                          _c(
                            "v-col",
                            { staticClass: "py-0", attrs: { cols: "12" } },
                            [
                              _c("n-label", {
                                attrs: {
                                  text: "Tarifa",
                                  required: "",
                                  error: _vm.$v.editedItem.Tarifa.$error
                                }
                              }),
                              _vm._v(" "),
                              _c("v-text-field", {
                                directives: [
                                  {
                                    name: "mask",
                                    rawName: "v-mask",
                                    value: "####.##",
                                    expression: "'####.##'"
                                  }
                                ],
                                attrs: {
                                  outlined: "",
                                  dense: "",
                                  "error-messages": _vm.TarifaErrors,
                                  required: "",
                                  readonly: _vm.editionBlocked
                                },
                                on: {
                                  input: function($event) {
                                    return _vm.$v.editedItem.Tarifa.$touch()
                                  },
                                  blur: function($event) {
                                    return _vm.$v.editedItem.Tarifa.$touch()
                                  }
                                },
                                model: {
                                  value: _vm.editedItem.Tarifa,
                                  callback: function($$v) {
                                    _vm.$set(_vm.editedItem, "Tarifa", $$v)
                                  },
                                  expression: "editedItem.Tarifa"
                                }
                              })
                            ],
                            1
                          ),
                          _vm._v(" "),
                          _c(
                            "v-col",
                            { staticClass: "py-0", attrs: { cols: "12" } },
                            [
                              _c("v-checkbox", {
                                attrs: {
                                  label:
                                    "Ofrece una entrega gratuita cuando un cliente compra más de una ciera cantidad",
                                  color: "success",
                                  "false-value": "0",
                                  "true-value": "1",
                                  "hide-details": "",
                                  readonly: _vm.editionBlocked
                                },
                                model: {
                                  value: _vm.editedItem.AplicaEnvioGratuito,
                                  callback: function($$v) {
                                    _vm.$set(
                                      _vm.editedItem,
                                      "AplicaEnvioGratuito",
                                      $$v
                                    )
                                  },
                                  expression: "editedItem.AplicaEnvioGratuito"
                                }
                              })
                            ],
                            1
                          ),
                          _vm._v(" "),
                          _c(
                            "v-col",
                            {
                              staticClass: "py-0",
                              attrs: { cols: "12", lg: "6" }
                            },
                            [
                              _vm.editedItem.AplicaEnvioGratuito === "1"
                                ? _c("v-text-field", {
                                    directives: [
                                      {
                                        name: "mask",
                                        rawName: "v-mask",
                                        value: "#####.##",
                                        expression: "'#####.##'"
                                      }
                                    ],
                                    attrs: {
                                      outlined: "",
                                      dense: "",
                                      "error-messages":
                                        _vm.MontoMinimoEnvioGratuitoErrors,
                                      required: "",
                                      readonly: _vm.editionBlocked
                                    },
                                    on: {
                                      input: function($event) {
                                        return _vm.$v.editedItem.MontoMinimoEnvioGratuito.$touch()
                                      },
                                      blur: function($event) {
                                        return _vm.$v.editedItem.MontoMinimoEnvioGratuito.$touch()
                                      }
                                    },
                                    model: {
                                      value:
                                        _vm.editedItem.MontoMinimoEnvioGratuito,
                                      callback: function($$v) {
                                        _vm.$set(
                                          _vm.editedItem,
                                          "MontoMinimoEnvioGratuito",
                                          $$v
                                        )
                                      },
                                      expression:
                                        "editedItem.MontoMinimoEnvioGratuito"
                                    }
                                  })
                                : _vm._e()
                            ],
                            1
                          )
                        ],
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "v-card-actions",
                [
                  _c("form-action-buttons", {
                    attrs: {
                      hideSaveBtn: _vm.editionBlocked,
                      isLoading: _vm.submitIsPending
                    },
                    on: { close: _vm.close, save: _vm.save }
                  })
                ],
                1
              )
            ],
            1
          )
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "layout-wrapper",
        { attrs: { transition: "scroll-y-transition" } },
        [
          _c(
            "v-card",
            { staticClass: "p-3" },
            [
              _c("dialog-delete", {
                attrs: { status: _vm.dialogDelete },
                on: { cancel: _vm.closeDelete, confirm: _vm.deleteItemConfirm }
              }),
              _vm._v(" "),
              _c("v-data-table", {
                staticClass: "elevation-1",
                attrs: {
                  headers: _vm.headers,
                  items: _vm.shippingConfigurations,
                  "item-key": "id",
                  search: _vm.search,
                  page: _vm.page,
                  "items-per-page": _vm.itemsPerPage,
                  "hide-default-footer": "",
                  "no-data-text": "No hay ningún registro",
                  "no-results-text": "No se encontraron coincidencias",
                  loading: _vm.loading,
                  "loading-text": "Cargando... Porfavor espere"
                },
                on: {
                  "update:page": function($event) {
                    _vm.page = $event
                  },
                  "page-count": function($event) {
                    _vm.pageCount = $event
                  }
                },
                scopedSlots: _vm._u([
                  {
                    key: "top",
                    fn: function() {
                      return [
                        _c("datatable-header", {
                          attrs: {
                            title: "Configurador de envíos",
                            "disable-creation": !_vm.permissions.includes(
                              "ShippingConfiguration.create"
                            )
                          },
                          on: {
                            searching: function($event) {
                              _vm.search = $event
                            },
                            open: function($event) {
                              _vm.dialog = $event
                            }
                          }
                        })
                      ]
                    },
                    proxy: true
                  },
                  {
                    key: "item.TiempoEstimadoEnvio",
                    fn: function(ref) {
                      var item = ref.item
                      return [
                        _vm._v(
                          "\n                    " +
                            _vm._s(item.TiempoEstimadoEnvio + " días hábiles") +
                            "\n                "
                        )
                      ]
                    }
                  },
                  {
                    key: "item.Tarifa",
                    fn: function(ref) {
                      var item = ref.item
                      return [
                        _vm._v(
                          "\n                    $" +
                            _vm._s(Math.round(item.Tarifa).toFixed(2)) +
                            "\n                "
                        )
                      ]
                    }
                  },
                  {
                    key: "item.status",
                    fn: function(ref) {
                      var item = ref.item
                      return [_c("chip-status", { attrs: { item: item } })]
                    }
                  },
                  {
                    key: "item.actions",
                    fn: function(ref) {
                      var item = ref.item
                      return [
                        _c("action-buttons", {
                          attrs: {
                            item: item,
                            editDisabled: !_vm.permissions.includes(
                              "ShippingConfiguration.edit"
                            ),
                            deleteDisabled: !_vm.permissions.includes(
                              "ShippingConfiguration.delete"
                            )
                          },
                          on: {
                            edit: _vm.editItem,
                            view: _vm.viewItem,
                            delete: _vm.openDeleteDialog
                          }
                        })
                      ]
                    }
                  }
                ])
              }),
              _vm._v(" "),
              _c("pagination", {
                attrs: {
                  last: _vm.pageCount,
                  current: _vm.page,
                  itemsPerPage: _vm.itemsPerPage,
                  total: this.shippingConfigurations.length
                },
                on: {
                  input: function($event) {
                    _vm.page = $event
                  }
                }
              })
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-style-loader/index.js!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/shipping_configurations/index.vue?vue&type=style&index=0&lang=css&":
/*!**********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-style-loader/index.js!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/shipping_configurations/index.vue?vue&type=style&index=0&lang=css& ***!
  \**********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(/*! !!../../../../node_modules/css-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[1]!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[2]!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./index.vue?vue&type=style&index=0&lang=css& */ "./node_modules/css-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/shipping_configurations/index.vue?vue&type=style&index=0&lang=css&");
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.id, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var add = __webpack_require__(/*! !../../../../node_modules/vue-style-loader/lib/addStylesClient.js */ "./node_modules/vue-style-loader/lib/addStylesClient.js").default
var update = add("51d09958", content, false, {});
// Hot Module Replacement
if(false) {}

/***/ })

}]);