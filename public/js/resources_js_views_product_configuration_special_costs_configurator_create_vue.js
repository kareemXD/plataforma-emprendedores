(self["webpackChunk"] = self["webpackChunk"] || []).push([["resources_js_views_product_configuration_special_costs_configurator_create_vue"],{

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product_configuration/special_costs_configurator/create.vue?vue&type=script&lang=js&":
/*!*********************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product_configuration/special_costs_configurator/create.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _partials_Costs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./partials/Costs */ "./resources/js/views/product_configuration/special_costs_configurator/partials/Costs.vue");
/* harmony import */ var _partials_TotalCosts__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./partials/TotalCosts */ "./resources/js/views/product_configuration/special_costs_configurator/partials/TotalCosts.vue");
/* harmony import */ var _partials_Registration__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./partials/Registration */ "./resources/js/views/product_configuration/special_costs_configurator/partials/Registration.vue");
/* harmony import */ var _partials_AddProducts__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./partials/AddProducts */ "./resources/js/views/product_configuration/special_costs_configurator/partials/AddProducts.vue");
/* harmony import */ var _partials_SpecialCostTable__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./partials/SpecialCostTable */ "./resources/js/views/product_configuration/special_costs_configurator/partials/SpecialCostTable.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//





/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  components: {
    Costs: _partials_Costs__WEBPACK_IMPORTED_MODULE_0__.default,
    TotalCosts: _partials_TotalCosts__WEBPACK_IMPORTED_MODULE_1__.default,
    Registration: _partials_Registration__WEBPACK_IMPORTED_MODULE_2__.default,
    AddProducts: _partials_AddProducts__WEBPACK_IMPORTED_MODULE_3__.default,
    SpecialCostTable: _partials_SpecialCostTable__WEBPACK_IMPORTED_MODULE_4__.default
  },
  data: function data() {
    return {
      e1: 1,
      steps: 5,
      titles: ["General", "Clasificación", "Almacenamiento", "Estadística", "Ficha Técnica"],
      tab: null,
      tabSelected: "",
      change: false,
      tabsDisabled: false,
      specialCosts: [],
      data: {
        Descripcion: "",
        Id_ReglaCostoEspecial: null,
        reglas_especiales_productos: [],
        reglas_especiales_productos_detalles: []
      }
    };
  },
  methods: {
    /**
     * Assign registration data.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-03-31
     * @param
     * @return void
     */
    fillRegistrationData: function fillRegistrationData(value) {
      this.data.Descripcion = value[1].Descripcion;
      this.data.Id_ReglaCostoEspecial = value[1].Id_ReglaCostoEspecial;
      this.tab = value[0];
    },

    /**
     * Assign products data.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-03-31
     * @param
     * @return void
     */
    fillProducts: function fillProducts(value) {
      this.data.reglas_especiales_productos_detalles = value[1];
      this.tab = value[0];
    },

    /**
     * Assign previous tab.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-03-31
     * @param
     * @return void
     */
    changeTab: function changeTab(value) {
      this.tab = value;
    },

    /**
     * Assign costs data.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-03-31
     * @param
     * @return void
     */
    fillCosts: function fillCosts(value) {
      this.data.reglas_especiales_productos = value[1];
      this.specialCosts = value[1];
      this.tab = value[0];
    },

    /**
     * Post a newspecial cost data.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-04-01
     * @param
     * @return void
     */
    post: function post() {
      var _this = this;

      axios.post("/api/special-costs-configurators", this.data).then(function (result) {
        swal2.fire("Creado", "Se agregaron los datos correctamente", "success");
      })["catch"](function (error) {
        swal2.fire({
          icon: "error",
          title: "Oops...",
          text: "Algo salió mal, vuelve a intentarlo!"
        });
      })["finally"](function () {
        _this.$router.push({
          name: 'special-costs-configurators'
        });
      });
    }
  },
  watch: {
    steps: function steps(val) {
      if (this.e1 > val) {
        this.e1 = val;
      }
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product_configuration/special_costs_configurator/partials/AddProducts.vue?vue&type=script&lang=js&":
/*!***********************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product_configuration/special_costs_configurator/partials/AddProducts.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  name: "AddProducts",
  mounted: function mounted() {
    this.getSettings();
  },
  data: function data() {
    return {
      brands: [],
      families: [],
      specialties: [],
      categories: [],
      products: [],
      productsSelected: [],
      filters: {
        brand: 0,
        family: 0,
        especiality: 0,
        category: 0
      },
      search: "",
      dialog: false,
      productId: null,
      dialogDelete: false,
      editedIndex: -1,
      page: 1,
      pageCount: 0,
      totalItems: 0,
      itemsPerPage: 10,
      loading: false,
      headers: [{
        text: "ID PRODUCTO",
        value: "Id_Prod1"
      }, {
        text: "DESCRIPCION",
        value: "Descrip1"
      }]
    };
  },
  methods: {
    /**
     * Emmit the next number tab to move, and return the products selected.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-04-01
     * @param
     * @return void
     */
    nextTab: function nextTab() {
      this.$emit("changed", [2, this.productsSelected]);
    },

    /**
     * Emmit the previous number tab to move.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-04-01
     * @param
     * @return void
     */
    previousTab: function previousTab() {
      this.$emit("previous", 0);
    },

    /**
     * Get settings to fill selects in add products from api.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-04-01
     * @param
     * @return void
     */
    getSettings: function getSettings() {
      var _this = this;

      axios.get("/api/special-costs-settings").then(function (result) {
        _this.brands = result.data.brands;
        _this.families = result.data.families;
        _this.specialties = result.data.especialities;
        _this.categories = result.data.categories;
      })["catch"](function (error) {
        swal2.fire({
          icon: "error",
          title: "Oops...",
          text: "Algo salió mal, vuelve a intentarlo!"
        });
      });
    },

    /**
     * Get settings to fill selects in add products from api.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-04-01
     * @param
     * @return void
     */
    filterProducts: function filterProducts() {
      var _this2 = this;

      this.loading = true;
      axios.get("/api/special-costs-products", {
        params: {
          filters: this.filters
        }
      }).then(function (result) {
        _this2.products = result.data.products;
      })["catch"](function (error) {
        swal2.fire({
          icon: "error",
          title: "Oops...",
          text: "Algo salió mal, vuelve a intentarlo!"
        });
      })["finally"](function () {
        _this2.loading = false;
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product_configuration/special_costs_configurator/partials/Costs.vue?vue&type=script&lang=js&":
/*!*****************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product_configuration/special_costs_configurator/partials/Costs.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var vuelidate__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vuelidate */ "./node_modules/vuelidate/lib/index.js");
/* harmony import */ var vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! vuelidate/lib/validators */ "./node_modules/vuelidate/lib/validators/index.js");
/* harmony import */ var _NewCost__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./NewCost */ "./resources/js/views/product_configuration/special_costs_configurator/partials/NewCost.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  name: "Costs",
  components: {
    NewCost: _NewCost__WEBPACK_IMPORTED_MODULE_0__.default
  },
  mounted: function mounted() {},
  mixins: [vuelidate__WEBPACK_IMPORTED_MODULE_1__.validationMixin],
  validations: {
    currentGlobalCost: {
      Descripcion: {
        required: vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_2__.required
      },
      Tipo: {
        required: vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_2__.required
      },
      Monto: {
        required: vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_2__.required
      }
    }
  },
  data: function data() {
    return {
      submitStatus: "",
      showBases: false,
      showPercentage: false,
      children: [{
        name: "NewCost",
        id: 1
      }],
      newCosts: [],
      costs: [1],
      currentGlobalCost: {
        Descripcion: "",
        Tipo: "",
        Porcentaje: null,
        Monto: null,
        Id_BaseCostoCalculado: null
      }
    };
  },
  methods: {
    /**
     * Call to submit method from NewCost component when next button is pressed.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-04-01
     * @param
     * @return void
     */
    nextTab: function nextTab() {
      for (var i = 0; i < this.costs.length; i++) {
        this.$refs.getCost[i].submit(i + 1);
      }
    },

    /**
     * Emmit the previous number tab to move.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-04-01
     * @param
     * @return void
     */
    previousTab: function previousTab() {
      this.$emit("previous", 1);
    },

    /**
     * Add a new cost to the array and return the same array to the create component.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-04-01
     * @param value number
     * @return void
     */
    addNewCost: function addNewCost(value) {
      if (value[2] == true) {
        if (value[1] != false) {
          this.$emit("changed", [3, this.newCosts]);
          this.newCosts.push(value[0]);
          this.newCosts = [];
        }
      } else {
        if (value[1] != false) {
          this.newCosts.push(value[0]);
        }
      }
    },

    /**
     * Add a new NewCost component form.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-04-05
     * @param
     * @return void
     */
    addCost: function addCost() {
      this.costs.push(1);
    }
  },
  computed: {
    /**
     * Evaluate if an input is valid.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-03-31
     * @param
     * @return array
     */
    descriptionErrors: function descriptionErrors() {
      var errors = [];
      if (!this.$v.currentGlobalCost.Descripcion.$dirty) return errors;
      !this.$v.currentGlobalCost.Descripcion.required && errors.push("El campo es requerido");
      return errors;
    },

    /**
     * Evaluate if an input is valid.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-03-31
     * @param
     * @return array
     */
    typeErrors: function typeErrors() {
      var errors = [];
      if (!this.$v.currentGlobalCost.Tipo.$dirty) return errors;
      !this.$v.currentGlobalCost.Tipo.required && errors.push("El campo es requerido");
      return errors;
    },

    /**
     * Evaluate if an input is valid.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-03-31
     * @param
     * @return array
     */
    amountErrors: function amountErrors() {
      var errors = [];
      if (!this.$v.currentGlobalCost.Monto.$dirty) return errors;
      !this.$v.currentGlobalCost.Monto.required && errors.push("El campo es requerido");
      return errors;
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product_configuration/special_costs_configurator/partials/NewCost.vue?vue&type=script&lang=js&":
/*!*******************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product_configuration/special_costs_configurator/partials/NewCost.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var vuelidate__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vuelidate */ "./node_modules/vuelidate/lib/index.js");
/* harmony import */ var vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vuelidate/lib/validators */ "./node_modules/vuelidate/lib/validators/index.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  name: "NewCost",
  props: ["costsLength"],
  mounted: function mounted() {
    this.getGlobalCosts();
  },
  mixins: [vuelidate__WEBPACK_IMPORTED_MODULE_0__.validationMixin],
  validations: {
    currentSpecialCost: {
      description: {
        required: vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_1__.required
      },
      type: {
        required: vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_1__.required
      },
      amount: {
        required: vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_1__.required
      }
    }
  },
  data: function data() {
    return {
      submitStatus: "",
      showBases: false,
      showPercentage: false,
      globalCosts: [],
      bases: [],
      date: new Date().toISOString().substr(0, 10),
      menu1: false,
      menu2: false,
      currentSpecialCost: {
        description: "",
        type: "",
        amount: null,
        cost: '',
        costName: "",
        showGlobalCosts: false,
        showDates: false,
        startDate: '',
        endDate: '',
        idBaseCostoCalculado: null
      }
    };
  },
  methods: {
    /**
     * Emmit a new cost to the Costs component.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-03-31
     * @param item number
     * @return void
     */
    submit: function submit(item) {
      var _this = this;

      this.$v.$touch();

      if (this.$v.$invalid) {
        this.submitStatus = "ERROR";
        this.$emit("changed", [this.currentSpecialCost, false]);
      } else if (!this.$v.$invalid) {
        this.returnCost(item);
      } else {
        this.submitStatus = "PENDING";
        setTimeout(function () {
          _this.submitStatus = "OK";
        }, 500);
      }
    },

    /**
     * Get a global costs from api.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-03-31
     * @param
     * @return void
     */
    getGlobalCosts: function getGlobalCosts() {
      var _this2 = this;

      this.loading = true;
      axios.get("/api/global-costs-configurators").then(function (result) {
        _this2.globalCosts = result.data.globalCosts;
        _this2.bases = result.data.bases;
      })["catch"](function (error) {
        swal2.fire({
          icon: "error",
          title: "Oops...",
          text: "Algo salió mal, vuelve a intentarlo!"
        });
      })["finally"](function () {
        _this2.loading = false;
      });
    },

    /**
     * Enables and disables inputs depending on the type.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-03-31
     * @param
     * @return void
     */
    enableGlobalCosts: function enableGlobalCosts() {
      this.currentSpecialCost.showGlobalCosts ? !this.currentSpecialCost.showGlobalCosts : this.currentSpecialCost.showGlobalCosts;
    },

    /**
     * Enables and disables inputs depending on the type.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-03-31
     * @param
     * @return void
     */
    enableDates: function enableDates() {
      this.currentSpecialCost.showDates ? !this.currentSpecialCost.showDates : this.currentSpecialCost.showDates;
    },

    /**
     * Enables and disables inputs depending on the type.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-03-31
     * @param
     * @return void
     */
    checkInputs: function checkInputs() {
      switch (this.currentSpecialCost.type) {
        case 'P':
          this.showBases = false;
          this.showPercentage = true;
          break;

        case 'M':
          this.showBases = false;
          this.showPercentage = false;
          break;

        case 'C':
          this.showBases = true;
          this.showPercentage = false;
          break;
      }
    },

    /**
     * Assigns a name to the current special cost and returns the current cost.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-03-31
     * @param item number
     * @return void
     */
    returnCost: function returnCost(item) {
      var _this3 = this;

      var cost = this.globalCosts.find(function (element) {
        return element.Id_ConceptoCosto === _this3.currentSpecialCost.cost;
      });
      if (cost != null) this.currentSpecialCost.costName = cost.Descripcion;
      item == this.costsLength ? this.$emit("changed", [this.currentSpecialCost, true, true]) : this.$emit("changed", [this.currentSpecialCost, true, false]);
    }
  },
  computed: {
    /**
     * Evaluate if an input is valid.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-03-31
     * @param
     * @return array
     */
    descriptionErrors: function descriptionErrors() {
      var errors = [];
      if (!this.$v.currentSpecialCost.description.$dirty) return errors;
      !this.$v.currentSpecialCost.description.required && errors.push("El campo es requerido");
      return errors;
    },

    /**
     * Evaluate if an input is valid.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-03-31
     * @param
     * @return array
     */
    typeErrors: function typeErrors() {
      var errors = [];
      if (!this.$v.currentSpecialCost.type.$dirty) return errors;
      !this.$v.currentSpecialCost.type.required && errors.push("El campo es requerido");
      return errors;
    },

    /**
     * Evaluate if an input is valid.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-03-31
     * @param
     * @return array
     */
    amountErrors: function amountErrors() {
      var errors = [];
      if (!this.$v.currentSpecialCost.amount.$dirty) return errors;
      !this.$v.currentSpecialCost.amount.required && errors.push("El campo es requerido");
      return errors;
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product_configuration/special_costs_configurator/partials/Registration.vue?vue&type=script&lang=js&":
/*!************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product_configuration/special_costs_configurator/partials/Registration.vue?vue&type=script&lang=js& ***!
  \************************************************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var vuelidate__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vuelidate */ "./node_modules/vuelidate/lib/index.js");
/* harmony import */ var vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vuelidate/lib/validators */ "./node_modules/vuelidate/lib/validators/index.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  name: "Registration",
  mixins: [vuelidate__WEBPACK_IMPORTED_MODULE_0__.validationMixin],
  validations: {
    currentRegistration: {
      Descripcion: {
        required: vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_1__.required
      },
      Id_ReglaCostoEspecial: {
        required: vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_1__.required
      }
    }
  },
  mounted: function mounted() {
    this.getRules();
  },
  data: function data() {
    return {
      rules: [],
      currentRegistration: {
        Descripcion: "",
        Id_ReglaCostoEspecial: null
      }
    };
  },
  methods: {
    /**
     * Call nextTab method if data is valid.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-03-31
     * @param
     * @return void
     */
    submit: function submit() {
      var _this = this;

      this.$v.$touch();

      if (this.$v.$invalid) {
        this.submitStatus = "ERROR";
      } else if (!this.$v.$invalid) {
        this.nextTab();
      } else {
        this.submitStatus = "PENDING";
        setTimeout(function () {
          _this.submitStatus = "OK";
        }, 500);
      }
    },

    /**
     * Emmit the next tab number and the current registration data.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-03-31
     * @param
     * @return void
     */
    nextTab: function nextTab() {
      this.$emit("changed", [1, this.currentRegistration]);
    },

    /**
     * Get rules and fill date table from api rules-configurators.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-03-25
     * @param
     * @return void
     */
    getRules: function getRules() {
      var _this2 = this;

      this.loading = true;
      axios.get("/api/rules-configurators").then(function (result) {
        _this2.rules = result.data.rules;
      })["catch"](function (error) {
        swal2.fire({
          icon: "error",
          title: "Oops...",
          text: "Algo salió mal, vuelve a intentarlo!"
        });
      })["finally"](function () {
        _this2.loading = false;
      });
    }
  },
  computed: {
    /**
     * Evaluate if an input is valid.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-03-31
     * @param
     * @return array
     */
    descriptionErrors: function descriptionErrors() {
      var errors = [];
      if (!this.$v.currentRegistration.Descripcion.$dirty) return errors;
      !this.$v.currentRegistration.Descripcion.required && errors.push("El campo es requerido");
      return errors;
    },

    /**
     * Evaluate if an input is valid.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-03-31
     * @param
     * @return array
     */
    specialRuleErrors: function specialRuleErrors() {
      var errors = [];
      if (!this.$v.currentRegistration.Id_ReglaCostoEspecial.$dirty) return errors;
      !this.$v.currentRegistration.Id_ReglaCostoEspecial.required && errors.push("El campo es requerido");
      return errors;
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product_configuration/special_costs_configurator/partials/SpecialCostTable.vue?vue&type=script&lang=js&":
/*!****************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product_configuration/special_costs_configurator/partials/SpecialCostTable.vue?vue&type=script&lang=js& ***!
  \****************************************************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  name: "SpecialCostTable",
  props: ["specialCosts"],
  mounted: function mounted() {//this.getGlobalCosts();
  },
  components: {},
  data: function data() {
    return {
      search: "",
      dialog: false,
      productId: null,
      dialogDelete: false,
      editedIndex: -1,
      editedItem: {},
      page: 1,
      pageCount: 0,
      totalItems: 0,
      itemsPerPage: 10,
      loading: false,
      globalCosts: [],
      headers: [{
        text: "Descripción",
        value: "description"
      }, {
        text: "Tipo",
        value: "type"
      }, {
        text: "Cantidad",
        value: "amount"
      }, {
        text: "Aplica de manera global",
        value: "showGlobalCosts"
      }, {
        text: "Tipo de costo",
        value: "costName"
      }, {
        text: "Vigencia",
        value: "endDate"
      }, {
        text: "Acciones",
        value: "actions"
      }]
    };
  },
  methods: {
    /**
     * Emmit the next number tab to move.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-04-01
     * @param
     * @return void
     */
    nextTab: function nextTab() {
      this.$emit("previous", 4);
    },

    /**
     * Emmit the previous number tab to move.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-04-01
     * @param
     * @return void
     */
    previousTab: function previousTab() {
      this.$emit("previous", 2);
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product_configuration/special_costs_configurator/partials/TotalCosts.vue?vue&type=script&lang=js&":
/*!**********************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product_configuration/special_costs_configurator/partials/TotalCosts.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  name: "SpecialCostTable",
  props: ["products"],
  components: {},
  data: function data() {
    return {
      search: "",
      dialog: false,
      productId: null,
      dialogDelete: false,
      editedIndex: -1,
      editedItem: {},
      page: 1,
      pageCount: 0,
      totalItems: 0,
      itemsPerPage: 10,
      loading: false,
      globalCosts: [],
      headers: [{
        text: "Id Producto",
        value: "Id_Prod1"
      }, {
        text: "Descripción",
        value: "Descrip1"
      }]
    };
  },
  methods: {
    /**
     * Emmit a boolean value to do submit (post) to the parent component.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-04-01
     * @param
     * @return void
     */
    submit: function submit() {
      this.$emit("changed", true);
    },

    /**
     * Redirect to special cost index component.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-04-01
     * @param
     * @return void
     */
    cancel: function cancel() {
      this.$router.push({
        name: 'special-costs-configurators'
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product_configuration/special_costs_configurator/create.vue?vue&type=style&index=0&lang=css&":
/*!*******************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product_configuration/special_costs_configurator/create.vue?vue&type=style&index=0&lang=css& ***!
  \*******************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ ((module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../../../node_modules/css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js");
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0__);
// Imports

var ___CSS_LOADER_EXPORT___ = _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0___default()(function(i){return i[1]});
// Module
___CSS_LOADER_EXPORT___.push([module.id, "\n.v-tabs__bar {display: none !important}\r\n", ""]);
// Exports
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (___CSS_LOADER_EXPORT___);


/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product_configuration/special_costs_configurator/partials/SpecialCostTable.vue?vue&type=style&index=0&lang=css&":
/*!**************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product_configuration/special_costs_configurator/partials/SpecialCostTable.vue?vue&type=style&index=0&lang=css& ***!
  \**************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ ((module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../../../../node_modules/css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js");
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0__);
// Imports

var ___CSS_LOADER_EXPORT___ = _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0___default()(function(i){return i[1]});
// Module
___CSS_LOADER_EXPORT___.push([module.id, "\n.data-table-style{\n    margin: 10px;\n}\n", ""]);
// Exports
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (___CSS_LOADER_EXPORT___);


/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product_configuration/special_costs_configurator/partials/TotalCosts.vue?vue&type=style&index=0&lang=css&":
/*!********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product_configuration/special_costs_configurator/partials/TotalCosts.vue?vue&type=style&index=0&lang=css& ***!
  \********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ ((module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../../../../node_modules/css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js");
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0__);
// Imports

var ___CSS_LOADER_EXPORT___ = _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0___default()(function(i){return i[1]});
// Module
___CSS_LOADER_EXPORT___.push([module.id, "\n.data-table-style{\n    margin: 10px;\n}\n", ""]);
// Exports
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (___CSS_LOADER_EXPORT___);


/***/ }),

/***/ "./node_modules/style-loader/dist/cjs.js!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product_configuration/special_costs_configurator/create.vue?vue&type=style&index=0&lang=css&":
/*!***********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader/dist/cjs.js!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product_configuration/special_costs_configurator/create.vue?vue&type=style&index=0&lang=css& ***!
  \***********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !../../../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ "./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js");
/* harmony import */ var _node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _node_modules_css_loader_dist_cjs_js_clonedRuleSet_10_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_10_0_rules_0_use_2_node_modules_vue_loader_lib_index_js_vue_loader_options_create_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! !!../../../../../node_modules/css-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[1]!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[2]!../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./create.vue?vue&type=style&index=0&lang=css& */ "./node_modules/css-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product_configuration/special_costs_configurator/create.vue?vue&type=style&index=0&lang=css&");

            

var options = {};

options.insert = "head";
options.singleton = false;

var update = _node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0___default()(_node_modules_css_loader_dist_cjs_js_clonedRuleSet_10_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_10_0_rules_0_use_2_node_modules_vue_loader_lib_index_js_vue_loader_options_create_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_1__.default, options);



/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_css_loader_dist_cjs_js_clonedRuleSet_10_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_10_0_rules_0_use_2_node_modules_vue_loader_lib_index_js_vue_loader_options_create_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_1__.default.locals || {});

/***/ }),

/***/ "./node_modules/style-loader/dist/cjs.js!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product_configuration/special_costs_configurator/partials/SpecialCostTable.vue?vue&type=style&index=0&lang=css&":
/*!******************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader/dist/cjs.js!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product_configuration/special_costs_configurator/partials/SpecialCostTable.vue?vue&type=style&index=0&lang=css& ***!
  \******************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !../../../../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ "./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js");
/* harmony import */ var _node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _node_modules_css_loader_dist_cjs_js_clonedRuleSet_10_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_10_0_rules_0_use_2_node_modules_vue_loader_lib_index_js_vue_loader_options_SpecialCostTable_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! !!../../../../../../node_modules/css-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[1]!../../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../../node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[2]!../../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./SpecialCostTable.vue?vue&type=style&index=0&lang=css& */ "./node_modules/css-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product_configuration/special_costs_configurator/partials/SpecialCostTable.vue?vue&type=style&index=0&lang=css&");

            

var options = {};

options.insert = "head";
options.singleton = false;

var update = _node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0___default()(_node_modules_css_loader_dist_cjs_js_clonedRuleSet_10_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_10_0_rules_0_use_2_node_modules_vue_loader_lib_index_js_vue_loader_options_SpecialCostTable_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_1__.default, options);



/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_css_loader_dist_cjs_js_clonedRuleSet_10_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_10_0_rules_0_use_2_node_modules_vue_loader_lib_index_js_vue_loader_options_SpecialCostTable_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_1__.default.locals || {});

/***/ }),

/***/ "./node_modules/style-loader/dist/cjs.js!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product_configuration/special_costs_configurator/partials/TotalCosts.vue?vue&type=style&index=0&lang=css&":
/*!************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader/dist/cjs.js!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product_configuration/special_costs_configurator/partials/TotalCosts.vue?vue&type=style&index=0&lang=css& ***!
  \************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !../../../../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ "./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js");
/* harmony import */ var _node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _node_modules_css_loader_dist_cjs_js_clonedRuleSet_10_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_10_0_rules_0_use_2_node_modules_vue_loader_lib_index_js_vue_loader_options_TotalCosts_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! !!../../../../../../node_modules/css-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[1]!../../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../../node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[2]!../../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./TotalCosts.vue?vue&type=style&index=0&lang=css& */ "./node_modules/css-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product_configuration/special_costs_configurator/partials/TotalCosts.vue?vue&type=style&index=0&lang=css&");

            

var options = {};

options.insert = "head";
options.singleton = false;

var update = _node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0___default()(_node_modules_css_loader_dist_cjs_js_clonedRuleSet_10_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_10_0_rules_0_use_2_node_modules_vue_loader_lib_index_js_vue_loader_options_TotalCosts_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_1__.default, options);



/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_css_loader_dist_cjs_js_clonedRuleSet_10_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_10_0_rules_0_use_2_node_modules_vue_loader_lib_index_js_vue_loader_options_TotalCosts_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_1__.default.locals || {});

/***/ }),

/***/ "./resources/js/views/product_configuration/special_costs_configurator/create.vue":
/*!****************************************************************************************!*\
  !*** ./resources/js/views/product_configuration/special_costs_configurator/create.vue ***!
  \****************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _create_vue_vue_type_template_id_1e7deabf___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./create.vue?vue&type=template&id=1e7deabf& */ "./resources/js/views/product_configuration/special_costs_configurator/create.vue?vue&type=template&id=1e7deabf&");
/* harmony import */ var _create_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./create.vue?vue&type=script&lang=js& */ "./resources/js/views/product_configuration/special_costs_configurator/create.vue?vue&type=script&lang=js&");
/* harmony import */ var _create_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./create.vue?vue&type=style&index=0&lang=css& */ "./resources/js/views/product_configuration/special_costs_configurator/create.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! !../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");



;


/* normalize component */

var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__.default)(
  _create_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__.default,
  _create_vue_vue_type_template_id_1e7deabf___WEBPACK_IMPORTED_MODULE_0__.render,
  _create_vue_vue_type_template_id_1e7deabf___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/product_configuration/special_costs_configurator/create.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/js/views/product_configuration/special_costs_configurator/partials/AddProducts.vue":
/*!******************************************************************************************************!*\
  !*** ./resources/js/views/product_configuration/special_costs_configurator/partials/AddProducts.vue ***!
  \******************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _AddProducts_vue_vue_type_template_id_d0ebcbea___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./AddProducts.vue?vue&type=template&id=d0ebcbea& */ "./resources/js/views/product_configuration/special_costs_configurator/partials/AddProducts.vue?vue&type=template&id=d0ebcbea&");
/* harmony import */ var _AddProducts_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./AddProducts.vue?vue&type=script&lang=js& */ "./resources/js/views/product_configuration/special_costs_configurator/partials/AddProducts.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! !../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */
;
var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__.default)(
  _AddProducts_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__.default,
  _AddProducts_vue_vue_type_template_id_d0ebcbea___WEBPACK_IMPORTED_MODULE_0__.render,
  _AddProducts_vue_vue_type_template_id_d0ebcbea___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/product_configuration/special_costs_configurator/partials/AddProducts.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/js/views/product_configuration/special_costs_configurator/partials/Costs.vue":
/*!************************************************************************************************!*\
  !*** ./resources/js/views/product_configuration/special_costs_configurator/partials/Costs.vue ***!
  \************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _Costs_vue_vue_type_template_id_0ad9a12c___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Costs.vue?vue&type=template&id=0ad9a12c& */ "./resources/js/views/product_configuration/special_costs_configurator/partials/Costs.vue?vue&type=template&id=0ad9a12c&");
/* harmony import */ var _Costs_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Costs.vue?vue&type=script&lang=js& */ "./resources/js/views/product_configuration/special_costs_configurator/partials/Costs.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! !../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */
;
var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__.default)(
  _Costs_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__.default,
  _Costs_vue_vue_type_template_id_0ad9a12c___WEBPACK_IMPORTED_MODULE_0__.render,
  _Costs_vue_vue_type_template_id_0ad9a12c___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/product_configuration/special_costs_configurator/partials/Costs.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/js/views/product_configuration/special_costs_configurator/partials/NewCost.vue":
/*!**************************************************************************************************!*\
  !*** ./resources/js/views/product_configuration/special_costs_configurator/partials/NewCost.vue ***!
  \**************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _NewCost_vue_vue_type_template_id_b266851a___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./NewCost.vue?vue&type=template&id=b266851a& */ "./resources/js/views/product_configuration/special_costs_configurator/partials/NewCost.vue?vue&type=template&id=b266851a&");
/* harmony import */ var _NewCost_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./NewCost.vue?vue&type=script&lang=js& */ "./resources/js/views/product_configuration/special_costs_configurator/partials/NewCost.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! !../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */
;
var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__.default)(
  _NewCost_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__.default,
  _NewCost_vue_vue_type_template_id_b266851a___WEBPACK_IMPORTED_MODULE_0__.render,
  _NewCost_vue_vue_type_template_id_b266851a___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/product_configuration/special_costs_configurator/partials/NewCost.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/js/views/product_configuration/special_costs_configurator/partials/Registration.vue":
/*!*******************************************************************************************************!*\
  !*** ./resources/js/views/product_configuration/special_costs_configurator/partials/Registration.vue ***!
  \*******************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _Registration_vue_vue_type_template_id_1bc1a87a___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Registration.vue?vue&type=template&id=1bc1a87a& */ "./resources/js/views/product_configuration/special_costs_configurator/partials/Registration.vue?vue&type=template&id=1bc1a87a&");
/* harmony import */ var _Registration_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Registration.vue?vue&type=script&lang=js& */ "./resources/js/views/product_configuration/special_costs_configurator/partials/Registration.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! !../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */
;
var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__.default)(
  _Registration_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__.default,
  _Registration_vue_vue_type_template_id_1bc1a87a___WEBPACK_IMPORTED_MODULE_0__.render,
  _Registration_vue_vue_type_template_id_1bc1a87a___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/product_configuration/special_costs_configurator/partials/Registration.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/js/views/product_configuration/special_costs_configurator/partials/SpecialCostTable.vue":
/*!***********************************************************************************************************!*\
  !*** ./resources/js/views/product_configuration/special_costs_configurator/partials/SpecialCostTable.vue ***!
  \***********************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _SpecialCostTable_vue_vue_type_template_id_a7b2a45c___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./SpecialCostTable.vue?vue&type=template&id=a7b2a45c& */ "./resources/js/views/product_configuration/special_costs_configurator/partials/SpecialCostTable.vue?vue&type=template&id=a7b2a45c&");
/* harmony import */ var _SpecialCostTable_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./SpecialCostTable.vue?vue&type=script&lang=js& */ "./resources/js/views/product_configuration/special_costs_configurator/partials/SpecialCostTable.vue?vue&type=script&lang=js&");
/* harmony import */ var _SpecialCostTable_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./SpecialCostTable.vue?vue&type=style&index=0&lang=css& */ "./resources/js/views/product_configuration/special_costs_configurator/partials/SpecialCostTable.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! !../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");



;


/* normalize component */

var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__.default)(
  _SpecialCostTable_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__.default,
  _SpecialCostTable_vue_vue_type_template_id_a7b2a45c___WEBPACK_IMPORTED_MODULE_0__.render,
  _SpecialCostTable_vue_vue_type_template_id_a7b2a45c___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/product_configuration/special_costs_configurator/partials/SpecialCostTable.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/js/views/product_configuration/special_costs_configurator/partials/TotalCosts.vue":
/*!*****************************************************************************************************!*\
  !*** ./resources/js/views/product_configuration/special_costs_configurator/partials/TotalCosts.vue ***!
  \*****************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _TotalCosts_vue_vue_type_template_id_3135446c___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./TotalCosts.vue?vue&type=template&id=3135446c& */ "./resources/js/views/product_configuration/special_costs_configurator/partials/TotalCosts.vue?vue&type=template&id=3135446c&");
/* harmony import */ var _TotalCosts_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./TotalCosts.vue?vue&type=script&lang=js& */ "./resources/js/views/product_configuration/special_costs_configurator/partials/TotalCosts.vue?vue&type=script&lang=js&");
/* harmony import */ var _TotalCosts_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./TotalCosts.vue?vue&type=style&index=0&lang=css& */ "./resources/js/views/product_configuration/special_costs_configurator/partials/TotalCosts.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! !../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");



;


/* normalize component */

var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__.default)(
  _TotalCosts_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__.default,
  _TotalCosts_vue_vue_type_template_id_3135446c___WEBPACK_IMPORTED_MODULE_0__.render,
  _TotalCosts_vue_vue_type_template_id_3135446c___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/product_configuration/special_costs_configurator/partials/TotalCosts.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/js/views/product_configuration/special_costs_configurator/create.vue?vue&type=script&lang=js&":
/*!*****************************************************************************************************************!*\
  !*** ./resources/js/views/product_configuration/special_costs_configurator/create.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_create_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./create.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product_configuration/special_costs_configurator/create.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_create_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__.default); 

/***/ }),

/***/ "./resources/js/views/product_configuration/special_costs_configurator/partials/AddProducts.vue?vue&type=script&lang=js&":
/*!*******************************************************************************************************************************!*\
  !*** ./resources/js/views/product_configuration/special_costs_configurator/partials/AddProducts.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_AddProducts_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./AddProducts.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product_configuration/special_costs_configurator/partials/AddProducts.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_AddProducts_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__.default); 

/***/ }),

/***/ "./resources/js/views/product_configuration/special_costs_configurator/partials/Costs.vue?vue&type=script&lang=js&":
/*!*************************************************************************************************************************!*\
  !*** ./resources/js/views/product_configuration/special_costs_configurator/partials/Costs.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Costs_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./Costs.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product_configuration/special_costs_configurator/partials/Costs.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Costs_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__.default); 

/***/ }),

/***/ "./resources/js/views/product_configuration/special_costs_configurator/partials/NewCost.vue?vue&type=script&lang=js&":
/*!***************************************************************************************************************************!*\
  !*** ./resources/js/views/product_configuration/special_costs_configurator/partials/NewCost.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_NewCost_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./NewCost.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product_configuration/special_costs_configurator/partials/NewCost.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_NewCost_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__.default); 

/***/ }),

/***/ "./resources/js/views/product_configuration/special_costs_configurator/partials/Registration.vue?vue&type=script&lang=js&":
/*!********************************************************************************************************************************!*\
  !*** ./resources/js/views/product_configuration/special_costs_configurator/partials/Registration.vue?vue&type=script&lang=js& ***!
  \********************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Registration_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./Registration.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product_configuration/special_costs_configurator/partials/Registration.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Registration_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__.default); 

/***/ }),

/***/ "./resources/js/views/product_configuration/special_costs_configurator/partials/SpecialCostTable.vue?vue&type=script&lang=js&":
/*!************************************************************************************************************************************!*\
  !*** ./resources/js/views/product_configuration/special_costs_configurator/partials/SpecialCostTable.vue?vue&type=script&lang=js& ***!
  \************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_SpecialCostTable_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./SpecialCostTable.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product_configuration/special_costs_configurator/partials/SpecialCostTable.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_SpecialCostTable_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__.default); 

/***/ }),

/***/ "./resources/js/views/product_configuration/special_costs_configurator/partials/TotalCosts.vue?vue&type=script&lang=js&":
/*!******************************************************************************************************************************!*\
  !*** ./resources/js/views/product_configuration/special_costs_configurator/partials/TotalCosts.vue?vue&type=script&lang=js& ***!
  \******************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_TotalCosts_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./TotalCosts.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product_configuration/special_costs_configurator/partials/TotalCosts.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_TotalCosts_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__.default); 

/***/ }),

/***/ "./resources/js/views/product_configuration/special_costs_configurator/create.vue?vue&type=style&index=0&lang=css&":
/*!*************************************************************************************************************************!*\
  !*** ./resources/js/views/product_configuration/special_costs_configurator/create.vue?vue&type=style&index=0&lang=css& ***!
  \*************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_dist_cjs_js_node_modules_css_loader_dist_cjs_js_clonedRuleSet_10_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_10_0_rules_0_use_2_node_modules_vue_loader_lib_index_js_vue_loader_options_create_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/style-loader/dist/cjs.js!../../../../../node_modules/css-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[1]!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[2]!../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./create.vue?vue&type=style&index=0&lang=css& */ "./node_modules/style-loader/dist/cjs.js!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product_configuration/special_costs_configurator/create.vue?vue&type=style&index=0&lang=css&");


/***/ }),

/***/ "./resources/js/views/product_configuration/special_costs_configurator/partials/SpecialCostTable.vue?vue&type=style&index=0&lang=css&":
/*!********************************************************************************************************************************************!*\
  !*** ./resources/js/views/product_configuration/special_costs_configurator/partials/SpecialCostTable.vue?vue&type=style&index=0&lang=css& ***!
  \********************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_dist_cjs_js_node_modules_css_loader_dist_cjs_js_clonedRuleSet_10_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_10_0_rules_0_use_2_node_modules_vue_loader_lib_index_js_vue_loader_options_SpecialCostTable_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/style-loader/dist/cjs.js!../../../../../../node_modules/css-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[1]!../../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../../node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[2]!../../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./SpecialCostTable.vue?vue&type=style&index=0&lang=css& */ "./node_modules/style-loader/dist/cjs.js!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product_configuration/special_costs_configurator/partials/SpecialCostTable.vue?vue&type=style&index=0&lang=css&");


/***/ }),

/***/ "./resources/js/views/product_configuration/special_costs_configurator/partials/TotalCosts.vue?vue&type=style&index=0&lang=css&":
/*!**************************************************************************************************************************************!*\
  !*** ./resources/js/views/product_configuration/special_costs_configurator/partials/TotalCosts.vue?vue&type=style&index=0&lang=css& ***!
  \**************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_dist_cjs_js_node_modules_css_loader_dist_cjs_js_clonedRuleSet_10_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_10_0_rules_0_use_2_node_modules_vue_loader_lib_index_js_vue_loader_options_TotalCosts_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/style-loader/dist/cjs.js!../../../../../../node_modules/css-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[1]!../../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../../node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[2]!../../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./TotalCosts.vue?vue&type=style&index=0&lang=css& */ "./node_modules/style-loader/dist/cjs.js!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product_configuration/special_costs_configurator/partials/TotalCosts.vue?vue&type=style&index=0&lang=css&");


/***/ }),

/***/ "./resources/js/views/product_configuration/special_costs_configurator/create.vue?vue&type=template&id=1e7deabf&":
/*!***********************************************************************************************************************!*\
  !*** ./resources/js/views/product_configuration/special_costs_configurator/create.vue?vue&type=template&id=1e7deabf& ***!
  \***********************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_create_vue_vue_type_template_id_1e7deabf___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   "staticRenderFns": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_create_vue_vue_type_template_id_1e7deabf___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_create_vue_vue_type_template_id_1e7deabf___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./create.vue?vue&type=template&id=1e7deabf& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product_configuration/special_costs_configurator/create.vue?vue&type=template&id=1e7deabf&");


/***/ }),

/***/ "./resources/js/views/product_configuration/special_costs_configurator/partials/AddProducts.vue?vue&type=template&id=d0ebcbea&":
/*!*************************************************************************************************************************************!*\
  !*** ./resources/js/views/product_configuration/special_costs_configurator/partials/AddProducts.vue?vue&type=template&id=d0ebcbea& ***!
  \*************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_AddProducts_vue_vue_type_template_id_d0ebcbea___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   "staticRenderFns": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_AddProducts_vue_vue_type_template_id_d0ebcbea___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_AddProducts_vue_vue_type_template_id_d0ebcbea___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./AddProducts.vue?vue&type=template&id=d0ebcbea& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product_configuration/special_costs_configurator/partials/AddProducts.vue?vue&type=template&id=d0ebcbea&");


/***/ }),

/***/ "./resources/js/views/product_configuration/special_costs_configurator/partials/Costs.vue?vue&type=template&id=0ad9a12c&":
/*!*******************************************************************************************************************************!*\
  !*** ./resources/js/views/product_configuration/special_costs_configurator/partials/Costs.vue?vue&type=template&id=0ad9a12c& ***!
  \*******************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Costs_vue_vue_type_template_id_0ad9a12c___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   "staticRenderFns": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Costs_vue_vue_type_template_id_0ad9a12c___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Costs_vue_vue_type_template_id_0ad9a12c___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./Costs.vue?vue&type=template&id=0ad9a12c& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product_configuration/special_costs_configurator/partials/Costs.vue?vue&type=template&id=0ad9a12c&");


/***/ }),

/***/ "./resources/js/views/product_configuration/special_costs_configurator/partials/NewCost.vue?vue&type=template&id=b266851a&":
/*!*********************************************************************************************************************************!*\
  !*** ./resources/js/views/product_configuration/special_costs_configurator/partials/NewCost.vue?vue&type=template&id=b266851a& ***!
  \*********************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_NewCost_vue_vue_type_template_id_b266851a___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   "staticRenderFns": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_NewCost_vue_vue_type_template_id_b266851a___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_NewCost_vue_vue_type_template_id_b266851a___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./NewCost.vue?vue&type=template&id=b266851a& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product_configuration/special_costs_configurator/partials/NewCost.vue?vue&type=template&id=b266851a&");


/***/ }),

/***/ "./resources/js/views/product_configuration/special_costs_configurator/partials/Registration.vue?vue&type=template&id=1bc1a87a&":
/*!**************************************************************************************************************************************!*\
  !*** ./resources/js/views/product_configuration/special_costs_configurator/partials/Registration.vue?vue&type=template&id=1bc1a87a& ***!
  \**************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Registration_vue_vue_type_template_id_1bc1a87a___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   "staticRenderFns": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Registration_vue_vue_type_template_id_1bc1a87a___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Registration_vue_vue_type_template_id_1bc1a87a___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./Registration.vue?vue&type=template&id=1bc1a87a& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product_configuration/special_costs_configurator/partials/Registration.vue?vue&type=template&id=1bc1a87a&");


/***/ }),

/***/ "./resources/js/views/product_configuration/special_costs_configurator/partials/SpecialCostTable.vue?vue&type=template&id=a7b2a45c&":
/*!******************************************************************************************************************************************!*\
  !*** ./resources/js/views/product_configuration/special_costs_configurator/partials/SpecialCostTable.vue?vue&type=template&id=a7b2a45c& ***!
  \******************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_SpecialCostTable_vue_vue_type_template_id_a7b2a45c___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   "staticRenderFns": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_SpecialCostTable_vue_vue_type_template_id_a7b2a45c___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_SpecialCostTable_vue_vue_type_template_id_a7b2a45c___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./SpecialCostTable.vue?vue&type=template&id=a7b2a45c& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product_configuration/special_costs_configurator/partials/SpecialCostTable.vue?vue&type=template&id=a7b2a45c&");


/***/ }),

/***/ "./resources/js/views/product_configuration/special_costs_configurator/partials/TotalCosts.vue?vue&type=template&id=3135446c&":
/*!************************************************************************************************************************************!*\
  !*** ./resources/js/views/product_configuration/special_costs_configurator/partials/TotalCosts.vue?vue&type=template&id=3135446c& ***!
  \************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_TotalCosts_vue_vue_type_template_id_3135446c___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   "staticRenderFns": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_TotalCosts_vue_vue_type_template_id_3135446c___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_TotalCosts_vue_vue_type_template_id_3135446c___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./TotalCosts.vue?vue&type=template&id=3135446c& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product_configuration/special_costs_configurator/partials/TotalCosts.vue?vue&type=template&id=3135446c&");


/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product_configuration/special_costs_configurator/create.vue?vue&type=template&id=1e7deabf&":
/*!**************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product_configuration/special_costs_configurator/create.vue?vue&type=template&id=1e7deabf& ***!
  \**************************************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render),
/* harmony export */   "staticRenderFns": () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { attrs: { "data-app": "" } },
    [
      _c(
        "v-card",
        [
          _c(
            "v-card-text",
            { staticClass: "mb-2", staticStyle: { padding: "0px" } },
            [
              _c(
                "v-tabs",
                {
                  staticClass: "v-tabs__bar",
                  attrs: {
                    "fixed-tabs": "",
                    "background-color": "#e3e6e8",
                    "slider-color": "#018085",
                    color: "#fff",
                    "slider-size": "2"
                  },
                  model: {
                    value: _vm.tab,
                    callback: function($$v) {
                      _vm.tab = $$v
                    },
                    expression: "tab"
                  }
                },
                [
                  _c("v-tab", [_vm._v(" General ")]),
                  _vm._v(" "),
                  _c("v-tab", { attrs: { disabled: _vm.tabsDisabled } }, [
                    _vm._v(" Clasificación ")
                  ]),
                  _vm._v(" "),
                  _c("v-tab", { attrs: { disabled: _vm.tabsDisabled } }, [
                    _vm._v(" Almacenamiento ")
                  ]),
                  _vm._v(" "),
                  _c("v-tab", { attrs: { disabled: _vm.tabsDisabled } }, [
                    _vm._v(" Estadística ")
                  ]),
                  _vm._v(" "),
                  _c("v-tab", { attrs: { disabled: _vm.tabsDisabled } }, [
                    _vm._v(" Ficha Técnica ")
                  ])
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "v-tabs-items",
                {
                  model: {
                    value: _vm.tab,
                    callback: function($$v) {
                      _vm.tab = $$v
                    },
                    expression: "tab"
                  }
                },
                [
                  _c(
                    "v-tab-item",
                    { staticClass: "mt-2" },
                    [
                      _c(
                        "v-card",
                        { attrs: { flat: "" } },
                        [
                          _c("Registration", {
                            on: { changed: _vm.fillRegistrationData }
                          })
                        ],
                        1
                      )
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "v-tab-item",
                    { staticClass: "mt-2" },
                    [
                      _c(
                        "v-card",
                        { attrs: { flat: "" } },
                        [
                          _c("AddProducts", {
                            on: {
                              changed: _vm.fillProducts,
                              previous: _vm.changeTab
                            }
                          })
                        ],
                        1
                      )
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "v-tab-item",
                    { staticClass: "mt-2" },
                    [
                      _c(
                        "v-card",
                        { attrs: { flat: "" } },
                        [
                          _c("Costs", {
                            on: {
                              changed: _vm.fillCosts,
                              previous: _vm.changeTab
                            }
                          })
                        ],
                        1
                      )
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "v-tab-item",
                    { staticClass: "mt-2" },
                    [
                      _c(
                        "v-card",
                        { attrs: { flat: "" } },
                        [
                          _c("SpecialCostTable", {
                            attrs: { specialCosts: _vm.specialCosts },
                            on: {
                              changed: _vm.fillCosts,
                              previous: _vm.changeTab
                            }
                          })
                        ],
                        1
                      )
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "v-tab-item",
                    { staticClass: "mt-2" },
                    [
                      _c(
                        "v-card",
                        { attrs: { flat: "" } },
                        [
                          _c("TotalCosts", {
                            attrs: {
                              products:
                                _vm.data.reglas_especiales_productos_detalles
                            },
                            on: { changed: _vm.post }
                          })
                        ],
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product_configuration/special_costs_configurator/partials/AddProducts.vue?vue&type=template&id=d0ebcbea&":
/*!****************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product_configuration/special_costs_configurator/partials/AddProducts.vue?vue&type=template&id=d0ebcbea& ***!
  \****************************************************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render),
/* harmony export */   "staticRenderFns": () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "v-card",
    { staticClass: "p-3" },
    [
      _c("v-card-title", { staticClass: "text-center" }, [
        _c("b", [_vm._v("Agregar Productos")])
      ]),
      _vm._v(" "),
      _c(
        "v-card-text",
        [
          _c(
            "v-row",
            [
              _c(
                "v-col",
                { attrs: { cols: "6", md: "2" } },
                [
                  _c("h6", [_vm._v("Marca")]),
                  _vm._v(" "),
                  _c("v-select", {
                    attrs: {
                      placeholder: "Marca",
                      items: _vm.brands,
                      "item-text": "Marca",
                      "item-value": "Id_Marca",
                      outlined: "",
                      dense: ""
                    },
                    model: {
                      value: _vm.filters.brand,
                      callback: function($$v) {
                        _vm.$set(_vm.filters, "brand", $$v)
                      },
                      expression: "filters.brand"
                    }
                  })
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "v-col",
                { attrs: { cols: "6", md: "2" } },
                [
                  _c("h6", [_vm._v("Familia")]),
                  _vm._v(" "),
                  _c("v-select", {
                    attrs: {
                      placeholder: "Familia",
                      items: _vm.families,
                      "item-text": "Familia",
                      "item-value": "Id_Familia",
                      outlined: "",
                      dense: ""
                    },
                    model: {
                      value: _vm.filters.family,
                      callback: function($$v) {
                        _vm.$set(_vm.filters, "family", $$v)
                      },
                      expression: "filters.family"
                    }
                  })
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "v-col",
                { attrs: { cols: "6", md: "2" } },
                [
                  _c("h6", [_vm._v("Especialidad")]),
                  _vm._v(" "),
                  _c("v-select", {
                    attrs: {
                      placeholder: "Especialidad",
                      items: _vm.specialties,
                      "item-text": "Especialidad",
                      "item-value": "Id_EspMed",
                      outlined: "",
                      dense: ""
                    },
                    model: {
                      value: _vm.filters.especiality,
                      callback: function($$v) {
                        _vm.$set(_vm.filters, "especiality", $$v)
                      },
                      expression: "filters.especiality"
                    }
                  })
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "v-col",
                { attrs: { cols: "6", md: "2" } },
                [
                  _c("h6", [_vm._v("Categoría")]),
                  _vm._v(" "),
                  _c("v-select", {
                    attrs: {
                      placeholder: "Categoría",
                      items: _vm.categories,
                      "item-text": "Categoria",
                      "item-value": "Id_Categoria",
                      outlined: "",
                      dense: ""
                    },
                    model: {
                      value: _vm.filters.category,
                      callback: function($$v) {
                        _vm.$set(_vm.filters, "category", $$v)
                      },
                      expression: "filters.category"
                    }
                  })
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "v-col",
                {
                  staticStyle: { "align-self": "center" },
                  attrs: { cols: "6", md: "2" }
                },
                [
                  _c(
                    "v-btn",
                    {
                      attrs: { block: "", color: "primary", text: "" },
                      on: { click: _vm.filterProducts }
                    },
                    [_vm._v("\n                    Buscar\n                ")]
                  )
                ],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _c("v-data-table", {
            staticClass: "elevation-1",
            attrs: {
              headers: _vm.headers,
              items: _vm.products,
              search: _vm.search,
              page: _vm.page,
              "items-per-page": _vm.itemsPerPage,
              loading: _vm.loading,
              "item-key": "Id_Prod1",
              "show-select": "",
              "hide-default-footer": "",
              "no-data-text": "No hay ningún registro",
              "no-results-text": "No se encontraron coincidencias",
              "loading-text": "Cargando... Porfavor espere"
            },
            on: {
              "update:page": function($event) {
                _vm.page = $event
              },
              "page-count": function($event) {
                _vm.pageCount = $event
              }
            },
            model: {
              value: _vm.productsSelected,
              callback: function($$v) {
                _vm.productsSelected = $$v
              },
              expression: "productsSelected"
            }
          }),
          _vm._v(" "),
          _c("pagination", {
            attrs: {
              last: _vm.pageCount,
              current: _vm.page,
              itemsPerPage: _vm.itemsPerPage,
              total: this.products.length
            },
            on: {
              input: function($event) {
                _vm.page = $event
              }
            }
          }),
          _vm._v(" "),
          _c(
            "v-row",
            { staticStyle: { "justify-content": "flex-end" } },
            [
              _c(
                "v-col",
                { attrs: { cols: "6", md: "2" } },
                [
                  _c(
                    "v-btn",
                    {
                      attrs: { block: "", color: "primary", text: "" },
                      on: { click: _vm.previousTab }
                    },
                    [
                      _c("v-icon", { attrs: { left: "" } }, [
                        _vm._v(
                          "\n                      mdi-arrow-left\n                    "
                        )
                      ]),
                      _vm._v(
                        "\n                    Anterior \n                "
                      )
                    ],
                    1
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "v-col",
                { attrs: { cols: "6", md: "2" } },
                [
                  _c(
                    "v-btn",
                    {
                      attrs: { block: "", color: "primary", text: "" },
                      on: { click: _vm.nextTab }
                    },
                    [
                      _vm._v(
                        "\n                    Siguiente \n                    "
                      ),
                      _c("v-icon", { attrs: { left: "" } }, [
                        _vm._v(
                          "\n                      mdi-arrow-right\n                    "
                        )
                      ])
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product_configuration/special_costs_configurator/partials/Costs.vue?vue&type=template&id=0ad9a12c&":
/*!**********************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product_configuration/special_costs_configurator/partials/Costs.vue?vue&type=template&id=0ad9a12c& ***!
  \**********************************************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render),
/* harmony export */   "staticRenderFns": () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "v-card",
    { staticClass: "p-3" },
    [
      _c("v-card-title", [_c("b", [_vm._v("Alta de Costo Especial")])]),
      _vm._v(" "),
      _vm._l(_vm.costs, function(item, index) {
        return _c(
          "div",
          { key: index },
          [
            _c("NewCost", {
              ref: "getCost",
              refInFor: true,
              attrs: { costsLength: _vm.costs.length },
              on: { changed: _vm.addNewCost }
            })
          ],
          1
        )
      }),
      _vm._v(" "),
      _c(
        "v-row",
        { staticStyle: { "justify-content": "flex-end" } },
        [
          _c(
            "v-col",
            { attrs: { cols: "6", md: "2" } },
            [
              _c(
                "v-btn",
                {
                  attrs: { block: "", color: "primary", text: "" },
                  on: { click: _vm.previousTab }
                },
                [
                  _c("v-icon", { attrs: { left: "" } }, [
                    _vm._v(" mdi-arrow-left ")
                  ]),
                  _vm._v("\n        Anterior\n      ")
                ],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "v-col",
            { attrs: { cols: "6", md: "5" } },
            [
              _c(
                "v-btn",
                {
                  attrs: { block: "", color: "primary", text: "" },
                  on: { click: _vm.addCost }
                },
                [
                  _c("v-icon", { attrs: { left: "" } }, [_vm._v(" mdi-plus ")]),
                  _vm._v("\n        Agregar nuevo costo especial\n      ")
                ],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "v-col",
            { attrs: { cols: "6", md: "2" } },
            [
              _c(
                "v-btn",
                {
                  attrs: { block: "", color: "primary", text: "" },
                  on: { click: _vm.nextTab }
                },
                [
                  _vm._v("\n        Siguiente\n        "),
                  _c("v-icon", { attrs: { left: "" } }, [
                    _vm._v(" mdi-arrow-right ")
                  ])
                ],
                1
              )
            ],
            1
          )
        ],
        1
      )
    ],
    2
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product_configuration/special_costs_configurator/partials/NewCost.vue?vue&type=template&id=b266851a&":
/*!************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product_configuration/special_costs_configurator/partials/NewCost.vue?vue&type=template&id=b266851a& ***!
  \************************************************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render),
/* harmony export */   "staticRenderFns": () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("v-card-text", [
    _c(
      "form",
      { attrs: { "data-app": "" } },
      [
        _c(
          "v-row",
          [
            _c(
              "v-col",
              { attrs: { cols: "6", md: "12" } },
              [
                _c("h6", [_vm._v("Descripción")]),
                _vm._v(" "),
                _c("v-text-field", {
                  attrs: {
                    "error-messages": _vm.descriptionErrors,
                    placeholder: "Descripción",
                    outlined: "",
                    required: "",
                    dense: ""
                  },
                  on: {
                    input: function($event) {
                      return _vm.$v.currentSpecialCost.description.$touch()
                    },
                    blur: function($event) {
                      return _vm.$v.currentSpecialCost.description.$touch()
                    }
                  },
                  model: {
                    value: _vm.currentSpecialCost.description,
                    callback: function($$v) {
                      _vm.$set(_vm.currentSpecialCost, "description", $$v)
                    },
                    expression: "currentSpecialCost.description"
                  }
                })
              ],
              1
            )
          ],
          1
        ),
        _vm._v(" "),
        _c(
          "v-row",
          [
            _c(
              "v-radio-group",
              {
                attrs: { "error-messages": _vm.typeErrors, row: "" },
                on: {
                  change: _vm.checkInputs,
                  input: function($event) {
                    return _vm.$v.currentSpecialCost.type.$touch()
                  },
                  blur: function($event) {
                    return _vm.$v.currentSpecialCost.type.$touch()
                  }
                },
                model: {
                  value: _vm.currentSpecialCost.type,
                  callback: function($$v) {
                    _vm.$set(_vm.currentSpecialCost, "type", $$v)
                  },
                  expression: "currentSpecialCost.type"
                }
              },
              [
                _c(
                  "v-col",
                  {
                    staticStyle: { "margin-right": "40px" },
                    attrs: { cols: "6", md: "3" }
                  },
                  [
                    _c("h6", [_vm._v("Tipo")]),
                    _vm._v(" "),
                    _c("v-radio", {
                      attrs: {
                        label: "Porcentaje",
                        color: "#018085",
                        value: "P"
                      }
                    })
                  ],
                  1
                ),
                _vm._v(" "),
                _c(
                  "v-col",
                  {
                    staticStyle: { "padding-top": "40px" },
                    attrs: { cols: "6", md: "3" }
                  },
                  [
                    _c("v-radio", {
                      attrs: { label: "Monto", color: "#018085", value: "M" }
                    })
                  ],
                  1
                ),
                _vm._v(" "),
                _c(
                  "v-col",
                  {
                    staticStyle: { "padding-top": "40px" },
                    attrs: { cols: "6", md: "3" }
                  },
                  [
                    _c("v-radio", {
                      attrs: {
                        label: "Calculado",
                        color: "#018085",
                        value: "C"
                      }
                    })
                  ],
                  1
                )
              ],
              1
            ),
            _vm._v(" "),
            _c(
              "v-col",
              {
                directives: [
                  {
                    name: "show",
                    rawName: "v-show",
                    value: _vm.showBases,
                    expression: "showBases"
                  }
                ],
                staticStyle: { "padding-top": "50px" },
                attrs: { cols: "6", md: "3" }
              },
              [
                _c("v-select", {
                  attrs: {
                    items: _vm.bases,
                    "item-text": "Descripcion",
                    "item-value": "Id_BaseCostoCalculado",
                    placeholder: "Base de cálculo",
                    outlined: "",
                    required: "",
                    dense: ""
                  },
                  model: {
                    value: _vm.currentSpecialCost.idBaseCostoCalculado,
                    callback: function($$v) {
                      _vm.$set(
                        _vm.currentSpecialCost,
                        "idBaseCostoCalculado",
                        $$v
                      )
                    },
                    expression: "currentSpecialCost.idBaseCostoCalculado"
                  }
                })
              ],
              1
            )
          ],
          1
        ),
        _vm._v(" "),
        _c(
          "v-row",
          [
            _c(
              "v-col",
              { attrs: { cols: "6", md: "2" } },
              [
                _c(
                  "h6",
                  {
                    directives: [
                      {
                        name: "show",
                        rawName: "v-show",
                        value: !_vm.showPercentage,
                        expression: "!showPercentage"
                      }
                    ]
                  },
                  [_vm._v("Cantidad")]
                ),
                _vm._v(" "),
                _c(
                  "h6",
                  {
                    directives: [
                      {
                        name: "show",
                        rawName: "v-show",
                        value: _vm.showPercentage,
                        expression: "showPercentage"
                      }
                    ]
                  },
                  [_vm._v("Porcentaje")]
                ),
                _vm._v(" "),
                _c("v-text-field", {
                  attrs: {
                    "error-messages": _vm.amountErrors,
                    placeholder: "0.00",
                    type: "number",
                    min: "0",
                    step: "0.01",
                    outlined: "",
                    required: "",
                    dense: ""
                  },
                  on: {
                    input: function($event) {
                      return _vm.$v.currentSpecialCost.amount.$touch()
                    },
                    blur: function($event) {
                      return _vm.$v.currentSpecialCost.amount.$touch()
                    }
                  },
                  model: {
                    value: _vm.currentSpecialCost.amount,
                    callback: function($$v) {
                      _vm.$set(_vm.currentSpecialCost, "amount", $$v)
                    },
                    expression: "currentSpecialCost.amount"
                  }
                })
              ],
              1
            )
          ],
          1
        ),
        _vm._v(" "),
        _c(
          "v-row",
          { attrs: { "no-gutters": "" } },
          [
            _c(
              "v-col",
              { attrs: { cols: "6", md: "3" } },
              [
                _c("v-checkbox", {
                  staticClass: "checkbox-style",
                  attrs: {
                    label: "¿Aplica de manera global?",
                    color: "#A6C236"
                  },
                  on: { change: _vm.enableGlobalCosts },
                  model: {
                    value: _vm.currentSpecialCost.showGlobalCosts,
                    callback: function($$v) {
                      _vm.$set(_vm.currentSpecialCost, "showGlobalCosts", $$v)
                    },
                    expression: "currentSpecialCost.showGlobalCosts"
                  }
                }),
                _vm._v(" "),
                _c(
                  "div",
                  {
                    directives: [
                      {
                        name: "show",
                        rawName: "v-show",
                        value: _vm.currentSpecialCost.showGlobalCosts,
                        expression: "currentSpecialCost.showGlobalCosts"
                      }
                    ]
                  },
                  [
                    _c("h6", [_vm._v("Tipo de costo")]),
                    _vm._v(" "),
                    _c("v-select", {
                      attrs: {
                        items: _vm.globalCosts,
                        "item-text": "Descripcion",
                        "item-value": "Id_ConceptoCosto",
                        placeholder: "Marca",
                        outlined: "",
                        dense: ""
                      },
                      model: {
                        value: _vm.currentSpecialCost.cost,
                        callback: function($$v) {
                          _vm.$set(_vm.currentSpecialCost, "cost", $$v)
                        },
                        expression: "currentSpecialCost.cost"
                      }
                    })
                  ],
                  1
                )
              ],
              1
            ),
            _vm._v(" "),
            _c(
              "v-col",
              { staticClass: "pl-3", attrs: { cols: "6", md: "3" } },
              [
                _c("v-checkbox", {
                  staticClass: "checkbox-style",
                  attrs: { label: "Tiene vigencia", color: "#A6C236" },
                  on: { change: _vm.enableDates },
                  model: {
                    value: _vm.currentSpecialCost.showDates,
                    callback: function($$v) {
                      _vm.$set(_vm.currentSpecialCost, "showDates", $$v)
                    },
                    expression: "currentSpecialCost.showDates"
                  }
                }),
                _vm._v(" "),
                _c(
                  "div",
                  {
                    directives: [
                      {
                        name: "show",
                        rawName: "v-show",
                        value: _vm.currentSpecialCost.showDates,
                        expression: "currentSpecialCost.showDates"
                      }
                    ]
                  },
                  [
                    _c("h6", [_vm._v("Fecha inicio")]),
                    _vm._v(" "),
                    _c(
                      "v-menu",
                      {
                        attrs: {
                          "close-on-content-click": false,
                          "nudge-right": 40,
                          transition: "scale-transition",
                          "offset-y": "",
                          "min-width": "auto"
                        },
                        scopedSlots: _vm._u([
                          {
                            key: "activator",
                            fn: function(ref) {
                              var on = ref.on
                              var attrs = ref.attrs
                              return [
                                _c(
                                  "v-text-field",
                                  _vm._g(
                                    _vm._b(
                                      {
                                        attrs: {
                                          placeholder: "dd/mm/aaaa",
                                          outlined: "",
                                          dense: "",
                                          readonly: ""
                                        },
                                        model: {
                                          value:
                                            _vm.currentSpecialCost.startDate,
                                          callback: function($$v) {
                                            _vm.$set(
                                              _vm.currentSpecialCost,
                                              "startDate",
                                              $$v
                                            )
                                          },
                                          expression:
                                            "currentSpecialCost.startDate"
                                        }
                                      },
                                      "v-text-field",
                                      attrs,
                                      false
                                    ),
                                    on
                                  )
                                )
                              ]
                            }
                          }
                        ]),
                        model: {
                          value: _vm.menu1,
                          callback: function($$v) {
                            _vm.menu1 = $$v
                          },
                          expression: "menu1"
                        }
                      },
                      [
                        _vm._v(" "),
                        _c("v-date-picker", {
                          attrs: {
                            color: "#018085",
                            "header-color": "#018085"
                          },
                          on: {
                            input: function($event) {
                              _vm.menu1 = false
                            }
                          },
                          model: {
                            value: _vm.currentSpecialCost.startDate,
                            callback: function($$v) {
                              _vm.$set(_vm.currentSpecialCost, "startDate", $$v)
                            },
                            expression: "currentSpecialCost.startDate"
                          }
                        })
                      ],
                      1
                    ),
                    _vm._v(" "),
                    _c("h6", [_vm._v("Fecha fin")]),
                    _vm._v(" "),
                    _c(
                      "v-menu",
                      {
                        attrs: {
                          "close-on-content-click": false,
                          "nudge-right": 40,
                          transition: "scale-transition",
                          "offset-y": "",
                          "min-width": "auto"
                        },
                        scopedSlots: _vm._u([
                          {
                            key: "activator",
                            fn: function(ref) {
                              var on = ref.on
                              var attrs = ref.attrs
                              return [
                                _c(
                                  "v-text-field",
                                  _vm._g(
                                    _vm._b(
                                      {
                                        attrs: {
                                          placeholder: "dd/mm/aaaa",
                                          outlined: "",
                                          dense: "",
                                          readonly: ""
                                        },
                                        model: {
                                          value: _vm.currentSpecialCost.endDate,
                                          callback: function($$v) {
                                            _vm.$set(
                                              _vm.currentSpecialCost,
                                              "endDate",
                                              $$v
                                            )
                                          },
                                          expression:
                                            "currentSpecialCost.endDate"
                                        }
                                      },
                                      "v-text-field",
                                      attrs,
                                      false
                                    ),
                                    on
                                  )
                                )
                              ]
                            }
                          }
                        ]),
                        model: {
                          value: _vm.menu2,
                          callback: function($$v) {
                            _vm.menu2 = $$v
                          },
                          expression: "menu2"
                        }
                      },
                      [
                        _vm._v(" "),
                        _c("v-date-picker", {
                          attrs: {
                            color: "#018085",
                            "header-color": "#018085"
                          },
                          on: {
                            input: function($event) {
                              _vm.menu2 = false
                            }
                          },
                          model: {
                            value: _vm.currentSpecialCost.endDate,
                            callback: function($$v) {
                              _vm.$set(_vm.currentSpecialCost, "endDate", $$v)
                            },
                            expression: "currentSpecialCost.endDate"
                          }
                        })
                      ],
                      1
                    )
                  ],
                  1
                )
              ],
              1
            )
          ],
          1
        )
      ],
      1
    )
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product_configuration/special_costs_configurator/partials/Registration.vue?vue&type=template&id=1bc1a87a&":
/*!*****************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product_configuration/special_costs_configurator/partials/Registration.vue?vue&type=template&id=1bc1a87a& ***!
  \*****************************************************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render),
/* harmony export */   "staticRenderFns": () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "v-card",
    { staticClass: "p-3" },
    [
      _c("v-card-title", { staticClass: "text-center" }, [
        _c("b", [_vm._v("Alta de Costo Especial")])
      ]),
      _vm._v(" "),
      _c(
        "v-card-text",
        [
          _c(
            "v-row",
            [
              _c(
                "v-col",
                { attrs: { cols: "6", md: "12" } },
                [
                  _c("h6", { staticClass: "mt-2" }, [_vm._v("Nombre")]),
                  _vm._v(" "),
                  _c("v-text-field", {
                    attrs: {
                      "error-messages": _vm.descriptionErrors,
                      placeholder: "Nombre",
                      outlined: "",
                      dense: ""
                    },
                    on: {
                      input: function($event) {
                        return _vm.$v.currentRegistration.Descripcion.$touch()
                      },
                      blur: function($event) {
                        return _vm.$v.currentRegistration.Descripcion.$touch()
                      }
                    },
                    model: {
                      value: _vm.currentRegistration.Descripcion,
                      callback: function($$v) {
                        _vm.$set(_vm.currentRegistration, "Descripcion", $$v)
                      },
                      expression: "currentRegistration.Descripcion"
                    }
                  })
                ],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "v-row",
            [
              _c(
                "v-col",
                { attrs: { cols: "6", md: "12" } },
                [
                  _c("h6", [_vm._v("Tipo de Regla")]),
                  _vm._v(" "),
                  _c("v-autocomplete", {
                    attrs: {
                      "error-messages": _vm.specialRuleErrors,
                      items: _vm.rules,
                      "item-text": "Descripcion",
                      "item-value": "Id_ReglaCostoEspecial",
                      placeholder: "Tipo de Regla",
                      outlined: "",
                      dense: ""
                    },
                    on: {
                      input: function($event) {
                        return _vm.$v.currentRegistration.Id_ReglaCostoEspecial.$touch()
                      },
                      blur: function($event) {
                        return _vm.$v.currentRegistration.Id_ReglaCostoEspecial.$touch()
                      }
                    },
                    model: {
                      value: _vm.currentRegistration.Id_ReglaCostoEspecial,
                      callback: function($$v) {
                        _vm.$set(
                          _vm.currentRegistration,
                          "Id_ReglaCostoEspecial",
                          $$v
                        )
                      },
                      expression: "currentRegistration.Id_ReglaCostoEspecial"
                    }
                  })
                ],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "v-row",
            { staticStyle: { "justify-content": "flex-end" } },
            [
              _c(
                "v-col",
                { attrs: { cols: "6", md: "2" } },
                [
                  _c(
                    "v-btn",
                    {
                      attrs: { block: "", color: "primary", text: "" },
                      on: { click: _vm.submit }
                    },
                    [
                      _vm._v(
                        "\n                    Siguiente \n                    "
                      ),
                      _c("v-icon", { attrs: { left: "" } }, [
                        _vm._v(
                          "\n                      mdi-arrow-right\n                    "
                        )
                      ])
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product_configuration/special_costs_configurator/partials/SpecialCostTable.vue?vue&type=template&id=a7b2a45c&":
/*!*********************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product_configuration/special_costs_configurator/partials/SpecialCostTable.vue?vue&type=template&id=a7b2a45c& ***!
  \*********************************************************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render),
/* harmony export */   "staticRenderFns": () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "v-card",
    { staticClass: "p-3", attrs: { "data-app": "" } },
    [
      _c("v-data-table", {
        staticClass: "elevation-1",
        attrs: {
          headers: _vm.headers,
          items: _vm.specialCosts,
          search: _vm.search,
          page: _vm.page,
          "items-per-page": _vm.itemsPerPage,
          loading: _vm.loading,
          "hide-default-footer": "",
          "no-data-text": "No hay ningún registro",
          "no-results-text": "No se encontraron coincidencias",
          "loading-text": "Cargando... Porfavor espere"
        },
        on: {
          "update:page": function($event) {
            _vm.page = $event
          },
          "page-count": function($event) {
            _vm.pageCount = $event
          }
        },
        scopedSlots: _vm._u(
          [
            {
              key: "top",
              fn: function() {
                return [
                  _c("datatable-header", {
                    attrs: {
                      title: "Costos especiales",
                      "disable-creation": true,
                      hideSearchBar: true
                    },
                    on: {
                      searching: function($event) {
                        _vm.search = $event
                      },
                      open: false
                    }
                  })
                ]
              },
              proxy: true
            },
            {
              key: "item.type",
              fn: function(ref) {
                var item = ref.item
                return [
                  _c("span", [
                    _vm._v(
                      _vm._s(
                        item.type === "C"
                          ? "Calculado"
                          : item.type === "M"
                          ? "Monto"
                          : item.type === "P"
                          ? "Porcentaje"
                          : ""
                      )
                    )
                  ])
                ]
              }
            },
            {
              key: "item.amount",
              fn: function(ref) {
                var item = ref.item
                return [
                  _c("span", [
                    _vm._v(
                      _vm._s(
                        item.type === "P" ? item.amount + "%" : item.amount
                      )
                    )
                  ])
                ]
              }
            },
            {
              key: "item.showGlobalCosts",
              fn: function(ref) {
                var item = ref.item
                return [
                  _c("span", [
                    _vm._v(_vm._s(item.showGlobalCosts === true ? "Si" : "No"))
                  ])
                ]
              }
            },
            {
              key: "item.endDate",
              fn: function(ref) {
                var item = ref.item
                return [
                  _c("span", [
                    _vm._v(_vm._s(new Date(item.endDate).toLocaleDateString()))
                  ])
                ]
              }
            },
            {
              key: "item.actions",
              fn: function(ref) {
                var item = ref.item
                return [
                  _c("action-buttons", {
                    attrs: {
                      item: item,
                      editDisabled: false,
                      deleteDisabled: false,
                      showDisabled: true
                    }
                  })
                ]
              }
            }
          ],
          null,
          true
        )
      }),
      _vm._v(" "),
      _c("pagination", {
        attrs: {
          last: _vm.pageCount,
          current: _vm.page,
          itemsPerPage: _vm.itemsPerPage,
          total: this.specialCosts.length
        },
        on: {
          input: function($event) {
            _vm.page = $event
          }
        }
      }),
      _vm._v(" "),
      _c(
        "v-row",
        { staticStyle: { "justify-content": "flex-end" } },
        [
          _c(
            "v-col",
            { attrs: { cols: "6", md: "2" } },
            [
              _c(
                "v-btn",
                {
                  attrs: { block: "", color: "primary", text: "" },
                  on: { click: _vm.previousTab }
                },
                [
                  _c("v-icon", { attrs: { left: "" } }, [
                    _vm._v(
                      "\n                  mdi-arrow-left\n                "
                    )
                  ]),
                  _vm._v("\n                Anterior \n            ")
                ],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "v-col",
            { attrs: { cols: "6", md: "2" } },
            [
              _c(
                "v-btn",
                {
                  attrs: { block: "", color: "primary", text: "" },
                  on: { click: _vm.nextTab }
                },
                [
                  _vm._v("\n                Siguiente \n                "),
                  _c("v-icon", { attrs: { left: "" } }, [
                    _vm._v(
                      "\n                  mdi-arrow-right\n                "
                    )
                  ])
                ],
                1
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product_configuration/special_costs_configurator/partials/TotalCosts.vue?vue&type=template&id=3135446c&":
/*!***************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product_configuration/special_costs_configurator/partials/TotalCosts.vue?vue&type=template&id=3135446c& ***!
  \***************************************************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render),
/* harmony export */   "staticRenderFns": () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "v-card",
    { staticClass: "p-3", attrs: { "data-app": "" } },
    [
      _c("v-data-table", {
        staticClass: "elevation-1",
        attrs: {
          headers: _vm.headers,
          items: _vm.products,
          search: _vm.search,
          page: _vm.page,
          "items-per-page": _vm.itemsPerPage,
          loading: _vm.loading,
          "hide-default-footer": "",
          "no-data-text": "No hay ningún registro",
          "no-results-text": "No se encontraron coincidencias",
          "loading-text": "Cargando... Porfavor espere"
        },
        on: {
          "update:page": function($event) {
            _vm.page = $event
          },
          "page-count": function($event) {
            _vm.pageCount = $event
          }
        },
        scopedSlots: _vm._u([
          {
            key: "top",
            fn: function() {
              return [
                _c("datatable-header", {
                  attrs: {
                    title: "Costos especiales",
                    "disable-creation": true,
                    hideSearchBar: true
                  },
                  on: {
                    searching: function($event) {
                      _vm.search = $event
                    },
                    open: false
                  }
                })
              ]
            },
            proxy: true
          }
        ])
      }),
      _vm._v(" "),
      _c("pagination", {
        attrs: {
          last: _vm.pageCount,
          current: _vm.page,
          itemsPerPage: _vm.itemsPerPage,
          total: this.products.length
        },
        on: {
          input: function($event) {
            _vm.page = $event
          }
        }
      }),
      _vm._v(" "),
      _c(
        "v-card-actions",
        { staticClass: "mt-5 d-block" },
        [
          _c("form-action-buttons", {
            on: { close: _vm.cancel, save: _vm.submit }
          })
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ })

}]);