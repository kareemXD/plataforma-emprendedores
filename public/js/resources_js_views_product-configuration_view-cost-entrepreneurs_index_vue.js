(self["webpackChunk"] = self["webpackChunk"] || []).push([["resources_js_views_product-configuration_view-cost-entrepreneurs_index_vue"],{

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/view-cost-entrepreneurs/index.vue?vue&type=script&lang=js&":
/*!*****************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/view-cost-entrepreneurs/index.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _components_chipStatus__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../components/chipStatus */ "./resources/js/components/chipStatus.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  created: function created() {
    this.getCosts();
  },
  mounted: function mounted() {
    this.getSettings();
  },
  components: {
    ChipStatus: _components_chipStatus__WEBPACK_IMPORTED_MODULE_0__.default
  },
  data: function data() {
    return {
      brands: [],
      families: [],
      specialties: [],
      categories: [],
      weight: ['<', '>', '='],
      volumen: ['<', '>', '='],
      filters: {
        brand: 0,
        family: 0,
        especiality: 0,
        category: 0,
        weight: '',
        weight_t: '',
        volumen: '',
        volume_t: ''
      },
      search: "",
      dialog: false,
      productId: null,
      dialogDelete: false,
      page: 1,
      pageCount: 0,
      totalItems: 0,
      itemsPerPage: 50,
      loading: false,
      headers: [],
      costs: [],
      amounts: [{
        id: 20,
        name: '20'
      }, {
        id: 50,
        name: '50'
      }, {
        id: 100,
        name: '100'
      }]
    };
  },
  methods: {
    /**
     * Get costs and fill date table from api view-cost-entrepreneurs.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-03-25
     * @param
     * @return void
     */
    getCosts: function getCosts() {
      var _this = this;

      this.loading = true;
      axios.get("/api/view-cost-entrepreneurs").then(function (result) {
        for (var key in result.data.costs[0]) {
          key == 'Código_de_Barras_Universal' || key == 'Descripción_Corta' || key == 'Clave_Unificada' ? _this.headers.push({
            text: key,
            value: key,
            filterable: true
          }) : _this.headers.push({
            text: key,
            value: key,
            filterable: false
          });
        }

        _this.costs = result.data.costs;
      })["catch"](function (error) {
        swal2.fire({
          icon: "error",
          title: "Oops...",
          text: "Algo salió mal, vuelve a intentarlo!"
        });
      })["finally"](function () {
        _this.loading = false;
      });
    },

    /**
    * Fill selects.
    * @auth Oscar Castellanos
    * @date 2021-06-25
    * @param Object params
    * @return void
    */
    getSettings: function getSettings() {
      var _this2 = this;

      axios.get("/api/special-costs-settings").then(function (result) {
        _this2.brands = result.data.brands;
        _this2.families = result.data.families;
        _this2.specialties = result.data.especialities;
        _this2.categories = result.data.categories;
      })["catch"](function (error) {
        swal2.fire({
          icon: "error",
          title: "Oops...",
          text: "Algo salió mal, vuelve a intentarlo!"
        });
      });
    },

    /**
     * Allows you to filter products from api.
     * @auth Oscar Castellanos
     * @date 2021-06-25
     * @param
     * @return void
     */
    filterProducts: function filterProducts() {
      var _this3 = this;

      this.loading = true;
      this.search = "";
      axios.get("/api/products-special-filters", {
        params: {
          filters: this.filters
        }
      }).then(function (result) {
        _this3.costs = result.data.products;
      })["catch"](function (error) {
        swal2.fire({
          icon: "error",
          title: "Oops...",
          text: "Algo salió mal, vuelve a intentarlo!"
        });
      })["finally"](function () {
        _this3.loading = false;
        _this3.selectAll = false;
      });
    },

    /**
    * Clean the filters.
    * @auth José Vega <jose.vega@nuvem.mx>
    * @date 2021-05-04
    * @param
    * @return void
    */
    cleanFilters: function cleanFilters() {
      this.filters.brand = 0;
      this.filters.family = 0;
      this.filters.especiality = 0;
      this.filters.category = 0;
      this.filters.weight = 0;
      this.filters.weight_t = "";
      this.filters.volumen = 0;
      this.filters.volumen_t = "";
      this.selectAll = false;
    },

    /**
     * Open a window to export a file in excel from laravel with the costs available.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-04-14
     * @param
     * @return void
     */
    exportCosts: function exportCosts() {
      axios.get("/api/export-costs", {
        responseType: 'arraybuffer'
      }).then(function (result) {
        var blob = new Blob([result.data], {
          type: 'application/xlsx'
        });
        var link = document.createElement('a');
        link.href = window.URL.createObjectURL(blob);
        link.download = 'costs.xlsx';
        link.click();
      })["catch"](function (error) {
        swal2.fire({
          icon: "error",
          title: "Oops...",
          text: "Algo salió mal, vuelve a intentarlo!"
        });
      });
    },
    resetSearch: function resetSearch() {
      this.search = "";
    }
  }
});

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/view-cost-entrepreneurs/index.vue?vue&type=style&index=0&lang=css&":
/*!***************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/view-cost-entrepreneurs/index.vue?vue&type=style&index=0&lang=css& ***!
  \***************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ ((module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../../../node_modules/css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js");
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0__);
// Imports

var ___CSS_LOADER_EXPORT___ = _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0___default()(function(i){return i[1]});
// Module
___CSS_LOADER_EXPORT___.push([module.id, "\n.data-table-style{\n    margin: 10px;\n}\n.v-btn__content {\n  color: white !important;\n}\n", ""]);
// Exports
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (___CSS_LOADER_EXPORT___);


/***/ }),

/***/ "./resources/js/views/product-configuration/view-cost-entrepreneurs/index.vue":
/*!************************************************************************************!*\
  !*** ./resources/js/views/product-configuration/view-cost-entrepreneurs/index.vue ***!
  \************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _index_vue_vue_type_template_id_2529913f___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./index.vue?vue&type=template&id=2529913f& */ "./resources/js/views/product-configuration/view-cost-entrepreneurs/index.vue?vue&type=template&id=2529913f&");
/* harmony import */ var _index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./index.vue?vue&type=script&lang=js& */ "./resources/js/views/product-configuration/view-cost-entrepreneurs/index.vue?vue&type=script&lang=js&");
/* harmony import */ var _index_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./index.vue?vue&type=style&index=0&lang=css& */ "./resources/js/views/product-configuration/view-cost-entrepreneurs/index.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! !../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");



;


/* normalize component */

var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__.default)(
  _index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__.default,
  _index_vue_vue_type_template_id_2529913f___WEBPACK_IMPORTED_MODULE_0__.render,
  _index_vue_vue_type_template_id_2529913f___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/product-configuration/view-cost-entrepreneurs/index.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/js/views/product-configuration/view-cost-entrepreneurs/index.vue?vue&type=script&lang=js&":
/*!*************************************************************************************************************!*\
  !*** ./resources/js/views/product-configuration/view-cost-entrepreneurs/index.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./index.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/view-cost-entrepreneurs/index.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__.default); 

/***/ }),

/***/ "./resources/js/views/product-configuration/view-cost-entrepreneurs/index.vue?vue&type=template&id=2529913f&":
/*!*******************************************************************************************************************!*\
  !*** ./resources/js/views/product-configuration/view-cost-entrepreneurs/index.vue?vue&type=template&id=2529913f& ***!
  \*******************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_2529913f___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   "staticRenderFns": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_2529913f___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_2529913f___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./index.vue?vue&type=template&id=2529913f& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/view-cost-entrepreneurs/index.vue?vue&type=template&id=2529913f&");


/***/ }),

/***/ "./resources/js/views/product-configuration/view-cost-entrepreneurs/index.vue?vue&type=style&index=0&lang=css&":
/*!*********************************************************************************************************************!*\
  !*** ./resources/js/views/product-configuration/view-cost-entrepreneurs/index.vue?vue&type=style&index=0&lang=css& ***!
  \*********************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_node_modules_css_loader_dist_cjs_js_clonedRuleSet_10_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_10_0_rules_0_use_2_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-style-loader/index.js!../../../../../node_modules/css-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[1]!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[2]!../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./index.vue?vue&type=style&index=0&lang=css& */ "./node_modules/vue-style-loader/index.js!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/view-cost-entrepreneurs/index.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_vue_style_loader_index_js_node_modules_css_loader_dist_cjs_js_clonedRuleSet_10_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_10_0_rules_0_use_2_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_node_modules_css_loader_dist_cjs_js_clonedRuleSet_10_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_10_0_rules_0_use_2_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ var __WEBPACK_REEXPORT_OBJECT__ = {};
/* harmony reexport (unknown) */ for(const __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_node_modules_css_loader_dist_cjs_js_clonedRuleSet_10_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_10_0_rules_0_use_2_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== "default") __WEBPACK_REEXPORT_OBJECT__[__WEBPACK_IMPORT_KEY__] = () => _node_modules_vue_style_loader_index_js_node_modules_css_loader_dist_cjs_js_clonedRuleSet_10_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_10_0_rules_0_use_2_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__[__WEBPACK_IMPORT_KEY__]
/* harmony reexport (unknown) */ __webpack_require__.d(__webpack_exports__, __WEBPACK_REEXPORT_OBJECT__);


/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/view-cost-entrepreneurs/index.vue?vue&type=template&id=2529913f&":
/*!**********************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/view-cost-entrepreneurs/index.vue?vue&type=template&id=2529913f& ***!
  \**********************************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render),
/* harmony export */   "staticRenderFns": () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "v-card",
    { staticClass: "p-3", attrs: { "data-app": "" } },
    [
      _c(
        "v-card-title",
        [
          _vm._v("\n  Vista de costo base emprendedore\n  "),
          _c("v-spacer"),
          _vm._v(" "),
          _c(
            "div",
            [
              _c("sub", [_vm._v("Registros por vista")]),
              _vm._v(" "),
              _c("v-select", {
                attrs: {
                  items: _vm.amounts,
                  "item-text": "name",
                  "item-value": "id",
                  placeholder: "Elige una opción",
                  outlined: "",
                  dense: "",
                  attach: "",
                  auto: ""
                },
                on: { change: _vm.resetSearch },
                model: {
                  value: _vm.itemsPerPage,
                  callback: function($$v) {
                    _vm.itemsPerPage = $$v
                  },
                  expression: "itemsPerPage"
                }
              })
            ],
            1
          ),
          _vm._v(" "),
          _c("v-spacer"),
          _vm._v(" "),
          _c(
            "v-btn",
            { attrs: { color: "primary" }, on: { click: _vm.exportCosts } },
            [_vm._v("\n     Exportar a excel\n   ")]
          ),
          _vm._v(" "),
          _c("v-spacer"),
          _vm._v(" "),
          _c("v-text-field", {
            attrs: {
              outlined: "",
              dense: "",
              "append-icon": "mdi-magnify",
              placeholder: "Buscar",
              "hide-details": ""
            },
            model: {
              value: _vm.search,
              callback: function($$v) {
                _vm.search = $$v
              },
              expression: "search"
            }
          })
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "v-row",
        [
          _c(
            "v-col",
            { attrs: { cols: "6", md: "2" } },
            [
              _c("h6", [_vm._v("Marca")]),
              _vm._v(" "),
              _c("v-select", {
                attrs: {
                  placeholder: "Marca",
                  items: _vm.brands,
                  attach: "",
                  "item-text": "Marca",
                  "item-value": "Id_Marca",
                  outlined: "",
                  dense: ""
                },
                model: {
                  value: _vm.filters.brand,
                  callback: function($$v) {
                    _vm.$set(_vm.filters, "brand", $$v)
                  },
                  expression: "filters.brand"
                }
              })
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "v-col",
            { attrs: { cols: "6", md: "2" } },
            [
              _c("h6", [_vm._v("Familia")]),
              _vm._v(" "),
              _c("v-select", {
                attrs: {
                  placeholder: "Familia",
                  items: _vm.families,
                  attach: "",
                  "item-text": "Familia",
                  "item-value": "Id_Familia",
                  outlined: "",
                  dense: ""
                },
                model: {
                  value: _vm.filters.family,
                  callback: function($$v) {
                    _vm.$set(_vm.filters, "family", $$v)
                  },
                  expression: "filters.family"
                }
              })
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "v-col",
            { attrs: { cols: "6", md: "2" } },
            [
              _c("h6", [_vm._v("Especialidad")]),
              _vm._v(" "),
              _c("v-select", {
                attrs: {
                  placeholder: "Especialidad",
                  items: _vm.specialties,
                  attach: "",
                  "item-text": "Especialidad",
                  "item-value": "Id_EspMed",
                  outlined: "",
                  dense: ""
                },
                model: {
                  value: _vm.filters.especiality,
                  callback: function($$v) {
                    _vm.$set(_vm.filters, "especiality", $$v)
                  },
                  expression: "filters.especiality"
                }
              })
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "v-col",
            { attrs: { cols: "6", md: "2" } },
            [
              _c("h6", [_vm._v("Categoría")]),
              _vm._v(" "),
              _c("v-select", {
                attrs: {
                  placeholder: "Categoría",
                  items: _vm.categories,
                  attach: "",
                  "item-text": "Categoria",
                  "item-value": "Id_Categoria",
                  outlined: "",
                  dense: ""
                },
                model: {
                  value: _vm.filters.category,
                  callback: function($$v) {
                    _vm.$set(_vm.filters, "category", $$v)
                  },
                  expression: "filters.category"
                }
              })
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "v-col",
            { attrs: { cols: "6", md: "2" } },
            [
              _c("h6", [_vm._v("Peso")]),
              _vm._v(" "),
              _c("v-select", {
                attrs: {
                  placeholder: "Peso",
                  items: _vm.weight,
                  attach: "",
                  "item-text": "Peso",
                  "item-value": "Id_Peso",
                  outlined: "",
                  dense: ""
                },
                model: {
                  value: _vm.filters.weight,
                  callback: function($$v) {
                    _vm.$set(_vm.filters, "weight", $$v)
                  },
                  expression: "filters.weight"
                }
              }),
              _vm._v(" "),
              _c("v-text-field", {
                attrs: {
                  placeholder: "Peso_t",
                  "item-text": "Peso",
                  "item-value": "Id_Peso_t",
                  outlined: "",
                  dense: ""
                },
                model: {
                  value: _vm.filters.weight_t,
                  callback: function($$v) {
                    _vm.$set(_vm.filters, "weight_t", $$v)
                  },
                  expression: "filters.weight_t"
                }
              })
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "v-col",
            { attrs: { cols: "6", md: "2" } },
            [
              _c("h6", [_vm._v("Volumen")]),
              _vm._v(" "),
              _c("v-select", {
                attrs: {
                  placeholder: "Volumen",
                  items: _vm.volumen,
                  attach: "",
                  "item-text": "Volumen",
                  "item-value": "Id_Volumen",
                  outlined: "",
                  dense: ""
                },
                model: {
                  value: _vm.filters.volumen,
                  callback: function($$v) {
                    _vm.$set(_vm.filters, "volumen", $$v)
                  },
                  expression: "filters.volumen"
                }
              }),
              _vm._v(" "),
              _c("v-text-field", {
                attrs: {
                  placeholder: "Volumen_t",
                  "item-text": "Volumen",
                  "item-value": "Id_volumen_t",
                  outlined: "",
                  dense: ""
                },
                model: {
                  value: _vm.filters.volumen_t,
                  callback: function($$v) {
                    _vm.$set(_vm.filters, "volumen_t", $$v)
                  },
                  expression: "filters.volumen_t"
                }
              })
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "v-col",
            {
              staticStyle: { "align-self": "center" },
              attrs: { cols: "6", md: "2" }
            },
            [
              _c(
                "v-btn",
                {
                  attrs: { block: "", color: "primary", text: "" },
                  on: { click: _vm.filterProducts }
                },
                [_vm._v("\n        Buscar\n      ")]
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "v-col",
            {
              staticStyle: { "align-self": "center" },
              attrs: { cols: "6", md: "2" }
            },
            [
              _c(
                "v-btn",
                {
                  attrs: { block: "", color: "primary", text: "" },
                  on: { click: _vm.cleanFilters }
                },
                [_vm._v("\n        Limpiar\n      ")]
              )
            ],
            1
          )
        ],
        1
      ),
      _vm._v(" "),
      _c("v-data-table", {
        staticClass: "elevation-1",
        attrs: {
          headers: _vm.headers,
          items: _vm.costs,
          search: _vm.search,
          page: _vm.page,
          "items-per-page": _vm.itemsPerPage,
          loading: _vm.loading,
          "hide-default-footer": "",
          "fixed-header": "",
          height: "100vh",
          "no-data-text": "No hay ningún registro",
          "no-results-text": "No se encontraron coincidencias",
          "loading-text": "Cargando... Porfavor espere"
        },
        on: {
          "update:page": function($event) {
            _vm.page = $event
          },
          "page-count": function($event) {
            _vm.pageCount = $event
          }
        }
      }),
      _vm._v(" "),
      _c("pagination", {
        attrs: {
          last: _vm.pageCount,
          current: _vm.page,
          itemsPerPage: _vm.itemsPerPage,
          total: this.costs.length
        },
        on: {
          input: function($event) {
            _vm.page = $event
          }
        }
      })
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-style-loader/index.js!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/view-cost-entrepreneurs/index.vue?vue&type=style&index=0&lang=css&":
/*!********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-style-loader/index.js!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/view-cost-entrepreneurs/index.vue?vue&type=style&index=0&lang=css& ***!
  \********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(/*! !!../../../../../node_modules/css-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[1]!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[2]!../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./index.vue?vue&type=style&index=0&lang=css& */ "./node_modules/css-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/view-cost-entrepreneurs/index.vue?vue&type=style&index=0&lang=css&");
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.id, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var add = __webpack_require__(/*! !../../../../../node_modules/vue-style-loader/lib/addStylesClient.js */ "./node_modules/vue-style-loader/lib/addStylesClient.js").default
var update = add("3d905d33", content, false, {});
// Hot Module Replacement
if(false) {}

/***/ })

}]);