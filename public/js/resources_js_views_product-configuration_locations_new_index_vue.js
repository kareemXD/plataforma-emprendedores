(self["webpackChunk"] = self["webpackChunk"] || []).push([["resources_js_views_product-configuration_locations_new_index_vue"],{

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/locations_new/index.vue?vue&type=script&lang=js&":
/*!*******************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/locations_new/index.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _Layout_Components_LayoutWrapper__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../Layout/Components/LayoutWrapper */ "./resources/js/Layout/Components/LayoutWrapper.vue");
/* harmony import */ var _components_chipStatus__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../components/chipStatus */ "./resources/js/components/chipStatus.vue");
/* harmony import */ var _store__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../store */ "./resources/js/store/index.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  mounted: function mounted() {
    this.getRacks();
    this.permissionsUser = _store__WEBPACK_IMPORTED_MODULE_2__.default.state.permissions;
  },
  components: {
    "layout-wrapper": _Layout_Components_LayoutWrapper__WEBPACK_IMPORTED_MODULE_0__.default,
    ChipStatus: _components_chipStatus__WEBPACK_IMPORTED_MODULE_1__.default
  },
  data: function data() {
    return {
      search: "",
      dialog: false,
      dialogDelete: false,
      dialogShow: false,
      permissionsUser: [],
      page: 1,
      pageCount: 0,
      totalItems: 0,
      itemsPerPage: 10,
      loading: false,
      racks: [],
      itemDelete: {},
      headers: [{
        text: "Id",
        value: "Id"
      }, {
        text: "Rack",
        value: "Nombre"
      }, {
        text: "Estatus",
        value: "Estatus"
      }, {
        text: "Acciones",
        value: "actions",
        sortable: false
      }],
      amounts: [{
        id: 10,
        name: '10'
      }, {
        id: 50,
        name: '50'
      }, {
        id: 100,
        name: '100'
      }],
      isDeleting: false
    };
  },
  methods: {
    /**
     * Get racks from api.
     * @auth Iván Morales <ivan.morales@nuvem.mx>
     * @date 2021-06-17
     * @param
     * @return void
     */
    getRacks: function getRacks() {
      var _this = this;

      this.loading = true;
      axios.get("/api/locations-new").then(function (result) {
        _this.racks = result.data.racks;
      })["catch"](function (error) {
        swal2.fire({
          icon: "error",
          title: "Oops...",
          text: "Algo salió mal, vuelve a intentarlo!"
        });
        console.log({
          error: error
        });
      })["finally"](function () {
        _this.loading = false;
      });
    },

    /**
     * Open the delete dialog confirm
     * @author Iván Morales <ivan.morales@nuvem.mx>
     * @created 2021/06/17
     * @param Object item
     * @return \Illuminate\Http\JsonResponse
     */
    openDeleteDialog: function openDeleteDialog(item) {
      this.itemDelete = item;
      this.dialogDelete = true;
    },

    /**
     * Send a request to api for change the status.
     * @auth Iván Morales | ivan.morales@nuvem.mx
     * @date 2021/06/17
     * @param
     * @return void
     */
    deleteItem: function deleteItem() {
      var _this2 = this;

      this.isDeleting = true;
      var url = "/api/locations-new/".concat(this.itemDelete.Id);
      axios["delete"](url).then(function (res) {
        _this2.isDeleting = false;
        _this2.dialogDelete = false;
        swal2.fire('Eliminado', 'Estatus cambiado correctamente', 'success');

        _this2.getRacks();
      })["catch"](function (err) {
        _this2.isDeleting = false;
        swal2.fire('Error', 'Ocurrió un error, intente mas tarde', 'error');
        console.log(err);
      });
    },

    /**
     * Open the route for store a rack
     * @auth Iván Morales <ivan.morales@nuvem.mx>
     * @date 2021-06-18
     * @param
     * @return void
     */
    openCreateRack: function openCreateRack() {
      this.$router.push({
        name: "locations-create"
      });
    },

    /**
     * Open the route to edit racks
     * @auth Iván Morales <ivan.morales@nuvem.mx>
     * @date 2021-06-18
     * @param Object item
     * @return void
     */
    editLocation: function editLocation(item) {
      this.$router.push({
        name: 'locations-edit',
        params: {
          id: item.Id
        }
      });
    },

    /**
     * Open the route to show the racks
     * @auth Iván Morales <ivan.morales@nuvem.mx>
     * @date 2021-06-25
     * @param Object item
     * @return void
     */
    showLocation: function showLocation(item) {
      this.$router.push({
        name: 'locations-show',
        params: {
          id: item.Id
        }
      });
    }
  }
});

/***/ }),

/***/ "./resources/js/views/product-configuration/locations_new/index.vue":
/*!**************************************************************************!*\
  !*** ./resources/js/views/product-configuration/locations_new/index.vue ***!
  \**************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _index_vue_vue_type_template_id_f2f94ec8___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./index.vue?vue&type=template&id=f2f94ec8& */ "./resources/js/views/product-configuration/locations_new/index.vue?vue&type=template&id=f2f94ec8&");
/* harmony import */ var _index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./index.vue?vue&type=script&lang=js& */ "./resources/js/views/product-configuration/locations_new/index.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! !../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */
;
var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__.default)(
  _index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__.default,
  _index_vue_vue_type_template_id_f2f94ec8___WEBPACK_IMPORTED_MODULE_0__.render,
  _index_vue_vue_type_template_id_f2f94ec8___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/product-configuration/locations_new/index.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/js/views/product-configuration/locations_new/index.vue?vue&type=script&lang=js&":
/*!***************************************************************************************************!*\
  !*** ./resources/js/views/product-configuration/locations_new/index.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./index.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/locations_new/index.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__.default); 

/***/ }),

/***/ "./resources/js/views/product-configuration/locations_new/index.vue?vue&type=template&id=f2f94ec8&":
/*!*********************************************************************************************************!*\
  !*** ./resources/js/views/product-configuration/locations_new/index.vue?vue&type=template&id=f2f94ec8& ***!
  \*********************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_f2f94ec8___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   "staticRenderFns": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_f2f94ec8___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_f2f94ec8___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./index.vue?vue&type=template&id=f2f94ec8& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/locations_new/index.vue?vue&type=template&id=f2f94ec8&");


/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/locations_new/index.vue?vue&type=template&id=f2f94ec8&":
/*!************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/locations_new/index.vue?vue&type=template&id=f2f94ec8& ***!
  \************************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render),
/* harmony export */   "staticRenderFns": () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "v-card",
    { staticClass: "px-3", attrs: { "data-app": "" } },
    [
      _c("dialog-delete", {
        attrs: {
          status: _vm.dialogDelete,
          loading: _vm.isDeleting,
          textDialog:
            "El rack, niveles y secciones relacionados serán dados de baja, ¿Desea continuar?"
        },
        on: {
          cancel: function($event) {
            _vm.dialogDelete = false
          },
          confirm: _vm.deleteItem
        }
      }),
      _vm._v(" "),
      _c("v-data-table", {
        staticClass: "elevation-1",
        attrs: {
          headers: _vm.headers,
          items: _vm.racks,
          search: _vm.search,
          page: _vm.page,
          "items-per-page": _vm.itemsPerPage,
          loading: _vm.loading,
          "hide-default-footer": "",
          "no-data-text": "No hay ningún registro",
          "no-results-text": "No se encontraron coincidencias",
          "loading-text": "Cargando... Porfavor espere"
        },
        on: {
          "update:page": function($event) {
            _vm.page = $event
          },
          "page-count": function($event) {
            _vm.pageCount = $event
          }
        },
        scopedSlots: _vm._u(
          [
            {
              key: "top",
              fn: function() {
                return [
                  _c("datatable-header", {
                    attrs: {
                      title: "Ubicaciones",
                      "disable-creation": !_vm.permissionsUser.includes(
                        "Location.create"
                      )
                    },
                    on: {
                      searching: function($event) {
                        _vm.search = $event
                      },
                      open: function($event) {
                        return _vm.openCreateRack()
                      }
                    }
                  }),
                  _vm._v(" "),
                  _c(
                    "v-row",
                    { staticStyle: { "margin-left": "17px" } },
                    [
                      _c(
                        "v-col",
                        {
                          staticStyle: {
                            "align-self": "center",
                            "padding-right": "0px",
                            "max-width": "80px"
                          },
                          attrs: { cols: "6", md: "1" }
                        },
                        [_c("p", [_vm._v("Mostrar")])]
                      ),
                      _vm._v(" "),
                      _c(
                        "v-col",
                        {
                          staticStyle: {
                            "padding-left": "0px",
                            "max-width": "95px"
                          },
                          attrs: { cols: "6", md: "2" }
                        },
                        [
                          _c(
                            "div",
                            [
                              _c("v-select", {
                                attrs: {
                                  items: _vm.amounts,
                                  "item-text": "name",
                                  "item-value": "id",
                                  placeholder: "Elige una opción",
                                  outlined: "",
                                  dense: "",
                                  attach: "",
                                  auto: ""
                                },
                                model: {
                                  value: _vm.itemsPerPage,
                                  callback: function($$v) {
                                    _vm.itemsPerPage = $$v
                                  },
                                  expression: "itemsPerPage"
                                }
                              })
                            ],
                            1
                          )
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "v-col",
                        {
                          staticStyle: {
                            "align-self": "center",
                            "padding-left": "0px"
                          },
                          attrs: { cols: "6", md: "1" }
                        },
                        [_c("p", [_vm._v("Registros")])]
                      )
                    ],
                    1
                  )
                ]
              },
              proxy: true
            },
            {
              key: "item.Estatus",
              fn: function(ref) {
                var item = ref.item
                return [_c("chip-status", { attrs: { item: item } })]
              }
            },
            {
              key: "item.actions",
              fn: function(ref) {
                var item = ref.item
                return [
                  _c("action-buttons", {
                    attrs: {
                      item: item,
                      editDisabled: !_vm.permissionsUser.includes(
                        "Location.edit"
                      ),
                      showDisabled: !_vm.permissionsUser.includes(
                        "Location.view"
                      ),
                      deleteDisabled: !_vm.permissionsUser.includes(
                        "Location.delete"
                      )
                    },
                    on: {
                      edit: function($event) {
                        return _vm.editLocation(item)
                      },
                      view: function($event) {
                        return _vm.showLocation(item)
                      },
                      delete: _vm.openDeleteDialog
                    }
                  })
                ]
              }
            }
          ],
          null,
          true
        )
      }),
      _vm._v(" "),
      _c("pagination", {
        attrs: {
          last: _vm.pageCount,
          current: _vm.page,
          itemsPerPage: _vm.itemsPerPage,
          total: _vm.racks.length
        },
        on: {
          input: function($event) {
            _vm.page = $event
          }
        }
      })
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ })

}]);