(self["webpackChunk"] = self["webpackChunk"] || []).push([["resources_js_views_product-configuration_products_edit_vue"],{

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/products/edit.vue?vue&type=script&lang=js&":
/*!*************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/products/edit.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _partials_GeneralForm__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./partials/GeneralForm */ "./resources/js/views/product-configuration/products/partials/GeneralForm.vue");
/* harmony import */ var _partials_StorageForm__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./partials/StorageForm */ "./resources/js/views/product-configuration/products/partials/StorageForm.vue");
/* harmony import */ var _partials_DataSheetForm__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./partials/DataSheetForm */ "./resources/js/views/product-configuration/products/partials/DataSheetForm.vue");
/* harmony import */ var _partials_StatisticsForm__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./partials/StatisticsForm */ "./resources/js/views/product-configuration/products/partials/StatisticsForm.vue");
/* harmony import */ var _partials_ClassificationForm__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./partials/ClassificationForm */ "./resources/js/views/product-configuration/products/partials/ClassificationForm.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//





/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  components: {
    GeneralForm: _partials_GeneralForm__WEBPACK_IMPORTED_MODULE_0__.default,
    StorageForm: _partials_StorageForm__WEBPACK_IMPORTED_MODULE_1__.default,
    DataSheetForm: _partials_DataSheetForm__WEBPACK_IMPORTED_MODULE_2__.default,
    StatisticsForm: _partials_StatisticsForm__WEBPACK_IMPORTED_MODULE_3__.default,
    ClassificationForm: _partials_ClassificationForm__WEBPACK_IMPORTED_MODULE_4__.default
  },
  mounted: function mounted() {
    if (this.$route.params.item != null) {
      this.currentProduct = this.$route.params.item;
    }
  },
  data: function data() {
    return {
      e1: 1,
      steps: 4,
      titles: ["General", "Clasificación", "Almacenamiento", "Estadística", "Ficha Técnica"],
      currentProduct: null,
      tab: null,
      tabSelected: "",
      change: false,
      tabsDisabled: false,
      data: new FormData(),
      canPost: false
    };
  },
  methods: {
    /**
     * Filling a new product with the data of the general component.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-04-16
     * @param
     * @return void
     */
    fillNewProductGeneralData: function fillNewProductGeneralData(value) {
      this.tab = value[0];
      this.canPost = value[2];
      this.tabsDisabled = false;
      this.currentProduct.Id_Prod1 = value[1].Id_Prod1;
      this.currentProduct.Id_Prod3 = value[1].bidKey;
      this.currentProduct.CBarrasN1 = value[1].barCode;
      this.currentProduct.Id_CodSat = value[1].satCode;
      this.currentProduct.Descrip1 = value[1].shortDescription;
      this.currentProduct.Descrip2 = value[1].longDescription;
      this.currentProduct.Id_Prov1 = value[1].supplier1;
      this.currentProduct.Id_Prov2 = value[1].supplier2;
      this.currentProduct.Inventaria = value[1].inventoryable;
      this.currentProduct.Lprecios = value[1].inPriceList;
      this.currentProduct.Man_LoteCad = value[1].applyExpiration;
      this.currentProduct.Activo = value[1].active;
      this.currentProduct.eCommerce = value[1].eCommerce;
      this.currentProduct.Observa = value[1].observations;
      this.currentProduct.Registro = value[1].healthRegistration;
      this.currentProduct.IVA = value[1].iva;
      this.currentProduct.Utmin_Lici = value[1].minimumBid;
      this.currentProduct.Flete = value[1].freight;
      this.currentProduct.Tiempo_Sur = value[1].timeAssortment;
    },

    /**
     * Filling a new product with the data of the classification component.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-04-16
     * @param
     * @return void
     */
    fillNewProductClassificationData: function fillNewProductClassificationData(value) {
      this.tab = value[0];
      this.tabsDisabled = false;
      this.currentProduct.Id_Categoria = value[1].category;
      this.currentProduct.Id_Familia = value[1].family;
      this.currentProduct.Id_Marca = value[1].mark;
      this.currentProduct.Id_EspMed = value[1].specialty;
      this.currentProduct.Id_estatus = value[1].status;
      this.currentProduct.Origen = value[1].origin;
    },

    /**
     * Filling a new product with the data of the storage component.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-04-16
     * @param
     * @return void
     */
    fillNewProductStorageData: function fillNewProductStorageData(value) {
      this.tab = value[0];
      this.tabsDisabled = false;
      this.currentProduct.Niveles = value[1].level;
      this.currentProduct.id_Empaque1 = value[1].packing1;
      this.currentProduct.id_Empaque2 = value[1].packing2;
      this.currentProduct.id_Empaque3 = value[1].packing3;
      this.currentProduct.Unidad_N2 = value[1].units1;
      this.currentProduct.Unidad_N3 = value[1].units2;
      this.currentProduct.Nivel_Comp = value[1].purchaseLevel;
      this.currentProduct.Nivel_Vta = value[1].saleLevel;
      this.currentProduct.Nivel_Vta2 = value[1].saleLevelMost;
    },

    /**
     * Filling a new product with the data of the classification component.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-04-16
     * @param
     * @return void
     */
    fillNewProductStatisticsData: function fillNewProductStatisticsData(value) {
      this.tab = value[0];
      this.tabsDisabled = false;
      this.currentProduct.Ult_Costo = value[1].lastCost;
      this.currentProduct.Costo_Prom = value[1].averageCost;
      this.currentProduct.CM_MasBajo = value[1].lowerMarketCost;
      this.currentProduct.UCambio_CM = value[1].lastMereCost;
      this.currentProduct.UCambio_CL = value[1].lastLiciCost;
      this.currentProduct.UCambio_Pre = value[1].lastChangePrices;
      this.currentProduct.Ult_Compra = value[1].lastPurchase;
      this.currentProduct.Ult_Venta = value[1].lastSale;
    },

    /**
     * Filling a new product with the data of the classification component.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-04-16
     * @param
     * @return void
     */
    fillNewProductDataSheetData: function fillNewProductDataSheetData(value) {
      this.tabsDisabled = false;
      this.currentProduct.Peso = value[1].weight;
      this.currentProduct.Ancho = value[1].width;
      this.currentProduct.Alto = value[1].high;
      this.currentProduct.Largo = value[1].length;
      this.currentProduct.Especificacion = value[1].technicalSpecification;
      this.currentProduct.Indicacion = value[1].indications;
      this.currentProduct.images = value[1].images;
      this.currentProduct.locations = value[1].locations;
      this.data.append('newProduct', JSON.stringify(this.currentProduct));

      if (value[1].images != null && value[1].images.length > 0) {
        for (var i = 0; i < value[1].images.length; i++) {
          var image = value[1].images[i];
          this.data.append('images[' + i + ']', image);
        }
      }

      if (this.canPost) {
        this.edit(this.data);
      } else {
        this.tab = 0;
        swal2.fire({
          icon: "error",
          title: "Oops...",
          text: "Los campos: clave nemotécnica y clave licitación son requeridos para el producto"
        });
      }
    },

    /**
     * Update a product.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-04-16
     * @param Object data
     * @return void
     */
    edit: function edit(data) {
      var _this = this;

      data.append("_method", "put");
      axios.post("/api/products/" + this.currentProduct.Id_Prod1, data, {
        headers: {
          "Content-type": "multipart/form-data"
        }
      }).then(function (result) {
        _this.$router.push({
          name: "products"
        });

        swal2.fire("Editado", "Se editaron los datos correctamente", "success");
      })["catch"](function (error) {
        swal2.fire({
          icon: "error",
          title: "Oops...",
          text: "Algo salió mal, vuelve a intentarlo!"
        });
      });
    },
    save: function save() {
      this.$refs.submit.submit();
    }
  },
  watch: {
    /**
     * Show the current tab.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-04-16
     * @param Object data
     * @return void
     */
    steps: function steps(val) {
      if (this.e1 > val) {
        this.e1 = val;
      }
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/products/partials/ClassificationForm.vue?vue&type=script&lang=js&":
/*!************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/products/partials/ClassificationForm.vue?vue&type=script&lang=js& ***!
  \************************************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var vuelidate__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vuelidate */ "./node_modules/vuelidate/lib/index.js");
/* harmony import */ var vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vuelidate/lib/validators */ "./node_modules/vuelidate/lib/validators/index.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  name: "ClassificationForm",
  mixins: [vuelidate__WEBPACK_IMPORTED_MODULE_0__.validationMixin],
  props: ["product", "inputsStatus", "type"],
  validations: {
    currentClassificationData: {
      category: {
        required: vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_1__.required
      },
      family: {
        required: vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_1__.required
      },
      mark: {
        required: vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_1__.required
      },
      specialty: {
        required: vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_1__.required
      },
      status: {
        required: vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_1__.required
      },
      origin: {
        required: vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_1__.required
      }
    }
  },
  data: function data() {
    return {
      submitStatus: "",
      currentClassificationData: {
        category: null,
        family: null,
        mark: null,
        specialty: null,
        status: null,
        origin: null
      },
      categories: [],
      families: [],
      marks: [],
      specialties: [],
      status: [],
      origins: []
    };
  },
  mounted: function mounted() {
    this.getCategories();
    this.getFamilies();
    this.getMarks();
    this.getSpecialties();
    this.getStatus();
    this.getCountries();
    if (this.product != null) this.fillProduct(this.product);
  },
  methods: {
    /**
     * Allows do submit if data is valid.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-03-29
     * @param
     * @return void
     */
    submit: function submit() {
      var _this = this;

      this.$v.$touch();

      if (this.$v.$invalid) {
        this.submitStatus = "ERROR";
      } else if (!this.$v.$invalid) {
        this.$emit("changed", [2, this.currentClassificationData]);
      } else {
        this.submitStatus = "PENDING";
        setTimeout(function () {
          _this.submitStatus = "OK";
        }, 500);
      }
    },

    /**
     * Get categories.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-03-26
     * @param
     * @return void
     */
    getCategories: function getCategories() {
      var _this2 = this;

      axios.get("/api/product/categories").then(function (result) {
        _this2.categories = result.data.categories;
      })["catch"](function (error) {
        swal2.fire({
          icon: "error",
          title: "Oops...",
          text: "Algo salió mal, vuelve a intentarlo!"
        });
      });
    },

    /**
     * Get families.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-03-29
     * @param
     * @return void
     */
    getFamilies: function getFamilies() {
      var _this3 = this;

      axios.get("/api/product/families").then(function (result) {
        _this3.families = result.data.families;
      })["catch"](function (error) {
        swal2.fire({
          icon: "error",
          title: "Oops...",
          text: "Algo salió mal, vuelve a intentarlo!"
        });
      });
    },

    /**
     * Get marks.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-03-29
     * @param
     * @return void
     */
    getMarks: function getMarks() {
      var _this4 = this;

      axios.get("/api/product/marks").then(function (result) {
        _this4.marks = result.data.marks;
      })["catch"](function (error) {
        swal2.fire({
          icon: "error",
          title: "Oops...",
          text: "Algo salió mal, vuelve a intentarlo!"
        });
      });
    },

    /**
     * Get specialties.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-03-29
     * @param
     * @return void
     */
    getSpecialties: function getSpecialties() {
      var _this5 = this;

      axios.get("/api/product/specialties").then(function (result) {
        _this5.specialties = result.data.specialties;
      })["catch"](function (error) {
        swal2.fire({
          icon: "error",
          title: "Oops...",
          text: "Algo salió mal, vuelve a intentarlo!"
        });
      });
    },

    /**
     * Get status.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-03-29
     * @param
     * @return void
     */
    getStatus: function getStatus() {
      var _this6 = this;

      axios.get("/api/product/status").then(function (result) {
        _this6.status = result.data.status;
      })["catch"](function (error) {
        swal2.fire({
          icon: "error",
          title: "Oops...",
          text: "Algo salió mal, vuelve a intentarlo!"
        });
      });
    },

    /**
     * Get countries.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-03-29
     * @param
     * @return void
     */
    getCountries: function getCountries() {
      var _this7 = this;

      axios.get("/api/product/countries").then(function (result) {
        _this7.origins = result.data.countries;
      })["catch"](function (error) {
        swal2.fire({
          icon: "error",
          title: "Oops...",
          text: "Algo salió mal, vuelve a intentarlo!"
        });
      });
    },

    /**
     * Redirect to index products.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-03-29
     * @param
     * @return void
     */
    cancel: function cancel() {
      this.$router.push({
        name: "products"
      });
    },

    /**
     * Fill in the currentClassificationData if a particular product is to be edited or displayed.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-04-16
     * @param
     * @return void
     */
    fillProduct: function fillProduct(product) {
      this.currentClassificationData.category = product.Id_Categoria;
      this.currentClassificationData.family = product.Id_Familia;
      this.currentClassificationData.mark = product.Id_Marca;
      this.currentClassificationData.specialty = product.Id_EspMed;
      this.currentClassificationData.status = product.Id_estatus;
      this.currentClassificationData.origin = product.Origen;
    }
  },
  computed: {
    /**
     * Evaluate if an input is valid.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-03-29
     * @param
     * @return array
     */
    categoryErrors: function categoryErrors() {
      var errors = [];
      if (!this.$v.currentClassificationData.category.$dirty) return errors;
      !this.$v.currentClassificationData.category.required && errors.push("El campo es requerido");
      return errors;
    },

    /**
     * Evaluate if an input is valid.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-03-29
     * @param
     * @return array
     */
    familyErrors: function familyErrors() {
      var errors = [];
      if (!this.$v.currentClassificationData.family.$dirty) return errors;
      !this.$v.currentClassificationData.family.required && errors.push("El campo es requerido");
      return errors;
    },

    /**
     * Evaluate if an input is valid.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-03-29
     * @param
     * @return array
     */
    markErrors: function markErrors() {
      var errors = [];
      if (!this.$v.currentClassificationData.mark.$dirty) return errors;
      !this.$v.currentClassificationData.mark.required && errors.push("El campo es requerido");
      return errors;
    },

    /**
     * Evaluate if an input is valid.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-03-29
     * @param
     * @return array
     */
    specialtyErrors: function specialtyErrors() {
      var errors = [];
      if (!this.$v.currentClassificationData.specialty.$dirty) return errors;
      !this.$v.currentClassificationData.specialty.required && errors.push("El campo es requerido");
      return errors;
    },

    /**
     * Evaluate if an input is valid.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-03-29
     * @param
     * @return array
     */
    statusErrors: function statusErrors() {
      var errors = [];
      if (!this.$v.currentClassificationData.status.$dirty) return errors;
      !this.$v.currentClassificationData.status.required && errors.push("El campo es requerido");
      return errors;
    },

    /**
     * Evaluate if an input is valid.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-03-29
     * @param
     * @return array
     */
    originErrors: function originErrors() {
      var errors = [];
      if (!this.$v.currentClassificationData.origin.$dirty) return errors;
      !this.$v.currentClassificationData.origin.required && errors.push("El campo es requerido");
      return errors;
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/products/partials/DataSheetForm.vue?vue&type=script&lang=js&":
/*!*******************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/products/partials/DataSheetForm.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _Uploade__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Uploade */ "./resources/js/views/product-configuration/products/partials/Uploade.vue");
/* harmony import */ var vuelidate__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! vuelidate */ "./node_modules/vuelidate/lib/index.js");
/* harmony import */ var vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! vuelidate/lib/validators */ "./node_modules/vuelidate/lib/validators/index.js");
/* harmony import */ var _addLocationModal_vue__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./addLocationModal.vue */ "./resources/js/views/product-configuration/products/partials/addLocationModal.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//




/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  name: "DataSheetForm",
  props: ["product", "inputsStatus", "type"],
  components: {
    Upload: _Uploade__WEBPACK_IMPORTED_MODULE_0__.default,
    "add-location-modal": _addLocationModal_vue__WEBPACK_IMPORTED_MODULE_1__.default
  },
  mixins: [vuelidate__WEBPACK_IMPORTED_MODULE_2__.validationMixin],
  validations: {
    currentDataSheetData: {
      technicalSpecification: {
        maxLength: (0,vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_3__.maxLength)(300)
      },
      indications: {
        maxLength: (0,vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_3__.maxLength)(300)
      }
    }
  },
  mounted: function mounted() {
    if (this.product != null) this.fillProduct(this.product);
    this.currentDataSheetData.locations = this.locations = this.product.locations.map(function (item) {
      return {
        rack: {
          Id: item.seccion.nivel.rack.Id,
          Nombre: item.seccion.nivel.rack.Nombre
        },
        nivel: {
          ID: item.seccion.nivel.ID,
          Nombre: item.seccion.nivel.Nombre
        },
        seccion: {
          Id: item.seccion.Id,
          Nombre: item.seccion.Nombre
        }
      };
    });
  },
  data: function data() {
    return {
      submitStatus: "",
      uploadDialog: false,
      imagesToEdit: [],
      currentDataSheetData: {
        weight: null,
        width: null,
        high: null,
        length: null,
        locations: [],
        technicalSpecification: null,
        indications: null,
        images: []
      },
      search: '',
      page: 1,
      itemsPerPage: 5,
      loading: false,
      locations: [],
      locationsTableHeaders: [{
        text: "Rack",
        value: "rack.Nombre",
        align: 'start'
      }, {
        text: "Nivel",
        value: "nivel.Nombre"
      }, {
        text: "Rack",
        value: "seccion.Nombre"
      }, {
        text: "Acciones",
        value: "actions",
        orderable: false
      }],
      pageCount: 0,
      showAddLocationModal: false
    };
  },
  methods: {
    /**
     * Removes a location from this.locations
     *
     * @author Giovanny González <giovanny.gonzalez@nuvem.mx>
     * created 15/06/2021
     * @params Object item
     * @return void
     *
     */
    removeLocation: function removeLocation(item) {
      this.locations.splice(this.locations.indexOf(item), 1);
    },

    /**
     * Adds a new location
     *
     * @author Giovanny González <giovanny.gonzalez@nuvem.mx>
     * created 15/06/2021
     * @params boolean closeModal
     * @return void
     *
     */
    addLocation: function addLocation(item, closeModal) {
      var location = Object.assign({}, item);

      if (this.locations.length < 1) {
        this.locations.push(location);
        this.currentDataSheetData.locations.push(location);
        return;
      }

      var found = this.locations.find(function (obj) {
        return obj.rack.Id == location.rack.Id && obj.nivel.ID == location.nivel.ID && obj.seccion.Id == location.seccion.Id;
      });

      if (!found) {
        this.locations.push(location);
      }

      if (closeModal) this.showAddLocationModal = false;
    },

    /**
     * Close add location modal
     *
     * @author Giovanny González <giovanny.gonzalez@nuvem.mx>
     * created 14/06/2021
     * @params
     * @return void
     *
     */
    closeAddLocationModal: function closeAddLocationModal() {
      this.showAddLocationModal = false;
    },

    /**
     * Open delete dialog
     *
     * @author Giovanny González <giovanny.gonzalez@nuvem.mx>
     * created 14/06/2021
     * @params
     * @return void
     *
     */
    openDeleteDialog: function openDeleteDialog() {},

    /**
     * Add a new location
     *
     * @author Giovanny González <giovanny.gonzalez@nuvem.mx>
     * created 14/06/2021
     * @params
     * @return void
     *
     */
    openAddLocationModal: function openAddLocationModal() {
      this.showAddLocationModal = true;
    },

    /**
     * Allows do submit if data is valid.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-03-29
     * @param
     * @return void
     */
    submit: function submit() {
      var _this = this;

      this.$v.$touch();

      if (this.$v.$invalid) {
        this.submitStatus = "ERROR";
      } else if (!this.$v.$invalid) {
        this.$refs.getImages.returnImages();
        this.$emit("changed", [0, this.currentDataSheetData]);
      } else {
        this.submitStatus = "PENDING";
        setTimeout(function () {
          _this.submitStatus = "OK";
        }, 500);
      }
    },

    /**
     * Redirect to index products.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-03-29
     * @param
     * @return void
     */
    cancel: function cancel() {
      this.$router.push({
        name: 'products'
      });
    },

    /**
     * Fill in the currentDataSheetData.images attay.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-04-16
     * @param
     * @return void
     */
    fillImages: function fillImages(images) {
      this.currentDataSheetData.images = images;
    },

    /**
     * Fill in the currentDataSheetData if a particular product is to be edited or displayed.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-04-16
     * @param
     * @return void
     */
    fillProduct: function fillProduct(product) {
      var _this2 = this;

      this.currentDataSheetData.weight = product.Peso;
      this.currentDataSheetData.width = product.Ancho;
      this.currentDataSheetData.high = product.Alto;
      this.currentDataSheetData.length = product.Largo;
      this.currentDataSheetData.technicalSpecification = product.Especificacion;
      this.currentDataSheetData.indications = product.Indicacion;

      if (product.images.length > 0) {
        product.images.forEach(function (image) {
          _this2.imagesToEdit.push(image);
        });
      } else this.imagesToEdit = [];
    }
  },
  computed: {
    /**
     * Evaluate if an input is valid.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-03-29
     * @param
     * @return array
     */
    technicalSpecificationErrors: function technicalSpecificationErrors() {
      var errors = [];
      if (!this.$v.currentDataSheetData.technicalSpecification.$dirty) return errors;
      !this.$v.currentDataSheetData.technicalSpecification.maxLength && errors.push("El campo debería contener máximo 300 caracteres");
      return errors;
    },

    /**
     * Evaluate if an input is valid.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-03-29
     * @param
     * @return array
     */
    indicationsErrors: function indicationsErrors() {
      var errors = [];
      if (!this.$v.currentDataSheetData.indications.$dirty) return errors;
      !this.$v.currentDataSheetData.indications.maxLength && errors.push("El campo debería contener máximo 300 caracteres");
      return errors;
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/products/partials/GeneralForm.vue?vue&type=script&lang=js&":
/*!*****************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/products/partials/GeneralForm.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var vuelidate__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vuelidate */ "./node_modules/vuelidate/lib/index.js");
/* harmony import */ var vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vuelidate/lib/validators */ "./node_modules/vuelidate/lib/validators/index.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  name: "GeneralForm",
  mixins: [vuelidate__WEBPACK_IMPORTED_MODULE_0__.validationMixin],
  validations: {
    currentGeneralData: {
      Id_Prod1: {
        required: vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_1__.required
      },
      bidKey: {
        required: vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_1__.required
      }
    }
  },
  props: ["product", "inputsStatus", "title", "type"],
  data: function data() {
    return {
      submitStatus: "",
      currentGeneralData: {
        Id_Prod1: "",
        bidKey: "",
        barCode: null,
        satCode: null,
        shortDescription: null,
        longDescription: null,
        supplier1: null,
        supplier2: null,
        inventoryable: false,
        inPriceList: false,
        applyExpiration: false,
        active: false,
        eCommerce: false,
        observations: null,
        healthRegistration: null,
        iva: null,
        minimumBid: null,
        freight: null,
        timeAssortment: null
      },
      suppliers: []
    };
  },
  mounted: function mounted() {
    this.getSuppliers();
    if (this.product != null) this.fillProduct(this.product);
  },
  methods: {
    /**
     * Allows do submit if data is valid.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-03-26
     * @param
     * @return void
     */
    submit: function submit() {
      var _this = this;

      this.$v.$touch();

      if (this.$v.$invalid) {
        this.submitStatus = "ERROR";
      } else if (!this.$v.$invalid) {
        this.$emit("changed", [1, this.currentGeneralData, true]);
      } else {
        this.submitStatus = "PENDING";
        setTimeout(function () {
          _this.submitStatus = "OK";
        }, 500);
      }
    },

    /**
     * Get ther user's general data.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-03-26
     * @param
     * @return void
     */
    getSuppliers: function getSuppliers() {
      var _this2 = this;

      axios.get("/api/product/suppliers").then(function (result) {
        _this2.suppliers = result.data.suppliers;
      })["catch"](function (error) {
        swal2.fire({
          icon: "error",
          title: "Oops...",
          text: "Algo salió mal, vuelve a intentarlo!"
        });
      });
    },

    /**
     * Redirect to index products.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-03-29
     * @param
     * @return void
     */
    cancel: function cancel() {
      this.$router.push({
        name: 'products'
      });
    },

    /**
     * Fill in the currentGeneralData if a particular product is to be edited or displayed.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-04-16
     * @param
     * @return void
     */
    fillProduct: function fillProduct(product) {
      this.currentGeneralData.Id_Prod1 = product.Id_Prod1;
      this.currentGeneralData.bidKey = product.Id_Prod3;
      this.currentGeneralData.barCode = product.CBarrasN1;
      this.currentGeneralData.satCode = product.Id_CodSat;
      this.currentGeneralData.shortDescription = product.Descrip1;
      this.currentGeneralData.longDescription = product.Descrip2;
      product.Id_Prov1 != null ? this.currentGeneralData.supplier1 = product.Id_Prov1.replace(/\s+/g, '') : this.currentGeneralData.supplier1 = null;
      product.Id_Prov2 != null ? this.currentGeneralData.supplier2 = product.Id_Prov2.replace(/\s+/g, '') : this.currentGeneralData.supplier2 = null;
      this.currentGeneralData.inventoryable = product.Inventaria;
      this.currentGeneralData.inPriceList = product.LPrecios;
      this.currentGeneralData.applyExpiration = product.Man_LoteCad;
      this.currentGeneralData.active = product.Activo;
      this.currentGeneralData.eCommerce = product.eCommerce;
      this.currentGeneralData.observations = product.Observa;
      this.currentGeneralData.healthRegistration = product.Registro;
      this.currentGeneralData.iva = product.IVA;
      this.currentGeneralData.minimumBid = product.Utmin_Lici;
      this.currentGeneralData.freight = product.Flete;
      this.currentGeneralData.timeAssortment = product.Tiempo_Sur;
    }
  },
  computed: {
    /**
     * Evaluate if an input is valid.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-03-26
     * @param
     * @return array
     */
    nemotecnicaKeyErrors: function nemotecnicaKeyErrors() {
      var errors = [];
      if (!this.$v.currentGeneralData.Id_Prod1.$dirty) return errors;
      !this.$v.currentGeneralData.Id_Prod1.required && errors.push("El campo es requerido");
      return errors;
    },

    /**
     * Evaluate if an input is valid.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-03-26
     * @param
     * @return array
     */
    bidKeyErrors: function bidKeyErrors() {
      var errors = [];
      if (!this.$v.currentGeneralData.bidKey.$dirty) return errors;
      !this.$v.currentGeneralData.bidKey.required && errors.push("El campo es requerido");
      return errors;
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/products/partials/StatisticsForm.vue?vue&type=script&lang=js&":
/*!********************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/products/partials/StatisticsForm.vue?vue&type=script&lang=js& ***!
  \********************************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  name: "StatisticsForm",
  props: ["product", "inputsStatus", "type"],
  mounted: function mounted() {
    if (this.product != null) this.fillProduct(this.product);
  },
  data: function data() {
    return {
      date: new Date().toISOString().substr(0, 10),
      menu1: false,
      menu2: false,
      menu3: false,
      menu4: false,
      menu5: false,
      submitStatus: "",
      currentStatisticsData: {
        lastCost: null,
        averageCost: null,
        lowerMarketCost: null,
        lastMereCost: null,
        lastLiciCost: null,
        lastChangePrices: null,
        lastPurchase: null,
        lastSale: null
      }
    };
  },
  methods: {
    /**
     * Allows do submit if data is valid.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-03-29
     * @param
     * @return void
     */
    submit: function submit() {
      this.$emit("changed", [4, this.currentStatisticsData]);
    },

    /**
     * Redirect to index products.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-03-29
     * @param
     * @return void
     */
    cancel: function cancel() {
      this.$router.push({
        name: 'products'
      });
    },

    /**
     * Fill in the currentStatisticsData if a particular product is to be edited or displayed.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-04-16
     * @param
     * @return void
     */
    fillProduct: function fillProduct(product) {
      this.currentStatisticsData.lastCost = product.Ult_Costo;
      this.currentStatisticsData.averageCost = product.Costo_Prom;
      this.currentStatisticsData.lowerMarketCost = product.CM_MasBajo;
      this.currentStatisticsData.lastMereCost = product.UCambio_CM;
      this.currentStatisticsData.lastLiciCost = product.UCambio_CL;
      this.currentStatisticsData.lastChangePrices = product.UCambio_Pre;
      this.currentStatisticsData.lastPurchase = product.Ult_Compra;
      this.currentStatisticsData.lastSale = product.Ult_Venta;
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/products/partials/StorageForm.vue?vue&type=script&lang=js&":
/*!*****************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/products/partials/StorageForm.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  name: "StorageForm",
  props: ["product", "inputsStatus", "type"],
  data: function data() {
    return {
      submitStatus: "",
      levels: [{
        id: 1,
        name: '1'
      }, {
        id: 2,
        name: '2'
      }, {
        id: 3,
        name: '3'
      }],
      currentStorageData: {
        level: null,
        packing1: null,
        packing2: null,
        packing3: null,
        units1: null,
        units2: null,
        units3: null,
        purchaseLevel: null,
        saleLevel: null,
        saleLevelMost: null
      },
      packaging: []
    };
  },
  mounted: function mounted() {
    this.getPackaging();
    if (this.product != null) this.fillProduct(this.product);
  },
  methods: {
    /**
     * Get packaging.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-03-29
     * @param
     * @return void
     */
    getPackaging: function getPackaging() {
      var _this = this;

      axios.get("/api/product/packaging").then(function (result) {
        _this.packaging = result.data.packaging;
      })["catch"](function (error) {
        swal2.fire({
          icon: "error",
          title: "Oops...",
          text: "Algo salió mal, vuelve a intentarlo!"
        });
      });
    },

    /**
     * Allows do submit if data is valid.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-03-29
     * @param
     * @return void
     */
    submit: function submit() {
      this.$emit("changed", [3, this.currentStorageData]);
    },

    /**
     * Redirect to index products.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-04-29
     * @param
     * @return void
     */
    cancel: function cancel() {
      this.$router.push({
        name: 'products'
      });
    },

    /**
     * Fill in the current currentStorageData if a particular product is to be edited or displayed.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-04-16
     * @param
     * @return void
     */
    fillProduct: function fillProduct(product) {
      this.currentStorageData.level = parseInt(product.Niveles);
      this.currentStorageData.packing1 = product.id_Empaque1;
      this.currentStorageData.packing2 = product.id_Empaque2;
      this.currentStorageData.packing3 = product.id_Empaque3;
      this.currentStorageData.units1 = product.Unidad_N2;
      this.currentStorageData.units2 = product.Unidad_N3;
      this.currentStorageData.purchaseLevel = parseInt(product.Nivel_Comp);
      this.currentStorageData.saleLevel = parseInt(product.Nivel_Vta);
      this.currentStorageData.saleLevelMost = parseInt(product.Nivel_Vta2);
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/products/partials/Uploade.vue?vue&type=script&lang=js&":
/*!*************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/products/partials/Uploade.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  name: "Upload",
  props: ["imagesToEdit"],
  delimiters: ['${', '}'],
  // Avoid Twig conflicts
  mounted: function mounted() {
    var _this = this;

    if (this.imagesToEdit) {
      this.imagesToEdit.forEach(function (image) {
        image.url = '../storage/products/' + image.Foto;

        _this.fileList.push(image);
      });
    }
  },
  data: function data() {
    return {
      fileList: [],
      changeOfImages: false
    };
  },
  methods: {
    /**
     * Adds images to fileList array and generate url to show preview.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-04-15
     * @param event
     * @return array
     */
    onChange: function onChange() {
      if (this.$refs.file.files.length > 3) {
        swal2.fire({
          icon: "error",
          title: "Oops...",
          text: "Solo se puede subir un máximo de 3 imágenes por producto!"
        });
      } else {
        this.fileList = _toConsumableArray(this.$refs.file.files);
        this.fileList.forEach(function (file) {
          file.url = URL.createObjectURL(file);
        });
        this.changeOfImages = true;
      }
    },

    /**
     * Remove the image from fileList.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-04-15
     * @param Number i
     * @return array
     */
    remove: function remove(i) {
      this.fileList.splice(i, 1);
      this.fileList.length > 0 ? this.changeOfImages = true : this.changeOfImages = false;
    },

    /**
     * Return images if new ones were added.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-04-15
     * @param Number i
     * @return array
     */
    returnImages: function returnImages() {
      this.changeOfImages ? this.$emit("images", this.fileList) : this.$emit("images", null);
    },

    /**
     * Action when drag over the image.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-04-15
     * @param event
     * @return array
     */
    dragover: function dragover(event) {
      event.preventDefault(); // Add some visual fluff to show the user can drop its files

      if (!event.currentTarget.classList.contains('bg-green-300')) {
        event.currentTarget.classList.remove('bg-gray-100');
        event.currentTarget.classList.add('bg-green-300');
      }
    },

    /**
     * Action when drag leave the image.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-04-15
     * @param event
     * @return array
     */
    dragleave: function dragleave(event) {
      // Clean up
      event.currentTarget.classList.add('bg-gray-100');
      event.currentTarget.classList.remove('bg-green-300');
    },

    /**
     * Action when drop the image.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-04-15
     * @param event
     * @return array
     */
    drop: function drop(event) {
      event.preventDefault();
      this.$refs.file.files = event.dataTransfer.files;
      this.onChange(); // Trigger the onChange event manually
      // Clean up

      event.currentTarget.classList.add('bg-gray-100');
      event.currentTarget.classList.remove('bg-green-300');
    }
  },
  watch: {
    /**
     * Asign a new value to imagesToEdit if there is a new value.
     * @auth José Vega <jose.vega@nuvem.mx>
     * @date 2021-04-16
     * @param any images
     * @return void
     */
    imagesToEdit: function imagesToEdit(images) {
      var _this2 = this;

      if (images) {
        images.forEach(function (image) {
          image.url = image.Foto;

          _this2.fileList.push(image);
        });
      }
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/products/partials/addLocationModal.vue?vue&type=script&lang=js&":
/*!**********************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/products/partials/addLocationModal.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var vuelidate__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vuelidate */ "./node_modules/vuelidate/lib/index.js");
/* harmony import */ var vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vuelidate/lib/validators */ "./node_modules/vuelidate/lib/validators/index.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  props: {
    status: Boolean
  },
  mixins: [vuelidate__WEBPACK_IMPORTED_MODULE_0__.validationMixin],
  validations: {
    editedItem: {
      rack: {
        required: vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_1__.required
      },
      nivel: {
        required: vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_1__.required
      },
      seccion: {
        required: vuelidate_lib_validators__WEBPACK_IMPORTED_MODULE_1__.required
      }
    }
  },
  created: function created() {
    Object.assign(this.editedItem, {
      rack: null,
      nivel: null,
      seccion: null
    });
    this.racks = this.niveles = this.secciones = [];
    this.getRackSelectsOptions();
  },
  data: function data() {
    return {
      editedItem: {
        rack: null,
        nivel: null,
        seccion: null
      },
      racks: [],
      niveles: [],
      secciones: [],
      loadingNivelSelect: false,
      loadingSeccionSelect: false
    };
  },
  computed: {
    rackErrors: function rackErrors() {},
    nivelErrors: function nivelErrors() {},
    seccionErrors: function seccionErrors() {}
  },
  watch: {
    'editedItem.rack': function editedItemRack() {
      var _this = this;

      this.loadingNivelSelect = true;
      axios.get("/api/products/add-location/rack/".concat(this.editedItem.rack.Id, "/get-nivel-select-options")).then(function (res) {
        _this.niveles = res.data;
        _this.loadingNivelSelect = false;
      })["finally"](function () {
        _this.loadingNivelSelect = false;
      });
    },
    'editedItem.nivel': function editedItemNivel() {
      var _this2 = this;

      this.loadingSeccionSelect = true;
      axios.get("/api/products/add-location/nivel/".concat(this.editedItem.nivel.Id, "/get-seccion-select-options")).then(function (res) {
        _this2.secciones = res.data;
        _this2.loadingSeccionSelect = false;
      })["finally"](function () {
        _this2.loadingSeccionSelect = false;
      });
    }
  },
  methods: {
    /**
     * gets form's selects options
     *
     * @author Giovanny González <giovanny.gonzalez@nuvem.mx>
     * created 14/06/2021
     * @params
     * @return void
     *
     */
    getRackSelectsOptions: function getRackSelectsOptions() {
      var _this3 = this;

      axios.get('/api/products/add-location/get-rack-select-options').then(function (res) {
        _this3.racks = res.data;
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/products/edit.vue?vue&type=style&index=0&lang=css&":
/*!***********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/products/edit.vue?vue&type=style&index=0&lang=css& ***!
  \***********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ ((module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../../../node_modules/css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js");
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0__);
// Imports

var ___CSS_LOADER_EXPORT___ = _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0___default()(function(i){return i[1]});
// Module
___CSS_LOADER_EXPORT___.push([module.id, "\n.v-tab--active {\n  background-color: #018085 !important;\n}\n", ""]);
// Exports
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (___CSS_LOADER_EXPORT___);


/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/products/partials/Uploade.vue?vue&type=style&index=0&lang=css&":
/*!***********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/products/partials/Uploade.vue?vue&type=style&index=0&lang=css& ***!
  \***********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ ((module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../../../../node_modules/css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js");
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0__);
// Imports

var ___CSS_LOADER_EXPORT___ = _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0___default()(function(i){return i[1]});
// Module
___CSS_LOADER_EXPORT___.push([module.id, "\n[v-cloak] {\n    display: none;\n}\n.dropAndDragInput{\n    opacity: 0;\n    position: absolute;\n    overflow: hidden;\n}\n.dropAndDrag{\n    border-radius: 5px;\n    border: 1px solid #9e9e9e !important;\n    height: 150px;\n}\n.dropAndDragLabel{\n  margin-top: 60px;\n}\nul {\n    list-style-type: none;\n    display: flex;\n    justify-content: center;\n    place-content: space-around;\n}\nli{\n  max-width: 200px;\n}\n.showImage{\n    width: 100%;\n    margin: auto;\n}\n", ""]);
// Exports
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (___CSS_LOADER_EXPORT___);


/***/ }),

/***/ "./resources/js/views/product-configuration/products/edit.vue":
/*!********************************************************************!*\
  !*** ./resources/js/views/product-configuration/products/edit.vue ***!
  \********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _edit_vue_vue_type_template_id_36b78f22___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./edit.vue?vue&type=template&id=36b78f22& */ "./resources/js/views/product-configuration/products/edit.vue?vue&type=template&id=36b78f22&");
/* harmony import */ var _edit_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./edit.vue?vue&type=script&lang=js& */ "./resources/js/views/product-configuration/products/edit.vue?vue&type=script&lang=js&");
/* harmony import */ var _edit_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./edit.vue?vue&type=style&index=0&lang=css& */ "./resources/js/views/product-configuration/products/edit.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! !../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");



;


/* normalize component */

var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__.default)(
  _edit_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__.default,
  _edit_vue_vue_type_template_id_36b78f22___WEBPACK_IMPORTED_MODULE_0__.render,
  _edit_vue_vue_type_template_id_36b78f22___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/product-configuration/products/edit.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/js/views/product-configuration/products/partials/ClassificationForm.vue":
/*!*******************************************************************************************!*\
  !*** ./resources/js/views/product-configuration/products/partials/ClassificationForm.vue ***!
  \*******************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _ClassificationForm_vue_vue_type_template_id_8ac5409c___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ClassificationForm.vue?vue&type=template&id=8ac5409c& */ "./resources/js/views/product-configuration/products/partials/ClassificationForm.vue?vue&type=template&id=8ac5409c&");
/* harmony import */ var _ClassificationForm_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ClassificationForm.vue?vue&type=script&lang=js& */ "./resources/js/views/product-configuration/products/partials/ClassificationForm.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! !../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */
;
var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__.default)(
  _ClassificationForm_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__.default,
  _ClassificationForm_vue_vue_type_template_id_8ac5409c___WEBPACK_IMPORTED_MODULE_0__.render,
  _ClassificationForm_vue_vue_type_template_id_8ac5409c___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/product-configuration/products/partials/ClassificationForm.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/js/views/product-configuration/products/partials/DataSheetForm.vue":
/*!**************************************************************************************!*\
  !*** ./resources/js/views/product-configuration/products/partials/DataSheetForm.vue ***!
  \**************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _DataSheetForm_vue_vue_type_template_id_cfb61bfe___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./DataSheetForm.vue?vue&type=template&id=cfb61bfe& */ "./resources/js/views/product-configuration/products/partials/DataSheetForm.vue?vue&type=template&id=cfb61bfe&");
/* harmony import */ var _DataSheetForm_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./DataSheetForm.vue?vue&type=script&lang=js& */ "./resources/js/views/product-configuration/products/partials/DataSheetForm.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! !../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */
;
var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__.default)(
  _DataSheetForm_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__.default,
  _DataSheetForm_vue_vue_type_template_id_cfb61bfe___WEBPACK_IMPORTED_MODULE_0__.render,
  _DataSheetForm_vue_vue_type_template_id_cfb61bfe___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/product-configuration/products/partials/DataSheetForm.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/js/views/product-configuration/products/partials/GeneralForm.vue":
/*!************************************************************************************!*\
  !*** ./resources/js/views/product-configuration/products/partials/GeneralForm.vue ***!
  \************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _GeneralForm_vue_vue_type_template_id_749c2c14___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./GeneralForm.vue?vue&type=template&id=749c2c14& */ "./resources/js/views/product-configuration/products/partials/GeneralForm.vue?vue&type=template&id=749c2c14&");
/* harmony import */ var _GeneralForm_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./GeneralForm.vue?vue&type=script&lang=js& */ "./resources/js/views/product-configuration/products/partials/GeneralForm.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! !../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */
;
var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__.default)(
  _GeneralForm_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__.default,
  _GeneralForm_vue_vue_type_template_id_749c2c14___WEBPACK_IMPORTED_MODULE_0__.render,
  _GeneralForm_vue_vue_type_template_id_749c2c14___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/product-configuration/products/partials/GeneralForm.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/js/views/product-configuration/products/partials/StatisticsForm.vue":
/*!***************************************************************************************!*\
  !*** ./resources/js/views/product-configuration/products/partials/StatisticsForm.vue ***!
  \***************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _StatisticsForm_vue_vue_type_template_id_27f41022___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./StatisticsForm.vue?vue&type=template&id=27f41022& */ "./resources/js/views/product-configuration/products/partials/StatisticsForm.vue?vue&type=template&id=27f41022&");
/* harmony import */ var _StatisticsForm_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./StatisticsForm.vue?vue&type=script&lang=js& */ "./resources/js/views/product-configuration/products/partials/StatisticsForm.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! !../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */
;
var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__.default)(
  _StatisticsForm_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__.default,
  _StatisticsForm_vue_vue_type_template_id_27f41022___WEBPACK_IMPORTED_MODULE_0__.render,
  _StatisticsForm_vue_vue_type_template_id_27f41022___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/product-configuration/products/partials/StatisticsForm.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/js/views/product-configuration/products/partials/StorageForm.vue":
/*!************************************************************************************!*\
  !*** ./resources/js/views/product-configuration/products/partials/StorageForm.vue ***!
  \************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _StorageForm_vue_vue_type_template_id_021622a7___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./StorageForm.vue?vue&type=template&id=021622a7& */ "./resources/js/views/product-configuration/products/partials/StorageForm.vue?vue&type=template&id=021622a7&");
/* harmony import */ var _StorageForm_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./StorageForm.vue?vue&type=script&lang=js& */ "./resources/js/views/product-configuration/products/partials/StorageForm.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! !../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */
;
var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__.default)(
  _StorageForm_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__.default,
  _StorageForm_vue_vue_type_template_id_021622a7___WEBPACK_IMPORTED_MODULE_0__.render,
  _StorageForm_vue_vue_type_template_id_021622a7___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/product-configuration/products/partials/StorageForm.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/js/views/product-configuration/products/partials/Uploade.vue":
/*!********************************************************************************!*\
  !*** ./resources/js/views/product-configuration/products/partials/Uploade.vue ***!
  \********************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _Uploade_vue_vue_type_template_id_5ba944cc___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Uploade.vue?vue&type=template&id=5ba944cc& */ "./resources/js/views/product-configuration/products/partials/Uploade.vue?vue&type=template&id=5ba944cc&");
/* harmony import */ var _Uploade_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Uploade.vue?vue&type=script&lang=js& */ "./resources/js/views/product-configuration/products/partials/Uploade.vue?vue&type=script&lang=js&");
/* harmony import */ var _Uploade_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Uploade.vue?vue&type=style&index=0&lang=css& */ "./resources/js/views/product-configuration/products/partials/Uploade.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! !../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");



;


/* normalize component */

var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__.default)(
  _Uploade_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__.default,
  _Uploade_vue_vue_type_template_id_5ba944cc___WEBPACK_IMPORTED_MODULE_0__.render,
  _Uploade_vue_vue_type_template_id_5ba944cc___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/product-configuration/products/partials/Uploade.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/js/views/product-configuration/products/partials/addLocationModal.vue":
/*!*****************************************************************************************!*\
  !*** ./resources/js/views/product-configuration/products/partials/addLocationModal.vue ***!
  \*****************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _addLocationModal_vue_vue_type_template_id_453ed81f___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./addLocationModal.vue?vue&type=template&id=453ed81f& */ "./resources/js/views/product-configuration/products/partials/addLocationModal.vue?vue&type=template&id=453ed81f&");
/* harmony import */ var _addLocationModal_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./addLocationModal.vue?vue&type=script&lang=js& */ "./resources/js/views/product-configuration/products/partials/addLocationModal.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! !../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */
;
var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__.default)(
  _addLocationModal_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__.default,
  _addLocationModal_vue_vue_type_template_id_453ed81f___WEBPACK_IMPORTED_MODULE_0__.render,
  _addLocationModal_vue_vue_type_template_id_453ed81f___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/product-configuration/products/partials/addLocationModal.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/js/views/product-configuration/products/edit.vue?vue&type=script&lang=js&":
/*!*********************************************************************************************!*\
  !*** ./resources/js/views/product-configuration/products/edit.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_edit_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./edit.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/products/edit.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_edit_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__.default); 

/***/ }),

/***/ "./resources/js/views/product-configuration/products/partials/ClassificationForm.vue?vue&type=script&lang=js&":
/*!********************************************************************************************************************!*\
  !*** ./resources/js/views/product-configuration/products/partials/ClassificationForm.vue?vue&type=script&lang=js& ***!
  \********************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ClassificationForm_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./ClassificationForm.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/products/partials/ClassificationForm.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ClassificationForm_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__.default); 

/***/ }),

/***/ "./resources/js/views/product-configuration/products/partials/DataSheetForm.vue?vue&type=script&lang=js&":
/*!***************************************************************************************************************!*\
  !*** ./resources/js/views/product-configuration/products/partials/DataSheetForm.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_DataSheetForm_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./DataSheetForm.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/products/partials/DataSheetForm.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_DataSheetForm_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__.default); 

/***/ }),

/***/ "./resources/js/views/product-configuration/products/partials/GeneralForm.vue?vue&type=script&lang=js&":
/*!*************************************************************************************************************!*\
  !*** ./resources/js/views/product-configuration/products/partials/GeneralForm.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_GeneralForm_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./GeneralForm.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/products/partials/GeneralForm.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_GeneralForm_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__.default); 

/***/ }),

/***/ "./resources/js/views/product-configuration/products/partials/StatisticsForm.vue?vue&type=script&lang=js&":
/*!****************************************************************************************************************!*\
  !*** ./resources/js/views/product-configuration/products/partials/StatisticsForm.vue?vue&type=script&lang=js& ***!
  \****************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_StatisticsForm_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./StatisticsForm.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/products/partials/StatisticsForm.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_StatisticsForm_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__.default); 

/***/ }),

/***/ "./resources/js/views/product-configuration/products/partials/StorageForm.vue?vue&type=script&lang=js&":
/*!*************************************************************************************************************!*\
  !*** ./resources/js/views/product-configuration/products/partials/StorageForm.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_StorageForm_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./StorageForm.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/products/partials/StorageForm.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_StorageForm_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__.default); 

/***/ }),

/***/ "./resources/js/views/product-configuration/products/partials/Uploade.vue?vue&type=script&lang=js&":
/*!*********************************************************************************************************!*\
  !*** ./resources/js/views/product-configuration/products/partials/Uploade.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Uploade_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./Uploade.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/products/partials/Uploade.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Uploade_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__.default); 

/***/ }),

/***/ "./resources/js/views/product-configuration/products/partials/addLocationModal.vue?vue&type=script&lang=js&":
/*!******************************************************************************************************************!*\
  !*** ./resources/js/views/product-configuration/products/partials/addLocationModal.vue?vue&type=script&lang=js& ***!
  \******************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_addLocationModal_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./addLocationModal.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/products/partials/addLocationModal.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_addLocationModal_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__.default); 

/***/ }),

/***/ "./resources/js/views/product-configuration/products/edit.vue?vue&type=template&id=36b78f22&":
/*!***************************************************************************************************!*\
  !*** ./resources/js/views/product-configuration/products/edit.vue?vue&type=template&id=36b78f22& ***!
  \***************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_edit_vue_vue_type_template_id_36b78f22___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   "staticRenderFns": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_edit_vue_vue_type_template_id_36b78f22___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_edit_vue_vue_type_template_id_36b78f22___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./edit.vue?vue&type=template&id=36b78f22& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/products/edit.vue?vue&type=template&id=36b78f22&");


/***/ }),

/***/ "./resources/js/views/product-configuration/products/partials/ClassificationForm.vue?vue&type=template&id=8ac5409c&":
/*!**************************************************************************************************************************!*\
  !*** ./resources/js/views/product-configuration/products/partials/ClassificationForm.vue?vue&type=template&id=8ac5409c& ***!
  \**************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ClassificationForm_vue_vue_type_template_id_8ac5409c___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   "staticRenderFns": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ClassificationForm_vue_vue_type_template_id_8ac5409c___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ClassificationForm_vue_vue_type_template_id_8ac5409c___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./ClassificationForm.vue?vue&type=template&id=8ac5409c& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/products/partials/ClassificationForm.vue?vue&type=template&id=8ac5409c&");


/***/ }),

/***/ "./resources/js/views/product-configuration/products/partials/DataSheetForm.vue?vue&type=template&id=cfb61bfe&":
/*!*********************************************************************************************************************!*\
  !*** ./resources/js/views/product-configuration/products/partials/DataSheetForm.vue?vue&type=template&id=cfb61bfe& ***!
  \*********************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_DataSheetForm_vue_vue_type_template_id_cfb61bfe___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   "staticRenderFns": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_DataSheetForm_vue_vue_type_template_id_cfb61bfe___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_DataSheetForm_vue_vue_type_template_id_cfb61bfe___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./DataSheetForm.vue?vue&type=template&id=cfb61bfe& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/products/partials/DataSheetForm.vue?vue&type=template&id=cfb61bfe&");


/***/ }),

/***/ "./resources/js/views/product-configuration/products/partials/GeneralForm.vue?vue&type=template&id=749c2c14&":
/*!*******************************************************************************************************************!*\
  !*** ./resources/js/views/product-configuration/products/partials/GeneralForm.vue?vue&type=template&id=749c2c14& ***!
  \*******************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_GeneralForm_vue_vue_type_template_id_749c2c14___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   "staticRenderFns": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_GeneralForm_vue_vue_type_template_id_749c2c14___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_GeneralForm_vue_vue_type_template_id_749c2c14___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./GeneralForm.vue?vue&type=template&id=749c2c14& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/products/partials/GeneralForm.vue?vue&type=template&id=749c2c14&");


/***/ }),

/***/ "./resources/js/views/product-configuration/products/partials/StatisticsForm.vue?vue&type=template&id=27f41022&":
/*!**********************************************************************************************************************!*\
  !*** ./resources/js/views/product-configuration/products/partials/StatisticsForm.vue?vue&type=template&id=27f41022& ***!
  \**********************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_StatisticsForm_vue_vue_type_template_id_27f41022___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   "staticRenderFns": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_StatisticsForm_vue_vue_type_template_id_27f41022___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_StatisticsForm_vue_vue_type_template_id_27f41022___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./StatisticsForm.vue?vue&type=template&id=27f41022& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/products/partials/StatisticsForm.vue?vue&type=template&id=27f41022&");


/***/ }),

/***/ "./resources/js/views/product-configuration/products/partials/StorageForm.vue?vue&type=template&id=021622a7&":
/*!*******************************************************************************************************************!*\
  !*** ./resources/js/views/product-configuration/products/partials/StorageForm.vue?vue&type=template&id=021622a7& ***!
  \*******************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_StorageForm_vue_vue_type_template_id_021622a7___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   "staticRenderFns": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_StorageForm_vue_vue_type_template_id_021622a7___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_StorageForm_vue_vue_type_template_id_021622a7___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./StorageForm.vue?vue&type=template&id=021622a7& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/products/partials/StorageForm.vue?vue&type=template&id=021622a7&");


/***/ }),

/***/ "./resources/js/views/product-configuration/products/partials/Uploade.vue?vue&type=template&id=5ba944cc&":
/*!***************************************************************************************************************!*\
  !*** ./resources/js/views/product-configuration/products/partials/Uploade.vue?vue&type=template&id=5ba944cc& ***!
  \***************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Uploade_vue_vue_type_template_id_5ba944cc___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   "staticRenderFns": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Uploade_vue_vue_type_template_id_5ba944cc___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Uploade_vue_vue_type_template_id_5ba944cc___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./Uploade.vue?vue&type=template&id=5ba944cc& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/products/partials/Uploade.vue?vue&type=template&id=5ba944cc&");


/***/ }),

/***/ "./resources/js/views/product-configuration/products/partials/addLocationModal.vue?vue&type=template&id=453ed81f&":
/*!************************************************************************************************************************!*\
  !*** ./resources/js/views/product-configuration/products/partials/addLocationModal.vue?vue&type=template&id=453ed81f& ***!
  \************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_addLocationModal_vue_vue_type_template_id_453ed81f___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   "staticRenderFns": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_addLocationModal_vue_vue_type_template_id_453ed81f___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_addLocationModal_vue_vue_type_template_id_453ed81f___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./addLocationModal.vue?vue&type=template&id=453ed81f& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/products/partials/addLocationModal.vue?vue&type=template&id=453ed81f&");


/***/ }),

/***/ "./resources/js/views/product-configuration/products/edit.vue?vue&type=style&index=0&lang=css&":
/*!*****************************************************************************************************!*\
  !*** ./resources/js/views/product-configuration/products/edit.vue?vue&type=style&index=0&lang=css& ***!
  \*****************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_node_modules_css_loader_dist_cjs_js_clonedRuleSet_10_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_10_0_rules_0_use_2_node_modules_vue_loader_lib_index_js_vue_loader_options_edit_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-style-loader/index.js!../../../../../node_modules/css-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[1]!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[2]!../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./edit.vue?vue&type=style&index=0&lang=css& */ "./node_modules/vue-style-loader/index.js!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/products/edit.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_vue_style_loader_index_js_node_modules_css_loader_dist_cjs_js_clonedRuleSet_10_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_10_0_rules_0_use_2_node_modules_vue_loader_lib_index_js_vue_loader_options_edit_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_node_modules_css_loader_dist_cjs_js_clonedRuleSet_10_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_10_0_rules_0_use_2_node_modules_vue_loader_lib_index_js_vue_loader_options_edit_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ var __WEBPACK_REEXPORT_OBJECT__ = {};
/* harmony reexport (unknown) */ for(const __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_node_modules_css_loader_dist_cjs_js_clonedRuleSet_10_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_10_0_rules_0_use_2_node_modules_vue_loader_lib_index_js_vue_loader_options_edit_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== "default") __WEBPACK_REEXPORT_OBJECT__[__WEBPACK_IMPORT_KEY__] = () => _node_modules_vue_style_loader_index_js_node_modules_css_loader_dist_cjs_js_clonedRuleSet_10_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_10_0_rules_0_use_2_node_modules_vue_loader_lib_index_js_vue_loader_options_edit_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__[__WEBPACK_IMPORT_KEY__]
/* harmony reexport (unknown) */ __webpack_require__.d(__webpack_exports__, __WEBPACK_REEXPORT_OBJECT__);


/***/ }),

/***/ "./resources/js/views/product-configuration/products/partials/Uploade.vue?vue&type=style&index=0&lang=css&":
/*!*****************************************************************************************************************!*\
  !*** ./resources/js/views/product-configuration/products/partials/Uploade.vue?vue&type=style&index=0&lang=css& ***!
  \*****************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_node_modules_css_loader_dist_cjs_js_clonedRuleSet_10_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_10_0_rules_0_use_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Uploade_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/vue-style-loader/index.js!../../../../../../node_modules/css-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[1]!../../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../../node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[2]!../../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./Uploade.vue?vue&type=style&index=0&lang=css& */ "./node_modules/vue-style-loader/index.js!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/products/partials/Uploade.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_vue_style_loader_index_js_node_modules_css_loader_dist_cjs_js_clonedRuleSet_10_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_10_0_rules_0_use_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Uploade_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_node_modules_css_loader_dist_cjs_js_clonedRuleSet_10_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_10_0_rules_0_use_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Uploade_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ var __WEBPACK_REEXPORT_OBJECT__ = {};
/* harmony reexport (unknown) */ for(const __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_node_modules_css_loader_dist_cjs_js_clonedRuleSet_10_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_10_0_rules_0_use_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Uploade_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== "default") __WEBPACK_REEXPORT_OBJECT__[__WEBPACK_IMPORT_KEY__] = () => _node_modules_vue_style_loader_index_js_node_modules_css_loader_dist_cjs_js_clonedRuleSet_10_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_10_0_rules_0_use_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Uploade_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__[__WEBPACK_IMPORT_KEY__]
/* harmony reexport (unknown) */ __webpack_require__.d(__webpack_exports__, __WEBPACK_REEXPORT_OBJECT__);


/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/products/edit.vue?vue&type=template&id=36b78f22&":
/*!******************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/products/edit.vue?vue&type=template&id=36b78f22& ***!
  \******************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render),
/* harmony export */   "staticRenderFns": () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { attrs: { "data-app": "" } },
    [
      _c(
        "v-card",
        [
          _c(
            "v-card-text",
            { staticClass: "mb-2", staticStyle: { padding: "0px" } },
            [
              _c(
                "v-tabs",
                {
                  attrs: {
                    "fixed-tabs": "",
                    "background-color": "#e3e6e8",
                    "slider-color": "#018085",
                    color: "#fff",
                    "slider-size": "2"
                  },
                  model: {
                    value: _vm.tab,
                    callback: function($$v) {
                      _vm.tab = $$v
                    },
                    expression: "tab"
                  }
                },
                [
                  _c("v-tab", { on: { click: _vm.save } }, [
                    _vm._v(" General ")
                  ]),
                  _vm._v(" "),
                  _c(
                    "v-tab",
                    {
                      attrs: { disabled: _vm.tabsDisabled },
                      on: { click: _vm.save }
                    },
                    [_vm._v(" Clasificación ")]
                  ),
                  _vm._v(" "),
                  _c(
                    "v-tab",
                    {
                      attrs: { disabled: _vm.tabsDisabled },
                      on: { click: _vm.save }
                    },
                    [_vm._v(" Almacenamiento ")]
                  ),
                  _vm._v(" "),
                  _c(
                    "v-tab",
                    {
                      attrs: { disabled: _vm.tabsDisabled },
                      on: { click: _vm.save }
                    },
                    [_vm._v(" Estadística ")]
                  ),
                  _vm._v(" "),
                  _c(
                    "v-tab",
                    {
                      attrs: { disabled: _vm.tabsDisabled },
                      on: { click: _vm.save }
                    },
                    [_vm._v(" Ficha Técnica ")]
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "v-tabs-items",
                {
                  model: {
                    value: _vm.tab,
                    callback: function($$v) {
                      _vm.tab = $$v
                    },
                    expression: "tab"
                  }
                },
                [
                  _c(
                    "v-tab-item",
                    { staticClass: "mt-2" },
                    [
                      _c(
                        "v-card",
                        { attrs: { flat: "" } },
                        [
                          _c("GeneralForm", {
                            ref: "submit",
                            attrs: {
                              product: _vm.currentProduct,
                              title: "Información del Producto"
                            },
                            on: { changed: _vm.fillNewProductGeneralData }
                          })
                        ],
                        1
                      )
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "v-tab-item",
                    { staticClass: "mt-2" },
                    [
                      _c(
                        "v-card",
                        { attrs: { flat: "" } },
                        [
                          _c("ClassificationForm", {
                            ref: "submit",
                            attrs: { product: _vm.currentProduct },
                            on: {
                              changed: _vm.fillNewProductClassificationData
                            }
                          })
                        ],
                        1
                      )
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "v-tab-item",
                    { staticClass: "mt-2 justify-center" },
                    [
                      _c(
                        "v-card",
                        { attrs: { flat: "" } },
                        [
                          _c("StorageForm", {
                            ref: "submit",
                            attrs: { product: _vm.currentProduct },
                            on: { changed: _vm.fillNewProductStorageData }
                          })
                        ],
                        1
                      )
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "v-tab-item",
                    { staticClass: "mt-2" },
                    [
                      _c(
                        "v-card",
                        { attrs: { flat: "" } },
                        [
                          _c("StatisticsForm", {
                            ref: "submit",
                            attrs: { product: _vm.currentProduct },
                            on: { changed: _vm.fillNewProductStatisticsData }
                          })
                        ],
                        1
                      )
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "v-tab-item",
                    { staticClass: "mt-2" },
                    [
                      _c(
                        "v-card",
                        { attrs: { flat: "" } },
                        [
                          _c("DataSheetForm", {
                            attrs: { product: _vm.currentProduct },
                            on: { changed: _vm.fillNewProductDataSheetData }
                          })
                        ],
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/products/partials/ClassificationForm.vue?vue&type=template&id=8ac5409c&":
/*!*****************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/products/partials/ClassificationForm.vue?vue&type=template&id=8ac5409c& ***!
  \*****************************************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render),
/* harmony export */   "staticRenderFns": () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "v-card",
    { staticClass: "p-3" },
    [
      _c(
        "v-card-text",
        [
          _c(
            "v-row",
            [
              _c(
                "v-col",
                { attrs: { cols: "6", md: "4" } },
                [
                  _c("h6", [_vm._v("Categoría")]),
                  _vm._v(" "),
                  _c("v-autocomplete", {
                    attrs: {
                      readonly: _vm.inputsStatus,
                      "error-messages": _vm.categoryErrors,
                      items: _vm.categories,
                      "item-text": "Categoria",
                      "item-value": "Id_Categoria",
                      placeholder: "Selecciona una opción",
                      outlined: "",
                      dense: ""
                    },
                    on: {
                      input: function($event) {
                        return _vm.$v.currentClassificationData.category.$touch()
                      },
                      blur: function($event) {
                        return _vm.$v.currentClassificationData.category.$touch()
                      }
                    },
                    model: {
                      value: _vm.currentClassificationData.category,
                      callback: function($$v) {
                        _vm.$set(_vm.currentClassificationData, "category", $$v)
                      },
                      expression: "currentClassificationData.category"
                    }
                  })
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "v-col",
                { attrs: { cols: "6", sm: "4" } },
                [
                  _c("h6", [_vm._v("Familia")]),
                  _vm._v(" "),
                  _c("v-autocomplete", {
                    attrs: {
                      readonly: _vm.inputsStatus,
                      "error-messages": _vm.familyErrors,
                      items: _vm.families,
                      "item-text": "Familia",
                      "item-value": "Id_Familia",
                      placeholder: "Selecciona una opción",
                      outlined: "",
                      dense: ""
                    },
                    on: {
                      input: function($event) {
                        return _vm.$v.currentClassificationData.family.$touch()
                      },
                      blur: function($event) {
                        return _vm.$v.currentClassificationData.family.$touch()
                      }
                    },
                    model: {
                      value: _vm.currentClassificationData.family,
                      callback: function($$v) {
                        _vm.$set(_vm.currentClassificationData, "family", $$v)
                      },
                      expression: "currentClassificationData.family"
                    }
                  })
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "v-col",
                { attrs: { cols: "6", sm: "4" } },
                [
                  _c("h6", [_vm._v("Marca")]),
                  _vm._v(" "),
                  _c("v-autocomplete", {
                    attrs: {
                      readonly: _vm.inputsStatus,
                      "error-messages": _vm.markErrors,
                      items: _vm.marks,
                      "item-text": "Marca",
                      "item-value": "Id_Marca",
                      placeholder: "Selecciona una opción",
                      outlined: "",
                      dense: ""
                    },
                    on: {
                      input: function($event) {
                        return _vm.$v.currentClassificationData.mark.$touch()
                      },
                      blur: function($event) {
                        return _vm.$v.currentClassificationData.mark.$touch()
                      }
                    },
                    model: {
                      value: _vm.currentClassificationData.mark,
                      callback: function($$v) {
                        _vm.$set(_vm.currentClassificationData, "mark", $$v)
                      },
                      expression: "currentClassificationData.mark"
                    }
                  })
                ],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "v-row",
            [
              _c(
                "v-col",
                { attrs: { cols: "6", md: "4" } },
                [
                  _c("h6", [_vm._v("Especialidad")]),
                  _vm._v(" "),
                  _c("v-autocomplete", {
                    attrs: {
                      readonly: _vm.inputsStatus,
                      "error-messages": _vm.specialtyErrors,
                      items: _vm.specialties,
                      "item-text": "Especialidad",
                      "item-value": "Id_EspMed",
                      placeholder: "Selecciona una opción",
                      outlined: "",
                      dense: ""
                    },
                    on: {
                      input: function($event) {
                        return _vm.$v.currentClassificationData.specialty.$touch()
                      },
                      blur: function($event) {
                        return _vm.$v.currentClassificationData.specialty.$touch()
                      }
                    },
                    model: {
                      value: _vm.currentClassificationData.specialty,
                      callback: function($$v) {
                        _vm.$set(
                          _vm.currentClassificationData,
                          "specialty",
                          $$v
                        )
                      },
                      expression: "currentClassificationData.specialty"
                    }
                  })
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "v-col",
                { attrs: { cols: "6", sm: "4" } },
                [
                  _c("h6", [_vm._v("Estatus")]),
                  _vm._v(" "),
                  _c("v-autocomplete", {
                    attrs: {
                      readonly: _vm.inputsStatus,
                      "error-messages": _vm.statusErrors,
                      items: _vm.status,
                      "item-text": "Descripcion",
                      "item-value": "Id_Estatus",
                      placeholder: "Selecciona una opción",
                      outlined: "",
                      dense: ""
                    },
                    on: {
                      input: function($event) {
                        return _vm.$v.currentClassificationData.status.$touch()
                      },
                      blur: function($event) {
                        return _vm.$v.currentClassificationData.status.$touch()
                      }
                    },
                    model: {
                      value: _vm.currentClassificationData.status,
                      callback: function($$v) {
                        _vm.$set(_vm.currentClassificationData, "status", $$v)
                      },
                      expression: "currentClassificationData.status"
                    }
                  })
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "v-col",
                { attrs: { cols: "6", sm: "4" } },
                [
                  _c("h6", [_vm._v("Origen")]),
                  _vm._v(" "),
                  _c("v-autocomplete", {
                    attrs: {
                      readonly: _vm.inputsStatus,
                      "error-messages": _vm.originErrors,
                      items: _vm.origins,
                      "item-text": "Pais",
                      "item-value": "Id_Pais",
                      placeholder: "Selecciona una opción",
                      outlined: "",
                      dense: ""
                    },
                    on: {
                      input: function($event) {
                        return _vm.$v.currentClassificationData.origin.$touch()
                      },
                      blur: function($event) {
                        return _vm.$v.currentClassificationData.origin.$touch()
                      }
                    },
                    model: {
                      value: _vm.currentClassificationData.origin,
                      callback: function($$v) {
                        _vm.$set(_vm.currentClassificationData, "origin", $$v)
                      },
                      expression: "currentClassificationData.origin"
                    }
                  })
                ],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _vm.type != "show"
            ? _c("form-action-buttons", {
                on: { save: _vm.submit, close: _vm.cancel }
              })
            : _vm._e()
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/products/partials/DataSheetForm.vue?vue&type=template&id=cfb61bfe&":
/*!************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/products/partials/DataSheetForm.vue?vue&type=template&id=cfb61bfe& ***!
  \************************************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render),
/* harmony export */   "staticRenderFns": () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "v-card",
    { staticClass: "p-3" },
    [
      _c("add-location-modal", {
        attrs: { status: _vm.showAddLocationModal },
        on: {
          "close-add-location-modal": _vm.closeAddLocationModal,
          "add-location": _vm.addLocation
        }
      }),
      _vm._v(" "),
      _c(
        "v-card-text",
        [
          _c(
            "v-row",
            [
              _c(
                "v-col",
                { attrs: { cols: "6", md: "2" } },
                [
                  _c("h6", [_vm._v("Peso")]),
                  _vm._v(" "),
                  _c("v-text-field", {
                    attrs: {
                      readonly: _vm.inputsStatus,
                      placeholder: "Peso",
                      type: "number",
                      step: "0.01",
                      outlined: "",
                      dense: ""
                    },
                    model: {
                      value: _vm.currentDataSheetData.weight,
                      callback: function($$v) {
                        _vm.$set(_vm.currentDataSheetData, "weight", $$v)
                      },
                      expression: "currentDataSheetData.weight"
                    }
                  })
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "v-col",
                { attrs: { cols: "6", sm: "2" } },
                [
                  _c("h6", [_vm._v("Ancho")]),
                  _vm._v(" "),
                  _c("v-text-field", {
                    attrs: {
                      readonly: _vm.inputsStatus,
                      placeholder: "Ancho",
                      type: "number",
                      step: "0.01",
                      outlined: "",
                      dense: ""
                    },
                    model: {
                      value: _vm.currentDataSheetData.width,
                      callback: function($$v) {
                        _vm.$set(_vm.currentDataSheetData, "width", $$v)
                      },
                      expression: "currentDataSheetData.width"
                    }
                  })
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "v-col",
                { attrs: { cols: "6", sm: "2" } },
                [
                  _c("h6", [_vm._v("Alto")]),
                  _vm._v(" "),
                  _c("v-text-field", {
                    attrs: {
                      readonly: _vm.inputsStatus,
                      placeholder: "Alto",
                      type: "number",
                      step: "0.01",
                      outlined: "",
                      dense: ""
                    },
                    model: {
                      value: _vm.currentDataSheetData.high,
                      callback: function($$v) {
                        _vm.$set(_vm.currentDataSheetData, "high", $$v)
                      },
                      expression: "currentDataSheetData.high"
                    }
                  })
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "v-col",
                { attrs: { cols: "6", sm: "2" } },
                [
                  _c("h6", [_vm._v("Largo")]),
                  _vm._v(" "),
                  _c("v-text-field", {
                    attrs: {
                      readonly: _vm.inputsStatus,
                      placeholder: "Largo",
                      type: "number",
                      step: "0.01",
                      outlined: "",
                      dense: ""
                    },
                    model: {
                      value: _vm.currentDataSheetData.length,
                      callback: function($$v) {
                        _vm.$set(_vm.currentDataSheetData, "length", $$v)
                      },
                      expression: "currentDataSheetData.length"
                    }
                  })
                ],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "v-row",
            [
              _c(
                "v-col",
                [
                  _c("v-data-table", {
                    staticClass: "elevation-1",
                    attrs: {
                      headers: _vm.locationsTableHeaders,
                      items: _vm.locations,
                      search: _vm.search,
                      page: _vm.page,
                      "items-per-page": _vm.itemsPerPage,
                      loading: _vm.loading,
                      "hide-default-footer": "",
                      "no-data-text": "No hay ningún registro",
                      "no-results-text": "No se encontraron coincidencias",
                      "loading-text": "Cargando... Porfavor espere"
                    },
                    on: {
                      "update:page": function($event) {
                        _vm.page = $event
                      },
                      "page-count": function($event) {
                        _vm.pageCount = $event
                      }
                    },
                    scopedSlots: _vm._u(
                      [
                        {
                          key: "top",
                          fn: function() {
                            return [
                              _c("datatable-header", {
                                attrs: {
                                  title: "Ubicaciones",
                                  "disable-creation": false
                                },
                                on: {
                                  searching: function($event) {
                                    _vm.search = $event
                                  },
                                  open: _vm.openAddLocationModal
                                }
                              })
                            ]
                          },
                          proxy: true
                        },
                        {
                          key: "item.actions",
                          fn: function(ref) {
                            var item = ref.item
                            return [
                              _c(
                                "v-tooltip",
                                {
                                  attrs: { top: "" },
                                  scopedSlots: _vm._u(
                                    [
                                      {
                                        key: "activator",
                                        fn: function(ref) {
                                          var on = ref.on
                                          var attrs = ref.attrs
                                          return [
                                            _c(
                                              "v-icon",
                                              _vm._g(
                                                _vm._b(
                                                  {
                                                    attrs: {
                                                      small: "",
                                                      color: "primary"
                                                    },
                                                    on: {
                                                      click: function($event) {
                                                        return _vm.removeLocation(
                                                          item
                                                        )
                                                      }
                                                    }
                                                  },
                                                  "v-icon",
                                                  attrs,
                                                  false
                                                ),
                                                on
                                              ),
                                              [
                                                _vm._v(
                                                  "\n                                mdi-delete\n                            "
                                                )
                                              ]
                                            )
                                          ]
                                        }
                                      }
                                    ],
                                    null,
                                    true
                                  )
                                },
                                [_vm._v(" "), _c("span", [_vm._v("Eliminar")])]
                              )
                            ]
                          }
                        }
                      ],
                      null,
                      true
                    )
                  }),
                  _vm._v(" "),
                  _c("pagination", {
                    attrs: {
                      last: _vm.pageCount,
                      current: _vm.page,
                      itemsPerPage: _vm.itemsPerPage,
                      total: _vm.locations.length
                    },
                    on: {
                      input: function($event) {
                        _vm.page = $event
                      }
                    }
                  })
                ],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "v-row",
            [
              _c(
                "v-col",
                { attrs: { cols: "6", md: "12" } },
                [
                  _c("h6", [_vm._v("Fotos")]),
                  _vm._v(" "),
                  _c("Upload", {
                    ref: "getImages",
                    attrs: { imagesToEdit: _vm.imagesToEdit },
                    on: { images: _vm.fillImages }
                  })
                ],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "v-row",
            [
              _c(
                "v-col",
                { attrs: { cols: "6", md: "12" } },
                [
                  _c("h6", [_vm._v("Especificación Técnica")]),
                  _vm._v(" "),
                  _c("v-textarea", {
                    attrs: {
                      readonly: _vm.inputsStatus,
                      "error-messages": _vm.technicalSpecificationErrors,
                      placeholder: "Ingrese las especificaciones del producto",
                      counter: 300,
                      outlined: "",
                      dense: ""
                    },
                    on: {
                      input: function($event) {
                        return _vm.$v.currentDataSheetData.technicalSpecification.$touch()
                      },
                      blur: function($event) {
                        return _vm.$v.currentDataSheetData.technicalSpecification.$touch()
                      }
                    },
                    model: {
                      value: _vm.currentDataSheetData.technicalSpecification,
                      callback: function($$v) {
                        _vm.$set(
                          _vm.currentDataSheetData,
                          "technicalSpecification",
                          $$v
                        )
                      },
                      expression: "currentDataSheetData.technicalSpecification"
                    }
                  })
                ],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "v-row",
            [
              _c(
                "v-col",
                { attrs: { cols: "6", md: "12" } },
                [
                  _c("h6", [_vm._v("Indicaciones y Usos")]),
                  _vm._v(" "),
                  _c("v-textarea", {
                    attrs: {
                      readonly: _vm.inputsStatus,
                      "error-messages": _vm.indicationsErrors,
                      placeholder: "Ingrese los usos del producto",
                      counter: 300,
                      outlined: "",
                      dense: ""
                    },
                    on: {
                      input: function($event) {
                        return _vm.$v.currentDataSheetData.indications.$touch()
                      },
                      blur: function($event) {
                        return _vm.$v.currentDataSheetData.indications.$touch()
                      }
                    },
                    model: {
                      value: _vm.currentDataSheetData.indications,
                      callback: function($$v) {
                        _vm.$set(_vm.currentDataSheetData, "indications", $$v)
                      },
                      expression: "currentDataSheetData.indications"
                    }
                  })
                ],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _c("form-action-buttons", {
            attrs: { hideSaveBtn: _vm.type != "show" ? false : true },
            on: { save: _vm.submit, close: _vm.cancel }
          })
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/products/partials/GeneralForm.vue?vue&type=template&id=749c2c14&":
/*!**********************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/products/partials/GeneralForm.vue?vue&type=template&id=749c2c14& ***!
  \**********************************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render),
/* harmony export */   "staticRenderFns": () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "v-card",
    { staticClass: "p-3" },
    [
      _c("v-card-title", { staticClass: "text-center" }, [
        _c("b", [_vm._v(_vm._s(_vm.title))])
      ]),
      _vm._v(" "),
      _c(
        "v-card-text",
        [
          _c(
            "v-row",
            [
              _c(
                "v-col",
                { attrs: { cols: "6", md: "3" } },
                [
                  _c("h6", { staticClass: "mt-2" }, [
                    _vm._v("Clave Nemotécnica")
                  ]),
                  _vm._v(" "),
                  _c("v-text-field", {
                    attrs: {
                      disabled:
                        this.$router.currentRoute.name == "create-products"
                          ? false
                          : true,
                      "error-messages": _vm.nemotecnicaKeyErrors,
                      readonly: _vm.inputsStatus,
                      placeholder: "Ingrese la clave nemotécnica",
                      outlined: "",
                      dense: ""
                    },
                    on: {
                      input: function($event) {
                        return _vm.$v.currentGeneralData.Id_Prod1.$touch()
                      },
                      blur: function($event) {
                        return _vm.$v.currentGeneralData.Id_Prod1.$touch()
                      }
                    },
                    model: {
                      value: _vm.currentGeneralData.Id_Prod1,
                      callback: function($$v) {
                        _vm.$set(_vm.currentGeneralData, "Id_Prod1", $$v)
                      },
                      expression: "currentGeneralData.Id_Prod1"
                    }
                  })
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "v-col",
                { attrs: { cols: "6", md: "3" } },
                [
                  _c("h6", { staticClass: "mt-2" }, [
                    _vm._v("Clave Licitación")
                  ]),
                  _vm._v(" "),
                  _c("v-text-field", {
                    attrs: {
                      "error-messages": _vm.bidKeyErrors,
                      readonly: _vm.inputsStatus,
                      placeholder: "Ingrese la clave de licitación",
                      outlined: "",
                      dense: ""
                    },
                    model: {
                      value: _vm.currentGeneralData.bidKey,
                      callback: function($$v) {
                        _vm.$set(_vm.currentGeneralData, "bidKey", $$v)
                      },
                      expression: "currentGeneralData.bidKey"
                    }
                  })
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "v-col",
                { attrs: { cols: "6", md: "3" } },
                [
                  _c("h6", { staticClass: "mt-2" }, [
                    _vm._v("Código de barras")
                  ]),
                  _vm._v(" "),
                  _c("v-text-field", {
                    attrs: {
                      readonly: _vm.inputsStatus,
                      placeholder: "Ingrese el código de barras",
                      outlined: "",
                      dense: ""
                    },
                    model: {
                      value: _vm.currentGeneralData.barCode,
                      callback: function($$v) {
                        _vm.$set(_vm.currentGeneralData, "barCode", $$v)
                      },
                      expression: "currentGeneralData.barCode"
                    }
                  })
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "v-col",
                { attrs: { cols: "6", md: "3" } },
                [
                  _c("h6", { staticClass: "mt-2" }, [_vm._v("Código SAT")]),
                  _vm._v(" "),
                  _c("v-text-field", {
                    attrs: {
                      readonly: _vm.inputsStatus,
                      placeholder: "Ingrese el código del SAT",
                      outlined: "",
                      dense: ""
                    },
                    model: {
                      value: _vm.currentGeneralData.satCode,
                      callback: function($$v) {
                        _vm.$set(_vm.currentGeneralData, "satCode", $$v)
                      },
                      expression: "currentGeneralData.satCode"
                    }
                  })
                ],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "v-row",
            [
              _c(
                "v-col",
                { attrs: { cols: "6", md: "12" } },
                [
                  _c("h6", [_vm._v("Descripción Corta")]),
                  _vm._v(" "),
                  _c("v-text-field", {
                    attrs: {
                      readonly: _vm.inputsStatus,
                      placeholder: "Ingrese la descripción",
                      outlined: "",
                      dense: ""
                    },
                    model: {
                      value: _vm.currentGeneralData.shortDescription,
                      callback: function($$v) {
                        _vm.$set(
                          _vm.currentGeneralData,
                          "shortDescription",
                          $$v
                        )
                      },
                      expression: "currentGeneralData.shortDescription"
                    }
                  })
                ],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "v-row",
            [
              _c(
                "v-col",
                { attrs: { cols: "6", md: "12" } },
                [
                  _c("h6", [_vm._v("Descripción Larga")]),
                  _vm._v(" "),
                  _c("v-textarea", {
                    attrs: {
                      readonly: _vm.inputsStatus,
                      placeholder: "Ingrese la descripción",
                      outlined: "",
                      dense: ""
                    },
                    model: {
                      value: _vm.currentGeneralData.longDescription,
                      callback: function($$v) {
                        _vm.$set(_vm.currentGeneralData, "longDescription", $$v)
                      },
                      expression: "currentGeneralData.longDescription"
                    }
                  })
                ],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "v-row",
            [
              _c(
                "v-col",
                { attrs: { cols: "6", md: "6" } },
                [
                  _c("h6", [_vm._v("Proveedor 1")]),
                  _vm._v(" "),
                  _c("v-autocomplete", {
                    attrs: {
                      readonly: _vm.inputsStatus,
                      items: _vm.suppliers,
                      "item-text": "Id_Prov",
                      "item-value": "Id_Prov",
                      placeholder: "Selecciona una opción",
                      outlined: "",
                      dense: ""
                    },
                    model: {
                      value: _vm.currentGeneralData.supplier1,
                      callback: function($$v) {
                        _vm.$set(_vm.currentGeneralData, "supplier1", $$v)
                      },
                      expression: "currentGeneralData.supplier1"
                    }
                  })
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "v-col",
                { attrs: { cols: "6", sm: "6" } },
                [
                  _c("h6", [_vm._v("Proveedor 2")]),
                  _vm._v(" "),
                  _c("v-autocomplete", {
                    attrs: {
                      readonly: _vm.inputsStatus,
                      items: _vm.suppliers,
                      "item-text": "Id_Prov",
                      "item-value": "Id_Prov",
                      placeholder: "Selecciona una opción",
                      outlined: "",
                      dense: ""
                    },
                    model: {
                      value: _vm.currentGeneralData.supplier2,
                      callback: function($$v) {
                        _vm.$set(_vm.currentGeneralData, "supplier2", $$v)
                      },
                      expression: "currentGeneralData.supplier2"
                    }
                  })
                ],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "v-row",
            [
              _c(
                "v-col",
                [
                  _c("v-checkbox", {
                    attrs: {
                      "false-value": "0",
                      "true-value": "1",
                      readonly: _vm.inputsStatus,
                      label: "Inventariable",
                      color: "primary",
                      "hide-details": ""
                    },
                    model: {
                      value: _vm.currentGeneralData.inventoryable,
                      callback: function($$v) {
                        _vm.$set(_vm.currentGeneralData, "inventoryable", $$v)
                      },
                      expression: "currentGeneralData.inventoryable"
                    }
                  })
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "v-col",
                [
                  _c("v-checkbox", {
                    attrs: {
                      "false-value": "0",
                      "true-value": "1",
                      readonly: _vm.inputsStatus,
                      label: "En lista de Precios",
                      color: "primary",
                      "hide-details": ""
                    },
                    model: {
                      value: _vm.currentGeneralData.inPriceList,
                      callback: function($$v) {
                        _vm.$set(_vm.currentGeneralData, "inPriceList", $$v)
                      },
                      expression: "currentGeneralData.inPriceList"
                    }
                  })
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "v-col",
                [
                  _c("v-checkbox", {
                    attrs: {
                      "false-value": "0",
                      "true-value": "1",
                      readonly: _vm.inputsStatus,
                      label: "Aplica Lote y Caducidad",
                      color: "primary",
                      "hide-details": ""
                    },
                    model: {
                      value: _vm.currentGeneralData.applyExpiration,
                      callback: function($$v) {
                        _vm.$set(_vm.currentGeneralData, "applyExpiration", $$v)
                      },
                      expression: "currentGeneralData.applyExpiration"
                    }
                  })
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "v-col",
                [
                  _c("v-checkbox", {
                    attrs: {
                      "false-value": "0",
                      "true-value": "1",
                      readonly: _vm.inputsStatus,
                      label: "Activo",
                      color: "primary",
                      "hide-details": ""
                    },
                    model: {
                      value: _vm.currentGeneralData.active,
                      callback: function($$v) {
                        _vm.$set(_vm.currentGeneralData, "active", $$v)
                      },
                      expression: "currentGeneralData.active"
                    }
                  })
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "v-col",
                [
                  _c("v-checkbox", {
                    attrs: {
                      "false-value": "0",
                      "true-value": "1",
                      readonly: _vm.inputsStatus,
                      label: "E-commerce",
                      color: "primary",
                      "hide-details": ""
                    },
                    model: {
                      value: _vm.currentGeneralData.eCommerce,
                      callback: function($$v) {
                        _vm.$set(_vm.currentGeneralData, "eCommerce", $$v)
                      },
                      expression: "currentGeneralData.eCommerce"
                    }
                  })
                ],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "v-row",
            [
              _c(
                "v-col",
                { attrs: { cols: "6", md: "12" } },
                [
                  _c("h6", [_vm._v("Observaciones")]),
                  _vm._v(" "),
                  _c("v-textarea", {
                    attrs: {
                      readonly: _vm.inputsStatus,
                      placeholder: "Observaciones",
                      outlined: "",
                      dense: ""
                    },
                    model: {
                      value: _vm.currentGeneralData.observations,
                      callback: function($$v) {
                        _vm.$set(_vm.currentGeneralData, "observations", $$v)
                      },
                      expression: "currentGeneralData.observations"
                    }
                  })
                ],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "v-row",
            [
              _c(
                "v-col",
                { attrs: { cols: "6", md: "12" } },
                [
                  _c("h6", [_vm._v("Registro Salubridad")]),
                  _vm._v(" "),
                  _c("v-text-field", {
                    attrs: {
                      readonly: _vm.inputsStatus,
                      placeholder: "Registro salubridad",
                      outlined: "",
                      dense: ""
                    },
                    model: {
                      value: _vm.currentGeneralData.healthRegistration,
                      callback: function($$v) {
                        _vm.$set(
                          _vm.currentGeneralData,
                          "healthRegistration",
                          $$v
                        )
                      },
                      expression: "currentGeneralData.healthRegistration"
                    }
                  })
                ],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "v-row",
            [
              _c(
                "v-col",
                { attrs: { cols: "6", md: "3" } },
                [
                  _c("h6", [_vm._v("IVA")]),
                  _vm._v(" "),
                  _c("v-text-field", {
                    attrs: {
                      readonly: _vm.inputsStatus,
                      placeholder: "%",
                      type: "number",
                      outlined: "",
                      dense: ""
                    },
                    model: {
                      value: _vm.currentGeneralData.iva,
                      callback: function($$v) {
                        _vm.$set(_vm.currentGeneralData, "iva", $$v)
                      },
                      expression: "currentGeneralData.iva"
                    }
                  })
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "v-col",
                { attrs: { cols: "6", md: "3" } },
                [
                  _c("h6", [_vm._v("Utilidad Mínima Licitación")]),
                  _vm._v(" "),
                  _c("v-text-field", {
                    attrs: {
                      readonly: _vm.inputsStatus,
                      placeholder: "%",
                      type: "number",
                      outlined: "",
                      dense: ""
                    },
                    model: {
                      value: _vm.currentGeneralData.minimumBid,
                      callback: function($$v) {
                        _vm.$set(_vm.currentGeneralData, "minimumBid", $$v)
                      },
                      expression: "currentGeneralData.minimumBid"
                    }
                  })
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "v-col",
                { attrs: { cols: "6", md: "3" } },
                [
                  _c("h6", [_vm._v("Flete")]),
                  _vm._v(" "),
                  _c("v-text-field", {
                    attrs: {
                      readonly: _vm.inputsStatus,
                      placeholder: "$",
                      type: "number",
                      outlined: "",
                      dense: ""
                    },
                    model: {
                      value: _vm.currentGeneralData.freight,
                      callback: function($$v) {
                        _vm.$set(_vm.currentGeneralData, "freight", $$v)
                      },
                      expression: "currentGeneralData.freight"
                    }
                  })
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "v-col",
                { attrs: { cols: "6", md: "3" } },
                [
                  _c("h6", [_vm._v("T Surtido")]),
                  _vm._v(" "),
                  _c("v-text-field", {
                    attrs: {
                      readonly: _vm.inputsStatus,
                      placeholder: "Días",
                      type: "number",
                      outlined: "",
                      dense: ""
                    },
                    model: {
                      value: _vm.currentGeneralData.timeAssortment,
                      callback: function($$v) {
                        _vm.$set(_vm.currentGeneralData, "timeAssortment", $$v)
                      },
                      expression: "currentGeneralData.timeAssortment"
                    }
                  })
                ],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _vm.type != "show"
            ? _c("form-action-buttons", {
                on: { save: _vm.submit, close: _vm.cancel }
              })
            : _vm._e()
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/products/partials/StatisticsForm.vue?vue&type=template&id=27f41022&":
/*!*************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/products/partials/StatisticsForm.vue?vue&type=template&id=27f41022& ***!
  \*************************************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render),
/* harmony export */   "staticRenderFns": () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "v-card",
    { staticClass: "p-3" },
    [
      _c(
        "v-card-text",
        [
          _c(
            "v-row",
            [
              _c(
                "v-col",
                { attrs: { cols: "6", md: "4" } },
                [
                  _c("h6", [_vm._v("Último Costo")]),
                  _vm._v(" "),
                  _c("v-text-field", {
                    attrs: {
                      readonly: _vm.inputsStatus,
                      placeholder: "$0.00",
                      type: "number",
                      step: "0.01",
                      outlined: "",
                      dense: ""
                    },
                    model: {
                      value: _vm.currentStatisticsData.lastCost,
                      callback: function($$v) {
                        _vm.$set(_vm.currentStatisticsData, "lastCost", $$v)
                      },
                      expression: "currentStatisticsData.lastCost"
                    }
                  })
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "v-col",
                { attrs: { cols: "6", sm: "4" } },
                [
                  _c("h6", [_vm._v("Costo Promedio")]),
                  _vm._v(" "),
                  _c("v-text-field", {
                    attrs: {
                      readonly: _vm.inputsStatus,
                      placeholder: "$0.00",
                      type: "number",
                      step: "0.01",
                      outlined: "",
                      dense: ""
                    },
                    model: {
                      value: _vm.currentStatisticsData.averageCost,
                      callback: function($$v) {
                        _vm.$set(_vm.currentStatisticsData, "averageCost", $$v)
                      },
                      expression: "currentStatisticsData.averageCost"
                    }
                  })
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "v-col",
                { attrs: { cols: "6", sm: "4" } },
                [
                  _c("h6", [_vm._v("Costo de Mercado más Bajo")]),
                  _vm._v(" "),
                  _c("v-text-field", {
                    attrs: {
                      readonly: _vm.inputsStatus,
                      placeholder: "$0.00",
                      type: "number",
                      step: "0.01",
                      outlined: "",
                      dense: ""
                    },
                    model: {
                      value: _vm.currentStatisticsData.lowerMarketCost,
                      callback: function($$v) {
                        _vm.$set(
                          _vm.currentStatisticsData,
                          "lowerMarketCost",
                          $$v
                        )
                      },
                      expression: "currentStatisticsData.lowerMarketCost"
                    }
                  })
                ],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "v-row",
            [
              _c(
                "v-col",
                { attrs: { cols: "6", md: "4" } },
                [
                  _c("h6", [_vm._v("Último Costo Mero")]),
                  _vm._v(" "),
                  _c(
                    "v-menu",
                    {
                      attrs: {
                        "close-on-content-click": false,
                        "nudge-right": 40,
                        transition: "scale-transition",
                        "offset-y": "",
                        "min-width": "auto"
                      },
                      scopedSlots: _vm._u([
                        {
                          key: "activator",
                          fn: function(ref) {
                            var on = ref.on
                            var attrs = ref.attrs
                            return [
                              _c(
                                "v-text-field",
                                _vm._g(
                                  _vm._b(
                                    {
                                      attrs: {
                                        placeholder: "00/00/00",
                                        outlined: "",
                                        dense: "",
                                        readonly: ""
                                      },
                                      model: {
                                        value:
                                          _vm.currentStatisticsData
                                            .lastMereCost,
                                        callback: function($$v) {
                                          _vm.$set(
                                            _vm.currentStatisticsData,
                                            "lastMereCost",
                                            $$v
                                          )
                                        },
                                        expression:
                                          "currentStatisticsData.lastMereCost"
                                      }
                                    },
                                    "v-text-field",
                                    attrs,
                                    false
                                  ),
                                  on
                                )
                              )
                            ]
                          }
                        }
                      ]),
                      model: {
                        value: _vm.menu1,
                        callback: function($$v) {
                          _vm.menu1 = $$v
                        },
                        expression: "menu1"
                      }
                    },
                    [
                      _vm._v(" "),
                      _c("v-date-picker", {
                        attrs: {
                          readonly: _vm.inputsStatus,
                          color: "#018085",
                          "header-color": "#018085"
                        },
                        on: {
                          input: function($event) {
                            _vm.menu1 = false
                          }
                        },
                        model: {
                          value: _vm.currentStatisticsData.lastMereCost,
                          callback: function($$v) {
                            _vm.$set(
                              _vm.currentStatisticsData,
                              "lastMereCost",
                              $$v
                            )
                          },
                          expression: "currentStatisticsData.lastMereCost"
                        }
                      })
                    ],
                    1
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "v-col",
                { attrs: { cols: "6", sm: "4" } },
                [
                  _c("h6", [_vm._v("Último Costo Lici")]),
                  _vm._v(" "),
                  _c(
                    "v-menu",
                    {
                      attrs: {
                        "close-on-content-click": false,
                        "nudge-right": 40,
                        transition: "scale-transition",
                        "offset-y": "",
                        "min-width": "auto"
                      },
                      scopedSlots: _vm._u([
                        {
                          key: "activator",
                          fn: function(ref) {
                            var on = ref.on
                            var attrs = ref.attrs
                            return [
                              _c(
                                "v-text-field",
                                _vm._g(
                                  _vm._b(
                                    {
                                      attrs: {
                                        placeholder: "00/00/00",
                                        outlined: "",
                                        dense: "",
                                        readonly: ""
                                      },
                                      model: {
                                        value:
                                          _vm.currentStatisticsData
                                            .lastLiciCost,
                                        callback: function($$v) {
                                          _vm.$set(
                                            _vm.currentStatisticsData,
                                            "lastLiciCost",
                                            $$v
                                          )
                                        },
                                        expression:
                                          "currentStatisticsData.lastLiciCost"
                                      }
                                    },
                                    "v-text-field",
                                    attrs,
                                    false
                                  ),
                                  on
                                )
                              )
                            ]
                          }
                        }
                      ]),
                      model: {
                        value: _vm.menu2,
                        callback: function($$v) {
                          _vm.menu2 = $$v
                        },
                        expression: "menu2"
                      }
                    },
                    [
                      _vm._v(" "),
                      _c("v-date-picker", {
                        attrs: {
                          readonly: _vm.inputsStatus,
                          color: "#018085",
                          "header-color": "#018085"
                        },
                        on: {
                          input: function($event) {
                            _vm.menu2 = false
                          }
                        },
                        model: {
                          value: _vm.currentStatisticsData.lastLiciCost,
                          callback: function($$v) {
                            _vm.$set(
                              _vm.currentStatisticsData,
                              "lastLiciCost",
                              $$v
                            )
                          },
                          expression: "currentStatisticsData.lastLiciCost"
                        }
                      })
                    ],
                    1
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "v-col",
                { attrs: { cols: "6", sm: "4" } },
                [
                  _c("h6", [_vm._v("Último Cambio Precios")]),
                  _vm._v(" "),
                  _c(
                    "v-menu",
                    {
                      attrs: {
                        "close-on-content-click": false,
                        "nudge-right": 40,
                        transition: "scale-transition",
                        "offset-y": "",
                        "min-width": "auto"
                      },
                      scopedSlots: _vm._u([
                        {
                          key: "activator",
                          fn: function(ref) {
                            var on = ref.on
                            var attrs = ref.attrs
                            return [
                              _c(
                                "v-text-field",
                                _vm._g(
                                  _vm._b(
                                    {
                                      attrs: {
                                        placeholder: "00/00/00",
                                        outlined: "",
                                        dense: "",
                                        readonly: ""
                                      },
                                      model: {
                                        value:
                                          _vm.currentStatisticsData
                                            .lastChangePrices,
                                        callback: function($$v) {
                                          _vm.$set(
                                            _vm.currentStatisticsData,
                                            "lastChangePrices",
                                            $$v
                                          )
                                        },
                                        expression:
                                          "currentStatisticsData.lastChangePrices"
                                      }
                                    },
                                    "v-text-field",
                                    attrs,
                                    false
                                  ),
                                  on
                                )
                              )
                            ]
                          }
                        }
                      ]),
                      model: {
                        value: _vm.menu3,
                        callback: function($$v) {
                          _vm.menu3 = $$v
                        },
                        expression: "menu3"
                      }
                    },
                    [
                      _vm._v(" "),
                      _c("v-date-picker", {
                        attrs: {
                          readonly: _vm.inputsStatus,
                          color: "#018085",
                          "header-color": "#018085"
                        },
                        on: {
                          input: function($event) {
                            _vm.menu3 = false
                          }
                        },
                        model: {
                          value: _vm.currentStatisticsData.lastChangePrices,
                          callback: function($$v) {
                            _vm.$set(
                              _vm.currentStatisticsData,
                              "lastChangePrices",
                              $$v
                            )
                          },
                          expression: "currentStatisticsData.lastChangePrices"
                        }
                      })
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "v-row",
            [
              _c(
                "v-col",
                { attrs: { cols: "6", md: "4" } },
                [
                  _c("h6", [_vm._v("Última Compra")]),
                  _vm._v(" "),
                  _c(
                    "v-menu",
                    {
                      attrs: {
                        "close-on-content-click": false,
                        "nudge-right": 40,
                        transition: "scale-transition",
                        "offset-y": "",
                        "min-width": "auto"
                      },
                      scopedSlots: _vm._u([
                        {
                          key: "activator",
                          fn: function(ref) {
                            var on = ref.on
                            var attrs = ref.attrs
                            return [
                              _c(
                                "v-text-field",
                                _vm._g(
                                  _vm._b(
                                    {
                                      attrs: {
                                        placeholder: "00/00/00",
                                        outlined: "",
                                        dense: "",
                                        readonly: ""
                                      },
                                      model: {
                                        value:
                                          _vm.currentStatisticsData
                                            .lastPurchase,
                                        callback: function($$v) {
                                          _vm.$set(
                                            _vm.currentStatisticsData,
                                            "lastPurchase",
                                            $$v
                                          )
                                        },
                                        expression:
                                          "currentStatisticsData.lastPurchase"
                                      }
                                    },
                                    "v-text-field",
                                    attrs,
                                    false
                                  ),
                                  on
                                )
                              )
                            ]
                          }
                        }
                      ]),
                      model: {
                        value: _vm.menu4,
                        callback: function($$v) {
                          _vm.menu4 = $$v
                        },
                        expression: "menu4"
                      }
                    },
                    [
                      _vm._v(" "),
                      _c("v-date-picker", {
                        attrs: {
                          readonly: _vm.inputsStatus,
                          color: "#018085",
                          "header-color": "#018085"
                        },
                        on: {
                          input: function($event) {
                            _vm.menu4 = false
                          }
                        },
                        model: {
                          value: _vm.currentStatisticsData.lastPurchase,
                          callback: function($$v) {
                            _vm.$set(
                              _vm.currentStatisticsData,
                              "lastPurchase",
                              $$v
                            )
                          },
                          expression: "currentStatisticsData.lastPurchase"
                        }
                      })
                    ],
                    1
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "v-col",
                { attrs: { cols: "6", sm: "4" } },
                [
                  _c("h6", [_vm._v("Última Venta")]),
                  _vm._v(" "),
                  _c(
                    "v-menu",
                    {
                      attrs: {
                        "close-on-content-click": false,
                        "nudge-right": 40,
                        transition: "scale-transition",
                        "offset-y": "",
                        "min-width": "auto"
                      },
                      scopedSlots: _vm._u([
                        {
                          key: "activator",
                          fn: function(ref) {
                            var on = ref.on
                            var attrs = ref.attrs
                            return [
                              _c(
                                "v-text-field",
                                _vm._g(
                                  _vm._b(
                                    {
                                      attrs: {
                                        placeholder: "00/00/00",
                                        outlined: "",
                                        dense: "",
                                        readonly: ""
                                      },
                                      model: {
                                        value:
                                          _vm.currentStatisticsData.lastSale,
                                        callback: function($$v) {
                                          _vm.$set(
                                            _vm.currentStatisticsData,
                                            "lastSale",
                                            $$v
                                          )
                                        },
                                        expression:
                                          "currentStatisticsData.lastSale"
                                      }
                                    },
                                    "v-text-field",
                                    attrs,
                                    false
                                  ),
                                  on
                                )
                              )
                            ]
                          }
                        }
                      ]),
                      model: {
                        value: _vm.menu5,
                        callback: function($$v) {
                          _vm.menu5 = $$v
                        },
                        expression: "menu5"
                      }
                    },
                    [
                      _vm._v(" "),
                      _c("v-date-picker", {
                        attrs: {
                          readonly: _vm.inputsStatus,
                          color: "#018085",
                          "header-color": "#018085"
                        },
                        on: {
                          input: function($event) {
                            _vm.menu5 = false
                          }
                        },
                        model: {
                          value: _vm.currentStatisticsData.lastSale,
                          callback: function($$v) {
                            _vm.$set(_vm.currentStatisticsData, "lastSale", $$v)
                          },
                          expression: "currentStatisticsData.lastSale"
                        }
                      })
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _vm.type != "show"
            ? _c("form-action-buttons", {
                on: { save: _vm.submit, close: _vm.cancel }
              })
            : _vm._e()
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/products/partials/StorageForm.vue?vue&type=template&id=021622a7&":
/*!**********************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/products/partials/StorageForm.vue?vue&type=template&id=021622a7& ***!
  \**********************************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render),
/* harmony export */   "staticRenderFns": () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "v-card",
    { staticClass: "p-3 justify-center" },
    [
      _c(
        "v-card-text",
        [
          _c(
            "v-row",
            [
              _c(
                "v-col",
                { attrs: { cols: "6", md: "1" } },
                [
                  _c("h6", [_vm._v("Nivel")]),
                  _vm._v(" "),
                  _c("v-select", {
                    attrs: {
                      readonly: _vm.inputsStatus,
                      items: _vm.levels,
                      "item-text": "name",
                      "item-value": "id",
                      placeholder: "0",
                      outlined: "",
                      dense: ""
                    },
                    model: {
                      value: _vm.currentStorageData.level,
                      callback: function($$v) {
                        _vm.$set(_vm.currentStorageData, "level", $$v)
                      },
                      expression: "currentStorageData.level"
                    }
                  })
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "v-col",
                { attrs: { cols: "6", sm: "2" } },
                [
                  _c("h6", [_vm._v("Empaque")]),
                  _vm._v(" "),
                  _c("v-autocomplete", {
                    attrs: {
                      readonly: _vm.inputsStatus,
                      items: _vm.packaging,
                      "item-text": "Empaque",
                      "item-value": "Id_Empaque",
                      placeholder: "Selecciona una opción",
                      outlined: "",
                      dense: ""
                    },
                    model: {
                      value: _vm.currentStorageData.packing1,
                      callback: function($$v) {
                        _vm.$set(_vm.currentStorageData, "packing1", $$v)
                      },
                      expression: "currentStorageData.packing1"
                    }
                  })
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "v-col",
                { attrs: { cols: "6", sm: "2" } },
                [
                  _c("h6", [_vm._v("Unidades")]),
                  _vm._v(" "),
                  _c("v-text-field", {
                    attrs: {
                      readonly: _vm.inputsStatus,
                      placeholder: "Unidad",
                      outlined: "",
                      dense: "",
                      type: "number"
                    },
                    model: {
                      value: _vm.currentStorageData.units1,
                      callback: function($$v) {
                        _vm.$set(_vm.currentStorageData, "units1", $$v)
                      },
                      expression: "currentStorageData.units1"
                    }
                  })
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "v-col",
                { staticClass: "pb-0", attrs: { cols: "6", sm: "1" } },
                [
                  _c("h6", { staticStyle: { width: "100px" } }, [
                    _vm._v("Nivel Compra")
                  ]),
                  _vm._v(" "),
                  _c("v-select", {
                    attrs: {
                      readonly: _vm.inputsStatus,
                      items: _vm.levels,
                      "item-text": "name",
                      "item-value": "id",
                      placeholder: "0",
                      outlined: "",
                      dense: ""
                    },
                    model: {
                      value: _vm.currentStorageData.purchaseLevel,
                      callback: function($$v) {
                        _vm.$set(_vm.currentStorageData, "purchaseLevel", $$v)
                      },
                      expression: "currentStorageData.purchaseLevel"
                    }
                  })
                ],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "v-row",
            [
              _c("v-col", { attrs: { cols: "6", md: "1" } }),
              _vm._v(" "),
              _c(
                "v-col",
                { staticClass: "pt-4", attrs: { cols: "6", sm: "2" } },
                [
                  _c("v-autocomplete", {
                    attrs: {
                      readonly: _vm.inputsStatus,
                      items: _vm.packaging,
                      "item-text": "Empaque",
                      "item-value": "Id_Empaque",
                      placeholder: "Selecciona una opción",
                      outlined: "",
                      dense: ""
                    },
                    model: {
                      value: _vm.currentStorageData.packing2,
                      callback: function($$v) {
                        _vm.$set(_vm.currentStorageData, "packing2", $$v)
                      },
                      expression: "currentStorageData.packing2"
                    }
                  })
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "v-col",
                { staticClass: "pt-4", attrs: { cols: "6", sm: "2" } },
                [
                  _c("v-text-field", {
                    attrs: {
                      readonly: _vm.inputsStatus,
                      placeholder: "Unidad",
                      outlined: "",
                      dense: "",
                      type: "number"
                    },
                    model: {
                      value: _vm.currentStorageData.units2,
                      callback: function($$v) {
                        _vm.$set(_vm.currentStorageData, "units2", $$v)
                      },
                      expression: "currentStorageData.units2"
                    }
                  })
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "v-col",
                { staticClass: "py-0", attrs: { cols: "6", sm: "1" } },
                [
                  _c("h6", { staticStyle: { width: "100px" } }, [
                    _vm._v("Nivel Venta")
                  ]),
                  _vm._v(" "),
                  _c("v-select", {
                    attrs: {
                      readonly: _vm.inputsStatus,
                      items: _vm.levels,
                      "item-text": "name",
                      "item-value": "id",
                      placeholder: "0",
                      outlined: "",
                      dense: ""
                    },
                    model: {
                      value: _vm.currentStorageData.saleLevel,
                      callback: function($$v) {
                        _vm.$set(_vm.currentStorageData, "saleLevel", $$v)
                      },
                      expression: "currentStorageData.saleLevel"
                    }
                  })
                ],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "v-row",
            [
              _c("v-col", { attrs: { cols: "6", md: "1" } }),
              _vm._v(" "),
              _c(
                "v-col",
                { staticClass: "pt-4", attrs: { cols: "6", sm: "2" } },
                [
                  _c("v-autocomplete", {
                    attrs: {
                      readonly: _vm.inputsStatus,
                      items: _vm.packaging,
                      "item-text": "Empaque",
                      "item-value": "Id_Empaque",
                      placeholder: "Selecciona una opción",
                      outlined: "",
                      dense: ""
                    },
                    model: {
                      value: _vm.currentStorageData.packing3,
                      callback: function($$v) {
                        _vm.$set(_vm.currentStorageData, "packing3", $$v)
                      },
                      expression: "currentStorageData.packing3"
                    }
                  })
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "v-col",
                { staticClass: "pt-4", attrs: { cols: "6", sm: "2" } },
                [
                  _c("v-text-field", {
                    attrs: {
                      readonly: _vm.inputsStatus,
                      placeholder: "Unidad",
                      outlined: "",
                      dense: "",
                      type: "number"
                    },
                    model: {
                      value: _vm.currentStorageData.units3,
                      callback: function($$v) {
                        _vm.$set(_vm.currentStorageData, "units3", $$v)
                      },
                      expression: "currentStorageData.units3"
                    }
                  })
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "v-col",
                { staticClass: "pt-0", attrs: { cols: "6", sm: "1" } },
                [
                  _c("h6", { staticStyle: { width: "150px" } }, [
                    _vm._v("Nivel Venta Most")
                  ]),
                  _vm._v(" "),
                  _c("v-select", {
                    attrs: {
                      readonly: _vm.inputsStatus,
                      items: _vm.levels,
                      "item-text": "name",
                      "item-value": "id",
                      placeholder: "0",
                      outlined: "",
                      dense: ""
                    },
                    model: {
                      value: _vm.currentStorageData.saleLevelMost,
                      callback: function($$v) {
                        _vm.$set(_vm.currentStorageData, "saleLevelMost", $$v)
                      },
                      expression: "currentStorageData.saleLevelMost"
                    }
                  })
                ],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _vm.type != "show"
            ? _c("form-action-buttons", {
                on: { save: _vm.submit, close: _vm.cancel }
              })
            : _vm._e()
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/products/partials/Uploade.vue?vue&type=template&id=5ba944cc&":
/*!******************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/products/partials/Uploade.vue?vue&type=template&id=5ba944cc& ***!
  \******************************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render),
/* harmony export */   "staticRenderFns": () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    {
      staticClass:
        "flex w-full h-screen items-center justify-center text-center"
    },
    [
      _vm.fileList.length == 0
        ? _c(
            "div",
            {
              staticClass: "p-12 dropAndDrag",
              on: {
                dragover: _vm.dragover,
                dragleave: _vm.dragleave,
                drop: _vm.drop
              }
            },
            [
              _c("input", {
                ref: "file",
                staticClass: "dropAndDragInput",
                attrs: {
                  type: "file",
                  multiple: "",
                  name: "fields[assetsFieldHandle][]",
                  id: "assetsFieldHandle",
                  accept: ".pdf,.jpg,.jpeg,.png"
                },
                on: { change: _vm.onChange }
              }),
              _vm._v(" "),
              _vm._m(0)
            ]
          )
        : _vm._e(),
      _vm._v(" "),
      this.fileList.length
        ? _c(
            "ul",
            { staticClass: "mt-4" },
            _vm._l(_vm.fileList, function(file, key) {
              return _c(
                "li",
                { key: key, staticClass: "text-sm p-1" },
                [
                  _c(
                    "v-btn",
                    {
                      staticClass: "mx-2",
                      staticStyle: { "margin-bottom": "5px" },
                      attrs: {
                        fab: "",
                        dark: "",
                        "x-small": "",
                        color: "#dc3545"
                      },
                      on: {
                        click: function($event) {
                          _vm.remove(_vm.fileList.indexOf(file))
                        }
                      }
                    },
                    [
                      _c("v-icon", { attrs: { dark: "" } }, [
                        _vm._v("\n          mdi-close\n        ")
                      ])
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c("v-img", {
                    staticClass: "showImage",
                    attrs: { src: file.url }
                  })
                ],
                1
              )
            }),
            0
          )
        : _vm._e()
    ]
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "label",
      {
        staticClass: "block cursor-pointer dropAndDragLabel",
        attrs: { for: "assetsFieldHandle" }
      },
      [
        _c("div", [
          _vm._v("\n        Arrastra aquí las imágenes\n        o "),
          _c("span", { staticClass: "underline" }, [_vm._v("Buscar imagen")])
        ])
      ]
    )
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/products/partials/addLocationModal.vue?vue&type=template&id=453ed81f&":
/*!***************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/products/partials/addLocationModal.vue?vue&type=template&id=453ed81f& ***!
  \***************************************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render),
/* harmony export */   "staticRenderFns": () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c(
        "v-dialog",
        {
          attrs: {
            "max-width": "600px",
            transition: "scroll-x-reverse-transition",
            persistent: ""
          },
          model: {
            value: _vm.status,
            callback: function($$v) {
              _vm.status = $$v
            },
            expression: "status"
          }
        },
        [
          _c(
            "v-card",
            { staticClass: "overflow-hidden" },
            [
              _c(
                "v-card-text",
                { staticClass: "p-0" },
                [
                  _c(
                    "v-row",
                    [
                      _c(
                        "v-col",
                        { attrs: { cols: "12" } },
                        [
                          _c(
                            "v-icon",
                            {
                              staticClass: "float-right",
                              attrs: {
                                medium: "",
                                role: "button",
                                color: "primary"
                              },
                              on: {
                                click: function($event) {
                                  return _vm.$emit("close-add-location-modal")
                                }
                              }
                            },
                            [_vm._v("mdi-close\n                  ")]
                          )
                        ],
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c("v-card-title", { staticClass: "headline mb-3" }, [
                _vm._v("Alta de ubicación del producto")
              ]),
              _vm._v(" "),
              _c(
                "v-card-text",
                [
                  _c(
                    "v-row",
                    [
                      _c(
                        "v-col",
                        {
                          staticClass: "py-0",
                          attrs: { cols: "12", sm: "12", md: "4" }
                        },
                        [
                          _c("n-label", {
                            attrs: {
                              text: "Elige el rack",
                              required: "",
                              error: _vm.$v.editedItem.rack.$error
                            }
                          }),
                          _vm._v(" "),
                          _c("v-select", {
                            attrs: {
                              "return-object": "",
                              outlined: "",
                              dense: "",
                              placeholder: "Rack",
                              items: _vm.racks,
                              "item-text": "Nombre",
                              "item-value": "Id",
                              required: "",
                              "error-messages": _vm.rackErrors
                            },
                            on: {
                              change: function($event) {
                                return _vm.$v.editedItem.rack.$touch()
                              }
                            },
                            model: {
                              value: _vm.editedItem.rack,
                              callback: function($$v) {
                                _vm.$set(_vm.editedItem, "rack", $$v)
                              },
                              expression: "editedItem.rack"
                            }
                          })
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "v-col",
                        {
                          staticClass: "py-0",
                          attrs: { cols: "12", sm: "12", md: "4" }
                        },
                        [
                          _c("n-label", {
                            attrs: {
                              text: "Elige el nivel",
                              required: "",
                              error: _vm.$v.editedItem.nivel.$error
                            }
                          }),
                          _vm._v(" "),
                          _c("v-select", {
                            attrs: {
                              "return-object": "",
                              outlined: "",
                              dense: "",
                              placeholder: "Nivel",
                              items: _vm.niveles,
                              "item-text": "Nombre",
                              "item-value": "ID",
                              loading: _vm.loadingNivelSelect,
                              disabled: _vm.loadingNivelSelect,
                              required: "",
                              "error-messages": _vm.nivelErrors
                            },
                            on: {
                              change: function($event) {
                                return _vm.$v.editedItem.nivel.$touch()
                              }
                            },
                            model: {
                              value: _vm.editedItem.nivel,
                              callback: function($$v) {
                                _vm.$set(_vm.editedItem, "nivel", $$v)
                              },
                              expression: "editedItem.nivel"
                            }
                          })
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "v-col",
                        {
                          staticClass: "py-0",
                          attrs: { cols: "12", sm: "12", md: "4" }
                        },
                        [
                          _c("n-label", {
                            attrs: {
                              text: "Elige la sección",
                              required: "",
                              error: _vm.$v.editedItem.seccion.$error
                            }
                          }),
                          _vm._v(" "),
                          _c("v-select", {
                            attrs: {
                              "return-object": "",
                              outlined: "",
                              dense: "",
                              placeholder: "Sección",
                              items: _vm.secciones,
                              "item-text": "Nombre",
                              "item-value": "Id",
                              loading: _vm.loadingSeccionSelect,
                              disabled: _vm.loadingSeccionSelect,
                              required: "",
                              "error-messages": _vm.seccionErrors
                            },
                            on: {
                              change: function($event) {
                                return _vm.$v.editedItem.seccion.$touch()
                              }
                            },
                            model: {
                              value: _vm.editedItem.seccion,
                              callback: function($$v) {
                                _vm.$set(_vm.editedItem, "seccion", $$v)
                              },
                              expression: "editedItem.seccion"
                            }
                          })
                        ],
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "v-card-actions",
                [
                  _c("v-spacer"),
                  _vm._v(" "),
                  _c(
                    "v-btn",
                    {
                      staticClass: "mr-2",
                      attrs: { color: "primary", text: "" },
                      on: {
                        click: function($event) {
                          return _vm.$emit(
                            "add-location",
                            _vm.editedItem,
                            false
                          )
                        }
                      }
                    },
                    [_vm._v("Agregar otra ubicación")]
                  ),
                  _vm._v(" "),
                  _c(
                    "v-btn",
                    {
                      attrs: { color: "primary-blue", text: "" },
                      on: {
                        click: function($event) {
                          return _vm.$emit("add-location", _vm.editedItem, true)
                        }
                      }
                    },
                    [_vm._v("Guardar")]
                  ),
                  _vm._v(" "),
                  _c("v-spacer")
                ],
                1
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-style-loader/index.js!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/products/edit.vue?vue&type=style&index=0&lang=css&":
/*!****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-style-loader/index.js!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/products/edit.vue?vue&type=style&index=0&lang=css& ***!
  \****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(/*! !!../../../../../node_modules/css-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[1]!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[2]!../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./edit.vue?vue&type=style&index=0&lang=css& */ "./node_modules/css-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/products/edit.vue?vue&type=style&index=0&lang=css&");
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.id, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var add = __webpack_require__(/*! !../../../../../node_modules/vue-style-loader/lib/addStylesClient.js */ "./node_modules/vue-style-loader/lib/addStylesClient.js").default
var update = add("647492a2", content, false, {});
// Hot Module Replacement
if(false) {}

/***/ }),

/***/ "./node_modules/vue-style-loader/index.js!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/products/partials/Uploade.vue?vue&type=style&index=0&lang=css&":
/*!****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-style-loader/index.js!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/products/partials/Uploade.vue?vue&type=style&index=0&lang=css& ***!
  \****************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(/*! !!../../../../../../node_modules/css-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[1]!../../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../../node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[2]!../../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./Uploade.vue?vue&type=style&index=0&lang=css& */ "./node_modules/css-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/views/product-configuration/products/partials/Uploade.vue?vue&type=style&index=0&lang=css&");
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.id, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var add = __webpack_require__(/*! !../../../../../../node_modules/vue-style-loader/lib/addStylesClient.js */ "./node_modules/vue-style-loader/lib/addStylesClient.js").default
var update = add("04ea9e20", content, false, {});
// Hot Module Replacement
if(false) {}

/***/ })

}]);