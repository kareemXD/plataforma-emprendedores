<?php

return [
    'admin' => [
        [
            'header' => true,
            'title' => 'Menú principal',
            'hidden' => false
        ],
        [
            'title' => 'Administración',
            'icon' => 'users',
            'hidden' => false,
            'child' => [
                [
                    'title' => 'Usuarios',
                    'href' => '/users',
                    'permission' => 'Usuarios',
                    'hidden' => false,
                ],
                [
                    'title' => 'Roles y Permisos',
                    'href' => '/roles',
                    'permission' => 'Roles',
                    'hidden' => false
                ],
                [
                    'title' => 'Configuración',
                    'href' => '/configurations',
                    'permission' => 'Configurations',
                    'hidden' => false
                ],
            ]
        ],
        [
            'title' => 'Pedidos',
            'icon' => 'shopping-cart',
            'hidden' => false,
            'child' => [
                [
                    'title' => 'Picking',
                    'href' => '/picking',
                    'permission' => 'Usuarios',
                    'hidden' => false,
                ],
                [
                    'title' => 'Packing',
                    'href' => '/packing',
                    'permission' => 'Usuarios',
                    'hidden' => false,
                ],
            ]
        ],
        [
            'title' => 'Emprendedores',
            'icon' => 'user-plus',
            'hidden' => false,
            'child' => [
                [
                    'title' => 'Listado emprendedores',
                    'href' => '/enterprisings',
                    'hidden' => false,
                    'permission' => 'EnterprisingsList',
                ],
            ]
        ],
        [
            'title' => 'Roles y permisos',
            'href' => '/roles',
            'icon' => 'pe-7s-key',
            'permission' => 'Roles.view',
            'hidden' => false
        ],
        [
            'title' => 'Productos',
            'icon' => 'list-alt',
            'hidden' => false,
            'child' => [
                    [
                        'title' => 'Listado general',
                        'href' => '/products',
                        'permission' => 'Products',
                        'hidden' => false,
                    ],
                    [
                        'title' => 'Vista de costos',
                        'href' => '/view-cost-entrepreneurs',
                        'permission' => 'ViewEntrepreneurialCost',
                        'hidden' => false,
                    ],
                    [
                        'title' => 'Cajas',
                        'href' => '/boxes',
                        'permission' => 'Box',
                        'hidden' => false,
                    ],
                    [
                        'title' => 'Cajas de picking',
                        'href' => '/container',
                        'permission' => 'Container',
                        'hidden' => false,
                    ],
                    [
                        'title' => 'Ubicaciones',
                        'href' => '/locations',
                        'permission' => 'Location',
                        'hidden' => false,
                    ],
                ],
        ],
        [
            'title' => 'Crédito',
            'icon' => 'dollar-sign',
            'hidden' => false,
            'child' => [
                    [
                        'title' => 'Solicitud de créditos',
                        'href' => '/credit-requests',
                        'hidden' => false,
                        'permission' => 'CreditRequest',
                    ],
                ],
        ],
        [
            'title' => 'Configurar envío',
            'icon' => 'pe-7s-menu',
            'hidden' => false,

            //'href' => '/shipping-configurations',
            //'hidden' => false,
        ],
        [
            'title' => 'Configuraciones',
            'icon' => 'link',
            'hidden' => false,
            'child' => [
                [
                    'title' => 'Costos globales',
                    'href' => '/global-costs-configurators',
                    'permission' => 'GlobalCostConfiguration',
                    'hidden' => false,
                ],
                [
                    'title' => 'Costos especiales',
                    'href' => '/special-costs-configurators',
                    'permission' => 'SpecialCostConfiguration',
                    'hidden' => false,
                ],
                [
                    'title' => 'Envíos',
                    'href' => '/shipment-cost-configuration',
                    'hidden' => false,
                    'permission' => 'ShipmentCostConfiguration'
                ],
            ]
        ],
        [
            'title' => 'Salir',
            'href' => '/logout',
            'icon' => 'sign-out-alt',
            'hidden' => false,
            'permission' => '0',
        ],
    ],
];
