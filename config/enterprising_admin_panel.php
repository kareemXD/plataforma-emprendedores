<?php 

return [
  'api_url' => env('ENTERPRISING_ADMIN_PANEL_API_URL', 'http://domain.tld/api'),
  'email' => env('ENTERPRISING_ADMIN_PANEL_USER', 'admin@test.com'),
  'password' => env('ENTERPRISING_ADMIN_PANEL_PASSWORD', 'secret')
];