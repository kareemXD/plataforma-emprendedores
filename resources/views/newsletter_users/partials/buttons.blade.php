@permission('*newsletter_users')
<div class="btn-group dropleft">
    <button class="btn btn-default btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        Opciones
    </button>
    <div class="dropdown-menu">
        @permission('delete_newsletter_users')
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" onclick="_global.toggleStatus('{{ route('newsletter-users.destroy', $newsletter_user->id) }}')" href="#">
                Cambiar estatus
            </a>
        @endpermission
    </div>
</div>
@endpermission