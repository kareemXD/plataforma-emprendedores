@extends('adminlte::page')

@section('title', 'Usuarios del Newsletter')

@section('content_header')
    @component('components.breadcrumb', [
        'current' => 'Listado'
    ])
    @endcomponent
@stop

@section('content')
@if (session()->has('message'))
    <div class="alert-success" id="popup_notification">
        <strong>{{ session('message') }}</strong>
    </div>
@endif
    <div id="accordion">
        <div class="card animated bounceInDown">
            <div class="card-header">
                <h5 class="mb-0">
                    <a class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                        Listado
                    </a>
                    <a href="{{ route('newsletter-users.export') }}" class="btn btn-outline-success float-right" data-toggle="tooltip" data-placement="left" title="Exportar"
                        style="border-color:transparent;color: white;">
                        <i class="fas fa-file-excel fa-lg"></i>
                    </a>
                </h5>
            </div>

            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                <div class="card-body">
                    <table class="table table-sm table-hover table-responsive-lg" id="myTable">
                        <thead>
                        <tr>
                            <th>Email</th>
                            <th>Estatus</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop

@push('js')
    <script>
        $(function () {
            _index.datatable();
        });

        const _index = {
            datatable: function () {
                $('#myTable').DataTable({
                    processing: true,
                    serverSide: true,
                    language:{
                        url: "{{ asset('/js/Spanish.json') }}"
                    },
                    ajax: '/newsletter-users',
                    columns: [
                        { data: 'email', name: 'email' },
                        { data: 'status', name: 'status' },
                        { data: 'actions', name: 'actions', orderable: false, searchable: false, width: '8em' },
                    ]
                });
            }
        }
    </script>
@endpush
