@extends('adminlte::page')

@section('title', 'Configurador de Reglas')

@section('content')      
<div class="card">
  <!-- Nav pills -->
  <ul class="nav nav-pills nav-justified" role="tablist">
    <li class="nav-item">
      <a class="nav-link active" data-toggle="pill" href="#general">General</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" data-toggle="pill" href="#classification">Clasificación</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" data-toggle="pill" href="#storage">Almacenamiento</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" data-toggle="pill" href="#statistics">Estadística</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" data-toggle="pill" href="#dataSheet">Ficha Técnica</a>
    </li>
  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
    <div id="general" class="tab-pane active ml-3 mr-3"><br>
      @include('products.partials.generalForm')
    </div>
    <div id="classification" class="tab-pane fade ml-3 mr-3"><br>
      @include('products.partials.classificationForm')
    </div>
    <div id="storage" class="tab-pane fade ml-3 mr-3"><br>
      @include('products.partials.storageForm')
    </div>
    <div id="statistics" class="container tab-pane fade"><br>
      <h3>Menu 3</h3>
      <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.</p>
    </div>
    <div id="dataSheet" class="container tab-pane fade"><br>
      <h3>Menu 4</h3>
      <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.</p>
    </div>
  </div>
  </div>
@stop
