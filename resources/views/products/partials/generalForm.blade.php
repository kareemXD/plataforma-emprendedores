
    @includeIf('alerts.errors')
    <h4><b>Alta del Producto</b></h4>
    {{ Form::open(['route' => 'rules-configurator.store', 'method' => 'POST', 'class' => 'animated bounceInDown']) }}    
        <div class="form-row">
            <div class="col-3">
                {{ Form::label('Clave Nemotécnica', 'Clave Nemotécnica', ['class' => '']) }}
                {{ Form::text('Descripcion', null, ['class' => 'form-control form-control-sm', 'required', 'placeholder'=>'Ingresa la clave nemotécnica', 'maxlength' => '80']) }}
            </div>
            <div class="col-3">
                {{ Form::label('Descripcion', 'Clave Licitación', ['class' => '']) }}
                {{ Form::text('Descripcion', null, ['class' => 'form-control form-control-sm', 'required', 'placeholder'=>'Ingresa la Clave de licitación', 'maxlength' => '80']) }}
            </div>
            <div class="col-3">
                {{ Form::label('Descripcion', 'Código de Barras', ['class' => '']) }}
                {{ Form::text('Descripcion', null, ['class' => 'form-control form-control-sm', 'required', 'placeholder'=>'Ingresa el código de barras', 'maxlength' => '80']) }}
            </div>
            <div class="col-3">
                {{ Form::label('Descripcion', 'Código SAT', ['class' => '']) }}
                {{ Form::text('Descripcion', null, ['class' => 'form-control form-control-sm', 'required', 'placeholder'=>'Ingresa el código SAT', 'maxlength' => '80']) }}
            </div>
        </div>
        <div class="form-row">
            <div class="col-12">
                {{ Form::label('Clave Nemotécnica', 'Descripción Corta', ['class' => '']) }}
                {{ Form::text('Descrip1', null, ['class' => 'form-control form-control-sm', 'required', 'placeholder'=>'Descripción Corta', 'maxlength' => '80']) }}
            </div>
        </div>
        <div class="form-row">
            <div class="col-12">
                {{ Form::label('Clave Nemotécnica', 'Descripción Larga', ['class' => '']) }}
                {{ Form::textarea('Descrip2', null, ['class' => 'form-control form-control-sm', 'style'=>'height:70px', 'required', 'placeholder'=>'Descripción Larga','maxlength' => '300']) }}
            </div>
        </div>
        <div class="form-row">
            <div class="col-6">
                {{ Form::label('supplier1', 'Proveedor 1', ['class' => '']) }}
                {{ Form::select('supplier1', array('L' => 'Large', 'S' => 'Small'), null, ['class' => 'form-control form-control-sm', 'required']) }}
            </div>
            <div class="col-6">
                {{ Form::label('supplier2', 'Proveedor 2', ['class' => '']) }}
                {{ Form::select('supplier2', array('L' => 'Large', 'S' => 'Small'), null, ['class' => 'form-control form-control-sm', 'required']) }}
            </div>
        </div>
        <div class="form-row">
            <div class="col">
                {{ Form::label('needs_diary', 'Inventariable', ['class' => '']) }}
                {{ Form::checkbox('Inventaria', 1, null, ['class' => 'form-control form-control-sm']) }}
            </div>
            <div class="col">
                {{ Form::label('turn_service', 'En lista de precios', ['class' => '']) }}
                {{ Form::checkbox('turn_service', 1, null, ['class' => 'form-control form-control-sm']) }}
            </div>

            <div class="col">
                {{ Form::label('hour_service', 'Aplica Lote y Caducidad', ['class' => '']) }}
                {{ Form::checkbox('hour_service', 1, null, ['class' => 'form-control form-control-sm']) }}
            </div>

            <div class="col">
                {{ Form::label('event_service', 'Activo', ['class' => '']) }}
                {{ Form::checkbox('event_service', 1, null, ['class' => 'form-control form-control-sm']) }}
            </div>
            <div class="col">
                {{ Form::label('event_service', 'E-commerce', ['class' => '']) }}
                {{ Form::checkbox('event_service', 1, null, ['class' => 'form-control form-control-sm']) }}
            </div>
        </div>
        <div class="form-row">
            <div class="col-12">
                {{ Form::label('Clave Nemotécnica', 'Observaciones', ['class' => '']) }}
                {{ Form::textarea('Observa', null, ['class' => 'form-control form-control-sm', 'style'=>'height:100px', 'placeholder'=>'Observaciones', 'required', 'maxlength' => '100']) }}
            </div>
        </div>
        <div class="form-row">
            <div class="col-12">
                {{ Form::label('Clave Nemotécnica', 'Registro Salubridad', ['class' => '']) }}
                {{ Form::text('Descripcion', null, ['class' => 'form-control form-control-sm', 'placeholder'=>'Registro salubridad', 'required', 'maxlength' => '80']) }}
            </div>
        </div>
        <div class="form-row">
            <div class="col-2">
                {{ Form::label('Clave Nemotécnica', 'IVA', ['class' => '']) }}
                {{ Form::text('IVA', null, ['class' => 'form-control form-control-sm', 'required', 'placeholder'=>'%', 'maxlength' => '80']) }}
            </div>
            <div class="col-2">
                {{ Form::label('Clave Nemotécnica', 'Utilidad Mínima Licitación', ['class' => '']) }}
                {{ Form::text('Descripcion', null, ['class' => 'form-control form-control-sm', 'required', 'placeholder'=>'%', 'maxlength' => '80']) }}
            </div>
            <div class="col-2">
                {{ Form::label('Clave Nemotécnica', 'Flete', ['class' => '']) }}
                {{ Form::text('Descripcion', null, ['class' => 'form-control form-control-sm', 'required', 'placeholder'=>'$', 'maxlength' => '80']) }}
            </div>
            <div class="col-2">
                {{ Form::label('Clave Nemotécnica', 'T Surtido', ['class' => '']) }}
                {{ Form::text('Descripcion', null, ['class' => 'form-control form-control-sm', 'required', 'placeholder'=>'Días', 'maxlength' => '80']) }}
            </div>
        </div>
        
        <div class="mt-3 mb-3" style="text-align: center">
            <button type="submit" class="btn btn-success btn-flat">
                <i class="fas fa-save"></i> Guardar
            </button>
            <a class="btn btn-danger btn-flat" href="{{ url()->route('rules-configurator.index') }}">
                <i class="fas fa-ban"></i> Cancelar
            </a>
        </div>
    {{ Form::close() }}