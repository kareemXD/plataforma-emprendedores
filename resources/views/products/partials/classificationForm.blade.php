
    @includeIf('alerts.errors')
    {{ Form::open(['route' => 'rules-configurator.store', 'method' => 'POST', 'class' => 'animated bounceInDown']) }}    
        <div class="form-row">
            <div class="col-4">
                {{ Form::label('supplier1', 'Categoría', ['class' => '']) }}
                {{ Form::select('category', array('L' => 'Large', 'S' => 'Small'), null, ['class' => 'form-control form-control-sm', 'required']) }}
            </div>
            <div class="col-4">
                {{ Form::label('supplier2', 'Familia', ['class' => '']) }}
                {{ Form::select('supplier2', array('L' => 'Large', 'S' => 'Small'), null, ['class' => 'form-control form-control-sm', 'required']) }}
            </div>
            <div class="col-4">
                {{ Form::label('supplier2', 'Marca', ['class' => '']) }}
                {{ Form::select('supplier2', array('L' => 'Large', 'S' => 'Small'), null, ['class' => 'form-control form-control-sm', 'required']) }}
            </div>
        </div>

        <div class="form-row">
            <div class="col-4">
                {{ Form::label('supplier1', 'Especialidad', ['class' => '']) }}
                {{ Form::select('supplier1', array('L' => 'Large', 'S' => 'Small'), null, ['class' => 'form-control form-control-sm', 'required']) }}
            </div>
            <div class="col-4">
                {{ Form::label('supplier2', 'Estatus', ['class' => '']) }}
                {{ Form::select('supplier2', array('L' => 'Large', 'S' => 'Small'), null, ['class' => 'form-control form-control-sm', 'required']) }}
            </div>
            <div class="col-4">
                {{ Form::label('supplier2', 'Origen', ['class' => '']) }}
                {{ Form::select('supplier2', array('L' => 'Large', 'S' => 'Small'), null, ['class' => 'form-control form-control-sm', 'required']) }}
            </div>
        </div>
        
        <div class="mt-3 mb-3" style="text-align: center">
            <button type="submit" class="btn btn-success btn-flat">
                <i class="fas fa-save"></i> Guardar
            </button>
            <a class="btn btn-danger btn-flat" href="{{ url()->route('rules-configurator.index') }}">
                <i class="fas fa-ban"></i> Cancelar
            </a>
        </div>
    {{ Form::close() }}