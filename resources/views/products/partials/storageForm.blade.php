@includeIf('alerts.errors')
{{ Form::open(['route' => 'rules-configurator.store', 'method' => 'POST', 'class' => 'animated bounceInDown']) }}    
    <div class="form-row">
        <div class="col-3">
            {{ Form::label('supplier1', 'Nivel', ['class' => '']) }}
            {{ Form::select('category', array('L' => 'Large', 'S' => 'Small'), null, ['class' => 'form-control form-control-sm', 'style'=>'width:10px', 'required']) }}
        </div>
        <div class="col-3">
            {{ Form::label('supplier2', 'Empaque', ['class' => '']) }}
            {{ Form::select('supplier2', array('L' => 'Large', 'S' => 'Small'), null, ['class' => 'form-control form-control-sm', 'required']) }}
        </div>
        <div class="col-3">
            {{ Form::label('Clave Nemotécnica', 'Unidades', ['class' => '']) }}
            {{ Form::number('Descrip2', null, ['class' => 'form-control form-control-sm', 'required', 'placeholder'=>'Descripción Larga']) }}
        </div>
        <div class="col-3">
            {{ Form::label('supplier2', 'Nivel Compra', ['class' => '']) }}
            {{ Form::select('supplier2', array('L' => 'Large', 'S' => 'Small'), null, ['class' => 'form-control form-control-sm', 'required']) }}
        </div>
    </div>
    
    <div class="mt-3 mb-3" style="text-align: center">
        <button type="submit" class="btn btn-success btn-flat">
            <i class="fas fa-save"></i> Guardar
        </button>
        <a class="btn btn-danger btn-flat" href="{{ url()->route('rules-configurator.index') }}">
            <i class="fas fa-ban"></i> Cancelar
        </a>
    </div>
{{ Form::close() }}