@permission('*article')
<div class="btn-group dropleft">
    <button class="btn btn-default btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        Opciones
    </button>
    <div class="dropdown-menu">
        @permission('list_article')
            <a class="dropdown-item" data-rule="{{ json_encode($product) }}" onclick="window.fillMdl(this)" href="#"
               data-toggle="modal" data-target="#show-modal">
                Detalles
            </a>
        @endpermission
        @permission('edit_article')
            <a href="{{ route('rules-configurator.edit', $product->Id_Prod1) }}" class="dropdown-item">
                Editar
            </a>
        @endpermission
        @permission('delete_article')
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" onclick="_global.toggleStatus('{{ route('rules-configurator.destroy', $product->Id_Prod1) }}')" href="#">
                Cambiar estatus
            </a>
        @endpermission
    </div>
</div>
@endpermission
