@extends('adminlte::page')

@section('title', 'Banners')

@section('content_header')
    @component('components.breadcrumb', [
       'links' => [
           'Listado' => url('banners')
       ],
       'current' => 'Editar'
   ])
    @endcomponent
@stop

@section('content')
    @includeIf('alerts.errors')
    {{ Form::model($banner, ['route' => ['banners.update',$banner->id], 'method' => 'PUT', 'class' => 'animated bounceInDown', 'files' => true]) }}
        <div id="accordion">
            <div class="card animated bounceInDown">
                <div class="card-header">
                    <h5 class="mb-0">
                        <a class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                            Información
                        </a>
                    </h5>
                </div>

                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                    <div class="card-body">
                        @includeIf('banners.partials.inputs')
                    </div>
                </div>
            </div>
        </div>
        <div style="text-align: center">
            <button type="submit" class="btn btn-success btn-flat">
                <i class="fas fa-save"></i> Actualizar
            </button>
            <a class="btn btn-danger btn-flat" href="{{ url()->previous() }}">
                <i class="fas fa-ban"></i> Cancelar
            </a>
        </div>
    {{ Form::close() }}
@stop
