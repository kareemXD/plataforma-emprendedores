@extends('adminlte::page')

@section('title', 'Banners')

@section('content_header')
    @component('components.breadcrumb', [
        'current' => 'Listado'
    ])
    @endcomponent
@stop

@section('content')
@if (session()->has('message'))
    <div class="alert-success" id="popup_notification">
        <strong>{{ session('message') }}</strong>
    </div>
@endif
    <div id="accordion">
        <div class="card animated bounceInDown">
            <div class="card-header">
                <h5 class="mb-0">
                    <a class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                        Listado
                    </a>
                    <a href="{{ route('banners.create') }}" class="btn btn-outline-success float-right" data-toggle="tooltip" data-placement="left" title="Agregar Nuevo"
                       style="border-color:transparent;color: white;">
                        <i class="fas fa-plus-circle fa-lg"></i>
                    </a>
                </h5>
            </div>

            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                <div class="card-body table-responsive">
                    <table class="table" id="myTable">
                        <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>Imagen</th>
                            <th>URL</th>
                            <th>Estatus</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>

    @includeIf('banners.partials.show')
@stop

@push('js')
    <script>
        $(function () {
            _index.datatable();
        });

        const _index = {
            $mdl: $('#show-modal'),
            datatable: function () {
                $('#myTable').DataTable({
                    processing: true,
                    serverSide: true,
                    language:{
                        url: "{{ asset('/js/Spanish.json') }}"
                    },
                    ajax: '/banners',
                    columns: [
                        { data: 'name', name: 'name' },
                        { data: 'image', name: 'image' },
                        { data: 'url', name: 'url' },
                        { data: 'status', name: 'status' },
                        { data: 'actions', name: 'actions', orderable: false, searchable: false, width: '8em' },
                    ]
                });
            },
            fillMdl: function (that) {
                this.$mdl.find('label').html('');
                let data = JSON.parse(that.dataset['banner']);
                $.each(data, (idx, val) => {
                    if(idx == 'image'){
                        document.getElementById("show_img").src= `/storage/${val}`;
                    } else {
                        this.$mdl.find(`#${idx}`).html(val);
                    }
                });
            }
        }
    </script>
@endpush
