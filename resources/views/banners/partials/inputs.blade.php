<div class="form-row">
    <div class="col-6">
        {{ Form::label('name', 'Nombre:', ['class' => '']) }}
        {{ Form::text('name', null, ['class' => 'form-control form-control-sm', 'required', 'maxlength' => '191']) }}
    </div>
    <div class="col-6">
        {{ Form::label('url', 'URL:', ['class' => '']) }}
        {{ Form::text('url', null, ['class' => 'form-control form-control-sm', 'maxlength' => '191', 'required']) }}
    </div>
    <div class="col-6">
        {{ Form::label('image_file', 'Imagen:', ['class' => '']) }}
        {{ Form::file('image_file', ['class' => 'form-control form-control-sm', 'required', 'accept' => 'image/png, image/jpeg']) }}
    </div>
</div>
