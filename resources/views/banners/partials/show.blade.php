<div class="modal fade" id="show-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Detalles</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row mdl-groups">
                    <div class="form-group col-md-6 col-sm-12">
                        {!! Form::labelMdl('name', 'Nombre') !!}
                    </div>
                    <div class="form-group col-md-6 col-sm-12">
                        {!! Form::labelMdl('url', 'URL') !!}
                    </div>
                </div>
                <div class="row mdl-groups mt-3 text-center">
                    <div class="form-group col-md-2 col-sm-12">
                        Imagen:
                    </div>
                    <div class="form-group col-md-10 col-sm-12">
                        <img id="show_img" alt="Banner image" class="img-fluid"/>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
