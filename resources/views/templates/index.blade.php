@extends('adminlte::page')

@section('htmlheader_title')
    Plantillas
@endsection

@section('contentheader_title')
    Plantillas
@endsection

@section('content')
    @if($template!=null)
        @include('templates.edit')
    @else
        @include('templates.create')
    @endif
    @if(!isset($show))
        <div id="accordion">
            <div class="card animated bounceInDown">
                <div class="card-header">
                    <h5 class="mb-0">
                        <a class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                            Configuración Global
                        </a>
                    </h5>
                </div>

            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                <div class="card-body table-responsive">
                    <table class="table table-bordered table-hover" id="template-table">
                        <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>Descripción</th>
                            <th>Estatus</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    @endif
    @include('templates.partials.modal_inputs')
    @include('templates.partials.script')
@endsection
