@push('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.js"
        integrity="sha256-kiFgik3ybDpn1VOoXqQiaSNcpp0v9HQZFIhTgw1c6i0=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/dropzone.js"></script>
    <script src="https://cdn.ckeditor.com/4.12.1/standard/ckeditor.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/min/dropzone.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.0/axios.min.js"
        integrity="sha256-S1J4GVHHDMiirir9qsXWc8ZWw74PHHafpsHp5PXtjTs=" crossorigin="anonymous"></script>
    <script>
        Dropzone.autoDiscover = false;
        $inputs = $('#inputs-modal');

        @if(request()->is('templates'))

            /**
             * @author Luis Peña <luis.pena@nuvem.mx> 13/11/2020
             */
            $('body').delegate('.store_template','click',function(){
                $('#store_template').click();
            });

            /**
             * change status
             * @author Luis Peña <luis.pena@nuvem.mx> 13/11/2020
             */
            $('body').delegate('.status-template','click',function(){
                id_template = $(this).attr('id_template');

                swal({
                    title: 'Está seguro?',
                    text: "¿Quiéres eliminar está plantilla?",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    cancelButtonText: 'Cancel',
                    confirmButtonText: 'Si!'
               }).then((result) => {
                   if (result.value) {
                       $.ajax({
                           url: '{{ URL::to("templates") }}/' + id_template,
                           headers:{'X-CSRF-TOKEN':'{{ csrf_token() }}'},
                           type: 'DELETE',
                           data: {
                              id: id_template
                           }
                       }).done(function(data){
                           dTable.ajax.reload();
                       });


                   }
               });
            });

            /**
             * add contenido
             * @author Luis Peña <luis.pena@nuvem.mx> 13/11/2020
             */
            $('body').delegate('.get-template-data','click',function(){
                id_template = $(this).attr('id_template');
                axios.get('/templates' + '/' + id_template,{
                    data : {id: id_template}
                }).then(function (response) {

                    $inputs.find("#name").html(response.data.name);
                    $inputs.find("#affair").html(response.data.affair);
                    $inputs.find("#type").html(response.data.type);

                    if ("#content_template") {
                        $("#content_template").remove();
                    }
                    $("#start").append('<div class="form-group" id="content_template" style="width:570px; height:300px; overflow:auto;" >Contenido:</div>');
                    $("#content_template").append(response.data.content);
                }).catch(function (error) {
                }).then(function () {});
            });//BUTTON
        @elseif(request()->is('templates/*/edit'))
            /**
             * delete file
             * @author Luis Peña <luis.pena@nuvem.mx> 13/11/2020
             */
            $('body').delegate('.file-delete','click',function(){
                file_id = $(this).attr('file_id');

                swal({
                    title: 'Está seguro?',
                    text: "¿Quiéres eliminar el archivo?",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    buttonsStyling: false,
                    cancelButtonText: 'No',
                    confirmButtonText: "Si"
               }).then((result) => {
                   if (result.value) {
                       $.ajax({
                           url: '{{ URL::to("templates/files") }}/' + file_id,
                           type: 'GET',
                           data: {
                               img: file_id
                           }
                       }).done(function(data){
                           location.reload(true);
                       });


                   }
               });
            });//BUTTON
        @elseif(request()->is('templates/create'))
        @endif

        $(document).ready(function () {
            /**
             * ckeditor start
             * @author Luis Peña <luis.pena@nuvem.mx> 13/11/2020
             */
            @if(request()->is('*/edit'))
                @if( empty(old('description')))
                    CKEDITOR.instances.editor1.setData({!! json_encode($template->description) !!});
                @else
                    CKEDITOR.instances.editor1.setData({!! json_encode(old('description') ) !!});
                @endif
            @elseif(request()->is('*/create'))
                @if( !empty(old('description')))
                    CKEDITOR.instances.editor1.setData({!!json_encode(old('description') )!!});
                @endif
            @endif

            /**
             * dropzone
             * @author Luis Peña <luis.pena@nuvem.mx> 13/11/2020
             */
            var filesArray = [];
            var maxFiles = 5; // Max files for dropzone


            $("#dropzone").dropzone(
            {
                url:'#',
                paramName: "file",
                autoProcessQueue: false,
                parallelUploads: 21,
                maxFiles: 5,
                uploadMultiple: true,
                acceptedFiles: "image/*",
                init: function () {
                    var wrapperThis = this;

                    //When dropzone detected the max file as exceeded, then removes the las file attempted to attach.
                    this.on("maxfilesexceeded", function (file) {
                        wrapperThis.removeFile(file);
                    });

                    this.on("addedfile", function (file) {

                        // Validates if user has exceeded the max amount of possible files.
                        if ((filesArray.length + 1) > maxFiles) {
                            toastr.warning('Ya no puede agregar otra imagen, ya que alcanzó el número máximo posible.', 'Atención!');
                            return;
                        }

                        items = {};
                        items['fileName'] = file.name;
                        items['file'] = file;
                        filesArray.push(items);

                        // Create edit button
                        var editButton = Dropzone.createElement("<button class='btn btn-sm btn-success' data-toggle='modal' data-target='#modal' title='Recortar'><i class='flaticon flaticon-edit'></i></button>");
                        // Listen to the click event
                        editButton.addEventListener("click", function (e) {
                            e.preventDefault();
                            indexEdit =  filesArray.findIndex(filesArray => filesArray.fileName === file.name); //get the index from the file to replace
                            image.attr('src', file.dataURL); //Set the image to modal
                        });
                        // Add edit button to preview
                        file.previewElement.appendChild(editButton);
                        // crear boton de eliminar
                        var removeButton = Dropzone.createElement("<button class='btn dark'>Eliminar</button>");
                        // Listen to the click event
                        removeButton.addEventListener("click", function (e) {
                            e.preventDefault();
                            e.stopPropagation();

                            // eliminar vista previa del file
                            var index =  filesArray.findIndex(filesArray => filesArray.fileName === file.name);
                            filesArray.splice( index, 1 );

                            wrapperThis.removeFile(file);
                            showMaxFilesCounter();
                        });
                        // agregar boton de eliminar a la vista previa
                        file.previewElement.appendChild(removeButton);
                        showMaxFilesCounter();
                    });

                    showMaxFilesCounter();
                }
            });

            /**
             * count files and show
             * @author Luis Peña <luis.pena@nuvem.mx> 13/11/2020
             */
            function showMaxFilesCounter()
            {
                $('#max-files').html(`( ${filesArray.length} / ${maxFiles} )`);
            }

            /**
             * save template
             * @author Luis Peña <luis.pena@nuvem.mx> 13/11/2020
             */
            $('.saveProduct').click(function () {
                var validInput = true;
                var content = CKEDITOR.instances.editor1.getData();
                var name = $('#name').val();
                var affair = $('#affair').val();

                var formData = new FormData();

                formData.append('content',content);
                formData.append('name',name);
                formData.append('affair',affair);

                $.each(filesArray, function(idx, obj) {
                    formData.append('images[]',filesArray[idx].file);
                });

                $.ajax({
                    url: '/templates',
                    headers:{'X-CSRF-TOKEN':'{{ csrf_token() }}'},
                    type: 'POST',
                    processData: false,
                    contentType: false,
                    data:formData,
                    success:function(data){
                        window.location = "/templates";
                    },
                    error: function(XMLHttpRequest, textStatus, errorThrown) {

                    var htmlErrors = '';

                    if(XMLHttpRequest.responseJSON.errors != undefined){
                        htmlErrors += '<ul>';
                        $.each(XMLHttpRequest.responseJSON.errors, function(idx, obj) {
                            for(var i = 0; i < obj.length; i++ ){
                                htmlErrors += '<li>'+obj[i]+'</li>';
                            }
                        });
                        htmlErrors += '</ul>';
                    }else{
                        htmlErrors += '<p>'+ XMLHttpRequest.statusText+ ' ' + XMLHttpRequest.status + ' ' +XMLHttpRequest.responseJSON.message +'<p>';
                    }

                    SwalAlert(htmlErrors);

                }

                });

                $(this).prop("disabled", false);


            });

            /**
             * edit template
             * @author Luis Peña <luis.pena@nuvem.mx> 13/11/2020
             */
            @if(request()->is('*/edit'))
                $('.updateProduct').click(function () {
                    var validInput = true;
                    var content = CKEDITOR.instances.editor1.getData();
                    var name = $('#name').val();
                    var affair = $('#affair').val();

                    var formData = new FormData();

                    formData.append('content',content);
                    formData.append('name',name);
                    formData.append('affair',affair);

                    $.each(filesArray, function(idx, obj) {
                        formData.append('images[]',filesArray[idx].file);
                    });
                    $.ajax({
                        url: '/templates/update/' + {{$template->id}},
                        headers:{'X-CSRF-TOKEN':'{{ csrf_token() }}'},
                        type: 'POST',
                        processData: false,
                        contentType: false,
                        data:formData,
                        success:function(data){
                            window.location = "/templates";
                        },
                        error: function(XMLHttpRequest, textStatus, errorThrown) {

                        var htmlErrors = '';

                        if(XMLHttpRequest.responseJSON.errors != undefined){
                            htmlErrors += '<ul>';
                            $.each(XMLHttpRequest.responseJSON.errors, function(idx, obj) {
                                for(var i = 0; i < obj.length; i++ ){
                                    htmlErrors += '<li>'+obj[i]+'</li>';
                                }
                            });
                            htmlErrors += '</ul>';
                        }else{
                            htmlErrors += '<p>'+ XMLHttpRequest.statusText+ ' ' + XMLHttpRequest.status + ' ' +XMLHttpRequest.responseJSON.message +'<p>';
                        }

                        SwalAlert(htmlErrors);

                    }

                    });

                    $(this).prop("disabled", false);


                });
            @endif

            function SwalAlert(htmlErrors) {
                Swal.fire({
                    type: 'warning',
                    title: 'Oops...',
                    html: htmlErrors
                });
            }

            /**
             * clear inputs
             * @author Luis Peña <luis.pena@nuvem.mx> 13/11/2020
             */
            var pathname = window.location.pathname;
            if(pathname.includes("edit") != true) {
                $inputs.find('#name-id').html('');
                $inputs.find('#affair-id').html('');
                $inputs.find('#type-id').html('Bienvenida');
            }

            var $owl = $('.owl-carousel').owlCarousel({
                loop:false,
                margin:10,
                nav:true,
                responsive:{
                    0:{
                        items:1
                    },
                    600:{
                        items:3
                    },
                    1000:{
                        items:5
                    }
                }
            })

        });


        /**
         * data table
         * @author Luis Peña <luis.pena@nuvem.mx> 13/11/2020
         */
        var dTable = $("#template-table").DataTable({
            ajax: '/templates?dt=index',
            columns: [
                {data: 'name'},
                {data: 'affair'},
                {data: 'status'},
                {data: 'actions', name: 'actions', orderable: false, serchable: false,  bSearchable: false},
            ],
        });

    </script>
@endpush
