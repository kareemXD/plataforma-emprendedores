<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
    <label for="templateName_lbl" class="col-sm-3 control-label">Nombre:</label>
    <div class="col-sm-9">
        {!!Form::text('name', $template ? $template->name : null,['class'=>'form-control', 'id'=>'name'])!!}
    </div>
</div>
<div class="form-group{{ $errors->has('affair') ? ' has-error' : '' }}">
    <label for="displayName_lbl" class="col-sm-3 control-label">Asunto:</label>
    <div class="col-sm-9">
        {!!Form::text('affair', $template ? $template->affait : null,['class'=>'form-control', 'id'=>'affair'])!!}
    </div>
</div>

<textarea class="ckeditor" name="description" id="editor1" rows="10" cols="80">
        {{ $template ? $template->content : null}}
</textarea>
