<div class="modal fade" id="show-data">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header header-nuvem"  style="background: #1792a4; color: white;">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Datos de Plantillas</h4>
            </div>
            <div class="modal-body" id="inputs-modal">
                <div class="form-group">
                    <small class="lbl_modal">Nombre:</small>
                    {!! Form::label('name', null, ['class'=>'form-control', 'id'=>'name']) !!}
                </div>
                <div class="form-group">
                    <small class="lbl_modal">Asunto:</small>
                    {!! Form::label('affair', null, ['class'=>'form-control', 'id'=>'affair']) !!}
                </div>
                <div class="form-group" id="start">
                </div>
            </div>
            <div class="modal-footer background-nuvem">
                <a href="#" data-dismiss="modal" class="btn btn-default">Cerrar</a>
            </div>
        </div>
    </div>
</div>
