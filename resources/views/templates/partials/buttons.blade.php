<a data-toggle="modal" id_template="{{$templates->id}}" data-target="#show-data"
   class="btn btn-info btn-sm get-template-data">
    <i class="glyphicon glyphicon-info-sign"></i>
    <t class="hidden-xs " style="color: white;">Mostrar</t>
</a>
<a href="{{ route('templates.edit', ['template' => $templates->id]) }}"  class="btn btn-success btn-sm">
    <span class="fa fa-edit"></span> Editar
</a>
<a id_template="{{ $templates->id }}" class="btn btn-danger btn-sm status-template"
   template_name="{{ $templates->name }}" style="color: white;">
    <span class="fa fa-trash"></span> Eliminar
</a>
