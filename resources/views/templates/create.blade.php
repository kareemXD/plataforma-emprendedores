@include('alerts.errors')

<div id="accordion">
    <div class="card animated bounceInDown">
        <div class="card-header">
            <h5 class="mb-0">
                <a class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                    Configuración Global
                </a>
            </h5>
        </div>

        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
            <div class="card-body table-responsive">
                {!!Form::open(['route'=>'templates.store', 'method'=>'POST',
                'class' => 'form-horizontal' ,'files' => true])!!}
                    @include('templates.partials.inputs', ['template' => null])
                    <div class="form-group m-form__group">
                        <span  style="color: red" class="required-val">* </span>
                            {!! Form::label('Imágenes') !!} <span id="max-files"></span>
                            <div class="m-dropzone dropzone m-dropzone--primary"  id="dropzone">
                                <div class="m-dropzone__msg dz-message needsclick">
                                    <h3 class="m-dropzone__msg-title">  Haga click para cargar archivos.</h3>
                                    <span class="m-dropzone__msg-desc">Agregar archivos de 100 x 100 px</span>
                                </div>
                            </div>
                    </div>
                    <div class="text-center" >
                        <div class="form-group">
                            <button type="button"  onClick="this.disabled='disabled'" class="btn btn-success saveProduct" >Aceptar</button>
                            <a  class="btn btn-secondary" href="{{URL::route('templates.index')}}">Cancelar</a>
                        </div>
                    </div>
                {!!Form::close()!!}


            </div>
        </div>
    </div>
</div>
