@extends('layouts.store.master')
@section('page','Inicio')
@section('classes_body','no-mg no-pd')
@section('body')
    @include('layouts.store.carousel', ['banners' => $banners])
    <h5 class="section-title">Nuestros productos destacados</h5>
    @include('layouts.store.products')
    @include('layouts.store.brand_carousel')
    <section class="">
        <div class="row no-mg custom-banners">
            <div class="light-green custom-banner col-12 col-md-5">
                <div class="custom-banner-container">
                    <img src="{{asset('/img/registradora.png')}}" alt="">
                </div>
                <div>
                    <div class="custom-banner-body">
                        <p class="custom-banner-title">Eléctrocardiograma</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
                            labore et dolore magna aliqua</p>
                        <p class="color-blue">desde <span class="font-bg">$2,500</span></p>
                        <button class="button green">Agregar al carrito</button>
                        <a class="sm-link-button mt-1" href="#">Ver más +</a>
                    </div>
                </div>
            </div>
            <div class="light-blue custom-banner col-12 col-md-5">
                <div class="custom-banner-container">
                    <img src="{{asset('/img/dove.png')}}" alt="">
                </div>
                <div>
                    <div class="custom-banner-body">
                        <p class="custom-banner-title">Eléctrocardiograma</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
                            labore et dolore magna aliqua</p>
                        <p class="color-blue">desde <span class="font-bg">$2,500</span></p>
                        <button class="button blue">Agregar al carrito</button>
                        <a class="sm-link-button mt-1" href="#">Ver más +</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="product-filter-group">
        <p class="product-filter no-mg no-pd">Ofertas</p>
        <p class="no-mg no-pd">Más vendidos</p>
        <p class="no-mg no-pd">Nuevos productos</p>
    </div>

    @include('layouts.store.products')
    <section class="gray pd-4">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-4 invoice-card">
                    <div class="section-invoice panel-container">
                        <p>Facturación electrónica</p>
                        <button class="button white color-blue sm-button">INGRESA AQUÍ</button>
                    </div>
                </div>
                <div class="col-12 col-md-4 invoice-card">
                    <div class="section-invoice panel-container">
                        <p>Facturación electrónica</p>
                        <button class="button white color-blue sm-button">INGRESA AQUÍ</button>
                    </div>
                </div>
                <div class="col-12 col-md-4 invoice-card">
                    <div class="section-invoice panel-container">
                        <p>Facturación electrónica</p>
                        <button class="button white color-blue sm-button">INGRESA AQUÍ</button>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @include('layouts.store.newsletter')
    @include('layouts.store.blog_entries')
    @include('layouts.store.testimonials')
    @push('scripts')
        <link rel="stylesheet"
              href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
        <link rel="stylesheet"
              href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js" integrity="sha512-AA1Bzp5Q0K1KanKKmvN/4d3IRKVlv9PYgwFPvm32nPO6QS8yH1HO7LbgB1pgiOxPtfeg5zEn2ba64MUcqJx6CA==" crossorigin="anonymous"></script>
        <script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
        <script type="text/javascript" src="{{asset('/js/slick.min.js')}}"></script>
        <script src="{{asset('/js/brand_carousel.js')}}"></script>
        <script>
            /**
             *
             * @author ramiro.garcilazo@losfra.dev
             * @created 2020-10-25
             * @param
             * @return void
             */
            function openNav() {
                document.getElementById("mySidebar").style.width = "250px";
                document.getElementById("main").style.marginLeft = "250px";
            }

            /**
             *
             * @author ramiro.garcilazo@losfra.dev
             * @created 2020-10-25
             * @param
             * @return void
             */
            function closeNav() {
                document.getElementById("mySidebar").style.width = "0";
                document.getElementById("main").style.marginLeft = "0";
            }

            /**
             *
             * @author  ramiro.garcilazo@losfra.dev
             * @created 2020-11-13
             * @param event e
             * @return void
             */
            function submitNewsletterUser(e) {
                var key=e.keyCode || e.which;
                if (key==13){
                    $.ajax({
                        type:'POST',
                        url: '{{ route("newsletter-users.submit") }}',
                        data: {_token: $('meta[name="csrf-token"]').attr('content'), email: $('#newsletter-input').val()},
                        success: function(data) {
                            $('#newsletter-input').val('');
                            swal ("Éxito" , "Ahora recibirás nuestras actualizaciones tu bandeja de entrada" , "success");
                        },
                        error: function(err) {
                            if(err.status == 422){
                                swal ("Error" , "Este correo es inválido o ya está registrado en el newsletter" , "error");
                            } else {
                                swal ("Error" , "Ocurrió un error, intenta de nuevo" , "error");
                            }
                        }
                    });
                }
            }
        </script>

    @endpush
@endsection
