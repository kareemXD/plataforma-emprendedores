@extends('layouts.store.master')
@section('page','Inicio')
@section('classes_body','no-mg no-pd')
@section('body')
    @include('layouts.store.breadcrum')
    @include('layouts.store.product')
    <h5 class="section-title">Productos relacionados</h5>
    @include('layouts.store.related_products')
    @include('layouts.store.newsletter')

@endsection
