@extends('adminlte::page')

@section('title', 'Roles')

@section('content_header')
    @component('components.breadcrumb', [
        'current' => 'Listado'
    ])
    @endcomponent
@stop

@section('content')
    <div id="accordion">
        <div class="card animated bounceInDown">
            <div class="card-header">
                <h5 class="mb-0">
                    <a class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                        Listado
                    </a>
                    <a href="{{ route('roles.create') }}" class="btn btn-outline-success float-right" data-toggle="tooltip" data-placement="left" title="Agregar Nuevo"
                       style="border-color:transparent;color: white;">
                        <i class="fas fa-plus-circle"></i>
                    </a>
                </h5>
            </div>

            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                <div class="card-body">
                    <table class="table table-sm table-hover table-responsive-lg" id="myTable">
                        <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>Descripción</th>
                            <th>Estatus</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>

    @includeIf('roles.partials.show')
    @includeIf('roles.permissions.modal')
@stop

@push('js')
    <script>
        $(function () {
            _index.datatable();
            _index.permissions.initialize();
            $('.select2').hide();
        });

        let _index = {
            $mdl: $('#show-modal'),
            datatable: function () {
                $('#myTable').DataTable({
                    processing: true,
                    serverSide: true,
                    language:{
                        url: "{{ asset('/js/Spanish.json') }}"
                    },
                    ajax: '/roles',
                    columns: [
                        { data: 'display_name', name: 'display_name' },
                        { data: 'description', name: 'description' },
                        { data: 'status', name: 'status' },
                        { data: 'actions', name: 'actions', orderable: false, searchable: false, width: '10em' },
                    ]
                });
            },
            fillMdl: function (that) {
                this.$mdl.find('label').html('');
                let data = JSON.parse(that.dataset['user']);
                $.each(data, (idx, val) => {
                    this.$mdl.find(`#${idx}`).html(val);
                });
            },
            permissions: {
                roleId: null,
                $selector: $('#permissions-select'),
                get: function (that) {
                    this.$selector.find('option').prop('selected', false);
                    this.roleId = that.dataset['roleId'];
                    axios.get(`/roles/${this.roleId}/permissions`)
                        .then(resp => {
                            let permissions = resp.data;
                            $.each(permissions, (i, val) => {
                                this.$selector.find(`option[value="${val.id}"]`).prop('selected', true);
                            });
                            this.$selector.multiSelect('refresh');
                        })
                        .catch(err => {
                            _swal.toast('Error!', 'error', 'No se pudo cargar los permisos');
                        });
                },
                assign: function (perms) {
                    axios.post(`roles/${this.roleId}/permissions/${perms[0]}/assign`, {perms: perms})
                        .then(resp => {
                            toastr.success('Se asignó el rol correctamente.', 'Asignado');
                        })
                        .catch(err => {
                            console.error({err});
                        });
                },
                unassign: function (perms) {
                    axios.post(`roles/${this.roleId}/permissions/${perms[0]}/unassign`, {perms: perms})
                        .then(resp => {
                            toastr.info('Se desasignó el rol correctamente.', 'Desasignado');
                        })
                        .catch(err => {
                                console.error({err});
                        });
                },
                assignAll: function() {
                    this.$selector.multiSelect('select_all');
                },
                unassignAll: function() {
                    this.$selector.multiSelect('deselect_all');
                },
                initialize: function () {
                    let ms = this;
                    this.$selector.multiSelect({
                        selectableHeader: "<h4 class='text-center'><b>Permisos <span class='text-danger'>NO</span> asignados</h4></b>",
                        selectionHeader: "<h4  class='text-center'><b>Permisos asignados</h4></b>",
                        selectableFooter: '<a class="btn btn-default btn-flat btn-sm btn-block" onclick="_index.permissions.unassignAll()">Desasignar todos</a>',
                        selectionFooter: '<a class="btn btn-default btn-flat btn-sm btn-block" onclick="_index.permissions.assignAll()">Asignar todos</a>',
                        afterSelect:function(value){
                            ms.assign(value);
                        },
                        afterDeselect:function(value){
                            ms.unassign(value);
                        }
                    });
                }
            }
        };
    </script>
@endpush
