<div class="modal fade" id="show-modal" role="dialog" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Detalles</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row mdl-groups">
                    <div class="form-group col-md-6 col-sm-12">
                        {!! Form::labelMdl('display_name', 'Nombre') !!}
                    </div>
                    <div class="form-group col-md-6 col-sm-12">
                        {!! Form::labelMdl('description', 'Descripción', ['style' => 'height:100%']) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
