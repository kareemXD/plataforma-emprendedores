<div class="form-row">
    <div class="col-6">
        {{ Form::label('display_name', 'Nombre:', ['class' => 'control-label']) }}
        {{ Form::text('display_name', null, ['class' => 'form-control form-control-sm', 'required', 'maxlength' => '50']) }}
    </div>
    <div class="col-6">
        {{ Form::label('description', 'Descripción:', ['class' => 'control-label']) }}
        {{ Form::textarea('description', null, ['class' => 'form-control form-control-sm', 'rows' => 3, 'maxlength' => '191']) }}
    </div>
</div>
