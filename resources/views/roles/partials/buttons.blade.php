@permission('*roles')
<div class="btn-group dropleft">
    <button class="btn btn-default btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        Opciones
    </button>
    <div class="dropdown-menu">
        @permission('list_roles')
        <a class="dropdown-item" data-role-id="{{ json_encode($role->id) }}" onclick="_index.permissions.get(this)"
           data-toggle="modal" data-target="#permissions">
            Permisos
        </a>
        @endpermission

        @permission('list_roles')
        <a class="dropdown-item" data-user="{{ json_encode($role) }}" onclick="_index.fillMdl(this)"
           data-toggle="modal" data-target="#show-modal">
            Detalles
        </a>
        @endpermission

        @permission('edit_roles')
        <a href="{{ route('roles.edit', $role->id) }}" class="dropdown-item" data-toggle="tooltip">
            Editar
        </a>
        @endpermission

        @permission('delete_roles')
        <div class="dropdown-divider"></div>
        <a class="dropdown-item" onclick="_global.toggleStatus('{{ route('roles.destroy', $role->id) }}')" href="#">
            Cambiar estatus
        </a>
        @endpermission
    </div>
</div>
@endpermission
