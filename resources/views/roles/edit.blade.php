@extends('adminlte::page')

@section('title', 'Roles')

@section('content_header')
    @component('components.breadcrumb', [
       'links' => [
           'Listado' => url('roles')
       ],
       'current' => 'Editar'
   ])
    @endcomponent
@stop

@section('content')
    @includeIf('alerts.errors')
    {{ Form::model($role, ['route' => ['roles.update',$role->id], 'method' => 'PUT', 'class' => 'animated bounceInDown']) }}
        <div id="accordion">
            <div class="card animated bounceInDown">
                <div class="card-header">
                    <h5 class="mb-0">
                        <a class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                            Información
                        </a>
                    </h5>
                </div>

                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                    <div class="card-body">
                        @includeIf('roles.partials.inputs')
                    </div>
                </div>
            </div>
        </div>
        <div style="text-align: center">
            <button type="submit" class="btn btn-success btn-flat">
                <i class="fas fa-save"></i> Actualizar
            </button>
            <a class="btn btn-danger btn-flat" href="{{ url()->previous() }}">
                <i class="fas fa-ban"></i> Cancelar
            </a>
        </div>
    {{ Form::close() }}
@stop
