<div class="form-row">
    <div class="col-6">
        {{ Form::label('Descripcion', 'Descripción:', ['class' => '']) }}
        {{ Form::text('Descripcion', null, ['class' => 'form-control form-control-sm', 'required', 'maxlength' => '80']) }}
    </div>
    <div class="col-6">
        {{ Form::label('Prioridad', 'Prioridad:', ['class' => '']) }}
        {{ Form::number('Prioridad', null, ['class' => 'form-control form-control-sm', 'required', 'min' => 0, 'maxlength' => '100']) }}
    </div>
</div>