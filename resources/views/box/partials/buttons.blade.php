@permission('*rules_configurator')
<div class="btn-group dropleft">
    <button class="btn btn-default btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        Opciones
    </button>
    <div class="dropdown-menu">
        @permission('list_rules_configurator')
            <a class="dropdown-item" data-rule="{{ json_encode($rule) }}" onclick="window.fillMdl(this)" href="#"
               data-toggle="modal" data-target="#show-modal">
                Detalles
            </a>
        @endpermission
        @permission('edit_rules_configurator')
            <a href="{{ route('rules-configurator.edit', $rule->Id_ReglaCostoEspecial) }}" class="dropdown-item">
                Editar
            </a>
        @endpermission
        @permission('delete_rules_configurator')
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" onclick="_global.toggleStatus('{{ route('rules-configurator.destroy', $rule->Id_ReglaCostoEspecial) }}')" href="#">
                Cambiar estatus
            </a>
        @endpermission
    </div>
</div>
@endpermission
