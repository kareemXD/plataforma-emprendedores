<div id="component-list">
    <div class="card animated bounceInDown">
        <div class="card-header">
            <h5 class="mb-0">
                <a class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                    Configurar envíos
                </a>
            </h5>
        </div>

        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
            <div class="card-body">
                <table class="table table-sm table-hover table-responsive-lg" id="table_components">
                    <thead>
                    <tr>
                        <th>Nombre Caja</th>
                        <th>Medidas</th>
                        <th>Estatus</th>
                        <th>Acciones</th>
                    </tr>

                    </thead>
                    <tbody>
                        <tr v-for="(item , index) in rowData" >
                            <th scope="row">@{{ item.box_name }}</th>
                            <th scope="row"> Ancho(CM): @{{ item.box_width }} ,
                                 Alto(CM): @{{ item.box_height }} ,
                                  Largo(CM): @{{ item.box_length }},
                                   Peso máx(KG): @{{ item.box_maximum_weight }} .</th>
                            <td v-if="item.status == 1">Activo</td>
                            <td v-if="item.status == 0">Inactivo</td>
                            <td>
                                <div class="btn-group dropleft">
                                    <button class="btn btn-default btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Opciones
                                    </button>
                                    <div class="dropdown-menu">
                                            <a  class="dropdown-item" @click="editComponent(index)">
                                                Editar
                                            </a>
                                            <div class="dropdown-divider"></div>
                                            <a class="dropdown-item"  @click="changeStatus(index)">
                                                Cambiar estatus
                                            </a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                  </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<modal v-if="showModal" @close="showModal = false">
  <!--
    you can use custom content here to overwrite
    default content
  -->
  <h3 slot="header"></h3>
</modal>
