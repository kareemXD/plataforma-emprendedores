<script type="text/x-template" id="modal-template">
  <transition name="modal">
    <div class="modal-mask">
      <div class="modal-wrapper">
        <div class="modal-container">

          <div class="modal-header">
            <slot name="header">

            </slot>
          </div>

          <div class="modal-body" class="col-12">
            <slot name="body" >
                <div class="col-12">
                    <div class="row">
                        <div class="col-12">
                            {{ Form::label('lb_box_name', 'Nombre de la caja :', ['class' => '']) }}
                            {{ Form::text('box_name', null, ['class' => 'form-control form-control-sm', 'maxlength' => '50', 'required' , 'v-model' => 'box.box_name']) }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            {{ Form::label('lb_box_length', 'Largo(CM) :', ['class' => '']) }}
                            {{ Form::number('box_length', null, ['class' => 'form-control form-control-sm', 'maxlength' => '50', 'required' , 'v-model' => 'box.box_length']) }}
                        </div>
                        <div class="col-6">
                            {{ Form::label('lb_box_height ', 'Alto(CM) :', ['class' => '']) }}
                            {{ Form::number('box_height', null, ['class' => 'form-control form-control-sm', 'maxlength' => '50', 'required' ,'v-model' => 'box.box_height']) }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            {{ Form::label('lb_box_width', 'Ancho(CM) :', ['class' => '']) }}
                            {{ Form::number('box_width', null, ['class' => 'form-control form-control-sm', 'maxlength' => '50', 'required' , 'v-model' => 'box.box_width']) }}
                        </div>
                        <div class="col-6">
                            {{ Form::label('lb_maximum_weight', 'Peso máximo (KG) :', ['class' => '']) }}
                            {{ Form::number('box_maximum_weight', null, ['class' => 'form-control form-control-sm', 'maxlength' => '50', 'required' , 'v-model' => 'box.box_maximum_weight']) }}
                        </div>
                    </div>
                </div>
                <div class="col-12" style="margin-top:50px;">
                <div style="text-align: center">
                    <a v-if="updateOrSave == 1" @click=" saveForm($event)" class="btn btn-success btn-flat">
                        <i class="fas fa-save"></i> Guardar
                    </a>
                    <a v-if="updateOrSave == 2" @click=" editForm($event ,indexEdit)" class="btn btn-success btn-flat">
                        <i class="fas fa-save"></i> Editar
                    </a>
                </div>
                </div>


            </slot>
          </div>

          <div class="modal-footer">
            <slot name="footer">
                <button class="modal-default-button" @click="$emit('close')">
                salir
              </button>
            </slot>
          </div>
        </div>
      </div>
    </div>
  </transition>
</script>
