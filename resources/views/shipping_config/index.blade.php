@extends('adminlte::page')

@push('css')
    <style>

        .modal-mask {
        position: fixed;
        z-index: 9998;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        background-color: rgba(0, 0, 0, .5);
        display: table;
        transition: opacity .3s ease;
        }

        .modal-wrapper {
        display: table-cell;
        vertical-align: middle;
        }

        .modal-container {
        width: 800px;
        margin: 0px auto;
        padding: 20px 30px;
        background-color: #fff;
        border-radius: 2px;
        box-shadow: 0 2px 8px rgba(0, 0, 0, .33);
        transition: all .3s ease;
        font-family: Helvetica, Arial, sans-serif;
        }

        .modal-header h3 {
        margin-top: 0;
        color: #42b983;
        }

        .modal-body {
        margin: 20px 0;
        }

        .modal-default-button {
        float: right;
        }

        /*
        * The following styles are auto-applied to elements with
        * transition="modal" when their visibility is toggled
        * by Vue.js.
        *
        * You can easily play with the modal transition by editing
        * these styles.
        */

        .modal-enter {
        opacity: 0;
        }

        .modal-leave-active {
        opacity: 0;
        }

        .modal-enter .modal-container,
        .modal-leave-active .modal-container {
        -webkit-transform: scale(1.1);
        transform: scale(1.1);
        }
    </style>
@endpush

@section('breadcrumb')

    <h3 class="m-subheader__title m-subheader__title--separator">Configurar envío</h3>
    <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
        <li class="m-nav__item m-nav__item--home">
            <a href="{!!URL::to('/')!!}" class="m-nav__link m-nav__link--icon">
                <i class="m-nav__link-icon la la-home"></i> Inicio
            </a>
        </li>
        <li class="m-nav__separator">-</li>
        <li class="m-nav__item">
            <a href="{!!URL::to('/my-account')!!}" class="m-nav__link">
                <span class="m-nav__link-text">Configurar envío</span>
            </a>
        </li>
    </ul>
@endsection

@section('content')
<div id="app">
    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                        <span class="m-portlet__head-icon m--hide">
                            <i class="la la-gear"></i>
                        </span>
                    <h3 class="m-portlet__head-text">
                        Configurar envío
                    </h3>
                </div>
            </div>

        </div>


            <div class="row" style="margin-left: 150px;">

                <div class="col-md-3">
                    <input type="checkbox" id="local_shipping" value="1" v-model="checkedTypeShipping">
                    <label for="local_shipping">Envío local</label>
                </div>
                <div class="col-md-3">
                    <input type="checkbox" id="national_shipping" value="2" v-model="checkedTypeShipping">
                    <label for="national_shipping">Envío nacional</label>
                </div>
                <div class="col-md-3">
                    <input type="checkbox" id="seller_point_shipping" value="3" v-model="checkedTypeShipping">
                    <label for="seller_point_shipping">Recoger en sucursal</label>
                </div>
                <div class="col-md-3">
                    <a  class="btn btn-success m-btn m-btn--custom m-btn--icon m-btn--air" id="show-modal" @click="addOrUpdate()" style="border-color: #f9f9f9;">
                        <span><i class="la la-plus" style="color: white;"></i>
                            <span style="color: white;"> Agregar caja</span>
                        </span>
                    </a>
                    <modal v-if="showModal" @close="showModal = false">
                        <h3 slot="header">Agregar caja</h3>
                    </modal>

                </div>
            </div>
            <div class="col-12"  style="margin-top: 50px;">
                @include('shipping_config.partials.table_component')
            </div>
    </div>
</div>


@endsection
@section('js')
    <script src="https://cdn.jsdelivr.net/npm/vue@2.5.16/dist/vue.js"></script>
    @include('shipping_config.partials.modal_vuejs_add_shipping')

    <script>
        Vue.component('modal', {
          template: '#modal-template',
            data: function () {
               return {
                   box: {
                       box_name: app.box.box_name,
                       box_length: app.box.box_length,
                       box_height: app.box.box_height,
                       box_width: app.box.box_width,
                       box_maximum_weight: app.box.box_maximum_weight
                   },
                   updateOrSave: app.updateOrSave,
                   indexEdit : app.indexEdit

               }
           },
           methods: {
               /**
                * save
                * @author luis peña <luis.pena@nuvem.mx>
                * @created 2020-11-04
                * @return void
                */
               saveForm(event){
                    app.box.box_name = this.box.box_name;
                    app.box.box_length = this.box.box_length;
                    app.box.box_height = this.box.box_height;
                    app.box.box_width = this.box.box_width;
                    app.box.box_maximum_weight = this.box.box_maximum_weight;
                    this.$emit('close');
                    app.addItem();
                },
                /**
                * edit
                * @author luis peña <luis.pena@nuvem.mx>
                * @created 2020-11-04
                * @param InputEvent event, integer index
                * @return void
                */
                editForm(event, index){
                    app.rowData[index].box_name = this.box.box_name;
                    app.rowData[index].box_length = this.box.box_length;
                    app.rowData[index].box_height = this.box.box_height;
                    app.rowData[index].box_width = this.box.box_width;
                    app.rowData[index].box_maximum_weight = this.box.box_maximum_weight;
                    var token = "{{ csrf_token() }}";
                    $.ajax({
                        url: "shippings-config/" + app.rowData[index].id,
                        data : {
                            shipping_config : app.rowData[index]
                        },
                        headers: {'X-CSRF-TOKEN': token},
                        type: "PUT",
                        success: function(response) {

                            swal({
                            title: "",
                            type: "success",
                            text: "Caja editada.",
                            confirmButtonText: "Continuar"
                            });
                        }
                    });

                    this.$emit('close');
                },
           }
      });

        var app = new Vue({
           el: '#app',
           data: {
                checkedTypeShipping: ["1","2","3"],
                errors: [],
                showModal: false,
                box: {
                    box_name: null,
                    box_length: null,
                    box_height: null,
                    box_width: null,
                    box_maximum_weight: null
                },
                updateOrSave: 1,
                indexEdit : 0,
                rowData:[]
            },
            methods:{
                /**
                    * add values to array
                    * @author luis peña <luis.pena@nuvem.mx>
                    * @created 2020-11-04
                    * @return void
                */
                addItem(){
                    var my_object = null;
                    my_object = {
                        id : 0,
                        status: 1,
                        box_name : this.box.box_name,
                        box_length : this.box.box_length,
                        box_height : this.box.box_height,
                        box_width : this.box.box_width,
                        box_maximum_weight : this.box.box_maximum_weight,
                        shipping_type : this.checkedTypeShipping
                    };
                    addConfig(my_object);

               },
               /**
              * clear inputs and show slected component
              * @author luis peña <luis.pena@nuvem.mx>
              * @created 2020-11-04
              * @return void
              */
              addOrUpdate(){
                   this.showModal = true;
                   this.clearAll();
              },
               /**
               * change status inputs
               * @author luis peña <luis.pena@nuvem.mx>
               * @created 2020-10-20
               * @return void
               */
               changeStatus(index){
                   var token = "{{ csrf_token() }}";
                   if (this.rowData[index].status == 1) {
                       this.rowData[index].status = 0;
                   }else{
                       this.rowData[index].status = 1;
                   }
                   $.ajax({
                       url: "shippings-config/" + app.rowData[index].id,
                       data : {
                           shipping_config : app.rowData[index]
                       },
                       headers: {'X-CSRF-TOKEN': token},
                       type: "DELETE",
                       success: function(response) {

                           swal({
                           title: "",
                           type: "success",
                           text: "Cambio de estatus.",
                           confirmButtonText: "Continuar"
                           });

                       }
                   });


               },

               /**
                   * add values to array befor edit
                   * @author luis peña <luis.pena@nuvem.mx>
                   * @created 2020-11-04
                   * @return void
               */
               editComponent(index){
                    this.updateOrSave = 2;
                    this.indexEdit = index;
                    this.box.box_name = this.rowData[index].box_name;
                    this.box.box_length = this.rowData[index].box_length;
                    this.box.box_height = this.rowData[index].box_height;
                    this.box.box_width = this.rowData[index].box_width;
                    this.box.box_maximum_weight = this.rowData[index].box_maximum_weight;

                    this.showModal = true;

               },
               /**
                   * celar all inputs
                   * @author luis peña <luis.pena@nuvem.mx>
                   * @created 2020-11-04
                   * @return void
               */
               clearAll(){
                   this.box.box_name = null;
                   this.box.box_length = null;
                   this.box.box_height = null;
                   this.box.box_width = null;
                   this.box.box_maximum_weight = null;
               },
            },beforeMount(){
                   loadConfig();
                }
            })
        /**
            * sen data to save
            * @author luis peña <luis.pena@nuvem.mx>
            * @created 2020-11-04
            * @param  array(ShippingConfig) shipping_config
            * @return void
        */
        function addConfig(shipping_config){
            var token = "{{ csrf_token() }}";

            $.ajax({
                url: "shippings-config",
                data : {
                    shipping_config : shipping_config
                },
                headers: {'X-CSRF-TOKEN': token},
                type: "POST",
                success: function(response) {

                    swal({
                    title: "",
                    type: "success",
                    text: "Caja registrada.",
                    confirmButtonText: "Continuar"
                    });
                    app.rowData.push(response)
                }
            });
        }

        /**
            * load data
            * @author luis peña <luis.pena@nuvem.mx>
            * @created 2020-11-04
            * @return void
        */
        function loadConfig(){
            var token = "{{ csrf_token() }}";

            $.ajax({
                url: "/shippings-config-user",
                headers: {'X-CSRF-TOKEN': token},
                type: "get",
                success: function(response) {
                    response.forEach( function(currentValue, index, array) {
                        app.rowData.push(currentValue)
                    });
                }
            });
        }

    </script>
@endsection
