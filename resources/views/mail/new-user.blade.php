<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0">
    <title>Nuevo Usuario</title>
</head>
<body>
    <p>Se a creado un nuevo usuario</p>
    <ul>
        <li>Usuario: {{ $user->email }}</li>
        <li>Contraseña: {{ $user->password }}</li>
    </ul>
</body>
</html>