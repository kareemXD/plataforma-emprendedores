@extends('adminlte::page')

@section('title', 'Usuarios')

@section('content_header')
    @component('components.breadcrumb', [
        'current' => 'Listado'
    ])
    @endcomponent
@stop

@section('content')
    <div id="accordion">
        <div class="card animated bounceInDown">
            <div class="card-header">
                <h5 class="mb-0">
                    <a class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                        Listado
                    </a>
                    <a href="{{ route('users.create') }}" class="btn btn-outline-success float-right" data-toggle="tooltip" data-placement="left" title="Agregar Nuevo"
                       style="border-color:transparent;color: white;">
                        <i class="fas fa-plus-circle fa-lg"></i>
                    </a>
                </h5>
            </div>

            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                <div class="card-body">
                    <table class="table table-sm table-hover table-responsive-lg" id="myTable">
                        <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>Apellido Paterno</th>
                            <th>Apellido Materno</th>
                            <th>Correo electrónico</th>
                            <th>Estatus</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>

    @includeIf('users.partials.show')
@stop

@push('js')
    <script>
        $(function () {
            _index.datatable();
        });

        const _index = {
            $mdl: $('#show-modal'),
            datatable: function () {
                $('#myTable').DataTable({
                    processing: true,
                    serverSide: true,
                    language:{
                        url: "{{ asset('/js/Spanish.json') }}"
                    },
                    ajax: '/users',
                    columns: [
                        { data: 'name', name: 'name' },
                        { data: 'last_name', name: 'last_name' },
                        { data: 'second_last_name', name: 'second_last_name' },
                        { data: 'email', name: 'email' },
                        { data: 'status', name: 'status' },
                        { data: 'actions', name: 'actions', orderable: false, searchable: false, width: '8em' },
                    ]
                });
            },
            fillMdl: function (that) {
                this.$mdl.find('label').html('');
                let data = JSON.parse(that.dataset['user']);
                $.each(data, (idx, val) => {
                    this.$mdl.find(`#${idx}`).html(val);
                });
            }
        }
    </script>
@endpush
