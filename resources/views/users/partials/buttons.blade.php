@permission('*users')
<div class="btn-group dropleft">
    <button class="btn btn-default btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        Opciones
    </button>
    <div class="dropdown-menu">
        @permission('list_users')
            <a class="dropdown-item" data-user="{{ json_encode($user) }}" onclick="_index.fillMdl(this)" href="#"
               data-toggle="modal" data-target="#show-modal">
                Detalles
            </a>
        @endpermission
        @permission('edit_users')
            <a href="{{ route('users.edit', $user->id) }}" class="dropdown-item">
                Editar
            </a>
        @endpermission
        @permission('delete_users')
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" onclick="_global.toggleStatus('{{ route('users.destroy', $user->id) }}')" href="#">
                Cambiar estatus
            </a>
        @endpermission
    </div>
</div>
@endpermission
