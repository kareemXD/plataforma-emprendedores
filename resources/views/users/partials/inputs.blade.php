<div class="form-row">
    <div class="col-6">
        {{ Form::label('name', 'Nombre:', ['class' => '']) }}
        {{ Form::text('name', null, ['class' => 'form-control form-control-sm', 'required', 'maxlength' => '80']) }}
    </div>
    <div class="col-6">
        {{ Form::label('email', 'Correo electrónico:', ['class' => '']) }}
        {{ Form::email('email', null, ['class' => 'form-control form-control-sm', 'required', 'maxlength' => '100']) }}
    </div>
    <div class="col-6">
        {{ Form::label('last_name', 'Apellido Paterno:', ['class' => '']) }}
        {{ Form::text('last_name', null, ['class' => 'form-control form-control-sm', 'maxlength' => '50', 'required']) }}
    </div>
    <div class="col-6">
        {{ Form::label('password', 'Contraseña:', ['class' => '']) }}
        {{ Form::password('password', ['class' => 'form-control form-control-sm', $passConfirmed ? 'required' : '', 'maxlength' => '20', 'minlength' => '8']) }}
    </div>
    <div class="col-6">
        {{ Form::label('second_last_name', 'Apellido Materno:', ['class' => '']) }}
        {{ Form::text('second_last_name', null, ['class' => 'form-control form-control-sm', 'maxlength' => '50']) }}
    </div>
    <div class="col-6">
        {{ Form::label('password_confirmation', 'Confirmación de contraseña:', ['class' => '']) }}
        {{ Form::password('password_confirmation', ['class' => 'form-control form-control-sm', $passConfirmed ? 'required' : '', 'maxlength' => '20', 'minlength' => '8']) }}
    </div>
</div>
