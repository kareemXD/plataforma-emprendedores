@extends('adminlte::page')

@section('meta_tags')
    <meta name="conceptos_costos" content="{{$conceptos_costos}}">
    <meta name="bases_calculo" content="{{$bases_calculo}}">
@endsection

@section('title', 'Banners')

@section('content_header')
    @component('components.breadcrumb', [
        'links' => [
            'Listado' => url('special_prices')
        ],
        'current' => 'Crear'
    ])
    @endcomponent
@stop

@section('content')
    <div id="request_errors" class="alert alert-danger d-none">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <ul>
            <li></li>
        </ul>
    </div>
    <div id="accordion">
        <div class="card animated bounceInDown">
            <div class="card-header">
                <h5 class="mb-0">
                    <a class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true"
                       aria-controls="collapseOne">
                        Información
                    </a>
                </h5>
            </div>

            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                <div class="card-body">
                    <form method="POST" action="{{ route('special_prices.store') }}" id="special_pricesForm">
                        @csrf
                        <div id="special_prices_step_one">
                            <div class="form-row">
                                <div class="form-group col-12">
                                    <label for="rule_description"><span>* </span>Nombre</label>
                                    <input type="text" id="rule_description" class="form-control">
                                </div>
                                <div class="form-group col-12">
                                    <label for="rule_type_id"><span>* </span>Tipo de regla</label>
                                    <select id="rule_type_id" class="form-control" required>
                                        @foreach ($prices as $price)
                                            <option value="{{ $price->Id_ReglaCostoEspecial }}">{{ $price->Descripcion }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-6 offset-6 d-none" id="percentaje-group">
                                    <div class="form-group col-12">
                                        <label for="percentaje">Porcentaje</label>
                                        <input type="number" name="percentaje" id="percentaje"
                                               value="{{ old('percentaje') }}" min="0" max="100" class="form-control" step="0.01">
                                    </div>
                                </div>
                                <div class="form-group col-6 offset-6 d-none" id="amount-group">
                                    <div class="form-group col-12">
                                        <label for="amount">Monto</label>
                                        <input type="number" name="amount" id="amount" value="{{ old('amount') }}"
                                               min="0" class="form-control" step="0.01">
                                    </div>
                                </div>
                            </div>
                            <div class="form-row mt-3">
                                <div class="form-group col-3">
                                    <label for="family">Familia</label>
                                    <select class="form-control" name="family" id="family" required
                                            onchange="_index.runQuery()">
                                        <option value="0" selected>Todos</option>
                                        @foreach ($families as $family)
                                            <option value="{{ $family->Id_Familia }}">{{ $family->Familia }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-3">
                                    <label for="brand">Marca</label>
                                    <select class="form-control" name="brand" id="brand" required
                                            onchange="_index.runQuery()">
                                        <option value="0" selected>Todos</option>
                                        @foreach ($brands as $brand)
                                            <option value="{{ $brand->Id_Marca }}">{{ $brand->Marca }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-3">
                                    <label for="category">Categoría</label>
                                    <select class="form-control" name="category" id="category" required
                                            onchange="_index.runQuery()">
                                        <option value="0" selected>Todos</option>
                                        @foreach ($categories as $category)
                                            <option
                                                value="{{ $category->Id_Categoria }}">{{ $category->Categoria }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-3">
                                    <label for="especiality">Especialidad</label>
                                    <select class="form-control" name="especiality" id="especiality" required
                                            onchange="_index.runQuery()">
                                        <option value="0" selected>Todos</option>
                                        @foreach ($especialities as $especiality)
                                            <option
                                                value="{{ $especiality->Id_EspMed }}">{{ $especiality->Especialidad }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-12">
                                    <label for="array_products">Productos añadidos:</label>
                                    <input type="text" name="array_products" id="array_products" value=""
                                           class="form-control">
                                </div>
                            </div>
                            <div class="card-body table-responsive">
                                <table class="table" id="myTable">
                                    <thead>
                                    <tr>
                                        <th>Nombre</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                        <div style="display: none" id="special_prices_step_two">
                            <div class="form-row">
                                <div class="col-12">
                                    <label for="description_input_0"><span>* </span>Descripción</label>
                                    <input type="text" name="description_input_0" id="description_input_0" class="form-control" onchange="addData(event)">
                                </div>
                                <div class="form-group col-12 mt-2">
                                    <div class="form-row">
                                        <label class="form-check-label ml-1">* Tipo:</label>
                                    </div>
                                    <div class="form-group col-6">
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="type" value="P"
                                                   onclick="typeOfPrice(event)" id="type_percentage_0" onchange="addData(event)"
                                                   checked>
                                            <label class="form-check-label" for="inlineRadio1">Porcentaje</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="type" value="M"
                                                   onclick="typeOfPrice(event)" id="type_amount_0" onchange="addData(event)">
                                            <label class="form-check-label" for="inlineRadio2">Monto</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="type" value="C"
                                                   onclick="typeOfPrice(event)" id="type_calculated_0" onchange="addData(event)">
                                            <label class="form-check-label" for="inlineRadio2">Calculado</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="mt-1 col-12">
                                    <div class="row">
                                        <div class="col-3">
                                            <label for="description">Cantidad</label>
                                            <input type="number" name="amount_input_0" id="amount_input_0" class="form-control" onchange="addData(event)"
                                            step="0.01">
                                        </div>
                                        <div id="base_calculo_0" class="col-3 d-none">
                                            {{ Form::label('Id_BaseCostoCalculado', 'Base de Cálculo:') }}
                                            {{ Form::select('Id_BaseCostoCalculado', $bases_calculo_pluck, null, ['placeholder' => 'Seleccione una opción',
                                            'class' => 'form-control', 'required', 'id' => 'select_base_cost_0', 'name' => 'select_base_cost_0',
                                            'onchange' => 'addData(event)']) }}
                                        </div>
                                    </div>
                                </div>
                                <div class="mt-3 col-12">
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="form-check">
                                                <input
                                                    class="form-check-input"
                                                    type="checkbox"
                                                    name="global_checkbox_0"
                                                    id="global_checkbox_0"
                                                    onchange="showConditionalFields(event,'cost_container_','block'); addData(event)"
                                                >
                                                <label class="form-check-label" for="global_checkbox_0">
                                                    ¿Aplica de manera global?
                                                </label>
                                            </div>
                                            <div style="display: none" id="cost_container_0">
                                                <label for="cost_0">Tiene costo:</label>
                                                <select class="form-control" name="cost_select_0"
                                                        id="cost_select_0" required onchange=" addData(event)">
                                                    @foreach($conceptos_costos as $costo)
                                                    <option value="{{$costo->id_ConceptoCosto}}">{{$costo->Descripcion}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" name="expiring"
                                                       id="expiration_checkbox_0" onclick="showConditionalFields(event,'expiring_container_','flex'); addData(event)">
                                                <label class="form-check-label" for="expiration_checkbox_0">Tiene
                                                    vigencia</label>
                                            </div>
                                            <div class="form-inline" style="display: none" id="expiring_container_0">
                                                <div class="mr-2">
                                                    <label for="startDate_input_0">Fecha inicio</label>
                                                    <input type="date" id="startDate_input_0" name="start" class="form-control" onchange="addData(event)">
                                                </div>
                                                <div>
                                                    <label for="endDate_input_0">Fecha fin</label>
                                                    <input type="date" id="endDate_input_0" name="end" class="form-control" onchange="addData(event)">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div style="display: none" id="special_prices_step_three">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th scope="col">Descripción</th>
                                    <th scope="col">Tipo</th>
                                    <th scope="col">Cantidad</th>
                                    <th scope="col">Aplica de manera global</th>
                                    <th scope="col">Tipo de costo</th>
                                    <th scope="col">Vigencia</th>
                                    <th scope="col">Acciones</th>
                                </tr>
                                </thead>
                                <tbody id="data_promotion">


                                </tbody>
                            </table>
                        </div>

                        <div style="display: none" id="special_prices_step_four">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th scope="col">ID Producto</th>
                                    <th scope="col">Descripción</th>
                                    {{--<th scope="col">Ready to Sell</th>
                                    <th scope="col">Nuevo Ready to Sell</th>
                                    <th scope="col">Utilidad</th>
                                    <th scope="col">Nueva Utilidad</th>--}}
                                </tr>
                                </thead>
                                <tbody id="data_products">

                                </tbody>
                            </table>
                        </div>

                        <div class="btn-group mt-5" role="group"> {{--TODO: CENTRAR ESTO--}}
                            <button style="display: none" id="back_button" type="button" class="btn btn-default m-2"
                                    onclick="handleOnPressBack()">
                                <i class="fas fa-arrow-left"></i> Atrás
                            </button>
                            <button style="display: none" id="new_special_cost_button" type="button"
                                    class="btn btn-default m-2" onclick="handleNewSpecialCost()">
                                Agregar nuevo costo especial
                            </button>
                            <button id="next_button" type="button" class="btn btn-default m-2"
                                    onclick="handleOnPressNext()">
                                <i class="fas fa-arrow-right"></i> Siguiente
                            </button>
                            <button style="display: none" id="save_data" type="button" onclick="saveData()" class="btn btn-success m-2">
                                <i class="fas fa-save"></i> Guardar
                            </button>
                            <a type="button" class="btn btn-danger m-2" href="{{ url()->previous() }}">
                                <i class="fas fa-ban"></i> Cancelar
                            </a>
                        </div>
                        <div style="display: none" class="btn-group mt-5" role="group">

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop

@push('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tagify/3.21.5/tagify.min.css"/>
    <style>
        .tagify {
            overflow-x: auto;
            overflow-y: hidden;
            flex-wrap: unset;
            height: 5rem;
        }

        .tagify__input {
            display: none !important;
        }
    </style>
@endpush

@push('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tagify/3.21.5/tagify.min.js"></script>
    <script>

        var activeStep = 'special_prices_step_one';

        var specialCostCounter = 1

        var data2Save = [{
            object_index: 0,
            description: '',
            type: 'P',
            amount: '',
            global: false,
            cost: '',
            expiration: false,
            startDate: '',
            endDate: ''
        }]

        const products = document.getElementById('array_products');
        const productsTagify = new Tagify(products, {
            delimiters: '\n',
            editTags: false,
        });

        $(function () {
            _index.datatable();
        });

        /**
         * Create the properly radio buttons for the form
         * @author daniel.cruz@losfra.dev
         * @created 2020-02-26
         * @params  string, string, string
         * @return html element
         */
        function createRadioContainer(identifier, value, label) {
            var inputRadioContainer = document.createElement('div')
            inputRadioContainer.classList.add('form-check', 'form-check-inline')

            var inputRadio = document.createElement('input')
            inputRadio.type = 'radio'
            inputRadio.value = value
            inputRadio.className = 'form-check-input'
            inputRadio.id = identifier + specialCostCounter
            inputRadio.name = 'radio_group_' + specialCostCounter
            inputRadio.setAttribute('onclick', 'typeOfPrice(event)');

            var radioLabel = document.createElement('label')
            radioLabel.innerHTML = label

            inputRadioContainer.appendChild(inputRadio)
            inputRadioContainer.appendChild(radioLabel)

            return inputRadioContainer
        }

        /**
         * Shows and hides the steps to create specialCost (goes forward)
         * @author daniel.cruz@losfra.dev
         * @created 2020-02-26
         * @param
         * @return
         */
        function handleOnPressNext() {
            if (activeStep == 'special_prices_step_one') {
                document.getElementById('special_prices_step_one').style.display = 'none'
                document.getElementById('special_prices_step_two').style.display = 'block'
                document.getElementById('back_button').style.display = 'block'
                document.getElementById('new_special_cost_button').style.display = 'block'
                activeStep = 'special_prices_step_two'
            } else if (activeStep == 'special_prices_step_two') {
                document.getElementById('new_special_cost_button').style.display = 'none'
                document.getElementById('special_prices_step_two').style.display = 'none'
                document.getElementById('special_prices_step_three').style.display = 'block'
                activeStep = 'special_prices_step_three'
                showData();
            } else if (activeStep == 'special_prices_step_three') {
                document.getElementById('special_prices_step_three').style.display = 'none'
                document.getElementById('special_prices_step_four').style.display = 'block'
                activeStep = 'special_prices_step_four'
                document.getElementById('save_data').style.display = 'block'
                showProducts();
            }
        }

        /**
         * Shows and hides the steps to create specialCost (goes back)
         * @author daniel.cruz@losfra.dev
         * @created 2020-02-26
         * @param
         * @return
         */
        function handleOnPressBack() {
            if (activeStep == 'special_prices_step_two') {
                document.getElementById('special_prices_step_one').style.display = 'block'
                document.getElementById('special_prices_step_two').style.display = 'none'
                document.getElementById('new_special_cost_button').style.display = 'none'
                document.getElementById('back_button').style.display = 'none'
                activeStep = 'special_prices_step_one'
            } else if (activeStep == 'special_prices_step_three') {
                document.getElementById('new_special_cost_button').style.display = 'block'
                document.getElementById('special_prices_step_two').style.display = 'block'
                document.getElementById('special_prices_step_three').style.display = 'none'
                activeStep = 'special_prices_step_two'
                $('#data_promotion').empty()
            } else if (activeStep == 'special_prices_step_four') {
                document.getElementById('special_prices_step_three').style.display = 'block'
                document.getElementById('special_prices_step_four').style.display = 'none'
                activeStep = 'special_prices_step_three'
                $('#data_products').empty()
            }
        }

        /**
         * Generates a new tab to add a new special price
         * @author daniel.cruz@losfra.dev
         * @created 2020-02-26
         * @param
         * @return html element
         */
        function handleNewSpecialCost() {

            data2Save.push({
                object_index: data2Save.length,
                description: '',
                type: 'P',
                amount: '',
                global: false,
                cost: '',
                expiration: false,
                startDate: '',
                endDate: ''
            })

            var mainContainer = document.createElement('div')
            mainContainer.id = `special_prices_step_two_entry_${(data2Save.length - 1)}`;
            mainContainer.classList.add('form-row', 'mt-4', 'pt-2', 'border-top')
            specialCostCounter.id = 'special_row_' + specialCostCounter

            var descriptionContainer = document.createElement('div')
            descriptionContainer.className = 'col-12'

            var descriptionLabel = document.createElement('label')
            descriptionLabel.innerHTML = 'Descripción'

            var descriptionInput = document.createElement('input')
            descriptionInput.type = 'text'
            descriptionInput.className = 'form-control'
            descriptionInput.id = 'description_input_' + specialCostCounter
            descriptionInput.name = 'description_input_' + specialCostCounter
            descriptionInput.addEventListener('change', function (event) {
                addData(event)
            })
            descriptionContainer.appendChild(descriptionLabel)
            descriptionContainer.appendChild(descriptionInput)

            var formContainer = document.createElement('div')
            formContainer.classList.add('form-group', 'col-12', 'mt-2')

            var typeContainer = document.createElement('div')
            typeContainer.className = 'form-row'

            var typeLabel = document.createElement('label')
            typeLabel.innerHTML = '*Tipo'
            typeContainer.appendChild(typeLabel)
            formContainer.appendChild(typeContainer)

            var radioContainer = document.createElement('div')
            radioContainer.classList.add('form-group', 'col-6')

            var percentageRadio = createRadioContainer('type_percentage_', 'P', 'Porcentaje')
            percentageRadio.querySelector('input[type=radio]').checked = true;
            var amountRadio = createRadioContainer('type_amount_', 'M', 'Monto')
            var calculatedRadio = createRadioContainer('type_calculated_', 'C', 'Calculado')

            percentageRadio.setAttribute('onclick', 'typeOfPrice(event)');
            amountRadio.setAttribute('onclick', 'typeOfPrice(event)');
            calculatedRadio.setAttribute('onclick', 'typeOfPrice(event)');

            radioContainer.appendChild(percentageRadio)
            radioContainer.appendChild(amountRadio)
            radioContainer.appendChild(calculatedRadio)
            formContainer.appendChild(radioContainer)

            var amountForm = document.createElement('div')
            amountForm.classList.add('mt-1', 'col-12')

            var amountRow = document.createElement('div')
            amountRow.classList.add('row')

            var amountField = document.createElement('div')
            amountField.classList.add('col-3')

            var amountLabel = document.createElement('label')
            amountLabel.innerHTML = '*Cantidad'

            var amountInput = document.createElement("input")
            amountInput.type = 'number'
            amountInput.className = 'form-control'
            amountInput.id = 'amount_input_' + specialCostCounter
            amountInput.name = 'amount_input_' + specialCostCounter
            amountInput.addEventListener('change', function (event) {
                addData(event)
            })

            //base cost select
            let $baseCostCol = document.createElement('div');
            $baseCostCol.id = `base_calculo_${data2Save.length - 1}`;
            $baseCostCol.classList.add('col-3', 'd-none');
            let $baseCostLabel = document.createElement('label');
            $baseCostLabel.innerText = 'Base de Cálculo';

            let $baseCostSelect = document.createElement('select');
            $baseCostSelect.id = `select_base_cost_${data2Save.length - 1}`;
            $baseCostSelect.setAttribute('onchange', 'addData(event)');
            $baseCostSelect.classList.add('form-control');
            let $baseCostSelectDefaultOpt = document.createElement('option');
            $baseCostSelectDefaultOpt.value = '';
            $baseCostSelectDefaultOpt.innerText = 'Selecciona una opción';
            $baseCostSelect.appendChild($baseCostSelectDefaultOpt);
            const BASE_COSTS = JSON.parse(document.querySelector('meta[name=bases_calculo]').content);
            BASE_COSTS.forEach(element => {
                let $opt = document.createElement('option');
                $opt.value = element.Id_BaseCostoCalculado;
                $opt.innerText = element.Descripcion;
                $baseCostSelect.appendChild($opt);
            });

            $baseCostCol.appendChild($baseCostLabel);
            $baseCostCol.appendChild($baseCostSelect);

            amountField.appendChild(amountLabel)
            amountField.appendChild(amountInput)

            amountRow.appendChild(amountField)
            amountRow.appendChild($baseCostCol);
            amountForm.appendChild(amountRow)


            var checkForm = document.createElement('div')
            checkForm.classList.add('col-12', 'mt-3')

            var checkGlobalRow = document.createElement('div')
            checkGlobalRow.className = 'row'

            var globalRow = document.createElement('div')
            globalRow.className = 'col-6'

            var globalFormCheck = document.createElement('div')
            globalFormCheck.className = 'form-check'

            var globalInput = document.createElement('input')
            globalInput.type = 'checkbox'
            globalInput.className = 'form-check-input'
            globalInput.id = 'global_checkbox_' + specialCostCounter
            globalInput.name = 'global_checkbox_' + specialCostCounter
            globalInput.addEventListener('click', function (event) {
                showConditionalFields(event,'cost_container_','block'),
                addData(event)
            })

            var globalLabel = document.createElement('label')
            globalLabel.className = 'form-check-label'
            globalLabel.innerHTML = '¿Aplica de manera global?'

            globalFormCheck.appendChild(globalInput)
            globalFormCheck.appendChild(globalLabel)

            var globalFormCost = document.createElement('div')
            globalFormCost.id = 'cost_container_' + specialCostCounter
            globalFormCost.style.display = 'none'

            var globalCostLabel = document.createElement('label')
            globalCostLabel.innerHTML = 'Tiene costo:'

            var costSelect = document.createElement('select')
            costSelect.className = 'form-control'
            costSelect.name = 'cost_' + specialCostCounter
            costSelect.id = 'cost_select_' + specialCostCounter

            const CONCEPTOS_COSTOS = JSON.parse(document.querySelector('meta[name=conceptos_costos]').content);
            CONCEPTOS_COSTOS.forEach(element => {
                var $option = document.createElement('option')
                $option.value = element.id_ConceptoCosto;
                $option.text = element.Descripcion;
                costSelect.add($option)
            });
            costSelect.addEventListener('change', function (event) { addData(event) })

            var expiringRow = document.createElement('div')
            expiringRow.className = 'col-6'

            var expiringFormCheck = document.createElement('div')
            expiringFormCheck.className = 'form-check'

            var expiringInput = document.createElement('input')
            expiringInput.type = 'checkbox'
            expiringInput.className = 'form-check-input'
            expiringInput.id = 'expiration_checkbox_' + specialCostCounter
            expiringInput.name = 'expiration_checkbox_' + specialCostCounter
            expiringInput.addEventListener('click', function (event) {
                showConditionalFields(event,'expiring_container_','flex')
                addData(event)
            })

            var expiringLabel = document.createElement('label')
            expiringLabel.innerHTML = 'Tiene vigencia'

            expiringFormCheck.appendChild(expiringInput)
            expiringFormCheck.appendChild(expiringLabel)

            var expiringFormRange = document.createElement('div')
            expiringFormRange.className = 'form-inline'
            expiringFormRange.style.display = 'none'
            expiringFormRange.id = 'expiring_container_' + specialCostCounter


            var startInputContainer = document.createElement('div')
            startInputContainer.className = 'mr-2'

            var startLabel = document.createElement('label')
            startLabel.innerHTML = 'Fecha inicio'

            var startExpiringInput = document.createElement('input')
            startExpiringInput.type = 'date'
            startExpiringInput.className = 'form-control'
            startExpiringInput.id = 'startDate_input_' + specialCostCounter
            startExpiringInput.name = 'startDate_input_' + specialCostCounter
            startExpiringInput.addEventListener('change', function (event) {
                addData(event)
            })

            startInputContainer.appendChild(startLabel)
            startInputContainer.appendChild(startExpiringInput)

            var endInputContainer = document.createElement('div')

            var endLabel = document.createElement('label')
            endLabel.innerHTML = 'Fecha inicio'

            var endExpiringInput = document.createElement('input')
            endExpiringInput.type = 'date'
            endExpiringInput.className = 'form-control'
            endExpiringInput.id = 'endDate_input_' + specialCostCounter
            endExpiringInput.name = 'endDate_input_' + specialCostCounter
            endExpiringInput.addEventListener('change', function (event) {
                addData(event)
            })


            endInputContainer.appendChild(endLabel)
            endInputContainer.appendChild(endExpiringInput)

            expiringFormRange.appendChild(startInputContainer)
            expiringFormRange.appendChild(endInputContainer)

            expiringRow.appendChild(expiringFormCheck)
            expiringRow.appendChild(expiringFormRange)

            globalFormCost.appendChild(globalCostLabel)
            globalFormCost.appendChild(costSelect)
            globalRow.appendChild(globalFormCheck)
            globalRow.appendChild(globalFormCost)
            checkGlobalRow.appendChild(globalRow)
            checkGlobalRow.appendChild(expiringRow)
            checkForm.appendChild(checkGlobalRow)

            mainContainer.appendChild(descriptionContainer)
            mainContainer.appendChild(formContainer)
            mainContainer.appendChild(amountForm)
            mainContainer.appendChild(checkForm)

            document.getElementById('special_prices_step_two').appendChild(mainContainer)

            specialCostCounter = specialCostCounter + 1
        }

        /**
         * Shows and hides the fields from the form of special prices
         * @author daniel.cruz@losfra.dev
         * @created 2020-02-26
         * @param
         * @return
         */
        function showConditionalFields(event,target,display) {
            var select = document.getElementById(target + event.target.id.split('_')[2])
            select.style.display = event.target.checked ? display : 'none'
        }

        /**
         * Shows and hides the input corresponding to percentage and amount
         * @author daniel.cruz@losfra.dev
         * @created 2020-12-30
         * @param  string  input
         * @return void
         */
        function typeOfPrice(event) {
            let input = event.target.value;
            let id = event.target.id.split('_')[2];
            document.getElementById(`base_calculo_${id}`).classList.add('d-none');

            data2Save[id]['type'] = input;

            var percentajeInput = document.getElementById('percentaje');
            var amountInput = document.getElementById('amount');
            var amountGroup = document.getElementById('amount-group');
            var percentajeGroup = document.getElementById('percentaje-group');

            if (input === 'P') {
                amountInput.value = '';
                percentajeInput.setAttribute('required', '');
                percentajeGroup.classList.replace("d-none", "d-block");
                amountGroup.classList.replace("d-block", "d-none");
                amountInput.removeAttribute('required');
            } else if (input === 'M') {
                percentajeInput.value = '';
                amountInput.setAttribute('required', '');
                amountGroup.classList.replace("d-none", "d-block");
                percentajeGroup.classList.replace("d-block", "d-none");
                percentajeInput.removeAttribute('required');
            }
            else if(input === 'C') {
                document.getElementById(`base_calculo_${id}`).classList.remove('d-none');
            }
        }

        /**
         * Add the properly data to the array of special prices
         * @author daniel.cruz@losfra.dev
         * @created 2020-02-26
         * @param html event
         * @return
         */
        function addData(event) {

            var fieldName = event.target.id.split('_')[0]
            var id = event.target.id.split('_')[2]

            if(event.target.id.startsWith('cost_select_')) {
                let $select = document.getElementById(`cost_select_${id}`);
                data2Save[id]['cost_id'] = $select.value;
                data2Save[id]['cost'] = $select.options[$select.selectedIndex].innerText;
                return;
            }

            if(event.target.id.startsWith('select_base_cost_')) {
                let id = event.target.id.split('_')[3];
                data2Save[id]['base_cost_id'] = event.target.value;
                data2Save[id]['base_cost'] = event.target.options[event.target.selectedIndex].innerText;
                return;
            }

            switch (fieldName) {
                case 'global':
                    var value = !data2Save[id][fieldName]
                    data2Save[id][fieldName] = value
                    let $select = document.getElementById(`cost_select_${id}`);
                    data2Save[id]['cost_id'] = $select.value;
                    data2Save[id]['cost'] = $select.options[$select.selectedIndex].innerText;
                    break;

                case 'expiration':
                    var value = !data2Save[id][fieldName]
                    data2Save[id][fieldName] = value

                    if (data2Save[id]['startDate'] != '' || data2Save[id]['endDate'] != '') {
                        data2Save[id]['startDate'] = ''
                        data2Save[id]['endDate'] = ''
                    }
                    break;

                default:
                    var value = event.target.value
                    data2Save[id][fieldName] = value
                    break;

            }

        }

        /**
         * Display the data of the arra of special prices
         * @author daniel.cruz@losfra.dev
         * @created 2020-02-26
         * @param
         * @return
         */
        function showData() {

            var body = document.getElementById('data_promotion')

            for (let i = 0; i < data2Save.length; i++) {

                var row = document.createElement('tr')

                var cell_description = document.createElement("td")
                var cell_type = document.createElement("td")
                var cell_amount = document.createElement("td")
                var cell_global = document.createElement("td")
                var cell_cost = document.createElement("td")
                var cell_dates = document.createElement("td")
                var cell_actions = document.createElement("td")


                cell_description.innerHTML = data2Save[i]['description']


                if (data2Save[i]['type'] == 'P') {
                    cell_type.innerHTML = 'Porcentaje'
                    cell_amount.innerHTML = data2Save[i]['amount']+'%'
                } else if (data2Save[i]['description'] == 'M') {
                    cell_type.innerHTML = 'Monto'
                    cell_amount.innerHTML = data2Save[i]['amount']
                } else {
                    cell_type.innerHTML = 'Calculado'
                    cell_amount.innerHTML = data2Save[i]['amount']
                }

                if (data2Save[i]['global']) {
                    cell_global.innerHTML = 'Si'
                    cell_cost.innerHTML = data2Save[i].cost;
                } else {
                    cell_global.innerHTML = 'No'
                    cell_cost.innerHTML = ''
                }
                if (data2Save[i]['startDate'] != '' && data2Save[i]['endDate'] != '') {
                    cell_dates.innerHTML = 'Del ' + data2Save[i]['startDate'] + ' al ' + data2Save[i]['endDate']
                } else {
                    cell_dates.innerHTML = 'Sin vigencia'
                }

                var buttonEdit = document.createElement("button")
                buttonEdit.innerHTML = 'Editar'
                buttonEdit.className = 'btn btn-outline-success mr-2'
                buttonEdit.addEventListener('click', function (event) {
                    handleOnPressBack()
                })
                cell_actions.appendChild(buttonEdit)

                if(i > 0) {
                    var $buttonDelete = document.createElement("button")
                    $buttonDelete.innerText = 'Eliminar'
                    $buttonDelete.className = 'btn btn-outline-danger remove-special-price-entry'
                    $buttonDelete.dataset.object_index = i;

                    cell_actions.appendChild($buttonDelete)
                }

                row.appendChild(cell_description)
                row.appendChild(cell_type)
                row.appendChild(cell_amount)
                row.appendChild(cell_global)
                row.appendChild(cell_cost)
                row.appendChild(cell_dates)
                row.appendChild(cell_actions)

                body.appendChild(row);

            }
        }

        /**
         * Deletes an especial price entry from data2Save array
         *
         * @author Giovanny González <giovanny.gonzalez@nuvem.mx>
         * created 11/03/2021
         * @params
         * @return void
         *
         */
        $(document).on('click', '.remove-special-price-entry', function(e) {
            e.preventDefault();

            let $deleteBtn = e.currentTarget;

            Swal.fire({
            title: '¿Deseas remover este costo especial?',
            showCancelButton: true,
            confirmButtonText: `Sí`,
            cancelButtonText: `No`,
            }).then((result) => {
                if (result.value) {
                    let object = data2Save.find(element => element.object_index == $deleteBtn.dataset.object_index);
                    //deletes entry on step 2
                    let $stepTwoEntry = document.getElementById(`special_prices_step_two_entry_${object.object_index}`);
                    $stepTwoEntry.parentNode.removeChild($stepTwoEntry);
                    //deletes entry on table (current step/step 3)
                    let $tbody = $deleteBtn.closest('tr').parentNode;
                    $tbody.removeChild($deleteBtn.closest('tr'));
                    data2Save.splice(object.object_index, 1);
                    Swal.fire('¡Costo especial removido!', '', 'success');
                }
            })
        });

        /**
         * Display the data of the arra of the selected products
         * @author daniel.cruz@losfra.dev
         * @created 2020-02-26
         * @param
         * @return
         */
        function showProducts() {

            var body = document.getElementById('data_products')

            for (let i = 0; i < productsTagify.value.length; i++) {

                var row = document.createElement('tr')

                var cell_idProducto = document.createElement("td")
                var cell_description = document.createElement("td")
                var cell_amount = document.createElement("td")
                var cell_global = document.createElement("td")
                var cell_cost = document.createElement("td")
                var cell_dates = document.createElement("td")
                var cell_actions = document.createElement("td")

                cell_idProducto.innerHTML = productsTagify.value[i].data.Id_Prod1
                cell_description.innerHTML = productsTagify.value[i].data.Descrip1

                row.appendChild(cell_idProducto)
                row.appendChild(cell_description)

                body.appendChild(row);

            }
        }

        /**
         * Submits the form to save the data in the database
         * @author daniel.cruz@losfra.dev
         * @created 2020-02-26
         * @param
         * @return
         */
        function saveData() {
            let form = document.getElementById('special_pricesForm');
            let token = form.querySelector('input[name=_token]').value;
            let data = {
                Descripcion: document.getElementById('rule_description').value,
                Id_ReglaCostoEspecial: document.getElementById('rule_type_id').value,
                reglas_especiales_productos: data2Save,
                reglas_especiales_productos_detalles: productsTagify.value
            };

            let xhr = new XMLHttpRequest();
            xhr.open('POST', form.action);
            xhr.setRequestHeader('X-CSRF-TOKEN', token);
            xhr.setRequestHeader('Content-Type', 'application/json');
            xhr.responseType = 'json';
            xhr.onload = function() {
                if(xhr.readyState === 4) {
                    if(xhr.status == 422) {
                        let $errors = document.getElementById('request_errors');
                        $errors.querySelector('li').innerText = xhr.response[0];
                        $errors.classList.remove('d-none');
                        console.error(xhr.response);
                        window.scrollTo(0, 0);
                        return;
                    }
                    else if(xhr.status == 500) {
                        alert('Algo salió mal');
                        console.error(xhr.response);
                        return;
                    };

                    location.href = '/special_prices';
                }
            }
            xhr.send(JSON.stringify(data));
        }

        const _index = {
            $mdl: $('#show-modal'),
            datatable: function () {
                $('#myTable').DataTable({
                    processing: true,
                    serverSide: true,
                    destroy: true,
                    deferRender: true,
                    language: {
                        url: "{{ asset('/js/Spanish.json') }}"
                    },
                    ajax: {
                        url: "{{ route('special_prices.search') }}",
                        data: {
                            family: function () {
                                return $('#family').val()
                            },
                            brand: function () {
                                return $('#brand').val()
                            },
                            especiality: function () {
                                return $('#especiality').val()
                            },
                            category: function () {
                                return $('#category').val()
                            },
                            "_token": "{{ csrf_token() }}",
                        },
                        type: 'POST'
                    },
                    columns: [
                        {data: 'Descrip1', name: 'Nombre'},
                        {data: 'check', name: 'check'},
                    ],
                    select: true,
                });
            },
            fillMdl: function (that) {
                this.$mdl.find('label').html('');
                let data = JSON.parse(that.dataset['banner']);
                $.each(data, (idx, val) => {
                    if (idx == 'image') {
                        document.getElementById("show_img").src = `/storage/${val}`;
                    } else {
                        this.$mdl.find(`#${idx}`).html(val);
                    }
                });
            },
            runQuery: function () {
                $('#myTable').DataTable().ajax.reload();
            },
            addItem: function (that) {
                productsTagify.addTags([{value: that.name, name: that.value, data: JSON.parse(that.dataset['productdata'])}])

            }
        }
    </script>
@endpush
