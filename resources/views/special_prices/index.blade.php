@extends('adminlte::page')

@section('title', 'Precios')

@section('content_header')
    @component('components.breadcrumb', [
        'current' => 'Precios especiales'
    ])
    @endcomponent
@stop

@section('content')
@if (session()->has('message'))
    <div class="alert-success" id="popup_notification">
        <strong>{{ session('message') }}</strong>
    </div>
@endif
    <div id="accordion">
        <div class="card animated bounceInDown">
            <div class="card-header">
                <h5 class="mb-0">
                    <a class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                        Listado
                    </a>
                    <a href="{{ route('special_prices.create') }}" class="btn btn-outline-success float-right" data-toggle="tooltip" data-placement="left" title="Agregar Nuevo"
                       style="border-color:transparent;color: white;">
                        <i class="fas fa-plus-circle fa-lg"></i>
                    </a>
                </h5>
            </div>

            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                <div class="card-body table-responsive">
                    <table class="table" id="myTable">
                        <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>Productos</th>
                            <th>Estatus</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>

        </div>
    </div>

    @includeIf('special_prices.partials.show')
@stop

@push('js')
    <script>
        $(function () {
            _index.datatable();
        });

        const _index = {
            $mdl: $('#show-modal'),
            datatable: function () {
                $('#myTable').DataTable({
                    processing: true,
                    serverSide: true,
                    language:{
                        url: "{{ asset('/js/Spanish.json') }}"
                    },
                    ajax: "{{ route('special_prices.index') }}",
                    columns: [
                        { data: 'Descripcion', name: 'Descripcion' },
                        { data: 'productos', name: 'productos' },
                        { data: 'Estatus', name: 'Estatus' },
                        { data: 'actions', name: 'actions', orderable: false, searchable: false, width: '8em' },
                    ]
                });
            },
            fillMdl: function (element) {
                this.$mdl.find('label').html('');
                let data = JSON.parse(element.dataset.special_price);
                $.each(data, (idx, val) => {
                    this.$mdl.find(`#${idx}`).html(val);
                });
                $('#table_prices').DataTable({
                    processing: true,
                    serverSide: true,
                    destroy: true,
                    language:{
                        url: "{{ asset('/js/Spanish.json') }}"
                    },
                    ajax: {
                        url: "{{ route('special_prices.show') }}",
                        data: {
                            Id_CostoEspecial: JSON.parse(element.dataset.special_price)['Id_CostoEspecial'],
                            "_token": "{{ csrf_token() }}",
                        },
                        type: 'POST'
                    },
                    columns: [
                        { data: 'Descripcion', name: 'Descripcion' },
                        { data: 'Tipo', name: 'Tipo' },
                    ]
                });
            },
            toggleStatus: function (element) {
                let data = JSON.parse(element.dataset.special_price);
                swal({
                    title: '¿Estás seguro?',
                    text: "Se cambiará el estatus de registro.",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Si, cambialo!'
                }).then((result) => {
                    if (result.value) {
                        axios.post('{{ route('special_prices.destroy') }}', data)
                            .then(resp => {
                                _swal.alert('Cambiado!', 'success', 'Se cambió correctamente.');
                                let tables = $.fn.dataTable.tables(true); // get all visible DT instances
                                $(tables).DataTable().search('').draw();
                            })
                            .catch(err => {
                                console.log({err});
                                _swal.toast('Error!', 'error', 'No se pudo realizar esta acción. Intente en otro momento.');
                            });
                    }
                });
            },
            deleteItems: function (element) {
                let data = JSON.parse(element.dataset.special_price);
                swal({
                    title: '¿Estás seguro?',
                    text: "Se eliminaran los registros seleccionado.",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Si, eliminalos!'
                }).then((result) => {
                    if (result.value) {
                        axios.post('{{ route('special_prices.delete') }}', data)
                            .then(resp => {
                                _swal.alert('Exito!', 'success', 'Se elimino correctamente.');
                                let tables = $.fn.dataTable.tables(true); // get all visible DT instances
                                $(tables).DataTable().search('').draw();
                            })
                            .catch(err => {
                                console.log({err});
                                _swal.toast('Error!', 'error', 'No se pudo realizar esta acción. Intente en otro momento.');
                            });
                    }
                });
            }
        }
    </script>
@endpush
