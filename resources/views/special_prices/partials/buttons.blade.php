{{-- @permission('*banners') --}}
<div class="btn-group dropleft">
    <button class="btn btn-default btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        Opciones
    </button>
    <div class="dropdown-menu">
        {{-- @permission('list_banners') --}}
            <a class="dropdown-item" data-special_price="{{ json_encode($special_price) }}" onclick="_index.fillMdl(this)" href="#"
               data-toggle="modal" data-target="#show-modal">
                Detalles
            </a>
        {{-- @endpermission --}}
        {{-- @permission('edit_banners') --}}
            <a class="dropdown-item" data-special_price="{{ json_encode($special_price) }}" onclick="_index.deleteItems(this)" href="#">
                Eliminar
            </a>
        {{-- @endpermission --}}
        {{-- @permission('delete_banners') --}}
            {{-- <div class="dropdown-divider"></div> --}}
            <a class="dropdown-item" data-special_price="{{ json_encode($special_price) }}" onclick="_index.toggleStatus(this)" href="#">
                Cambiar estatus
            </a>
        {{-- @endpermission --}}
    </div>
</div>
{{-- @endpermission --}}