@extends('adminlte::page')

@section('title', 'Precios')

@section('content_header')
    @component('components.breadcrumb', [
        'current' => 'Costo base emprendedores'
    ])
    @endcomponent
@stop

@section('content')
@if (session()->has('message'))
    <div class="alert-success" id="popup_notification">
        <strong>{{ session('message') }}</strong>
    </div>
@endif
    <div id="accordion">
        <div class="card animated bounceInDown">
            <div class="card-header">
                <h5 class="mb-0">
                    <a class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                        Listado
                    </a>
                </h5>
            </div>

            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                <div class="card-body table-responsive">
                    <table class="table" id="myTable">
                        <thead>
                        <tr>
                            @foreach ($attributes as $attr)
                                <th>{{$attr}}</th>
                            @endforeach                            
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
            
        </div>
    </div>

    @includeIf('base_prices.partials.show')
@stop

@push('js')
    <script>
        $(function () {
            _index.datatable();
        });

        const _index = {
            datatable: function () {
                var resultAttributes = {!! json_encode($attributes) !!};

                var atttributes = [];
                resultAttributes.forEach(element => {
                 atttributes.push({data: element, name: element});
                });
                
                $('#myTable').DataTable({
                    processing: true,
                    serverSide: true,
                    language:{
                        url: "{{ asset('/js/Spanish.json') }}"
                    },
                    ajax: "{{ route('base-prices.index') }}",
                    columns: atttributes
                });
            },
            fillMdl: function (that) {
                let data = JSON.parse(that.dataset['price']);
                $('#rows-div').empty();
                $.each(data, (idx, val) => {
                    $('#rows-div').append('<div class="form-group col-md-6 col-sm-12"><span class="font-weight-bold">' + idx + '</span>: ' + val + '</div>');
                });
            }
        }

        
    </script>
@endpush