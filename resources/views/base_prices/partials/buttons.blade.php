<div class="btn-group dropleft">
    <button class="btn btn-default btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        Opciones
    </button>
    <div class="dropdown-menu">
            <a class="dropdown-item" data-price="{{ json_encode($basePrice) }}" onclick="_index.fillMdl(this)" href="#"
               data-toggle="modal" data-target="#show-modal">
                Detalles
            </a>
    </div>
</div>