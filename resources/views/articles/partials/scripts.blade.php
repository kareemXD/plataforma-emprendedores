{{-- @push('styles')  not working --}}
<style>
    span b{
        margin-left:-10px;
        margin-right:-10px;
    }
</style>
@push('js')
    <script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
    <script>
    /**
    * config SWAL
    * @author luis peña <luis.pena@nuvemtecnologia.mx>
    * @created 2020-10-09
    */
    @if (Session::has('message'))
        sAlert(
            "{{ Session::get('message.title') }}",
            "{{ Session::get('message.type') }}",
            "{{ Session::get('message.text') }}"
        );
    @endif
    function sAlert(title, type, text)
    {
        swal({
            title: title,
            type: type,
            text: text,
            confirmButtonText: "Continuar",
            timer: 3000
        });
    }
    </script>
    <script>
        var numNextPage = 1; // load pages recommendation default

        /**
        * valid if edito or create to fill CKEDITOR
        * @author luis peña <luis.pena@nuvemtecnologia.mx>
        * @created 2020-10-09
        */
        $(document).ready(function() {
            @if(request()->is('*/edit'))
                //edit blade
                loadCkeditor();
                @if( empty(old('description')))
                    CKEDITOR.instances.editor1.setData({!! json_encode($article->description) !!});
                @else
                    CKEDITOR.instances.editor1.setData({!! json_encode(old('description') ) !!});
                @endif
            @elseif(request()->is('*/create'))
                // create blade
                loadCkeditor();
                @if( !empty(old('description')))
                    CKEDITOR.instances.editor1.setData({!!json_encode(old('description') )!!});
                @endif
            @endif
         });

         /**
         * config CKEDITOR
         * @author luis peña <luis.pena@nuvemtecnologia.mx>
         * @created 2020-10-09
         */
         function loadCkeditor() {
            CKEDITOR.replace('editor1');
            CKEDITOR.config.height = '100%';
            CKEDITOR.config.width = '100%';
            CKEDITOR.config.extraPlugins = "base64image";
         }


         /**
         * config CKEDITOR
         * @author luis peña <luis.pena@nuvemtecnologia.mx>
         * @created 2020-10-09
         */
        function disableRecommendation(id_recommendation, status) {
            let label = ( status ? 'desactivar' : 'activar' );
            var token = $("#token").val();
            swal({
                title: 'Está seguro?',
                text: "Quiéres "+label+" este blog?",
                type: 'warning',
                showCancelButton: true,
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                buttonsStyling: false,
                cancelButtonText: 'No, cancel!',
                confirmButtonText: "Si, "+label+"!"
           }).then((result) => {
               if (result.value) {
                   $.ajax({
                       url: '{{ URL::to("articles") }}/' + id_recommendation,
                       headers: {'X-CSRF-TOKEN': token},
                       type: 'DELETE',
                       data: {
                           id: id_recommendation
                       }
                   }).done(function(data){
                       sAlert(data.title, data.type, data.text);
                       $('#recommendations-table').DataTable().ajax.reload();
                   });
               }
           });
        }

    </script>
@endpush
