
@permission('show_article')

<a href="{!! route('articles.show', [$article]) !!}" class="btn btn-warning" data-toggle="tooltip" title="Consultar">
	<i class="fa fa-envelope-open-o"> Consultar</i>
</a>
@endpermission
@permission('edit_article')
<a href="{!! route('articles.edit', [$article]) !!}" class="btn btn-primary" data-toggle="tooltip" title="Editar">
	<i class="fa fa-folder-open"> Editar</i>
</a>
@endpermission
<a href="#" class="btn {{ $article->status ? "btn-danger" : "btn-success"}} cli" data-toggle="tooltip"
   	title="{{ $article->status ? "Desactivar" : "Activar"}}"
   	onclick="disableRecommendation({{ $article->id }}, {{ $article->status }}); return false;">
	@if( $article->status )
		<i class="fa fa-trash"> Desactivar </i>
	@else
		<i class="fa fa-check" > Activar</i>
	@endif
</a>
