<div class="container col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <center>
                <h4>Blog</h4>
            </center>
		</div>
	</div>

	<div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="form-group row">
                <label for="recommendation_title" class="col-sm-2 col-form-label">T&iacute;tulo* </label>
                <div class="col-sm-10">
                    {!! Form::text('title', isset($article->title) ? $article->title : null, ['class' => 'form-control input-form', 'placeholder' => 'Titulo', 'id' => 'recommendation_title', 'required']) !!}
                    <small class="text-danger">{{ $errors->first('title') }}</small>
                </div>
            </div>
            <div class="form-group row">
                <label for="recommendation_title" class="col-sm-2 col-form-label">Imagen encabezado </label>
                <div class="col-sm-6">
                    <input type="file" name="icon" accept="image/*" id="recommendation_icon" class="form-control input-form" {{isset($article) ? "" : "required"}}>
                    <small class="text-danger">{{ $errors->first('icon') }}</small>
                </div>
                <div class="col-sm-4" align="center">
                    @if(isset($article->icon))
                        <img src="{{ asset('storage/article_files/'.$article->icon) }}" alt="" height="90" width="150">
                    @endif
                </div>
            </div>
            <div class="col-md-12">
                <div class="box">
                    <div class="box-body pad">
                        <textarea id="editor1" name="description" rows="10" cols="80"></textarea>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="text-center">
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">Guardar</button>
                        <a class="btn btn-danger btn-close" href="{{ route('articles.index') }}">Cancelar</a>
                    </div>
                </div>
            </div>
        </div>
	</div>
</div>
