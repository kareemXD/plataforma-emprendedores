@extends('adminlte::page')

@section('htmlheader_title')
    Blog
@endsection
@section('contentheader_title')
    Blog
@endsection
@php $contentFull = 1; @endphp
@section('content')

    <div class="box box-solid box-primary" width="100%">
        @include('alerts.errors')
        <div class="box-header with-border">
            <h3 class="box-title">Editar Blog</h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fa fa-minus"></i></button>
            </div>
        </div>
        <div class="box-body">
            {!!Form::open(['route'=> ['articles.update', $article->id], 'method'=> 'PUT', 'class' => 'form-horizontal', 'files' => true])!!}
                @include('articles.partials.inputs', ['articles' => $article])
            {!!Form::close()!!}
        </div>
    </div>
@endsection
@include('articles.partials.scripts')

{{-- @push('styles') --}}
    <style>
        span b{
            margin-left:-10px;
            margin-right:-10px;
        }
    </style>
@push('scripts')
    <script>
        activeOption = 'recommendations';
    </script>
@endpush
