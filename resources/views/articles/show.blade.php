@extends('adminlte::page')

@section('htmlheader_title')
    Blog
@endsection

@section('contentheader_title')
@endsection

@section('content')
    <div class="box-body">
        <div class="panel-body" style="overflow-x: auto; height:100%;">
            <div class="row" style="margin-bottom: 20px">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <img src="{{ asset('storage/article_files/'.$article->icon) }}" width="443" height="234"
                         style="margin-bottom: 10px; margin-right: 15px; float:left;">
                    <h3 align="center">{{ isset($article->title) ? $article->title : '' }}</h3>
                    {!! isset($article->description) ? $article->description : '' !!}
                </div>
            </div>
            <br/>
            <div class="col-md-3" style="float: right">
                <a href="{{route('articles.index')}}" class="btn btn-primary btn-block">
                    <b>Regresar</b>
                </a>
            </div>
        </div>
    </div>
@endsection
{{-- @push('styles') --}}
    <style>
        span b{
            margin-left:-10px;
            margin-right:-10px;
        }
    </style>
@push('scripts')
    <script>
        activeOption = 'recommendations';
    </script>
@endpush
