@extends('adminlte::page')

@section('htmlheader_title')
    Blog
@endsection
@section('contentheader_title')
    Blog
    <a href="{{ route('articles.create') }}" class="btn btn-primary btn-md" style="float:right;">
        <b>Nueva Blog</b>
    </a>
@endsection

@section('contentheader_button')
@endsection

@section('content')
    <div class="box box-solid box-primary">
        @include('alerts.errors')
        <div class="box-header with-border">
            <h3 class="box-title">Listado de Blog's</h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fa fa-minus"></i></button>
            </div>
        </div>
        <div class="box-body">
			<div class="panel-body" style="overflow-x: auto; height:100%;">
				 <table class="table table-bordered table-hover nowrap" id="recommendations-table">
					 <thead>
						 <tr>
							 <th>Título</th>
                             <th>Usuario</th>
                             <th>Imagen encabezado</th>
                             <th>Estatus</th>
							 <th width="210px;">Acciones</th>
						 </tr>
					 </thead>
				 </table>
 	 		</div>
        </div>
    </div>
    <input type="hidden" name="_token" value="{{csrf_token()}}" id="token">
@endsection
    @include('articles.partials.scripts')
@push('js')
    <script>
        activeOption = 'recommendations';
        $(document).ready(function() {
            $("#recommendations-table").DataTable({
                ajax: '/articles',
                processing: true,
                serverSide: true,
                deferRender: true,
                order: false,
                bDestroy: true,
                language: {
                    url: "{{ asset('/js/Spanish.json') }}"
                },
                columns: [
                    {data: 'title', name: 'title',width: "20%"},
                    {data: 'fullName', name: 'fullName'},
                    {data: 'img', name: 'img' , orderable: false, serchable: false, bSearchable: false},
                    {data: 'status', name: 'status'},
                    {data: 'actions', name: 'actions', orderable: false, serchable: false, bSearchable: false},
                ]
            });
         });
    </script>
@endpush
