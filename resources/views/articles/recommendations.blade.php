@extends('adminlte::page')

@section('htmlheader_title')
    Blog
@endsection
@section('contentheader_title')
    <div class="row" align="center">
        Blog
    </div>
@endsection

@section('contentheader_button')
@endsection

@section('content')
    <div class="box-body">
        <div class="panel-body" style="overflow-x: auto; height:100%;">
            <div id="box-recommendations-page">
            </div>
            <div align="center">
                <a href="#" class="btn btn-primary btn-md" style="float:center;" id="next-page" onclick="nextPage(); return false;">
                    <b>Cargar m&aacute;s</b>
                </a>
            </div>
        </div>
    </div>
@endsection
    @include('articles.partials.scripts')
@push('scripts')
    <script>
        activeOption = 'recommendations';
    </script>
@endpush
