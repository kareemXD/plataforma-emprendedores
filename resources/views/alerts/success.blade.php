@if (session('message'))
    <div id="success-alert" class="alert alert-success alert-dismissible fade show" role="alert">
	  	{{ session('message') }}
	  	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
	    	<span aria-hidden="true">&times;</span>
	  	</button>
	</div>
@endif
@push('js')
	<script type="text/javascript">
		$("#success-alert").fadeTo(2000, 500).slideUp(500, function() {
		    $("#success-alert").slideUp(2000);
		});
	</script>
@endpush