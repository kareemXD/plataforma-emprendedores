<section class="mt-1">
    <div class="row no-mg products-container">
        <div class="col-md-4 col-lg-3 col-sm-6 col-12"
             style="display: flex;justify-content: center;align-items: center">
            <div class="product">
                <a style="text-decoration: none;" class="div-link" href="{{route('product.show',['product' => 1])}}">
                    <div class="product-top">
                        <div class="colors">
                            <div class="color" style="background-color: #1e7bbe"></div>
                            <div class="color" style="background-color: #868686"></div>
                            <div class="color" style="background-color: #db4f4a"></div>
                        </div>
                        <i class="far fa-heart"></i>
                    </div>
                    <div class="product-image">
                        <img src="{{asset('/img/producto.png')}}" alt="">
                    </div>
                    <div class="product-data">
                        <p class="left-stock">¡Quedan 5 piezas!</p>
                        <p class="product-title">Estetoscopio</p>
                        <p class="product-brand">Medicare</p>
                        <p class="product-price">$250.00</p>
                        <button class="button blue">Agregar al carrito</button>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-md-4 col-lg-3 col-sm-6 col-12"
             style="display: flex;justify-content: center;align-items: center">
            <div class="product">
                <a style="text-decoration: none;" class="div-link" href="{{route('product.show',['product' => 1])}}">
                    <div class="product-top">
                        <div class="colors">
                            <div class="color" style="background-color: #1e7bbe"></div>
                            <div class="color" style="background-color: #868686"></div>
                            <div class="color" style="background-color: #db4f4a"></div>
                        </div>
                        <i class="far fa-heart"></i>
                    </div>
                    <div class="product-image">
                        <img src="{{asset('/img/producto.png')}}" alt="">
                    </div>
                    <div class="product-data">
                        <p class="left-stock">¡Quedan 5 piezas!</p>
                        <p class="product-title">Estetoscopio</p>
                        <p class="product-brand">Medicare</p>
                        <p class="product-price">$250.00</p>
                        <button class="button blue">Agregar al carrito</button>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-md-4 col-lg-3 col-sm-6 col-12"
             style="display: flex;justify-content: center;align-items: center">
            <div class="product">
                <a style="text-decoration: none;" class="div-link" href="{{route('product.show',['product' => 1])}}">
                    <div class="product-top">
                        <div class="colors">
                            <div class="color" style="background-color: #1e7bbe"></div>
                            <div class="color" style="background-color: #868686"></div>
                            <div class="color" style="background-color: #db4f4a"></div>
                        </div>
                        <i class="far fa-heart"></i>
                    </div>
                    <div class="product-image">
                        <img src="{{asset('/img/producto.png')}}" alt="">
                    </div>
                    <div class="product-data">
                        <p class="left-stock">¡Quedan 5 piezas!</p>
                        <p class="product-title">Estetoscopio</p>
                        <p class="product-brand">Medicare</p>
                        <p class="product-price">$250.00</p>
                        <button class="button blue">Agregar al carrito</button>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-md-4 col-lg-3 col-sm-6 col-12"
             style="display: flex;justify-content: center;align-items: center">
            <div class="product">
                <a style="text-decoration: none;" class="div-link" href="{{route('product.show',['product' => 1])}}">
                    <div class="product-top">
                        <div class="colors">
                            <div class="color" style="background-color: #1e7bbe"></div>
                            <div class="color" style="background-color: #868686"></div>
                            <div class="color" style="background-color: #db4f4a"></div>
                        </div>
                        <i class="far fa-heart"></i>
                    </div>
                    <div class="product-image">
                        <img src="{{asset('/img/producto.png')}}" alt="">
                    </div>
                    <div class="product-data">
                        <p class="left-stock">¡Quedan 5 piezas!</p>
                        <p class="product-title">Estetoscopio</p>
                        <p class="product-brand">Medicare</p>
                        <p class="product-price">$250.00</p>
                        <button class="button blue">Agregar al carrito</button>
                    </div>
                </a>
            </div>
        </div>
    </div>
</section>
