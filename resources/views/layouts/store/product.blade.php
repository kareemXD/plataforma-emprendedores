<section>
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-6">
                <div class="product-image  showcase">
                    <img class="image-showcase" src="{{$product->FOTO ?? asset('/img/default.jpg')}}" alt="">
                </div>
                <div class="row mt-3" style="justify-content: space-around">
                    <div class="thumbnail ">
                        <img class="" src="{{$product->FOTO ?? asset('/img/default.jpg')}}" alt="">
                    </div>
                    <div class="thumbnail ">
                        <img class="" src="{{$product->FOTO ?? asset('/img/default.jpg')}}" alt="">
                    </div>
                    <div class="thumbnail ">
                        <img class="" src="{{$product->FOTO ?? asset('/img/default.jpg')}}" alt="">
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-6">
                <h5 class="section-title align-left mb-1">{{$product->DESCRIP1}}</h5>
                <div class="star-meter color-blue">
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star"></i>
                    <i class="fas fa-star-half-alt"></i>
                    <i class="far fa-star"></i>
                </div>
                <p class="product-price">${{round($product->COSTO_PUBLICO,2)}}</p>
                <hr>
                <p class="description">
                {{$product->DESCRIP2}}

                <p>Colores disponibles</p>
                <div class="colors flex-row ml-1">
                    <div class="color" style="background-color: #1e7bbe"></div>
                    <div class="color" style="background-color: #868686"></div>
                    <div class="color" style="background-color: #db4f4a"></div>
                </div>
                <p style="font-size: 1.2rem">Cantidad: </p>
                <div class="flex-row" style="display: flex;align-items: center">
                    <div class="quantity-control full-center">
                        <i class="fas fa-angle-up"></i>
                    </div>
                    <input type="number" class="quantity-box" value="1">

                    <div class="quantity-control full-center">
                        <i class="fas fa-angle-down"></i>
                    </div>
                </div>
                <div class="mt-5" style="display: flex;align-items: center">
                    <button class="button red mr-2">Comprar ahora</button>
                    <i class="far fa-heart  icon-showcase"></i>
                </div>
                <hr>
                <div class="column link-list">
                    <a href="">Póliticas de seguridad</a>
                    <a href="">Póliticas de envíos</a>
                    <a href="">Póliticas de devoluciones</a>
                </div>

            </div>
            <div class="col-12 full-center column">
                <div class="flex-row">
                    <p class="action-label">Decripción del producto</p>
                    <p class="action-label">Detalles del producto</p>
                </div>
                <hr>
                <div>
                    <p class="align-center">{{$product->DESCRIP2}}</p>
                </div>
            </div>
        </div>

    </div>
</section>
