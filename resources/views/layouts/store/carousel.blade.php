<section style="padding-top: 212px;">

    <div class="responsive">
        @foreach ($banners as $banner)
            <img src="{{asset('storage/' . $banner->image)}}" alt="{{$banner->name}}"
                 onclick="window.open('{{ $banner->url }}')">
        @endforeach
    </div>
    <div class="spanner"></div>
    <div class="card-container">
        <div>
            <div class="card">
                <div class="containerImg">
                    <div class="title-card">
                        <p class="no-pd no-mg">Envío <span class="highlight-span">gratis</span> en tu primer pedido</p>
                    </div>
                    <div class="crates-bg"></div>
                    <div style="padding: 2rem 0;text-align: center">
                        <a class="card-link" href="#">Comprar ahora</a>
                    </div>
                    <div class="overlay overlay-purple"></div>
                </div>
            </div>
        </div>
        <div>
            <div class="card">
                <div class="containerImg">
                    <div class="title-card">
                        <p class="no-pd no-mg">Conoce las <span class="highlight-span">ofertas</span> del día y
                            aprovecha</p>
                    </div>
                    <div class="crates-bg"></div>
                    <div style="padding: 2rem 0;text-align: center">
                        <a class="card-link" href="#">Comprar ahora</a>
                    </div>
                    <div class="overlay overlay-red"></div>
                </div>
            </div>
        </div>

        <div>
            <div class="card">
                <div class="containerImg">
                    <div class="title-card">
                        <p class="no-pd no-mg">Los <span class="highlight-span">favoritos</span> de nuestros clientes
                        </p>
                    </div>
                    <div class="crates-bg"></div>
                    <div style="padding: 2rem 0;text-align: center">
                        <a class="card-link" href="#">Comprar ahora</a>
                    </div>
                    <div class="overlay overlay-green"></div>
                </div>
            </div>
        </div>

        <div>
            <div class="card">
                <div class="containerImg">
                    <div class="title-card">
                        <p class="no-pd no-mg">Envío <span class="highlight-span">gratis</span> en tu primer pedido</p>
                    </div>
                    <div class="crates-bg"></div>
                    <div style="padding: 2rem 0;text-align: center">
                        <a class="card-link" href="#">Comprar ahora</a>
                    </div>
                    <div class="overlay overlay-purple"></div>
                </div>
            </div>
        </div>
    </div>
</section>
