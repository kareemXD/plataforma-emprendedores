<footer>
    <div class="footer-top row no-mg">
        <div class="footer-vertical-menu col-md-3 col-6">
            <ul class="no-pd">
                <li class="list-head">Inicio</li>
                <li>Catálogo</li>
                <li>Conócenos</li>
                <li>Noticias</li>
                <li>Contáctanos</li>
                <li>Proveedores</li>
            </ul>
        </div>
        <div class="footer-vertical-menu col-md-3 col-6">
            <ul class="no-pd">
                <li class="list-head">Clientes</li>
                <li>Facturación en línea</li>
                <li>Renta de equipos</li>
                <li>Programas</li>
                <li>Blog</li>
            </ul>
        </div>
        <div class="footer-vertical-menu col-md-3 col-12">
            <ul class="no-pd">
                <li class="list-head">Aviso de privacidad</li>
                <li>Políticas de compra</li>
                <li>Preguntas frecuentes</li>
            </ul>
        </div>
        <div class="footer-contact col-md-3 col-12">
            <p class="no-pd no-mg">Samuel Rámos #575 Colonia Cuahutemoc</p>
            <p class="no-pd no-mg">Morela, Michoacán, C.P. 58020</p>
            <p class="no-pd no-mg">01 443 317 2616</p>
            <p class="no-pd no-mg">ventas@medicadepot.mx</p>
            <div class="follow-container d-none d-sm-none d-md-flex">
                <span class="follow-label">Síguenos</span>
                <br>
                <img src="{{ asset('/img/facebook.png') }}" alt="">
                <img src="{{ asset('/img/instagram.png') }}" alt="">
            </div>
        </div>
        <div class="col-12 d-md-none">
            <div class="follow-container">
                <span class="follow-label">Síguenos</span>
                <div>
                    <img src="{{ asset('/img/facebook.png') }}" alt="">
                    <img src="{{ asset('/img/instagram.png') }}" alt="">
                </div>
            </div>
        </div>
    </div>
    <div class="footer-middle">
        <img src="{{ asset('/img/logo.png') }}" alt="">
        <div class="image-list">
            <div class="image-container">
                <img src="{{ asset('/img/PayPal.png') }}" alt="">
            </div>
            <div class="image-container">
                <img src="{{ asset('/img/Visa.png') }}" alt="">
            </div>
            <div class="image-container">
                <img src="{{ asset('/img/mastercard.png') }}" alt="">
            </div>
            <div class="image-container">
                <img src="{{ asset('/img/oxxo.png') }}" alt="">
            </div>
        </div>
    </div>
    <div class="footer-bottom">
        <span style="text-align: center">© 2020 MEDICA DEPOT, S.A. DE C.V. Todos los derechos reservados.</span>
    </div>
</footer>
