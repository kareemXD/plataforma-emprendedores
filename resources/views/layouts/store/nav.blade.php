<div style="position: fixed;width: 100%;z-index: 100">
    <div class="top-menu">
        <p class="no-mg no-pd ">ATENCIÓN A CLIENTES. 443 317 2617</p>
    </div>
    <nav class="nav">
        <div class="d-md-flex">
            <img src="{{ asset('/img/white_logo.png') }}" alt="">
        </div>
        <div id="mySidebar" class="sidebar">
            <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">×</a>
            <a href="#">Carrito</a>
            <a href="#">Lista de deseos</a>
            <a href="#">Notificaciones</a>
            <a href="#">Catálogo</a>
            <a href="#">Conocenos</a>
            <a href="#">Nuestro blog</a>
            <a href="#">Contáctanos</a>
        </div>
        <div class="nav-container">
        <div class="d-sm-none d-flex d-md-none" id="main">
            <button class="openbtn" onclick="openNav()">☰</button>
        </div>
        <div>
            <input placeholder="BUSCAR PRODUCTO" class="search-input" type="text">
        </div>
        </div>
        <div class="icons-container">
            <div class="icon-menu">
                <div>
                    <span class="notification-number notification-container">3</span>
                    <i class="fa fa-heart"></i>
                </div>
            </div>
            <div class="icon-menu">
                <i class="fa fa-user"></i>
            </div>
            <div class="icon-menu active-icon">
                <div>
                    <span class="notification-number notification-container">9+</span>
                    <i class="fa fa-shopping-cart"></i>
                </div>
            </div>
        </div>
    </nav>
    <div class="nav-menu d-sm-none d-none d-md-flex">
        <div class="md-menu">
            <a href="#">catálogo</a>
            <a href="#">conocenos</a>
            <a href="#">nuestro blog</a>
            <a href="#">contáctanos</a>
        </div>
    </div>

</div>
