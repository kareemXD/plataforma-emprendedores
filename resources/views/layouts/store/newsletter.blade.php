<section class="newsletter">
    <div class="newsletter-label">
        <span>Recibe nuestras ofertas en tu correo </span><span class="bold">¡Inscribete!</span>
    </div>
    <div class="newsletter-input-container">
        <input class="newsletter-input" type="text" id="newsletter-input" placeholder="Ingresa tu correo" onkeypress="submitNewsletterUser(event)">
        <button class="newsletter-button">
            <i class="fas fa-paper-plane" style="color: #FFF;font-size: 1.2rem"></i>
        </button>
    </div>
</section>
