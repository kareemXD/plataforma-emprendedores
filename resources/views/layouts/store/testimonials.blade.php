<section class="testimonials">
    <div class="testimonials-container">
        <div class="testimonial-comment blue d-flex">
            <i class="fas fa-comment"></i>
            <p class="comment">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et
                dolore magna aliqua.
            </p>
            <p class="italics">María Perez</p>
        </div>
        <div class="testimonial-comment green d-none d-md-flex">
            <i class="fas fa-comment"></i>
            <p class="comment">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et
                dolore magna aliqua.
            </p>
            <p class="italics">María Perez</p>
        </div>
        <div class="testimonial-comment red d-none d-md-flex">
            <i class="fas fa-comment"></i>
            <p class="comment">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et
                dolore magna aliqua.
            </p>
            <p class="italics">María Perez</p>
        </div>
        <div class="testimonial-comment blue d-none d-md-flex">
            <i class="fas fa-comment"></i>
            <p class="comment">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et
                dolore magna aliqua.
            </p>
            <p class="italics">María Perez</p>
        </div>
    </div>
    <div class="arrow-button">
        <i class="fas fa-angle-right"></i>
    </div>
</section>
