<section class="mt-1">
    <div class="row no-mg products-container">
        @foreach($products as $product)
            <div class="col-md-4 col-sm-6 col-12"
                 style="display: flex;justify-content: center;align-items: center">
                <div class="product">
                    <a style="text-decoration: none;" class="div-link"
                       href="{{route('product.show',['product' => $product->ID_PROD1])}}">

                        <div>
                            <div class="product-image">
                                <div
                                    style="width: 300px;height: 300px;background-image: url('{{"http://" . str_replace("\\","/",$product->FOTO) ?? asset('/img/default.jpg')}}');
                                        background-repeat: no-repeat;
                                        background-position: center;
                                        background-size: cover;padding-top: 1rem; ">
                                    <div class="colors-container">
                                        <div class="colors">
                                            <div class="color" style="background-color: #1e7bbe"></div>
                                            <div class="color" style="background-color: #868686"></div>
                                            <div class="color" style="background-color: #db4f4a"></div>
                                        </div>
                                    </div>
                                    <div class="heart-container">
                                        <i class="far fa-heart"></i>
                                    </div>
                                    <div class="left-products-container">
                                        <span>Quedan 5</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="product-data">
                            <p class="product-title">{{(strlen($product->DESCRIP2) > 30) ? substr($product->DESCRIP2, 0, 30). "..." : $product->DESCRIP2}}</p>
                            <p class="product-brand">{{$product->MARCA}}</p>
                            <p class="product-price">${{round($product->COSTO_PUBLICO,2)}}</p>
                            <button class="button blue">Agregar</button>
                        </div>
                    </a>
                </div>
            </div>
        @endforeach

    </div>
</section>
