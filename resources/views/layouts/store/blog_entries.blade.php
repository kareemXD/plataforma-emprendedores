<section class="entries-container">
    <h3 class="section-title">Lo más nuevo en nuestro blog</h3>
    <div class="entries row brand-carousel section-padding owl-carousel">
        @foreach ($articles as $article)
        <div class="entry">
            <div style="width: 100%;padding: 0 !important;overflow: hidden">
                <img src="{{asset('/storage/article_files/' . $article->icon)}}"/>
            </div>
            <div>
                <p class="blog-title">{{$article->title}}</p>
                <div style="height: 100px; text-overflow: ellipsis !important;width: 100%;overflow: hidden;">
                    {!! $article->description !!}
                </div> 
            </div>
            <button class="button blue mt-3 mb-3">Leer más</button>
        </div>
        @endforeach
    </div>
</section>
