<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ config('app.name', 'Laravel Architect') }} @yield('title')</title>

    <link rel="stylesheet" href="{{ asset('css/base.css') }}" defer>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900">
</head>
<body>
    <div id="app">
        @if (auth()->check())
            @yield('content')
        @else
            @if (request()->is('password/reset'))
                <reset-password></reset-password>
            @elseif(request()->is('password/reset/nego'))
                <email-create-nego></email-create-nego>
            @elseif(request()->is('password/reset/*'))
                <password-update email="{{ $email }}" token="{{ $token }}"></password-update>
            @else
                <login-boxed></login-boxed>
            @endif

        @endif
    </div>

    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
