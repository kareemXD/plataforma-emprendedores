@php
    list($title, $icon, $color) = ($rule->Estatus == 1) ? ['Activo', 'check-square', '#38c172'] : ['Inactivado', 'square', '#343a40'];
@endphp

<i class="fa fa-{{ $icon }}" title="{{ $title }}" aria-hidden="true" style="color: {{ $color }}"></i>
{{ $title }}
