@extends('adminlte::page')


@section('breadcrumb')

    <h3 class="m-subheader__title m-subheader__title--separator">Configuración</h3>
    <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
        <li class="m-nav__item m-nav__item--home">
            <a href="{!!URL::to('/')!!}" class="m-nav__link m-nav__link--icon">
                <i class="m-nav__link-icon la la-home"></i> Inicio
            </a>
        </li>
        <li class="m-nav__separator">-</li>
        <li class="m-nav__item">
            <a href="{!!URL::to('/configurations')!!}" class="m-nav__link">
                <span class="m-nav__link-text">Configuración</span>
            </a>
        </li>
    </ul>
@endsection

@section('content')

<div id="accordion">
    <div class="card animated bounceInDown">
        <div class="card-header">
            <h5 class="mb-0">
                <a class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                    Configuración Global
                </a>
            </h5>
        </div>

        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
            <div class="card-body table-responsive">
                {!! Form::model($configuration, ['route' => ['configurations.update',$configuration->id], 'method'=>'PUT','class'=>'m-form m-form--fit m-form--label-align-right', 'enctype' => 'multipart/form-data']) !!}
                    <div style="padding: 10px">
                        @include('configurations.partials.inputs')

                        <div style="text-align: center;">
                            <button type="submit" class="btn btn-primary">
                                Guardar
                            </button>
                            <a class="btn btn-danger" style="color: white;">
                                Cancelar
                            </a>
                        </div>
                    </div>
                {!!Form::close() !!}
            </div>
        </div>
    </div>
</div>


@endsection

@push('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.13.4/jquery.mask.min.js"></script>
    <script>
        $('.moneda').mask("#,##0.00", {reverse: true});
    </script>
@endpush
