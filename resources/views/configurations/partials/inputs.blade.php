<h4 style="text-align: center;">Conekta</h4>
<div class="row">
    <div class="col-sm-2 offset-md-5" style="background: blue; height: 4px"></div>
</div>
<div class="col-md-12 col-sm-12">
    <div class="form-group">
        {{ Form::label('conekta_private_key', 'Llave privada:', ['class' => 'control-label']) }}
        {{ Form::text('conekta_private_key', null, ['class' => 'form-control', 'maxlength' => '100']) }}
    </div>
    <div class="form-group">
        {{ Form::label('conekta_public_key', 'Llave pública:', ['class' => 'control-label']) }}
        {{ Form::text('conekta_public_key', null, ['class' => 'form-control', 'maxlength' => '100']) }}
    </div>
</div>

<h4 style="text-align: center;">Legales</h4>
<div class="row">
    <div class="col-sm-2 offset-md-5" style="background: blue; height: 4px"></div>
</div>
<div class="col-md-12 col-sm-12">
    <div class="form-group">
        {{ Form::label('legal_notice_of_privacy', 'Aviso de privacidad:', ['class' => 'control-label']) }}
        {{ Form::file('legal_notice_of_privacy', ['class' => 'form-control', 'accept' => 'application/pdf']) }}
    </div>
    <div class="form-group">
        {{ Form::label('legal_terms_and_conditions', 'Términos y condiciones:', ['class' => 'control-label']) }}
        {{ Form::file('legal_terms_and_conditions', ['class' => 'form-control', 'accept' => 'application/pdf']) }}
    </div>
</div>

<h4 style="text-align: center;">Configuración de Ads</h4>
<div class="row">
    <div class="col-sm-2 offset-md-5" style="background: blue; height: 4px"></div>
</div>
<div class="col-md-12 col-sm-12">
    <div class="form-group">
        {{ Form::label('pixel_facebook_tag', 'Etiqueta Pixel Facebook:', ['class' => 'control-label']) }}
        {{ Form::textarea('pixel_facebook_tag', null, ['class' => 'form-control']) }}
    </div>
    <div class="form-group">
        {{ Form::label('google_analytics_tag', 'Etiqueta Google Analytics:', ['class' => 'control-label']) }}
        {{ Form::textarea('google_analytics_tag', null, ['class' => 'form-control']) }}
    </div>
    <div class="form-group">
        {{ Form::label('facebook_chat_tag', 'Etiqueta Facebook:', ['class' => 'control-label']) }}
        {{ Form::textarea('facebook_chat_tag', null, ['class' => 'form-control']) }}
    </div>
</div>
