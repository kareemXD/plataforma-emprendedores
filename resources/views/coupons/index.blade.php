@extends('adminlte::page')



@section('breadcrumb')

    <h3 class="m-subheader__title m-subheader__title--separator">Cupones</h3>
    <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
        <li class="m-nav__item m-nav__item--home">
            <a href="{!!URL::to('/')!!}" class="m-nav__link m-nav__link--icon">
                <i class="m-nav__link-icon la la-home"></i> Inicio
            </a>
        </li>
        <li class="m-nav__separator">-</li>
        <li class="m-nav__item">
            <a href="{!!URL::to('/users')!!}" class="m-nav__link">
                <span class="m-nav__link-text">Cupones</span>
            </a>
        </li>
    </ul>
@endsection

@section('styles')
    <style>
        .dt-buttons{
            display: none;
        }
    </style>
@endsection
@section('content')

    <div id="accordion">
        <div class="card animated bounceInDown">
            <div class="card-header">
                <h5 class="mb-0">
                    <a class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                        Listado
                    </a>
                    <a href="{{ route('coupons.create') }}" class="btn btn-outline-success float-right" data-toggle="tooltip" data-placement="left" title="Agregar Nuevo"
                       style="border-color:transparent;color: white;">
                        <i class="fas fa-plus-circle fa-lg"></i>
                    </a>
                </h5>
            </div>

            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                <div class="card-body">
                    <table class="table table-condensed table-hover table-responsive-lg" id="index-table">
                        <thead>
                        <tr>
                            <th>Nombre del cupón</th>
                            <th>Tipo de descuento</th>
                            <th>Monto</th>
                            <th>Vencimiento</th>
                            <th>Estatus</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>


    @includeIf('coupons.partials.show')


@endsection

@push('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pace/1.0.2/pace.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.0/axios.min.js"></script>
    <script>
        $(document).ready(function(){
            $('#listMenu').find('.start').removeClass('start');
            $('#li-cupons').addClass('start');
        });


        $("#export").on("click", function() {
            $('#index-table').DataTable().button('.buttons-excel').trigger();
        });

        /**
         * @author Luis Peña <luis.pena@nuvem.mx> 11/11/2020
         */
        const _global = {
            /**
             * Toggles instance status
             * @author Luis Peña <luis.pena@nuvem.mx> 11/11/2020
             * @return void
             */
            toggleStatus: function (url) {
                swal({
                    title: '¿Estás seguro?',
                    text: "Se cambiará el estatus de registro.",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Si, cambialo!'
                }).then((result) => {
                    if (result.value) {
                        axios.delete(url, {})
                            .then(resp => {
                                _swal.alert('Cambiado!', 'success', 'Se cambió correctamente.');
                                let tables = $.fn.dataTable.tables(true); // get all visible DT instances
                                $(tables).DataTable().search('').draw();
                            })
                            .catch(err => {
                                console.log({err});
                                _swal.toast('Error!', 'error', 'No se pudo realizar esta acción. Intente en otro momento.');
                            });
                    }
                });
            },
            /**
             * Deactivate status from given instance
             * @author Luis Peña <luis.pena@nuvem.mx> 11/11/2020
             * @return void
             */
            deactivateStatus: function (url) {
                swal({
                    title: '¿Estás seguro?',
                    text: "Cambiará el estatus a desactivado.",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Si, desactivar!'
                }).then((result) => {
                    if (result.value) {
                        axios.delete(url, {})
                            .then(resp => {
                                _swal.alert('Desactivado!', 'success', 'Se desactivó correctamente.');
                                let tables = $.fn.dataTable.tables(true); // get all visible DT instances
                                $(tables).DataTable().search('').draw();
                            })
                            .catch(err => {
                                console.log({err});
                                _swal.toast('Error!', 'error', 'No se pudo realizar esta acción. Intente en otro momento.');
                            });
                    }
                });
            }
        };

        /**
         * @author Luis Peña <luis.pena@nuvem.mx> 11/11/2020
         */
        const _swal = {
            /**
             * Shows SweetAlert alert
             * @author Octavio Cornejo <octavio.cornejo@nuvemtecnologia.mx>
             * @param String title
             * @param String type
             * @param String text
             * @param Integer timer
             * @return void
             */
            alert: function (title, type, text, timer = 3000) {
                return swal({
                    title: title,
                    type: type,
                    text: text,
                    confirmButtonText: "Continuar",
                    timer: timer
                });
            },
            /**
             * Shows SweetAlert toast alert
             * @author Octavio Cornejo <octavio.cornejo@nuvemtecnologia.mx>
             * @param String title
             * @param String type
             * @param String text
             * @param Integer timer
             * @return void
             */
            toast: function (title, type, text, timer = 1e4) {
                return swal({
                    title: title,
                    type: type,
                    text: text,
                    timer: timer,
                    toast: true,
                    position: 'top-right',
                });
            }
        }

        @if (Session::has('message'))
        swal({
            title: "{{ Session::get('message.title') }}",
            type: "{{ Session::get('message.type') }}",
            text: "{{ Session::get('message.text') }}",
            confirmButtonText: "Continuar",
            timer: "{{ Session::get('message.timer') }}"
        });
        @endif
    </script>
    <script>
        /**
         * @author Luis Peña <luis.pena@nuvem.mx> 11/11/2020
         */
        $(function () {
            _index.datatable();
        });

        /**
        * @author Luis Peña <luis.pena@nuvem.mx> 11/11/2020
         */
        let _index = {
            $mdl: $('#show-modal'),
            /**
             * initialize datatable
             * @author Luis Peña <luis.pena@nuvem.mx> 11/11/2020
             * @return void
             */
            datatable: function () {
                $('#index-table').DataTable({
                    processing: true,
                    serverSide: true,
                    language:{
                        url: "{{ asset('/js/Spanish.json') }}"
                    },
                    scrollX: true,
                    scrollY: 350,
                    scrollCollapse: true,
                    ajax: '/coupons',
                    columns: [
                        { data: 'name', name: 'name', width: '14.2%' },
                        { data: 'discount_type', name: 'discount_type', width: '14.2%' },
                        { data: 'amount', name: 'amount', width: '14.2%' },
                        { data: 'end_date', name: 'end_date', width: '14.2%' },
                        { data: 'status', name: 'status', width: '14.2%' },
                        { data: 'actions', name: 'actions', orderable: false, searchable: false, width: '15em' },
                    ],
                });
            },
            /**
             * Fills modal with instance data
             * @author Luis Peña <luis.pena@nuvem.mx> 11/11/2020
             * @param HTMLObject that
             * @return void
             */
            fillMdl: function (that) {
                this.$mdl.find('label').html('');
                let data = JSON.parse(that.dataset['instance']);
                $.each(data, (idx, val) => {
                    if (idx == "discount_type") {
                        let valuer = val == 0 ? 'Porcentaje': 'Efectivo';
                        this.$mdl.find(`#${idx}`).html(valuer);
                    }else if (idx == "apply_category") {
                        let valuer = val == 0 ? 'Producto': 'Membresía';
                        this.$mdl.find(`#${idx}`).html(valuer);
                    }else{
                        this.$mdl.find(`#${idx}`).html(val);
                    }


                    if (idx == 'city') {
                        this.$mdl.find('#city').html(val.name);
                        this.$mdl.find('#state').html(val.state.name);
                    }

                });
            },
        };
    </script>
@endpush
