@extends('adminlte::page')


@section('breadcrumb')

    <h3 class="m-subheader__title m-subheader__title--separator">Cupones</h3>
    <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
        <li class="m-nav__item m-nav__item--home">
            <a href="{!!URL::to('/')!!}" class="m-nav__link m-nav__link--icon">
                <i class="m-nav__link-icon la la-home"></i> Inicio
            </a>
        </li>
        <li class="m-nav__separator">-</li>
        <li class="m-nav__item">
            <a href="{!!URL::to('/users')!!}" class="m-nav__link">
                <span class="m-nav__link-text">Cupones</span>
            </a>
        </li>
    </ul>
@endsection

@section('content')

    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                        <span class="m-portlet__head-icon m--hide">
                            <i class="la la-gear"></i>
                        </span>
                    <h3 class="m-portlet__head-text">
                        Editar Cupón
                    </h3>
                </div>
            </div>
        </div>

            {!! Form::model($coupon, ['route'=>['coupons.update', $coupon->id],'method'=>'PUT']) !!}

                @include('coupons.partials.inputs_form')

            {!!Form::close() !!}

    </div>

@endsection
@section('scripts')
    <script>
        $(document).ready(function(){
            $('#listMenu').find('.start').removeClass('start');
            $('#menu-users').addClass('start')
        });

    </script>
@endsection
