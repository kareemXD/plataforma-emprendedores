@permission('list_users')
<a class="btn btn-info btn-sm" data-instance="{{ json_encode($coupon) }}" onclick="_index.fillMdl(this)" style="color: white"
   data-toggle="modal" data-target="#show-modal" title="Detalles">
    <i class="fa fa-info-circle fa-lg" aria-hidden="true"></i>
</a>
@endpermission

@permission('edit_users')
<a href="{{ route('coupons.edit', $coupon->id) }}" class="btn btn-primary btn-sm" data-toggle="tooltip" title="Editar" style="color: white">
    <i class="fa fa-edit fa-lg"></i>
</a>
@endpermission

@permission('delete_users')
<a class="btn btn-secondary btn-sm" data-toggle="tooltip" title="Cambiar estatus" style="color: white"
   onclick="_global.toggleStatus('{{ route('coupons.destroy', $coupon->id) }}')">
    <i class="fa fa-sync fa-lg" aria-hidden="true"></i>
</a>
@endpermission
