<div class="m-content">
    <div class="row">
        <div class="col-lg-12">
            <!--begin::Portlet-->
            <div class="m-portlet m-portlet--creative m-portlet--bordered-semi">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h2 class="m-portlet__head-label m-portlet__head-label--accent">
                                <span>Cupón</span>
                            </h2>
                        </div>
                    </div>
                </div>

                <div class="m-portlet__body">

                    <div class="row" id="app-6">
                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                {{ Form::label('name_lb', 'Nombre del cupón:', ['class' => 'control-label']) }}
                                {{ Form::text('name', null, ['class' => 'form-control', 'required', 'maxlength' => '191' ,'tabindex' => 1]) }}
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                {{ Form::label('consume_times_lb', 'Usos por usuario:', ['class' => 'control-label']) }}
                                {{ Form::number('consume_times', null, ['class' => 'form-control', 'required', 'maxlength' => '150' ,'tabindex' => 2]) }}
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                {{ Form::label('discount_type_lb', 'Tipo de descuento:', ['class' => 'control-label']) }}
                                {{ Form::select('discount_type',
                                    $selectDescountType,
                                    isset($coupon->discount_type) ? $coupon->discount_type : [],
                                    ['class'=>'form-control select2',
                                        'id' => 'discount',
                                        'required',
                                        'tabindex' => 9])
                                }}
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                {{ Form::label('amount_lb', 'Monto del cupón:', ['class' => 'control-label']) }}
                                {{ Form::number('amount',
                                    null,
                                    ['class' => 'form-control input-form',
                                        'maxlength' => 5,
                                        'tabindex' => 12,
                                        'required',
                                        'onkeypress' => 'return justNumbers(event)',
                                        'id' => 'postal-code',
                                        'tabindex' => 7])
                                }}
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                {{ Form::label('start_date_lb', 'Fecha de inicio de vigencia :', ['class' => 'control-label']) }}
                                {{ Form::text('start_date', null, ['class' => 'form-control', 'id'=>'datetimepicker']) }}
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                {{ Form::label('end_date-id', 'Fecha de vencimiento del cupón :', ['class' => 'control-label']) }}
                                {{ Form::text('end_date', null, ['class' => 'form-control', 'id'=>'datetimepicker2']) }}
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>


    <div class="m-portlet__foot m-portlet__foot--fit">
        <div class="m-form__actions">
            <button type="submit" class="btn btn-success saveProduct">Aceptar</button>
            <a  class="btn btn-secondary" href="{{URL::route('coupons.index')}}">Cancelar</a>
        </div>
    </div>

</div>


@include('coupons.partials.scripts')
