<div class="modal fade" id="show-modal" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" style="position: absolute">Detalles</h4>
            </div>
            <div class="modal-body">
                <div class="row mdl-groups">
                    <div class="form-group col-md-6 col-sm-12">
                        <small class="label-form">Nombre del cupón:</small>
                        <label name="name" class="form-control mdl-lbl" id="name"></label>
                    </div>
                    <div class="form-group col-md-6 col-sm-12">
                        <small class="label-form">Usos por usuario:</small>
                        <label name="consume_times" class="form-control mdl-lbl" id="consume_times"></label>
                    </div>
                    <div class="form-group col-md-12 col-sm-12">
                        <small class="label-form">Tipo de descuento:</small>
                        <label name="discount_type" class="form-control mdl-lbl" id="discount_type"></label>
                    </div>
                    <div class="form-group col-md-12 col-sm-12">
                        <small class="label-form">Monto del cupón:</small>
                        <label name="amount" class="form-control mdl-lbl" id="amount"></label>
                    </div>
                    <div class="form-group col-md-12 col-sm-12">
                        <small class="label-form">Fecha de inicio de vigencia:</small>
                        <label name="end_date" class="form-control mdl-lbl" id="start_date"></label>
                    </div>
                    <div class="form-group col-md-12 col-sm-12">
                        <small class="label-form">Fecha de vencimiento del cupón:</small>
                        <label name="end_date" class="form-control mdl-lbl" id="end_date"></label>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
