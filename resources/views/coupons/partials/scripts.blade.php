

@push('js')
    <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
    <script src="http://code.jquery.com/ui/1.11.0/jquery-ui.js"></script>
    <link href="https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css" rel="stylesheet" type="text/css">



    <script type="text/javascript">

        $(document).ready(function(){
            /*
            * @author Luis Peña <luis.pena@nuvem.mx> 30/10/2020
            */
            $('#datetimepicker').datepicker({
                minView: 2,
                format: 'yyyy-mm-dd'
            });

            $('#datetimepicker2').datepicker({
                minView: 2,
                format: 'yyyy-mm-dd'
            });

        });

    </script>


@endpush
