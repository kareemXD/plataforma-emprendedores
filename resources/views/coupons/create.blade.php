@extends('adminlte::page')

@section('content')

    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                        <span class="m-portlet__head-icon m--hide">
                            <i class="la la-gear"></i>
                        </span>
                    <h3 class="m-portlet__head-text">
                        Nuevo cupón
                    </h3>
                </div>
            </div>
        </div>

        {!! Form::open(['route'=>'coupons.store','method'=>'POST']) !!}
        <div id="accordion">
            <div class="card animated bounceInDown">
                <div class="card-header">
                    <h5 class="mb-0">
                        <a class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                            Nuevo cupón
                        </a>
                    </h5>
                </div>

                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                    <div class="card-body">
                            @include('coupons.partials.inputs_form')
                    </div>
                </div>
            </div>
        </div>


        {!!Form::close() !!}

    </div>

@stop
@push('css')
<style>
/* select2 version 4.0.0 Beta 2 */
.select2-dropdown--above {
  border: 1px solid blue !important;;
  border-bottom: none !important;

}


.select2-dropdown--below{
  border: 1px solid blue !important;;
  border-top: none !important;

}

span.select2-selection--multiple[aria-expanded=true] {
  border-color: blue !important;
}
</style>
@endpush
