<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        @forelse($links ?? [] as $name => $path)
            <li class="breadcrumb-item"><a href="{{ $path }}">{{ $name }}</a></li>
        @empty
        @endforelse
        <li class="breadcrumb-item active" aria-current="page">{{ $current }}</li>
    </ol>
</nav>
