<small class="label-form">{{ $label }}:</small>
<label
    name="{{ $id }}"
    class="form-control mdl-lbl {{ isset($container['class']) ? $container['class'] : null }}"
    style="{{ isset($container['style']) ? $container['style'] : null }}"
    id="{{ $id }}">
</label>