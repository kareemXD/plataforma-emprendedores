@extends('adminlte::page')

@section('title', 'Configurador de Costos Globales')

@section('content_header')
    @component('components.breadcrumb', [
        'current' => 'Listado'
    ])
    @endcomponent
@stop

@section('content')
    @includeIf('alerts.success')
    <div id="accordion">
        <div class="card animated bounceInDown">
            <div class="card-header">
                <h5 class="mb-0">
                    <a class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                        Costos Globales
                    </a>
                    @permission('global_utility')
                        <a href="{{ route('global-utility.index') }}" class="btn btn-info btn-flat m-1">Utilidad Global</a>
                    @endpermission
                    <a href="{{ route('global-costs-configurator.create') }}" class="btn btn-outline-success float-right m-1" data-toggle="tooltip" data-placement="left" title="Agregar Nuevo"
                       style="border-color:transparent;color: white;">
                        <i class="fas fa-plus-circle fa-lg"></i>
                    </a>
                </h5>
            </div>

            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                <div class="card-body table-responsive">
                    <table class="table" id="myTable">
                        <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>Tipo</th>
                            <th>Porcentaje</th>
                            <th>Monto</th>
                            <th>Estatus</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
            
        </div>
    </div>

    @includeIf('global_costs_configurator.partials.show')

@stop

@push('js')
    <script src="{{ asset('js/global_costs_configurator/index.js') }}"></script>
@endpush