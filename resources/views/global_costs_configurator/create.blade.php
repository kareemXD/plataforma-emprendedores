@extends('adminlte::page')

@section('title', 'Configurador de Costos Globales')

@section('content_header')
    @component('components.breadcrumb', [
        'links' => [
            'Listado' => url('global-costs-configurator')
        ],
        'current' => 'Crear'
    ])
    @endcomponent
@stop

@section('content')
    @includeIf('alerts.errors')
    {{ Form::open(['route' => 'global-costs-configurator.store', 'method' => 'POST', 'class' => 'animated bounceInDown']) }}
        <div id="accordion">
            <div class="card animated bounceInDown">
                <div class="card-header">
                    <h5 class="mb-0">
                        <a class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                            Información
                        </a>
                    </h5>
                </div>

                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                    <div class="card-body">
                        @includeIf('global_costs_configurator.partials.inputs')
                    </div>
                </div>
            </div>
        </div>
        <div style="text-align: center">
            <button type="submit" class="btn btn-success btn-flat">
                <i class="fas fa-save"></i> Guardar
            </button>
            <a class="btn btn-danger btn-flat" href="{{ url()->route('global-costs-configurator.index') }}">
                <i class="fas fa-ban"></i> Cancelar
            </a>
        </div>
    {{ Form::close() }}
@stop

@section('js')
    <script src="{{ asset('js/global_costs_configurator/create_edit.js') }}"></script>
@endsection