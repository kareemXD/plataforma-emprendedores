<div class="modal fade" id="show-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Detalles</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row mdl-groups">
                    <div class="form-group col-md-6 col-sm-12">
                        {!! Form::labelMdl('Descripcion', 'Descripcion') !!}
                    </div>
                    <div class="form-group col-md-6 col-sm-12">
                        {!! Form::labelMdl('Tipo', 'Tipo') !!}
                    </div>
                </div>
                <div class="row mdl-groups">
                    <div class="form-group col-md-6 col-sm-12">
                        {!! Form::labelMdl('Porcentaje', 'Porcentaje') !!}
                    </div>
                    <div class="form-group col-md-6 col-sm-12">
                        {!! Form::labelMdl('Monto', 'Monto') !!}
                    </div>
                </div>
                <div class="row mdl-groups">
                    <div class="form-group col-md-6 col-sm-12">
                        {!! Form::labelMdl('Estatus', 'Estatus') !!}
                    </div>
                    <div class="form-group col-md-6 col-sm-12">
                        <small class="label-form">Base de Cálculo:</small>
                        {{ Form::select('BaseCalculo', $bases, null, ['placeholder' => 'Seleccione una opción', 'class' => 'form-control', 'id' => 'Id_BaseCostoCalculado', 'disabled']) }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
