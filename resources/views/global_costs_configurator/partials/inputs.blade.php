<div class="form-row">
    <div class="col-12">
        {{ Form::label('Descripcion', 'Descripción:', ['class' => '']) }}
        {{ Form::text('Descripcion', null, ['class' => 'form-control form-control-sm', 'required', 'maxlength' => '191']) }}
    </div>
</div>

<div class="form-row">
    <div class="col-12">
        {{ Form::label('type', 'Tipo:', ['class' => '']) }}
        <span style="color:red; font-weight: bold;" title="requerido">*</span> <br>

        <div class="form-check form-check-inline">
            {{ Form::radio('Tipo', 'P', ['checked']) }}
            <label class="form-check-label" for="Porcentaje">&nbsp;Porcentaje</label>
        </div>

        <div class="form-check form-check-inline">
            {{ Form::radio('Tipo', 'M') }}
            <label class="form-check-label" for="Monto">&nbsp;Monto</label>
        </div>

        <div class="form-check form-check-inline">
            {{ Form::radio('Tipo', 'C') }}
            <label class="form-check-label" for="Calculado">&nbsp;Calculado</label>
        </div>

    </div>
</div>

<div class="form-row">
    <div id="div-porcentaje" class="col-lg-6 col-md-12 col-sm-12 col-xs-12" style="display: none;">
        {{ Form::label('Porcentaje', 'Porcentaje:', ['class' => '']) }}
        {{ Form::number('Porcentaje', null, ['id' => 'inputPorcentaje', 'class' => 'form-control form-control-sm', 'required', 'min' => '0', 'step' => "0.01"]) }}
    </div>
</div>

<div class="form-row">
    <div id="div-monto" class="col-lg-6 col-md-12 col-sm-12 col-xs-12" style="display: none;">
        {{ Form::label('Monto', 'Monto:', ['class' => '']) }}
        {{ Form::number('Monto', null, ['id' => 'inputMonto', 'class' => 'form-control form-control-sm', 'required', 'min' => '0', 'step' => "0.01"]) }}
    </div>
    <div id="div-base-calculo" class="col-lg-6 col-md-12 col-sm-12 col-xs-12" style="display: none;">
        {{ Form::label('Id_BaseCostoCalculado', 'Base de Cálculo:', ['class' => '']) }}
        {{ Form::select('Id_BaseCostoCalculado', $bases, null, ['placeholder' => 'Seleccione una opción', 'class' => 'form-control form-control-sm', 'required', 'id' => 'inputBase']) }}
    </div>
</div>
