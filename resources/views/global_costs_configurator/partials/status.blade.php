@php
    list($title, $icon, $color) = ($cost->Estatus) ? ['Activado', 'check-square', '#38c172'] : ['Desactivado', 'square', '#343a40'];
@endphp

<i class="fa fa-{{ $icon }}" title="{{ $title }}" aria-hidden="true" style="color: {{ $color }}"></i>
{{ $title }}
