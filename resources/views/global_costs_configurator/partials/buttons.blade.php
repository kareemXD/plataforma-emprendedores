@permission('*global_costs_configurator')
<div class="btn-group dropleft">
    <button class="btn btn-default btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        Opciones
    </button>
    <div class="dropdown-menu">
        @permission('list_global_costs_configurator')
            <a class="dropdown-item" data-cost="{{ json_encode($cost) }}" onclick="window.fillMdl(this)" href="#"
               data-toggle="modal" data-target="#show-modal">
                Detalles
            </a>
        @endpermission
        @permission('edit_global_costs_configurator')
            <a href="{{ route('global-costs-configurator.edit', $cost->Id_ConceptoCosto) }}" class="dropdown-item">
                Editar
            </a>
        @endpermission
        @permission('delete_global_costs_configurator')
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" onclick="_global.toggleStatus('{{ route('global-costs-configurator.destroy', $cost->Id_ConceptoCosto) }}')" href="#">
                Cambiar estatus
            </a>
        @endpermission
    </div>
</div>
@endpermission
