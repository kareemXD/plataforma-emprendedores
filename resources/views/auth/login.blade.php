@extends('layouts.app')

@section('content')
    <login-boxed action="{{ route('login') }}"
                 password-request="{{ route('password.request') }}"
                 csrf="{{ csrf_token() }}"
                 >
    </login-boxed>
@endsection
