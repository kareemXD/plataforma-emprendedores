<h4 style="text-align: center;">Conekta</h4>
<div class="col-md-12 col-sm-12">
    <div class="form-group">
        {{ Form::label('days_lb', 'Día(s):', ['class' => 'control-label']) }}
        {!! Form::number('days',
                isset($configuration->days) ?
                    $configuration->days :
                    null,
                ['class' => 'form-control',
                    'id' => 'days',
                    'step'=> '0.2']
            )
         !!}
    </div>
    <div class="form-group">
        {{ Form::label('hours_lb', 'Hora(s):', ['class' => 'control-label']) }}
        {!! Form::number('hours',
                isset($configuration->hours) ?
                    $configuration->hours :
                    null,
                ['class' => 'form-control',
                    'id' => 'hours',
                    'step'=> '0.2']
            )
         !!}
    </div>
</div>

<h4 style="text-align: center;">Conekta Suscripción</h4>
<div class="col-md-12 col-sm-12">
    <div class="form-group">
        <div class="col-md-6 col-sm-12">
            <div class="form-group">
                {{ Form::label('template_id_lb', 'Tipo de descuento:', ['class' => 'control-label']) }}
                {{ Form::select('template_id',
                    $templates,
                    isset($configuration->template_id) ? $configuration->template_id : [],
                    ['class'=>'form-control select2',
                        'id' => 'template_id',
                        'required',
                        'tabindex' => 9])
                }}
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-6 col-sm-12">
            <div class="form-group">
                {{ Form::label('coupon_id_lb', 'Tipo de descuento:', ['class' => 'control-label']) }}
                {{ Form::select('coupon_id',
                    $coupon,
                    isset($configuration->coupon_id) ? $configuration->coupon_id : [],
                    ['class'=>'form-control select2',
                        'id' => 'coupon_id',
                        'required',
                        'tabindex' => 9])
                }}
            </div>
        </div>
    </div>
</div>
