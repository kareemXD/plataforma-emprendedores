@extends('adminlte::page')


@section('breadcrumb')

    <h3 class="m-subheader__title m-subheader__title--separator">Configuración</h3>
    <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
        <li class="m-nav__item m-nav__item--home">
            <a href="{!!URL::to('/')!!}" class="m-nav__link m-nav__link--icon">
                <i class="m-nav__link-icon la la-home"></i> Inicio
            </a>
        </li>
        <li class="m-nav__separator">-</li>
        <li class="m-nav__item">
            <a href="{!!URL::to('/configuration')!!}" class="m-nav__link">
                <span class="m-nav__link-text">Configuración</span>
            </a>
        </li>
    </ul>
@endsection

@section('content')
    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                        <span class="m-portlet__head-icon m--hide">
                            <i class="la la-gear"></i>
                        </span>
                    <h3 class="m-portlet__head-text">
                        Datos de configuración
                    </h3>
                </div>
            </div>
        </div>

        {!! Form::model($configuration, ['route' => ['abandoned-cart.update',$configuration->id], 'method'=>'PUT','class'=>'m-form m-form--fit m-form--label-align-right', 'enctype' => 'multipart/form-data']) !!}
            <div style="padding: 10px">
                @include('abandoned_cart.partials.inputs')

                <div style="text-align: center;">
                    <button type="submit" class="btn btn-success">
                        Guardar
                    </button>
                    <a class="btn btn-default">
                        Cancelar
                    </a>
                </div>
            </div>
        {!!Form::close() !!}

    </div>

@endsection

@push('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.13.4/jquery.mask.min.js"></script>
    <script>
        $('.moneda').mask("#,##0.00", {reverse: true});
    </script>
@endpush
