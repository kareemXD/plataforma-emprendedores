@extends('adminlte::page')

@section('title', 'Utilidad Global')

@section('content_header')
    @component('components.breadcrumb', [
        'current' => 'Utilidad Global'
    ])
    @endcomponent
@stop

@section('content')
    @includeIf('alerts.errors')
     {{ Form::model($global_utility, ['route' => ['global-utility.update', $global_utility->Id_ConceptoCosto], 'method' => 'PUT', 'class' => 'animated bounceInDown']) }}
        <div id="accordion">
            <div class="card animated bounceInDown">
                <div class="card-header">
                    <h5 class="mb-0">
                        <a class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                            Información
                        </a>
                    </h5>
                </div>

                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                    <div class="card-body">
                        <div class="form-row">
                            <div id="div-porcentaje" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                {{ Form::label('Porcentaje', 'Porcentaje de Utilidad:', ['class' => '']) }}
                                {{ Form::number('Porcentaje', null, ['id' => 'inputPorcentaje', 'class' => 'form-control form-control-sm', 'required', 'min' => '0']) }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div style="text-align: center">
            <button type="submit" class="btn btn-success btn-flat">
                <i class="fas fa-save"></i> Guardar
            </button>
            <a class="btn btn-danger btn-flat" href="{{ url()->route('global-costs-configurator.index') }}">
                <i class="fas fa-ban"></i> Cancelar
            </a>
        </div>
    {{ Form::close() }}
@stop
