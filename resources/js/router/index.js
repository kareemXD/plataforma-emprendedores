import Vue from 'vue'
import Router from 'vue-router'
import store from '../store'

Vue.use(Router);


const router =  new Router({
    scrollBehavior() {
        return window.scrollTo({ top: 0, behavior: 'smooth' });
    },
    mode: 'history',
    routes: [

        // ========================================== COMPONENTS ==========================================
        {
            path: '/forbidden',
            name: 'forbidden',
            component: () => import('../components/forbidden.vue'),
            meta: {
                requiresAuth: false
            }
        },
        {
            path: '/logout',
            name: 'logout',
            component: () => import('../components/logout.vue'),
            meta: {
                requiresAuth: false
            }
        },

        // ========================================== VIEWS ==========================================

        {
            path: '/users',
            name: 'users',
            component: () => import('../views/users/index.vue'),
            meta: {
                requiresAuth: true,
                permission: ['Usuarios.*', 'Usuarios.view', 'Usuarios.create', 'Usuarios.delete', 'Usuarios.edit']
            }
        },
        {
            path: '/roles',
            name: 'roles',
            component: () => import('../views/roles/index.vue'),
            meta: {
                requiresAuth: true,
                permission: ['Roles.*', 'Roles.view', 'Roles.create', 'Roles.delete', 'Roles.edit']
            }
        },
        // ========================================== PRODUCTS ==========================================
        {
            path: '/products',
            name: 'products',
            component: () => import('../views/product-configuration/products/index.vue'),
        },
        {
            path: '/create-products',
            name: 'create-products',
            component: () => import('../views/product-configuration/products/create.vue'),
        },
        {
            path: '/show-products',
            name: 'show-products',
            component: () => import('../views/product-configuration/products/show.vue'),
        },
        {
            path: '/edit-products',
            name: 'edit-products',
            component: () => import('../views/product-configuration/products/edit.vue'),
        },
        {
            path: '/shipping-configurations',
            name: 'shipping-configurations',
            component: () => import('../views/shipping_configurations/index.vue'),
        },
        {
            path: '/rules-configurators',
            name: 'rules-configurators',
            component: () => import('../views/product-configuration/rules-configurator/index.vue'),
        },
        {
            path: '/rules-configurators/create',
            name: 'rules-configurators-create',
            component: () => import('../views/product-configuration/rules-configurator/create.vue'),
        },
        {
            path: '/rules-configurators/edit',
            name: 'rules-configurators-edit',
            component: () => import('../views/product-configuration/rules-configurator/edit.vue'),
        },
        {
            path: '/special-costs-configurators',
            name: 'special-costs-configurators',
            component: () => import('../views/product-configuration/special-costs-configurator/index.vue'),
            meta: {
                requiresAuth: true,
                permission: ['SpecialCostConfiguration.*', 'SpecialCostConfiguration.view', 'SpecialCostConfiguration.create', 'SpecialCostConfiguration.delete', 'SpecialCostConfiguration.edit']
            }
        },
        {
            path: '/special-costs-configurators-create',
            name: 'special-costs-configurators-create',
            component: () => import('../views/product-configuration/special-costs-configurator/create.vue'),
            meta: {
                requiresAuth: true,
                permission: ['SpecialCostConfiguration.*', 'SpecialCostConfiguration.view', 'SpecialCostConfiguration.create', 'SpecialCostConfiguration.delete', 'SpecialCostConfiguration.edit']
            }
        },
        {
            path: '/special-costs-configurators-edit',
            name: 'special-costs-configurators-edit',
            component: () => import('../views/product-configuration/special-costs-configurator/edit.vue'),
            meta: {
                requiresAuth: true,
                permission: ['SpecialCostConfiguration.*', 'SpecialCostConfiguration.view', 'SpecialCostConfiguration.create', 'SpecialCostConfiguration.delete', 'SpecialCostConfiguration.edit']
            }
        },
        {
            path: '/special-costs-configurators-show',
            name: 'special-costs-configurators-show',
            component: () => import('../views/product-configuration/special-costs-configurator/show.vue'),
            meta: {
                requiresAuth: true,
                permission: ['SpecialCostConfiguration.*', 'SpecialCostConfiguration.view', 'SpecialCostConfiguration.create', 'SpecialCostConfiguration.delete', 'SpecialCostConfiguration.edit']
            }
        },
        {
            path: '/view-cost-entrepreneurs',
            name: 'view-cost-entrepreneurs',
            component: () => import('../views/product-configuration/view-cost-entrepreneurs/index.vue'),
            meta: {
                requiresAuth: true,
                permission: ['ViewEntrepreneurialCost.*', 'ViewEntrepreneurialCost.view', 'ViewEntrepreneurialCost.create', 'ViewEntrepreneurialCost.delete', 'ViewEntrepreneurialCost.edit']
            }
        },
        {
            path: '/boxes',
            name: 'boxes',
            component: () => import('../views/shipping_configurations/box/index.vue'),
            meta: {
                requiresAuth: true,
                permission: ['Box.*', 'Box.view', 'Box.create', 'Box.delete', 'Box.edit']
            }

        },
        {
            path: '/boxes/create',
            name: 'boxes-create',
            component: () => import('../views/shipping_configurations/box/create.vue'),
            meta: {
                requiresAuth: true,
                permission: ['Box.*', 'Box.view', 'Box.create', 'Box.delete', 'Box.edit']
            }
        },
        {
            path: '/boxes/edit',
            name: 'boxes-edit',
            component: () => import('../views/shipping_configurations/box/edit.vue'),
            meta: {
                requiresAuth: true,
                permission: ['Box.*', 'Box.view', 'Box.create', 'Box.delete', 'Box.edit']
            }
        },
        {
            path: '/configurations',
            name: 'configurations',
            component: () => import('../views/administration/configuration/index.vue'),
            meta: {
                requiresAuth: true,
                permission: ['Configurations.*', 'Configurations.view', 'Configurations.create', 'Configurations.delete', 'Configurations.edit']
            }
        },
        // ========================================== CONFIGURATIONS ==========================================
        {
            path: '/global-costs-configurators',
            name: 'global-costs-configurators',
            component: () => import('../views/configurations/global-costs-configurator/index.vue'),
            meta: {
                requiresAuth: true,
                permission: ['GlobalCostConfiguration.*', 'GlobalCostConfiguration.view', 'GlobalCostConfiguration.create', 'GlobalCostConfiguration.delete', 'GlobalCostConfiguration.edit']
            }
        },
        {
            path: '/global-costs-configurators-create',
            name: 'global-costs-configurators-create',
            component: () => import('../views/configurations/global-costs-configurator/create.vue'),
            meta: {
                requiresAuth: true,
                permission: ['GlobalCostConfiguration.create']
            }
        },
        {
            path: '/global-costs-configurators-edit',
            name: 'global-costs-configurators-edit',
            component: () => import('../views/configurations/global-costs-configurator/edit.vue'),
            meta: {
                requiresAuth: true,
                permission: ['GlobalCostConfiguration.edit']
            }
        },
        {
            path: '/global-costs-configurators-utility',
            name: 'global-costs-configurators-utility',
            component: () => import('../views/configurations/global-costs-configurator/globalUtility.vue'),
            meta: {
                requiresAuth: true,
                permission: ['GlobalUtilityConfiguration.create']
            }
        },
        {
            path: '/shipment-cost-configuration',
            name: 'shipment-cost-configurations',
            component: () => import('../views/product-configuration/shipment-cost-configurator/index.vue'),
        },
        {
            path: '/shipment-cost-configuration/create',
            name: 'shipment-cost-configurations-create',
            component: () => import('../views/product-configuration/shipment-cost-configurator/create.vue'),
        },
        {
            path: '/shipment-cost-configuration/edit',
            name: 'shipment-cost-configurations-edit',
            component: () => import('../views/product-configuration/shipment-cost-configurator/edit.vue'),
        },
        {
            path: '/credit-requests',
            name: 'credit-requests',
            component: () => import('../views/credit-requests/index.vue'),
            meta: {
                requiresAuth: true,
                permission: ['CreditRequest.*']
            }
        },
        // ========================================== ENTERPRISINGS ==========================================
        {
            path: '/enterprisings',
            name: 'enterprisings',
            component: () => import('../views/enterprisings/enterprisings_list/index.vue'),
        },
        {
            path: '/enterprisings-edit/:id',
            name: 'enterprisings-edit',
            component: () => import('../views/enterprisings/enterprisings_list/enterprisingTabs.vue'),
        },
        // ========================================== Locations ==========================================
        {
            path: '/locations',
            name: 'locations',
            component: () => import('../views/product-configuration/locations_new/index.vue'),
        },
        {
            path: '/locations/create',
            name: 'locations-create',
            component: () => import('../views/product-configuration/locations_new/formRacks.vue'),
        },
        {
            path: '/locations/edit/:id',
            name: 'locations-edit',
            component: () => import('../views/product-configuration/locations_new/formRacks.vue'),
        },
        {
            path: '/locations/rack-distributions/:id',
            name: 'location-distributions',
            component: () => import('../views/product-configuration/locations_new/rackDistribution.vue'),
        },
        {
            path: '/locations/show/:id',
            name: 'locations-show',
            component: () => import('../views/product-configuration/locations_new/formRacks.vue'),
        },
        {
            path: '/locations/show-rack-distributions/:id',
            name: 'location-distributions-show',
            component: () => import('../views/product-configuration/locations_new/rackDistribution.vue'),
        },
        //=====================================Containers==========
        {
            path: '/container',
            name: 'containers',
            component: () => import('../views/product-configuration/containers/index.vue'),
            meta: {
                requiresAuth: true,
                permission: ['Container.view']
            }
        },
        {
            path: '/container/create',
            name: 'containers-create',
            component: () => import('../views/product-configuration/containers/create.vue'),
            meta: {
                requiresAuth: true,
                permission: ['Container.create']
            }
        },
        {
            path: '/container/edit',
            name: 'containers-edit',
            component: () => import('../views/product-configuration/containers/edit.vue'),
            meta: {
                requiresAuth: true,
                permission: ['Container.edit']
            }
        },
        {
            path: '/picking',
            name: 'picking',
            component: () => import('../views/picking/index.vue'),
        },
        {
            path: '/packing',
            name: 'packing',
            component: () => import('../views/orders/packing/index.vue'),
        },
    ]
});



/**
 * When there is an attempt to enter to a route, this will validate the user permissions and if it is logged in as well.
 * @auth Octavio Cornejo
 * @date 2021-03-02
 * @param Object to
 * @param Object from
 * @param Object next
 * @return void
 */
router.beforeEach((to, from, next) => {

    //Validate if the actual route needs an autheticated user.
    if (to.matched.some(record => record.meta.requiresAuth)) {

        //Check if the user is not logged in.
        if (!store.state.auth) {
            window.location.href = './login';
        } else{
            let forbidden = 1;
            to.meta.permission.forEach(permission=> {
                if(to.matched.some(record => store.state.permissions.includes(permission))){
                    forbidden = 0;
                }
            });
            //if the user has not the permission required, is redirected to the 'forbidden' view.
            forbidden != 1 ? next() : next('/forbidden');
        }
    } else {
        next()
    }
});

export default router;
