import Vue from 'vue';
import vuetify from './plugins/vuetify.js';
import router from './router';
import store from './store';
require('./bootstrap');

import BootstrapVue from "bootstrap-vue"

import App from './App.vue'

import Default from './Layout/Wrappers/baseLayout.vue';
import Pages from './Layout/Wrappers/pagesLayout.vue';
import Apps from './Layout/Wrappers/appLayout.vue';

import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

window.toastr = require('toastr')

import './views/index.js';

Vue.config.productionTip = false;

Vue.use(BootstrapVue);

Vue.component('default-layout', Default);
Vue.component('userpages-layout', Pages);
Vue.component('apps-layout', Apps);
Vue.component('App', App);
Vue.component('font-awesome-icon', FontAwesomeIcon)

new Vue({
  el: '#app',
  router,
  vuetify,
  store,
  components: { App }
});


