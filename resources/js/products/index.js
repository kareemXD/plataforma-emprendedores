$(function () {
    _index.datatable();
});

const _index = {
    $mdl: $('#show-modal'),
    /**
     * Display the datatable of collection
     *
     * @author José Vega <jose.vega@nuvem.mx>
     * @created 2021/03/24
     * @params 
     * @return void 
     */
    datatable: function () {
        $('#products-table').DataTable({
            processing: true,
            serverSide: true,
            language:{
                url: "/js/Spanish.json"
            },
            ajax: '/products',
            columns: [
                { data: 'Id_Prod1', name: 'Id_Prod1', "type": "string" },
                { data: 'Descrip1', name: 'Descrip1' },
                { data: 'Niveles', name: 'Niveles' },
                { data: 'id_Empaque1', name: 'id_Empaque1' },
                { data: 'Unidad_N2', name: 'id_Empaque1' },
                { data: 'Estatus', name: 'Estatus' },
                { data: 'Actions', name: 'Actions' },                
            ]
        });
    },
    
}

/**
 * Display a modal window with the row information
 *
 * @author José Vega <jose.vega@nuvem.mx>
 * @created 2021/03/24
 * @param HTML Entity that
 * @return void 
 */
window.fillMdl = function (that) {
    
    _index.$mdl.find('label').html('');
    let data = JSON.parse(that.dataset['rule']);

    $.each(data, (idx, val) => {

        if (idx == 'Estatus') {

            let label = val == 1 ? 'Activo' : 'Inactivo';
            _index.$mdl.find(`#${idx}`).html(label);

        } else {
            _index.$mdl.find(`#${idx}`).html(val);
        }

    });
}