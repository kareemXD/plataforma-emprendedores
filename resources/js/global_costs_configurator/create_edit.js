
$(function () {

	var value = $('input[name=Tipo]:checked').val();
	if (value && value != '')
		_index.setTypeValue(value);

	$('input[name=Tipo]').change(function() {
		var valueType = $( 'input[name=Tipo]:checked' ).val();
		_index.setTypeValue(valueType);
	});

});

const _index = {
	/**
	 * Verify which input field show
	 *
	 * @author Benito Huerta <benito.huerta@nuvem.mx>
     * @created 2020/02/17
	 * @param string value
	 * @return void
	 */
	setTypeValue: function(value) {
		switch(value) {
			case 'P':
				$('#div-porcentaje').css('display', 'inline-block');
				$('#div-monto').css('display', 'none');
				$('#div-base-calculo').css('display', 'none');

				$('#inputPorcentaje').prop('required', true);
				$('#inputMonto').prop('required', false);
				$('#inputBase').prop('required', false);
				break;

			case 'M':
				$('#div-porcentaje').css('display', 'none');
				$('#div-monto').css('display', 'inline-block');
				$('#div-base-calculo').css('display', 'none');

				$('#inputPorcentaje').prop('required', false);
				$('#inputMonto').prop('required', true);
				$('#inputBase').prop('required', false);

				break;

			case 'C':
				$('#div-porcentaje').css('display', 'none');
				$('#div-monto').css('display', 'inline-block');
				$('#div-base-calculo').css('display', 'inline-block');

				$('#inputPorcentaje').prop('required', false);
				$('#inputMonto').prop('required', true);
				$('#inputBase').prop('required', true);

				break;

			default:
				$('#div-porcentaje').css('display', 'none');
				$('#div-monto').css('display', 'none');
				$('#div-base-calculo').css('display', 'none');

				$('#inputPorcentaje').prop('required', false);
				$('#inputMonto').prop('required', false);
				$('#inputBase').prop('required', false);
				break;
		}
	}
}
