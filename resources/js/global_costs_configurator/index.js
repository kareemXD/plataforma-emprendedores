$(function () {
    _index.datatable();
});

const _index = {
    $mdl: $('#show-modal'),
    /**
     * Display the datatable of collection
     *
     * @author Benito Huerta <benito.huerta@nuvem.mx>
     * @created 2020/02/17
     * @return void 
     */
    datatable: function () {
        $('#myTable').DataTable({
            processing: true,
            serverSide: true,
            language:{
                url: "/js/Spanish.json"
            },
            ajax: "/global-costs-configurator",
            columns: [
                { data: 'Descripcion', name: 'Descripcion' },
                { data: 'Tipo', name: 'Tipo' },
                { data: 'Porcentaje', name: 'Porcentaje' },
                { data: 'Monto', name: 'Monto' },
                { data: 'Estatus', name: 'Estatus' },
                { data: 'Actions', name: 'Actions', orderable: false, searchable: false, width: '8em' },
            ]
        });
    },
    
}

/**
 * Display a modal window with the row information
 *
 * @author Benito Huerta <benito.huerta@nuvem.mx>
 * @created 2020/02/17
 * @param HTML Entity that
 * @return void 
 */
window.fillMdl = function (that) {
    
    _index.$mdl.find('label').html('');
    let data = JSON.parse(that.dataset['cost']);

    $.each(data, (idx, val) => {

        if (idx == 'Estatus') {

            let label = val == 1 ? 'Activo' : 'Inactivo';
            _index.$mdl.find(`#${idx}`).html(label);

        } else if(idx == 'Id_BaseCostoCalculado') {

            let idBase = idx && idx != '' ? val : '';
            $('#Id_BaseCostoCalculado').val(idBase).trigger('change');

        } else {
            _index.$mdl.find(`#${idx}`).html(val);
        }

    });
}