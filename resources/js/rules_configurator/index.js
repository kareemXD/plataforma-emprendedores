$(function () {
    _index.datatable();
});

const _index = {
    $mdl: $('#show-modal'),
    /**
     * Display the datatable of collection
     *
     * @author Benito Huerta <benito.huerta@nuvem.mx>
     * @created 2020/02/16
     * @return void 
     */
    datatable: function () {
        $('#rules-table').DataTable({
            processing: true,
            serverSide: true,
            language:{
                url: "/js/Spanish.json"
            },
            ajax: '/rules-configurator',
            columns: [
                { data: 'Id_ReglaCostoEspecial', name: 'Id_ReglaCostoEspecial' },
                { data: 'Descripcion', name: 'Descripcion' },
                { data: 'Prioridad', name: 'Prioridad' },
                { data: 'Estatus', name: 'Estatus' },
                { data: 'Actions', name: 'Actions', orderable: false, searchable: false, width: '8em' },
            ]
        });
    },
    
}

/**
 * Display a modal window with the row information
 *
 * @author Benito Huerta <benito.huerta@nuvem.mx>
 * @created 2020/02/16
 * @param HTML Entity that
 * @return void 
 */
window.fillMdl = function (that) {
    
    _index.$mdl.find('label').html('');
    let data = JSON.parse(that.dataset['rule']);

    $.each(data, (idx, val) => {

        if (idx == 'Estatus') {

            let label = val == 1 ? 'Activo' : 'Inactivo';
            _index.$mdl.find(`#${idx}`).html(label);

        } else {
            _index.$mdl.find(`#${idx}`).html(val);
        }

    });
}