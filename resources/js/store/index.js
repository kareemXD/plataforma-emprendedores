import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)
export default new Vuex.Store({
    state: {
        auth: null,
        permissions: [],
        menu: []
    },
    mutations: {
        /**
         * Set the user authenticated globally.
         * @auth Octavio Cornejo
         * @date 2021-03-02
         * @param Object Vuex
         * @param Object User
         * @return void
         */
        setAuth(state, user) {
            state.auth = user;
            state.permissions = user.assignedPermissions;
            state.menu = user.menu;
        },
        /**
         * Unset the user authenticated.
         * @auth Octavio Cornejo
         * @date 2021-03-02
         * @param param
         * @return void
         */
        unsetAuth(state) {
            state.auth = null;
            state.permissions = [];
            state.menu = [];
        }
    }
});
