import Vue from 'vue'

//layouts
import AppLayout from './Layouts/Wrappers/AppLayout.vue';
import LoginBoxed from './Layouts/LoginBoxed.vue';
import ResetPassword from './Layouts/ResetPassword.vue';
import PasswordUpdate from './Layouts/PasswordUpdate.vue';
import CreateEmailNego from './Layouts/CreateEmailNego.vue';

//componentes
import Header from './Layouts/Components/Header.vue';
import Sidebar from './Layouts/Components/Sidebar.vue';
import PageTitle from './Layouts/Components/PageTitle.vue';
import Pagination from "../components/pagination2.vue";
import Label from "../components/label.vue";
import FormActionButtons from "../components/formActionButtons.vue";
import ModalActionButtons from "../components/modalActionButtons.vue";
import DatatableHeader from "../components/datatableHeader.vue";
import ChipStatus from "../components/chipStatus.vue";
import ActionButtons from "../components/datatableActionButtons.vue";
import DialogDelete from "../components/dialogDelete.vue";


//listados
import UsersCrud from './users/index.vue';

Vue.component('app-layout', AppLayout);
Vue.component('login-boxed', LoginBoxed);
Vue.component('reset-password', ResetPassword);
Vue.component('password-update', PasswordUpdate);
Vue.component('email-create-nego', CreateEmailNego);

Vue.component('Sidebar', Sidebar);
Vue.component('Header', Header);
Vue.component('PageTitle', PageTitle);
Vue.component('n-label', Label);
Vue.component("pagination", Pagination);
Vue.component("n-label", Label);
Vue.component("form-action-buttons", FormActionButtons);
Vue.component("modal-action-buttons", ModalActionButtons);
Vue.component("datatable-header", DatatableHeader);
Vue.component('chip-status', ChipStatus);
Vue.component('action-buttons', ActionButtons);
Vue.component('dialog-delete', DialogDelete);

Vue.component('users-crud', UsersCrud);
